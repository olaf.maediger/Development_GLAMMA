
#include <ctime>
#include <conio.h>
#include <iostream>
#include <direct.h>
#include <string.h>
#include <stdio.h>


/*
Kit example folder structure,  Single AE API

example
\cal\
*.cal
\img\
*.bmp

*.exe
IPMS_SLM.dll
*/

static const int SMART_OFF = 1;		//!! CAREFULL, only for demonstration purpose
static const int SLM_SUCCESS = 0;
const char* LINE_UP = "\x1b[A";


/* load library in -1 level folder IPMS_SLM_DLL */
#include "..\\..\\include\\AE.h"
#pragma comment (lib, "..\\..\\lib64\\IPMS_SLM.lib")


static void Pause(double Time_s);
static void DieWithMessage(const char* msg);
static void DumpAETemperatures();



int main(int argc, char* argv[])
{
	//get current dir
	//
	//!! ADJUST PATH IF RUN FROM VS DEBUG -> will not dive into x64\Debug directory !!
	//
	char CurrentPath[260];
	_getcwd(CurrentPath, 260);

	/* connect to AE256a_mk2 */
	if (AE_Connect("192.168.0.2", 4002) == SLM_SUCCESS)
	{
		printf("1) AE connection established - ID : ");
	}
	else
	{
		DieWithMessage("AE256a connection error!");
	}

	char ID[32];
	if (AE_GetID(ID) == SLM_SUCCESS)
	{
		printf("%s\n", ID);
	}
	else
	{
		DieWithMessage("Failed to read system ID!");
	}

	/* some AE information */
	DumpAETemperatures();
	
	/* check for errors */
	unsigned int _statusbits = 0;
	if (AE_GetStatusBits(_statusbits) != SLM_SUCCESS)
	{
		DieWithMessage("Failed to read AE256a status bits.");
	}

	unsigned int _errorbits = 0;
	if (AE_GetErrorBits(_errorbits) != 0)
	{
		DieWithMessage("Failed to read AE256a error bits.");
	}

	if (_errorbits != 0)
	{
		printf("\nErrorbits : %Xh\n", _errorbits);
		DieWithMessage("System shows errors.  Abort!");
	}


	/*
	* loading the calibration
	*
	* working point is setup from the information
	* from the calibration file
	* 
	* !! REPLACE WITH MMA MATCHING .CAL FILE !!
	*
	*/

	char calpath[260] = { 0 };
	strcat(calpath, CurrentPath);
	strcat(calpath, "\\cal\\VC3471_18_01_Cal_-200-12200urad_21-Mar-2019.cal");

	if (AE_LoadCalibrationData(calpath) == SLM_SUCCESS)
	{
		printf("2) calibration data loaded\n");
	}
	else
	{
		DieWithMessage("Error loading calibration data.");
	}
	

	/* load a set of images to 1,2,3 position */

	int errcnt = 0;

	char imgpath[260] = { 0 };
	strcat(imgpath, CurrentPath);
	strcat(imgpath, "\\img\\_Rhombus.bmp");
	errcnt += AE_LoadImage(imgpath, 1);

	imgpath[0] = { 0 };
	strcat(imgpath, CurrentPath);
	strcat(imgpath, "\\img\\Fraunhofer1M.bmp");
	errcnt += AE_LoadImage(imgpath, 2);

	imgpath[0] = { 0 };
	strcat(imgpath, CurrentPath);
	strcat(imgpath, "\\img\\Graukeil2.bmp");
	errcnt += AE_LoadImage(imgpath, 3);

	if (errcnt != 0)
	{
		DieWithMessage("Error loading images.");
	}
	else
	{
		printf("3) Bitmaps loaded successfully.\n");
	}

	/* power for SLM */
	if (AE_SLM_PowerOn() == SLM_SUCCESS)
	{
		printf("4) SLM power supply - ON.\n");
	}
	else
	{
		DieWithMessage("Error with SLM power supply.");
	}

	/* !! define at least one picture sequence before start !! */
	errcnt = 0;
	errcnt += AE_SetPictureSequence(1, 1, 0);  //LastImage=0
	errcnt += AE_SetPictureSequence(2, 1, 0);  //LastImage=0
	errcnt += AE_SetPictureSequence(3, 1, 1);  //LastImage=0

	if (errcnt != 0)
	{
		DieWithMessage("Error setting initial picture sequence.");
	}
	else
	{
		printf("5) Initial PictureSequence setup.\n");
	}


// !! BE CAREFUL WITH THIS !!

	//static deflection with VDMD_L = VDMD_F
	if (SMART_OFF)
	{
		float VDMD_F = 0.0f;
		
		if (AE_GetVoltage("VDMD_F", VDMD_F) != SLM_SUCCESS)
		{
			DieWithMessage("SMART_OFF: Failed to read VDMD_F");
		}

		if (AE_SetVoltage("VDMD_L", VDMD_F) != SLM_SUCCESS)
		{
			DieWithMessage("SMART_OFF: Failed to setup VDMD_L");
		}
	}

// !! BE CAREFUL WITH THIS !!


	/* Peltier */
	if (AE_PeltierSetTemp(20.0f) == SLM_SUCCESS)
	{
		printf("6) set target temperature for MMA\n");
	}
	else
	{
		DieWithMessage("Failed to setup MMA cooling temperature.");
	}

	if (AE_PeltierControlEnable(true) == SLM_SUCCESS)
	{
		printf("7) enable temperature control\n");
	}
	else
	{
		DieWithMessage("Failed to enable SLM cooling.");
	}

	printf("\n5sec for SLM cooling to do some work ...\n\n");
	Pause(5.0);

	float MMATemp = 0.0f;
	if (AE_PeltierGetTemp(MMATemp) == SLM_SUCCESS)
	{
		printf("8) SLM cooling temperature: %.2f degrees\n", MMATemp);
	}
	else
	{
		DieWithMessage("Failed to read actual SLM cooling temperature");
	}

	/* start of addressing */
	if (AE_SLM_Start() == SLM_SUCCESS)
	{
		printf("9) SLM addressing started.\n");
	}
	else
	{
		DieWithMessage("Error with start of SLM addressing");
	}

	//give electronics some cycles to check SLM status after programming (ReadyMMA)
	Pause(0.5);

	if (AE_GetErrorBits(_errorbits) != SLM_SUCCESS)
	{
		DieWithMessage("Error with reading ErrorBits.");
	}
	
	if (_errorbits != 0)
	{
		DieWithMessage("AE shows errors -  shutdown.");
	}
	
	printf("\nSwitch image every 1sec .. <Press space to stop>\n");

	int imgnr = 1;
	for (;;)
	{
		if (AE_SetPictureSequence(imgnr, 1, 1) != SLM_SUCCESS)
		{
			DieWithMessage("Error setting picture sequence.\r\n");
		}
		else
		{
			printf("\rDisplaying image: %d", imgnr);
		}
		
		if (kbhit())
		{
			break;
		}

		Pause(1);

		imgnr = (imgnr == 3) ? 1 : (imgnr + 1);
	}

	
	printf("\n");
	DieWithMessage("AE, SLM programming and cooling are stopped now ...");
	return 0;
}


/* pause timer */
static void Pause(double Time_s)
{
	time_t starttime, endtime;

	time(&starttime);

	while (1)
	{
		time(&endtime);

		if (difftime(endtime, starttime) > Time_s) // > 500ms
		{
			return;
		}
	}
}


/* shutdown and close lib */
static void DieWithMessage(const char* msg)
{
	printf("%s\n", msg);
	AE_SLM_Stop();
	AE_PeltierControlEnable(false);
	AE_SLM_PowerOff();
	AE_Disconnect();
	printf("\n\n< Enter to exit ;) >");
	getchar();
	exit(-1);
}

/*
* Temperature sensor values of the electronics
*/
static void DumpAETemperatures()
{
	AETempSensors_t AESensors;

	if (AE_ReadTempSensorValues(AESensors) == SLM_SUCCESS)
	{
		printf("\n");
		printf("--------------------------------\n");
		printf("AE256a Temperatures (in degrees)\n");
		printf("--------------------------------\n");
		printf("         FPGA  PCB  CU  \n");
		printf("Master     %d   %d      \n",
			(AESensors.MASTER.FPGA.exists) ? (int)AESensors.MASTER.FPGA.temperature : -1,
			(AESensors.MASTER.PCB.exists) ? (int)AESensors.MASTER.PCB.temperature : -1
			);

		//redirected !!
		printf("Power               %d  \n",
			(AESensors.MASTER.CU.exists) ? (int)AESensors.MASTER.CU.temperature : -1
			);

		printf("DAC1");
		printf("       %d   %d  %d  \n",
			(AESensors.DAC1.FPGA.exists) ? (int)AESensors.DAC1.FPGA.temperature : -1,
			(AESensors.DAC1.PCB.exists) ? (int)AESensors.DAC1.PCB.temperature : -1,
			(AESensors.DAC1.CU.exists) ? (int)AESensors.DAC1.CU.temperature : -1
			);

		printf("DAC2");
		printf("       %d   %d  %d  \n",
			(AESensors.DAC2.FPGA.exists) ? (int)AESensors.DAC2.FPGA.temperature : -1,
			(AESensors.DAC2.PCB.exists) ? (int)AESensors.DAC2.PCB.temperature : -1,
			(AESensors.DAC2.CU.exists) ? (int)AESensors.DAC2.CU.temperature : -1
			);

		printf("DAC3");
		printf("       %d   %d  %d  \n",
			(AESensors.DAC3.FPGA.exists) ? (int)AESensors.DAC3.FPGA.temperature : -1,
			(AESensors.DAC3.PCB.exists) ? (int)AESensors.DAC3.PCB.temperature : -1,
			(AESensors.DAC3.CU.exists) ? (int)AESensors.DAC3.CU.temperature : -1
			);

		printf("DAC4");
		printf("       %d   %d  %d  \n",
			(AESensors.DAC4.FPGA.exists) ? (int)AESensors.DAC4.FPGA.temperature : -1,
			(AESensors.DAC4.PCB.exists) ? (int)AESensors.DAC4.PCB.temperature : -1,
			(AESensors.DAC4.CU.exists) ? (int)AESensors.DAC4.CU.temperature : -1
			);

		printf("\n");
	}
}
