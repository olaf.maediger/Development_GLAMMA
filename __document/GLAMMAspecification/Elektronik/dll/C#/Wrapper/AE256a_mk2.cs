using System;
using System.Text;
using System.Runtime.InteropServices;


//////////////////////////////////////////////////////////////////
//
// AE256a_mk2 C# control wrapper for IPMS_SLM.dll (C++ unmanaged)
//
// (c) 2019 Fraunhofer IPMS Dresden
//////////////////////////////////////////////////////////////////

namespace AE256a_mk2
{
    public class CAE256a_mk2
    {

        //set events
        public const int AE_READY_ERROR = 10020;
        public const int AE_TEMP_ERROR  = 10030;
        public const int AE_SOCKET_ERROR = 10040;
        public const int AE_SHORT_CIRCUIT = 10050;
        public const int AE_ERR_MODULE = 10060;
        public const int AE_SUPPLY_ERR = 10070;
        public const int AE_SMART_ERR = 10080;

        //clr events
        public const int AE_READY_ERROR_CLR = 10021;
        public const int AE_TEMP_ERROR_CLR = 10031;
        public const int AE_SOCKET_ERROR_CLR = 10041;
        public const int AE_SHORT_CIRCUIT_CLR = 10051;
        public const int AE_ERR_MODULE_CLR = 10061;
        public const int AE_SUPPLY_ERR_CLR = 10071;
        public const int AE_SMART_ERR_CLR = 10081;

        uint AE_E_BIT_MODULE_ERROR = 0x02;
        uint AE_E_BIT_MATRIX_READY_ERROR = 0x20;
        uint AE_E_BIT_TEMP_ERROR = 0x10;
        uint AE_E_BIT_SOCKET_OPEN = 0x08;
        uint AE_E_BIT_CHANNEL_SHORTS = 0x800;
        uint AE_E_BIT_SUPPLY_ERROR = 0x200;
        uint AE_E_BIT_SMART_ERROR = 0x01;

        uint AE_S_BIT_POWER_ON = 0x4000;
        uint AE_S_BIT_START_MATRIX = 0x8000;


        [StructLayout(LayoutKind.Sequential,CharSet=CharSet.Ansi,Pack=1)]
        struct STRUCT_TEMP_ENTRY
        {
            [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
 	        public byte exists;
            [MarshalAs(UnmanagedType.U1, SizeConst = 1)]
 	        public byte temperature;
        }
          //
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        struct STRUCT_TEMP_SENS 
        {
	      public STRUCT_TEMP_ENTRY FPGA;
	      public STRUCT_TEMP_ENTRY PCB;
	      public STRUCT_TEMP_ENTRY CU;
        } 
 
        //
        // AE temperatue sensor array
        //
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Ansi, Pack = 1)]
        struct STRUCT_TEMP_SENSORS_AE
        {
	       public STRUCT_TEMP_SENS MASTER;
	       public STRUCT_TEMP_SENS DAC1;
	       public STRUCT_TEMP_SENS DAC2;
	       public STRUCT_TEMP_SENS DAC3;
	       public STRUCT_TEMP_SENS DAC4;
         }

        STRUCT_TEMP_SENSORS_AE AE_SENSOR_VALUES;

        System.Threading.Thread AEStatusThread;
        private volatile bool _StopStatusThread = false;

        ///
        /// thread for status polling of the AE this will also force
        /// the detection of a disconnect
        /// 
        public void AE_StatusThread()
        {
            //internals if errors where reported already to prevent multiple events in UI
            bool SocketErrorReported = false;
            bool ReadyErrorReported = false;
            bool ReportedTempWarning = false;
            bool ReportedShortCircuit = false;
            bool ReportedModuleError = false;
            bool ReportedSupplyError = false;
            bool ReportedSmartError = false;

            uint ErrorBits = 0;
            //uint StatusBits = 0x0;

            while (true)
            {
                if (_StopStatusThread)
                {
                    return;
                }

                ErrorBits = 0;
                //StatusBits = 0;

                if (IsConnected())
                {
                    ErrorBits = GetErrorBits();
                }

                ///
                /// smart error
                ///
                if (ErrorBits != 0xFFFFFFFF)
                {
                    if (Convert.ToBoolean(ErrorBits & AE_E_BIT_SMART_ERROR))
                    {
                        if (_eventHandler != null)
                        {
                            if (!ReportedSmartError)
                            {
                                ReportedSmartError = true;
                                AE_EventArgs e = new AE_EventArgs("AE256a_mk2 - MMA smart error detected.", AE_SMART_ERR, 0);
                                _eventHandler(this, e);
                            }
                        }
                    }
                    else
                    {
                        if (ReportedSmartError)
                        //only send a clear event if we claimed before
                        {
                            ReportedSmartError = false;
                            AE_EventArgs e = new AE_EventArgs("MMA smart error cleared.", AE_SMART_ERR_CLR, 0);
                            _eventHandler(this, e);
                        }
                    }

                    ///
                    /// supply error
                    ///
                    if (Convert.ToBoolean(ErrorBits & AE_E_BIT_SUPPLY_ERROR))
                    {
                        if (_eventHandler != null)
                        {
                            if (!ReportedSupplyError)
                            {
                                ReportedSupplyError = true;
                                AE_EventArgs e = new AE_EventArgs("AE256a_mk2 - MMA supply error detected.", AE_SUPPLY_ERR, 0);
                                _eventHandler(this, e);
                            }
                        }
                    }
                    else
                    {
                        if (ReportedSupplyError)
                        {
                            ReportedSupplyError = false;
                            AE_EventArgs e = new AE_EventArgs("MMA supply error cleared.", AE_SUPPLY_ERR_CLR, 0);
                            _eventHandler(this, e);
                        }
                    }


                    ///
                    /// module error
                    ///
                    if (Convert.ToBoolean(ErrorBits & AE_E_BIT_MODULE_ERROR))
                    {
                        if (_eventHandler != null)
                        {
                            if (!ReportedModuleError)
                            {
                                ReportedModuleError = true;
                                AE_EventArgs e = new AE_EventArgs("AE256a_mk2 - module error detected.", AE_ERR_MODULE, 0);
                                _eventHandler(this, e);
                            }
                        }
                    }
                    else
                    {
                        if (ReportedModuleError)
                        {
                            ReportedModuleError = false;
                            AE_EventArgs e = new AE_EventArgs("Module error cleared.", AE_ERR_MODULE_CLR, 0);
                            _eventHandler(this, e);
                        }
                    }
                    ///
                    /// short circuit
                    ///
                    if (Convert.ToBoolean(ErrorBits & AE_E_BIT_CHANNEL_SHORTS))
                    {
                        if (_eventHandler != null)
                        {
                            if (!ReportedShortCircuit)
                            {
                                ReportedShortCircuit = true;
                                AE_EventArgs e = new AE_EventArgs("AE256a_mk2 - short circuit on channel(s) detected.", AE_SHORT_CIRCUIT, 0);
                                _eventHandler(this, e);
                            }
                        }
                    }
                    else
                    {
                        if (ReportedShortCircuit)
                        {
                            ReportedShortCircuit = false;
                            AE_EventArgs e = new AE_EventArgs("Short circuit condition cleared.", AE_SHORT_CIRCUIT_CLR, 0);
                            _eventHandler(this, e);
                        }
                    }


                    ///
                    /// matrix ready error
                    ///
                    if (Convert.ToBoolean(ErrorBits & AE_E_BIT_MATRIX_READY_ERROR))
                    {
                        if (_eventHandler != null)
                        {
                            if (!ReadyErrorReported)
                            {
                                ReadyErrorReported = true;
                                AE_EventArgs e = new AE_EventArgs("AE256a_mk2 - ASLM programming error (READY missing).", AE_READY_ERROR, 0);
                                _eventHandler(this, e);
                            }
                        }
                    }
                    else
                    {
                        if (ReadyErrorReported)
                        {
                            ReadyErrorReported = false;
                            AE_EventArgs e = new AE_EventArgs("Ready error cleared.", AE_READY_ERROR_CLR, 0);
                            _eventHandler(this, e);
                        }
                    }


                    ///
                    /// temperature error
                    ///
                    if (Convert.ToBoolean(ErrorBits & AE_E_BIT_TEMP_ERROR))
                    {
                        if (_eventHandler != null)
                        {
                            if (!ReportedTempWarning)
                            {
                                ReportedTempWarning = true;
                                AE_EventArgs e = new AE_EventArgs("AE256a_mk2 - temperature warning. !! Check water cooling !!", AE_TEMP_ERROR, 0);
                                _eventHandler(this, e);
                            }
                        }
                    }
                    else
                    {
                        if (ReportedTempWarning)
                        {
                            ReportedTempWarning = false;
                            AE_EventArgs e = new AE_EventArgs("Temperature warning cleared.", AE_TEMP_ERROR_CLR, 0);
                            _eventHandler(this, e);
                        }
                    }



                    ///
                    /// socket open error
                    ///
                    if (Convert.ToBoolean(ErrorBits & AE_E_BIT_SOCKET_OPEN))
                    {
                        if (_eventHandler != null)
                        {
                            if (!SocketErrorReported)
                            {
                                SocketErrorReported = true;

                                AE_EventArgs e = new AE_EventArgs("AE256a_mk2 - socket is open -> Check socket lever !! ", AE_SOCKET_ERROR, 0);
                                _eventHandler(this, e);
                            }
                        }
                    }
                    else
                    {
                        if (SocketErrorReported)
                        {
                            SocketErrorReported = false;
                            AE_EventArgs e = new AE_EventArgs("Socket closed.", AE_SOCKET_ERROR_CLR, 0);
                            _eventHandler(this, e);
                        }
                    }
                }                        

                System.Threading.Thread.Sleep(1000);
            }
        }
       
        ///
        /// IPMS_SLM.dll exported functions (used)
        ///
        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_Connect", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_Connect(String IP4Address, ushort port);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_Disconnect", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_Disconnect();

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_GetErrorBits", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_GetErrorBits(ref uint ErrorBits);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_GetStatusBits", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_GetStatusBits(ref uint StatusBits);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_GetID", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_GetID(StringBuilder ID);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_SLM_PowerOn", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_SLM_PowerOn();

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_SLM_PowerOff", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_SLM_PowerOff();

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_SLM_Start", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_SLM_Start();

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_SLM_Stop", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_SLM_Stop();

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_EnableExternStart", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_EnableExternStart(bool enable);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_LoadImage", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_LoadImage(String ImagePath, ushort position);
   
        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_LoadData", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_LoadData (IntPtr buffer, 
									    ushort type,
									     ushort width,
										  ushort height,
										   ushort RAMPosition );

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_SetPictureSequence", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_SetPictureSequence (int RAMPosition,
				    									  int ReadyOutSignalEn,
												           int LastImage );

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_SetVoltage", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_SetVoltage (String VoltageName, float value);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_GetVoltage", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_GetVoltage(String VoltageName, ref float value);

        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        private delegate int AE_CALLBACK (int EVENT_ID, int ERROR_MASK, int STATUS_MASK);
        private AE_CALLBACK InstAE_CALLBACK;

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_RegisterCallback", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_RegisterCallback(AE_CALLBACK fn, uint ErrorMask);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_SetDeflectionPhase", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_SetDeflectionPhase(float DeflectionDelay_us, float DeflectionWidth_us);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_GetDeflectionPhase", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_GetDeflectionPhase(ref float DeflectionDelay_us, ref float DeflectionWidth_us);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_SetReadySignal", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_SetReadySignal(float UserReadyDelay_us, float UserReadyWidth_us);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_GetReadySignal", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_GetReadySignal(ref float UserReadyDelay_us, ref float UserReadyWidth_us);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_ReadTempSensorValues", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_ReadTempSensorValues(ref STRUCT_TEMP_SENSORS_AE AE_SENSORS);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_PeltierControlEnable", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_PeltierControlEnable(bool op_enable);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_PeltierSetTemp", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_PeltierSetTemp(float val);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_PeltierGetTemp", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_PeltierGetTemp(ref float val);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_LoadCalibrationData", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_LoadCalibrationData(string filepath);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_IPAddressSetup", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_IPAddressSetup (string IP_Address,
												 string  netmask,
												 string gateway,
    											 ushort port);

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_Reset", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_Reset();

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_EnableDebugMode", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_EnableDebugMode();

        [DllImport(@"IPMS_SLM.dll", EntryPoint = "AE_DisableDebugMode", CallingConvention = CallingConvention.Cdecl)]
        private static extern int AE_DisableDebugMode();

        // make the library thread safe -> the DLL itself is not !!
        private readonly object LockingObject = new Object();

        public CAE256a_mk2()
        {}

        // variables, internal flags
        private bool isConnected;
        private bool isPowerOn;
        private bool isStarted;
        private bool isPeltierEnabled;
        private String LastErrorString;

        // get the last error string for additional information
        public String getLastErrorString()
        {
            return LastErrorString;
        }

        /// <summary>
        /// connect with the AE
        /// </summary>
        /// <param name="IP4Address"> default 192.168.0.2 </param>
        /// <param name="port"> default 4002 </param>
        /// <returns></returns>
        public bool Connect(String IP4Address, ushort port)
        {
            lock (LockingObject)
            {
                LastErrorString = "";
                int result = -1;

                try
                {
                    result = AE_Connect(IP4Address, port);
                }
                catch (Exception) {
                    LastErrorString = "Exception: check IPMS_SLM.dll location";
                    result = -1;
                };

                if (result == 0)
                {
                    isConnected = true;

                    //force this mode to be OFF per default
                    AE_DisableDebugMode();

                    //register the callback function
                    InstAE_CALLBACK = new AE_CALLBACK(AE_CALLBACK_HANDLER);

                    //regestering for disconnect event's
                    AE_RegisterCallback(InstAE_CALLBACK, 0xFFFF);

                    //starting poll
                    _StopStatusThread = false;
                    AEStatusThread = new System.Threading.Thread(AE_StatusThread);
                    AEStatusThread.Start();
                    while (!AEStatusThread.IsAlive) { }

                    return true;
                }

                isConnected = false;
                return false;
            }
        }


        /// <summary>
        /// disconnect from the AE, client should become closed
        /// </summary>
        /// <returns></returns>
        public bool Disconnect()
        {
            lock (LockingObject)
            {
                LastErrorString = "";

                if (IsStarted())
                {
                    AE_SLM_Stop();
                }

                if (IsPowerOn())
                {
                    AE_SLM_PowerOff();
                }

                if (IsPeltierEnabled())
                {
                    AE_PeltierControlEnable(false);
                }

                if (AEStatusThread != null)
                {
                    //close the thread
                    _StopStatusThread = true;
                    AEStatusThread.Join();
                }

                int result = -1;
                try
                {
                    result = AE_Disconnect();
                }
                catch (Exception)
                {
                    LastErrorString = "Exception: check IPMS_SLM.dll location";
                    result = -1;
                }

                isConnected = false;

                if (result == 0)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// a delegate look a like which should capture the events
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public delegate void EventHandler(object sender, AE_EventArgs e);
        //the stuff for it
        public event EventHandler _eventHandler = null;


        /// <summary>
        /// registering external function for the events for example for updating the UI
        /// </summary>
        /// <param name="fn"></param>
        public void RegisterForEvents (EventHandler fn)
        {
            _eventHandler += fn;
        }

        /// <summary>
        /// registering external function for the events for example for updating the UI
        /// </summary>
        /// <param name="fn"></param>
        public void DeRegisterForEvents(EventHandler fn)
        {
            _eventHandler -= fn;
        }

        /// <summary>
        /// the callback handler function
        /// </summary>
        /// <param name="EVENT_ID"></param>
        /// <param name="ERROR_MASK"></param>
        /// <param name="STATUS_MASK"></param>
        /// <returns></returns>
        private int AE_CALLBACK_HANDLER(int EVENT_ID, int ERROR_MASK, int STATUS_MASK)
        {
            lock (LockingObject)
            {
                if (EVENT_ID == 10002)
                {
                    AE_EventArgs e = new AE_EventArgs("AE256a_mk2 - communication error / connection loss.", Convert.ToUInt32(EVENT_ID), 0);
                    _eventHandler(this, e);
                }
            }

            return 0;
        }

        /// <summary>
        /// adjust the IP address of the system, reboot to take effect
        /// </summary>
        /// <param name="IP_Address">192.168.0.2</param>
        /// <param name="netmask">255.255.0.0</param>
        /// <param name="gateway">0.0.0.0</param>
        /// <param name="port">4002</param>
        /// <returns></returns>
        public bool IPAddressSetup (string IP_Address,
								string  netmask,
								string gateway,
    							ushort port)
        {
            lock (LockingObject)
            {
                return (AE_IPAddressSetup(IP_Address,
                                    netmask,
                                    gateway,
                                    port) == 0);
            }
        }

        /// <summary>
        /// eletronics reboot (coldboot)
        /// </summary>
        public void Reset()
        {
            lock (LockingObject)
            {
                AE_Reset();
            }
        }
              

        /// <summary>
        /// return connection status
        /// </summary>
        /// <returns></returns>
        public bool IsConnected()
        {
            if (!isConnected)
            {
                LastErrorString = "Not connected to AE-System.";
            }
            else
            {
                LastErrorString = "";
            }

            return isConnected;
        }


        /// <summary>
        /// fetch the system ID AE256a_mk2... for ex.
        /// </summary>
        /// <returns></returns>
        public String GetID()
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return null;
                }

                StringBuilder bn = new StringBuilder(32);

                if (AE_GetID(bn) == 0)
                {
                    return bn.ToString();
                }

                return null;
            }
        }


        /// <summary>
        /// check if we have enabled the power supplies
        /// </summary>
        /// <returns></returns>
        public bool IsPowerOn()
        {
            if (!isPowerOn)
            {
                LastErrorString = "SLM power supplies not enabled.";
                return false;
            }

            return true;
        }


        /// <summary>
        /// switch the MMA supplies ON (do before any Start command )
        /// </summary>
        /// <returns></returns>
        public bool PowerOn()
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                if (AE_SLM_PowerOn() == 0)
                {
                    isPowerOn = true;
                    return true;
                }

                return false;
            }
        }


        /// <summary>
        /// switch the MMA supplies OFF (do after any Stop command )
        /// </summary>
        /// <returns></returns>
        public bool PowerOff()
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                if (IsStarted())
                {
                    Stop();
                }

                if (AE_SLM_PowerOff() == 0)
                {
                    isPowerOn = false;
                    isStarted = false;
                    return true;
                }

                return false;
            }
        }


        /// <summary>
        /// enable the SMB.Start connector input, do before! Start command
        /// SMB.Start will wait for a high level to start a program cycle
        /// keep the duty cycle low 1% ON , 99% OFF to not accidently start
        /// the next programming cycle (duration ~435us)
        /// </summary>
        /// <param name="enable"></param>
        /// <returns></returns>
        public bool EnableExternStart(bool enable)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                if (AE_EnableExternStart(enable) == 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// public check if we already started the MMA addressing
        /// </summary>
        /// <returns></returns>
        public bool IsStarted()
        {
            return isStarted;
        }

        /// <summary>
        /// start the MMA addressing, -> PowerOn before start
        /// </summary>
        /// <param name="enable"></param>
        /// <returns></returns>
        public bool Start()
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                if (!IsPowerOn())
                {
                    return false;
                }

                if (AE_SLM_Start() == 0)
                {
                    isStarted = true;
                }

                return (IsStarted() == true);
            }
        }


        /// <summary>
        /// stop the MMA addressing, command is allowed any time
        /// </summary>
        /// <param name="enable"></param>
        /// <returns></returns>
        public bool Stop ()
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                if (AE_SLM_Stop() == 0)
                {
                    isStarted = false;
                }

                return (IsStarted() == false);
            }
        }

         /// <summary>
        /// upload an image file to AE system
        /// expected is 24Bit Windows Bitmap (BGR) format
        /// </summary>
        /// <param name="path"> full path to image file</param>
        /// <param name="position">1.. 511 frame buffer target location</param>
        /// <returns></returns>
        public bool LoadImage(String path, ushort position)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_LoadImage(path, position) == 0);
            }
        }

        /// <summary>
        /// upload data buffers to the AE system
        /// </summary>
        /// <param name="data"></param>
        /// <param name="type"></param>
        /// <param name="xSize"></param>
        /// <param name="ySize"></param>
        /// <param name="RAMposition"></param>
        /// <returns></returns>
        public bool LoadData(IntPtr data, ushort type, ushort xSize, ushort ySize, ushort RAMposition)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_LoadData(data, type, xSize, ySize, RAMposition) == 0);
            }
        }


        /// <summary>
        /// define a sequence of frame buffer position to be displayed
        /// 
        /// to display positions 1..5 in reverse order use
        /// 
        ///     SetPictureSequence(5, true, false)
        ///     SetPictureSequence(4, true, false)
        ///     SetPictureSequence(3, true, false)
        ///     SetPictureSequence(2, true, false)
        ///     SetPictureSequence(1, true, true )
        ///     
        /// to display a single image
        /// 
        ///     SetPictureSequence(4, true, true)
        /// 
        /// </summary>
        /// <param name="RAMPosition">1..511</param>
        /// <param name="ReadySignalOut">true = enable UserReady output signal for sync/trigger</param>
        /// <param name="IsLastImage"> false = start a sequence definition, true = end a sequence or single image, </param>
        /// <returns></returns>
        public bool SetPictureSequence(int RAMPosition, bool ReadySignalOut, bool IsLastImage )
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_SetPictureSequence(RAMPosition, Convert.ToInt32(ReadySignalOut), Convert.ToInt32(IsLastImage)) == 0);
            }
        }


        /// <summary>
        /// setup a voltage
        /// it is recommended to leave the values as setup by the calibration data 
        /// </summary>
        /// <param name="VoltageName">VDMD_L, VDMD_F, VCE_L, VCE_F, VSHIELD_L, VSHIELD_F</param>
        /// <param name="value">-10.0 .. +40.0V </param>
        /// <returns></returns>
        public bool SetVoltage(String VoltageName, float value)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                if (AE_SetVoltage(VoltageName, value) == 0)
                {
                    return true;
                }

                return false;
            }
        }

        /// <summary>
        /// get a voltage level
        /// </summary>
        /// <param name="VoltageName">VDMD_L, VDMD_F, VCE_L, VCE_F, VSHIELD_L, VSHIELD_F</param>
        /// <param name="value">-10.0 .. +40.0V</param>
        /// <returns></returns>
        public bool GetVoltage(String VoltageName, ref float value)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_GetVoltage(VoltageName, ref value) == 0);
            }
        }

        /// <summary>
        /// shift/delay UserReady trigger, must be located inside the deflection phase
        /// </summary>
        /// <param name="delay_us">0 ... 1e6 us</param>
        /// <param name="width_us">0 ...  16 us</param>
        /// <returns></returns>
        public bool SetReadySignal(float delay_us, float width_us)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_SetReadySignal(delay_us, width_us) == 0);
            }
        }

        /// <summary>
        /// read the actual settings for the UserReady output signel
        /// </summary>
        /// <param name="delay_us"></param>
        /// <param name="width_us"></param>
        /// <returns></returns>
        public bool GetReadySignal(ref float delay_us, ref float width_us)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_GetReadySignal(ref delay_us, ref width_us) == 0);
            }
        }

        /// <summary>
        /// adjust the timing of the deflection state of the mirrors
        /// in deflected state the information give by the mirrors
        /// the content is valid 25us after start of deflection
        /// 
        /// Warning!
        /// 
        /// It is recommended to keep the duty cycle as low as possible ~5%
        /// So larger duty cycles may not be block in software but can lead
        /// to unwanted side effects (imprinting ...)
        /// 
        /// </summary>
        /// <param name="delay_us"> 0 .. 16 us  </param>
        /// <param name="width_us"> 0 .. 1e6 us </param>
        /// <returns></returns>
        public bool SetDeflectionPhase (float delay_us, float width_us)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_SetDeflectionPhase(delay_us, width_us) == 0);
            }
        }


        /// <summary>
        /// get the timing of the deflection phase
        /// </summary>
        /// <param name="delay_us"></param>
        /// <param name="width_us"></param>
        /// <returns></returns>
        public bool GetDeflectionPhase(ref float delay_us, ref float width_us)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_GetDeflectionPhase(ref delay_us, ref width_us) == 0);
            }
        }


        /// <summary>
        /// read the error bit content of the status bits 
        /// </summary>
        /// <returns></returns>
        public uint GetErrorBits()
        {
            lock (LockingObject)
            {
                if (IsConnected())
                {
                    uint mask = 0xFFFFFFFF;
                    if (AE_GetErrorBits(ref mask) == 0)
                    {
                        return mask;
                    }
                }

                return 0xFFFFFFFF;
            }
        }

        /// <summary>
        /// read the status bits of the electronics
        /// </summary>
        /// <returns></returns>
        public uint GetStatusBits()
        {
            lock (LockingObject)
            {
                if (IsConnected())
                {
                    uint mask = 0xFFFFFFFF;
                    if (AE_GetStatusBits(ref mask) == 0)
                    {
                        return mask;
                    }
                }

                return 0xFFFFFFFF;
            }
        }


        /// <summary>
        /// simple error checking with no evaluation
        /// </summary>
        /// <returns></returns>
        public bool HasErrors ()
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                uint mask = 0xFFFFFFFF;

                if (AE_GetErrorBits(ref mask) == 0)
                {
                    if (mask != 0)
                    {
                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// read sensors into an class internal array, each sensor is marked with an exists flag (0x01)
        /// the master CU sensor is connected to the power module water cooling
        /// use GetTemperature to get specific values
        /// </summary>
        /// <returns></returns>
        public bool ReadTempSensors()
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_ReadTempSensorValues(ref AE_SENSOR_VALUES) == 0);
            }
        }

        /// <summary>
        /// ReadTempSensors() to update the values in before
        /// </summary>
        /// <param name="module">"MASTER", "DAC1", "DAC2", "DAC3", "DAC4"</param>
        /// <param name="sensor">"FPGA", "PCB", "CU"</param>
        /// <returns>"" for none existing sensor</returns>
        public String GetTemperature(String module, String sensor)
        {
            lock (LockingObject)
            {
                if (module == "MASTER")
                {
                    if (sensor == "FPGA")
                    {
                        if (AE_SENSOR_VALUES.MASTER.FPGA.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.MASTER.FPGA.temperature.ToString();
                        }
                    }

                    if (sensor == "PCB")
                    {
                        if (AE_SENSOR_VALUES.MASTER.PCB.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.MASTER.PCB.temperature.ToString();
                        }
                    }
                }
                else
                    if (module == "POWER")
                    {
                        if (sensor == "CU")
                        {
                            if (AE_SENSOR_VALUES.MASTER.CU.exists == 0x01)
                            {
                                return AE_SENSOR_VALUES.MASTER.CU.temperature.ToString();
                            }
                        }

                    }

                if (module == "DAC1")
                {
                    if (sensor == "FPGA")
                    {
                        if (AE_SENSOR_VALUES.DAC1.FPGA.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC1.FPGA.temperature.ToString();
                        }
                    }

                    if (sensor == "PCB")
                    {
                        if (AE_SENSOR_VALUES.DAC1.PCB.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC1.PCB.temperature.ToString();
                        }
                    }

                    if (sensor == "CU")
                    {
                        if (AE_SENSOR_VALUES.DAC1.CU.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC1.CU.temperature.ToString();
                        }
                    }
                }

                if (module == "DAC2")
                {
                    if (sensor == "FPGA")
                    {
                        if (AE_SENSOR_VALUES.DAC2.FPGA.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC2.FPGA.temperature.ToString();
                        }
                    }

                    if (sensor == "PCB")
                    {
                        if (AE_SENSOR_VALUES.DAC2.PCB.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC2.PCB.temperature.ToString();
                        }
                    }

                    if (sensor == "CU")
                    {
                        if (AE_SENSOR_VALUES.DAC2.CU.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC2.CU.temperature.ToString();
                        }
                    }
                }

                if (module == "DAC3")
                {
                    if (sensor == "FPGA")
                    {
                        if (AE_SENSOR_VALUES.DAC3.FPGA.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC3.FPGA.temperature.ToString();
                        }
                    }

                    if (sensor == "PCB")
                    {
                        if (AE_SENSOR_VALUES.DAC3.PCB.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC3.PCB.temperature.ToString();
                        }
                    }

                    if (sensor == "CU")
                    {
                        if (AE_SENSOR_VALUES.DAC3.CU.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC3.CU.temperature.ToString();
                        }
                    }
                }

                if (module == "DAC4")
                {
                    if (sensor == "FPGA")
                    {
                        if (AE_SENSOR_VALUES.DAC4.FPGA.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC4.FPGA.temperature.ToString();
                        }
                    }

                    if (sensor == "PCB")
                    {
                        if (AE_SENSOR_VALUES.DAC4.PCB.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC4.PCB.temperature.ToString();
                        }
                    }

                    if (sensor == "CU")
                    {
                        if (AE_SENSOR_VALUES.DAC4.CU.exists == 0x01)
                        {
                            return AE_SENSOR_VALUES.DAC4.CU.temperature.ToString();
                        }
                    }
                }

                return "";
            }
        }

        /// <summary>
        /// enable MMA cooling
        /// </summary>
        /// <param name="en"> true = enable, false = disable</param>
        /// <returns></returns>
        public bool PeltierEnable(bool en)
        {
            lock (LockingObject)
            {
                if (IsConnected())
                {
                    if (AE_PeltierControlEnable(en) == 0)
                    {
                        if (en)
                        {
                            isPeltierEnabled = true;
                        }
                        else
                        {
                            isPeltierEnabled = false;
                        }

                        return true;
                    }
                }

                return false;
            }
        }

        /// <summary>
        /// set target value
        /// </summary>
        /// <param name="val">18...30�C</param>
        /// <returns></returns>
        public bool PeltierSetMMATemperature(float val)
        {
            lock (LockingObject)
            {
                return (AE_PeltierSetTemp(val) == 0);
            }
        }

        /// <summary>
        /// get enabled status
        /// </summary>
        /// <returns></returns>
        public bool IsPeltierEnabled()
        {
            return isPeltierEnabled;
        }

        /// <summary>
        /// get current peltier temperature if the peltier
        /// controller is disabled it will return -99.99F
        /// </summary>
        /// <returns>0.0f </returns>
        public float PeltierGetMMATemperature()
        {
            lock (LockingObject)
            {
                float val = 0.0F;

                if (AE_PeltierGetTemp(ref val) == 0)
                {
                    return val;
                }

                return 0.0f;
            }
        }

        /// <summary>
        /// load the calibration data file
        /// </summary>
        /// <param name="file">path to calibration file *.cal</param>
        /// <returns></returns>
        public bool LoadCalibrationData(string file)
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_LoadCalibrationData(file) == 0);
            }
        }

        /// <summary>
        /// debug mode means the electronics can be operated without inserted SLM device
        /// </summary>
        /// <returns></returns>
        public bool EnableDebugMode()
        {
            lock (LockingObject)
            {
                if (!IsConnected())
                {
                    return false;
                }

                return (AE_EnableDebugMode() == 0);
            }
        }

        /// <summary>
        /// disable debug mode, SLM device needed
        /// </summary>
        /// <returns></returns>
        public bool DisableDebugMode()
        {
            lock (LockingObject)
            {
                if ( !IsConnected() )
                {
                    return false;
                }

                return (AE_DisableDebugMode() == 0);
            }
        }
    }


    /// <summary>
    /// class for custom event passing
    /// </summary>
    public class AE_EventArgs : EventArgs
    {
        public readonly string ErrorText;
        public readonly uint ErrorCode;
        public readonly uint StatusCode;

        public AE_EventArgs(string text, uint code, uint status)
        {            
            ErrorText  = text;
            ErrorCode  = code;
            StatusCode = status;
        }
    }
}
