﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using AE256a_mk2;

namespace IPMS_NetWrapperExample
{
    public class Example
    {
        private CAE256a_mk2 AE256a;

        private const bool ON = true;
        private const bool OFF = false;
        private const bool SUCCESS = true;
        private const bool FAIL = false;
        private const uint INVALID = 0xFFFFFFFF;
        private const bool USR_RDY_ENABLE = true;
        private const bool LAST_IMAGE = true;
        private const float INVALID_TEMP = -99.99F;
        private const double IMG_SWITCH_DELAY = 1.0;    //seconds

        // 1 = this turns OFF smart addressing !!
        private const bool SMART_DISABLED = false;     //!! CAREFULL, only for demonstration purpose

        public Example()
        {
        }

        public void Run()
        {
            /*
            Kit example folder structure,  Single AE API

            example
                \cal\*.cal
                \img\*.bmp

            *.exe
            IPMS_SLM.dll
            */

            AE256a = new CAE256a_mk2();

            if (AE256a.Connect("192.168.0.2", 4002))
            {
                Console.Write("1) AE connection established - ID:");
            }
            else
            {
                DieWithMessage("1) AE connection failed.");
            }

            string ID = AE256a.GetID();
            Console.WriteLine(ID);

            /* some AE information */
            DumpAETemperatures();

            /* check for errors */
            uint StatusBits = AE256a.GetStatusBits();
            if (StatusBits == INVALID)
            {
                DieWithMessage("Failed to read AE256a status bits.");
            }

            uint ErrorBits = AE256a.GetErrorBits();
            if (ErrorBits == INVALID)
            {
                DieWithMessage("Failed to read AE256a error bits.");
            }

            if (ErrorBits != 0)
            {
                Console.WriteLine(
                        String.Format("Errorbits : {0:X}h", ErrorBits));
                DieWithMessage("System shows errors.  Abort!");
            }

            /*
             * loading the calibration
             *
             * working point is setup from the information
             * from the calibration file
             * 
             * !! REPLACE WITH MMA MATCHING .CAL FILE !!
             *
             */

            string currdir = Directory.GetCurrentDirectory();
            string calpath = Path.Combine(currdir + @"\cal\VC3471_18_01_Cal_-200-12200urad_21-Mar-2019.cal");

            if (AE256a.LoadCalibrationData(calpath))
            {
                Console.WriteLine("2) calibration data loaded");    
            }
            else
            {
                DieWithMessage("Error loading calibration data.");
            }


            /*****************************************************
             * !LoadImage is a local subroutine! 
             * load a set of images to 1,2,3 position 
             */
            LoadImage("_Rhombus.bmp", 1);
            LoadImage("Fraunhofer1M.bmp", 2);
            LoadImage("Graukeil2.bmp", 3);
            Console.WriteLine("3) Bitmaps loaded successfully.");


            /*****************************************************
            * Turning on Power for the SLM
            */
            if (AE256a.PowerOn())
            {
                Console.WriteLine("4) SLM power supply - ON.");
            }
            else
            {
                DieWithMessage("Error with SLM power supply.");
            }


            /*****************************************************
            * Before any start define a picture sequence
            */
            SetPictureSequence("1-3");


            ////////////////////////////////////////////////////////////////////////////
            // !! BE CAREFUL WITH THIS !!
            // static deflection with VDMD_L = VDMD_F can cause effects on SLM mirrors
            if (SMART_DISABLED)
            {
                float VDMD_F = 0.0f;
                if (AE256a.GetVoltage("VDMD_F", ref VDMD_F) == FAIL)
                {
                    DieWithMessage("SMART_OFF: Failed to read VDMD_F");
                }

                if (AE256a.SetVoltage("VDMD_L", VDMD_F) == FAIL)
                {
                     DieWithMessage("SMART_OFF: Failed to setup VDMD_L");
                }
            }


            /*****************************************************
            /* Peltier 
             */
            if (AE256a.PeltierSetMMATemperature(20.0f))
            {
                Console.WriteLine("6) set target temperature for MMA");
            }
            else
            {
                DieWithMessage("Failed to setup MMA cooling temperature.");
            }

            if (AE256a.PeltierEnable(ON))
            {
                Console.WriteLine("7) enable temperature control");
            }
            else
            {
                DieWithMessage("Failed to enable SLM cooling.");
            }

            Console.Write("\n\n5sec for SLM cooling to do some work ...\n\n");
            Pause(5.0);

            float MMATemp = AE256a.PeltierGetMMATemperature();
            if (MMATemp != INVALID_TEMP)
            {
                Console.WriteLine(
                    String.Format("8) SLM cooling temperature: {0:F1} degrees", MMATemp));
            }
            else
            {
                DieWithMessage("Failed to read actual SLM cooling temperature");
            }


            // start of addressing  the SLM 
            if (AE256a.Start())
            {
                Console.WriteLine("9) SLM addressing started.");
            }
            else
            {
                DieWithMessage("Error with start of SLM addressing");
            }

            //give electronics some cycles to check SLM status after programming (ReadyMMA)
            Pause(0.5);

            //check the status
            ErrorBits = AE256a.GetErrorBits();
            if (ErrorBits == INVALID)
            { 
               DieWithMessage("Error with reading ErrorBits.");
            }

            if (ErrorBits != 0)
            {
                DieWithMessage("AE shows errors -  shutdown.");
            }

            Console.WriteLine("\nSwitch image every 1sec .. <Press space to stop>\n");

            int imgnr = 1;
            for (; ;)
            {
                //single image switching -> keep LastImage = true
                if (AE256a.SetPictureSequence(imgnr, USR_RDY_ENABLE, LAST_IMAGE) == FAIL)
                { 
                    DieWithMessage("Error setting picture sequence.\r\n");
                }
                else
                {
                    Console.Write("\rDisplaying image: {0:0}", imgnr);
                }

                //check if the user hits a key
                if (Console.KeyAvailable)
                {
                    break;
                }

                Pause(IMG_SWITCH_DELAY);

                imgnr = (imgnr == 3) ? 1 : (imgnr + 1);
            }

            CleanStop("AE, SLM programming and cooling are stopped now ...");
            }

        // helper to convert to integer
        int ToInt(string val)
        {
            return Convert.ToInt32(val);
        }

        // set picture sequence sub 
        void SetPictureSequence(string FromTo)
        {
            string[] ft = FromTo.Split('-');

            int from = ToInt(ft[0]);
            int to = ToInt(ft[1]);

            bool LastImage = false;
            for (int i = from; i <= to; i++)
            {
                if (i==to)
                {
                    LastImage = true;
                }

                if (AE256a.SetPictureSequence(i, USR_RDY_ENABLE, LastImage) == FAIL)
                {
                    AE256a.SetPictureSequence(1, USR_RDY_ENABLE, LAST_IMAGE);
                    DieWithMessage("Error setting picture sequence : " + i.ToString());
                }
            }
        }


        // load image sub 
        void LoadImage(string name, ushort position)
        {
            string currdir = Directory.GetCurrentDirectory();
            string imgpath = Path.Combine(currdir + @"\img\" + name);

            if (AE256a.LoadImage(imgpath, position) == FAIL)
            {
                DieWithMessage("Error loading image: " + name);
            }
        }

        // pause timer 
        void Pause(double Time_s)
        {
            System.Threading.Thread.Sleep(Convert.ToInt32(Time_s * 1000.0));
        }

        void Stop()
        {
            AE256a.Stop();
            AE256a.PeltierEnable(OFF);
            AE256a.PowerOff();
            AE256a.Disconnect();
        }

        // shutdown and close lib 
        void CleanStop(string msg)
        {
            Console.WriteLine("\n");
            Console.WriteLine(msg);
            Stop();
            Environment.Exit(0);
        }

        // shutdown and close lib 
        void DieWithMessage(string msg)
        {
            Console.WriteLine(msg);
            Stop();
            Environment.Exit(-1);
        }

        // Temperature sensor values of the electronics
        void DumpAETemperatures()
        {
            if (AE256a.ReadTempSensors())
            {
                Console.WriteLine("");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("AE256a Temperatures (in degrees)");
                Console.WriteLine("--------------------------------");
                Console.WriteLine("         FPGA  PCB  CU  ");
                Console.WriteLine(
                    String.Format("Master     {0:0}   {1:0}      ",
                               AE256a.GetTemperature("MASTER", "FPGA"),
                               AE256a.GetTemperature("MASTER", "PCB")));
                Console.WriteLine(
                    String.Format("Power               {0:0}  ",
                            AE256a.GetTemperature("POWER", "CU")));
                Console.Write("DAC1");
                Console.WriteLine(
                    String.Format("       {0:0}   {1:0}  {2:0}  ",
                            AE256a.GetTemperature("DAC1", "FPGA"),
                            AE256a.GetTemperature("DAC1", "PCB"),
                            AE256a.GetTemperature("DAC1", "CU")));
                Console.Write("DAC2");
                Console.WriteLine(
                    String.Format("       {0:0}   {1:0}  {2:0}  ",
                            AE256a.GetTemperature("DAC2", "FPGA"),
                            AE256a.GetTemperature("DAC2", "PCB"),
                            AE256a.GetTemperature("DAC2", "CU")));
                Console.Write("DAC3");
                Console.WriteLine(
                    String.Format("       {0:0}   {1:0}  {2:0}  ",
                            AE256a.GetTemperature("DAC3", "FPGA"),
                            AE256a.GetTemperature("DAC3", "PCB"),
                            AE256a.GetTemperature("DAC3", "CU")));
                Console.Write("DAC4");
                Console.WriteLine(
                    String.Format("       {0:0}   {1:0}  {2:0}  ",
                            AE256a.GetTemperature("DAC4", "FPGA"),
                            AE256a.GetTemperature("DAC4", "PCB"),
                            AE256a.GetTemperature("DAC4", "CU")));
                Console.WriteLine("");
            }
        }
    }
}
