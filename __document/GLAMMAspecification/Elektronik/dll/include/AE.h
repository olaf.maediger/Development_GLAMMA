/************************************************************************************************
 * AE256axxx control interface library                                                         *
 ************************************************************************************************
 * AE_ underlinded the functions for single AE control                                          *
 *						                                                                        *
 * v1.0.0.1  25-04-2013 initial version			                                                *
 * v1.0.0.2  20-06-2013 bugfix AE_Connect and no AE found (crash on access like AE_GetVoltage)  *
 *                      added  AE_LoadCalibrationData / AE_LoadCalibrationDataU                 *
 *                      define input data format to support calibration data                    *
 *                      input data is now expected/translated to 16Bit short in �rad            *
 *                      the range typ. [-1000 ... 15000]  depends on calibration target MIN, MAX*
 *                      24Bit Bitmap BGR data is interpreted as B = Bits 0..7 G = 8..15 R = /   *
 *                      grayscale Bitmaps will become stretched to complete range [-10..14mrad] *
 * v1.0.0.3	 27-03-2014 added function AE_GetMinMaxDeflection to readout these values from a    *
 *                      loaded calibration file                                                 *
 ************************************************************************************************
 * (c) 2013 - 2014 Fraunhofer IPMS Dresden                                                      *
 ************************************************************************************************/

/**
 * @author Mark Eckert
 * @date 20.06.2013
 */

extern "C" {

#define AE_E_DLL_NOT_INITIALIZED	0x4000001	/**< returned if DLL was not initialized AE_Connect missing*/
#define AE_E_DLL_BOARD_NOT_FOUND	0x4000002	/**< returned connection to AE failed */
#define AE_E_DLL_STR_UTF8_FAIL		0x4000003	/**< returned UNICODE string convertion failed */

#ifndef __AETempSensors_t__
#define __AETempSensors_t__

  /**
   * a temperature entry for AE sensors
   */
  typedef struct STRUCT_TEMP_ENTRY
  {
	unsigned char exists;		/**< 1 = sensors exits */
 	signed   char temperature;  /**< temperature value */
  } TempEntry_t;
  
  /**
   * group entries to FPGA, PCB and Cooling Unit
   */
  typedef struct STRUCT_TEMP_SENS 
 {
	TempEntry_t FPGA;
	TempEntry_t PCB;
	TempEntry_t CU;
 } TempSensor_t;
 
   /**
   * combine temperature groups to AE related layout
   */
  typedef struct STRUCT_TEMP_SENSORS_AE
 {
	TempSensor_t MASTER;
	TempSensor_t DAC1;
	TempSensor_t DAC2;
	TempSensor_t DAC3;
	TempSensor_t DAC4;
 } AETempSensors_t, *PAETempSensors_t;

#endif

  /**
   * callback prototype for registering with AE_RegisterCallback
   *
   * @param		EVENT_ID	ID of event happend
   * @param		ERROR_MASK	AE error bits
   * @param		STATUS_MASK	AE status bits
   * @return	SUCCESS(0), FAIL(-1)
   */
  typedef int (*AE_CALLBACK) (int EVENT_ID, int ERROR_MASK, int STATUS_MASK);


  /**
   *  @brief connect to an AE system 
   *
   *  the function uses an IPv4 address and a TCP port to connect \n
   *  to the on system TCP/IP server
   *
   *  for ex.\n
   *
   *	AE_Connect ("192.168.0.2", "4002");
   *
   * @param		IP_Address	IPv4 address
   * @param		port		TCP port
   * @return	SUCCESS(0), FAIL(-1)
   */
  __declspec(dllexport)  int AE_Connect (const char *IP_Address,
 											unsigned short port);
 
  /**
   * @brief change the IP settings of a system
   *
   * The user can change the IP settings for the onboard server.\n
   * The new setting will used on next AE reboot.
   *
   * @param	IP_address	IPv4 address 
   * @param netmask		the netmask for the network segment
   * @param gateway		by default 0.0.0.0
   * @param port		the server TCP port
   * @return SUCCESS(0), FAIL(-1) 
   */
  __declspec (dllexport) int AE_IPAddressSetup (const char *IP_Address,
												 const char *netmask,
												  const char *gateway,
												   unsigned short port);

  /**
   * @brief disconnect from the AE
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_Disconnect ();

  
  /** 
   * @brief read the connected system ID
   *
   * @param IDString a pointer which become filled in with the ID\n
   *                 at least 16Bytes length
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_GetID (const char* IDString);


  /** 
   * @brief load a calibration data file ANSI/UTF8 version
   *
   * @param path  an absolute file path to the source of the calibration file\n
   *
   * @return SUCCESS(0), FAIL(-1)
   */
	__declspec (dllexport) int AE_LoadCalibrationData (const char *path);


   /** 
   * @brief load a calibration data file UTF16 version
   *
   * @param path  an absolute file path to the source of the calibration file\n
   *
   * @return SUCCESS(0), FAIL(-1)
   */
	__declspec (dllexport) int AE_LoadCalibrationDataU (const wchar_t *path);


  /** 
   * @brief get the min and max allowed values from the calibration file
   *
   * @param CalMin, CalMax	values to return the deflection limits (in �rad)
   *
   * @return SUCCESS(0), FAIL(-1)
   */
	__declspec (dllexport) int AE_GetMinMaxDeflection (short &CalMin, short &CalMax);

	
  /**
   * @brief read the AE status
   *
   * The status bits can give information if for ex. MMA supplies are enabled etc.
   *
   * @param &StatusBits	actual status returned into the given variable
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_GetStatusBits (unsigned int &StatusBits );
  

  /**
   * @brief read the AE error bits
   *
   * Error bits normally should be zero. The errors are bit masks.\n
   *
   * table of defined error bit numbers\n
   * <table>
   *    <tr>
   *     <td> error bit </td><td>number</td><td>description</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_MIRROR_VOLTAGE </td><td> 0 </td><td>mirror voltage check failed</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_MODULE_ERROR </td><td> 1 </td><td>error in DAC module (picture data overflow)</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_CALIBRATION_ERROR </td><td> 2 </td><td>calibration of AE channel(s) failed</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_SOCKET_OPEN_ERROR </td><td> 3 </td><td>Socket7 lever not closed</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_TEMPERATURE_ALERT </td><td> 4 </td><td> temperature of AE component to high</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_MATRIX_READY_ERROR </td><td> 5 </td><td> MMA ready signal not detected</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_RAM_TEST_ERROR </td><td> 8 </td><td>failed check picture RAM </td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_SUPPLY_ERROR </td><td> 9 </td><td>MMA supply error on AE_SLM_PowerOn</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_CONFIG_ERROR </td><td> 10 </td><td>failed to read / setup AE default configuration</td>
   *	</tr>
   *    <tr>
   *     <td> SLM_E_BIT_CHANNEL_SHORTS </td><td> 11 </td><td>short ciruit on AE channel(s) detected</td>
   *    </tr>
   * </table>
   *
   * @param &ErrorBits	actual status returned into the given variable
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_GetErrorBits (unsigned int &ErrorBits );



  /**
   * @brief get  MMA related voltage
   *
   * This function can be used to retrieve actual all MMA pixel related voltages settings. \n
   *
   * @param name	VDMD_L, VDMD_F		 potential of mirror plate \n
   *                VCE_L, VCE_F		 potential of counter electrode \n
   *                VFRAME_L, VFRAME_F   voltage to deflect the surrounding passive area \n
   *			    VPIX				 max pixel voltage for scaling input data \n
   *									 commonly used with uncalibrated data (direct voltage maps) \n\n
   *				
   *                VSHIELD_L VSHIELD_F  potential of shield plane if used\
   *                VCAP_L VCAP_F	     pixel cell footpoint capacitor reference potential voltage\
   *
   * @param	voltage voltage value
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_GetVoltage (const char *name, float &voltage );


  /**
   * @brief setup a MMA related voltage
   *
   * This function can be used to change all MMA pixel related voltages. \n
   *
   * @param name	VDMD_L, VDMD_F		 potential of mirror plate \n
   *                VCE_L, VCE_F		 potential of counter electrode \n
   *                VFRAME_L, VFRAME_F   voltage to deflect the surrounding passive area \n
   *			    VPIX				 max pixel voltage for scaling input data \n
   *									 commonly used with uncalibrated data (direct voltage maps) \n\n
   *				
   *                VSHIELD_L VSHIELD_F  potential of shield plane if used\
   *                VCAP_L VCAP_F	     pixel cell footpoint capacitor reference potential voltage\
   *
   * @param	voltage voltage value
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_SetVoltage (const char *name, float  voltage );


  /**
   * @brief load data from a drive or network resource
   *
   * Commonly this function is used to load bitmap data. The data translates to pixel voltages.
   * The input data is intensity depending on the characteristic of the MMA device. 
   *
   * @param filepath	absolute path to source file
   * @param RAMPosition position in AE memory where data is stored\n
   *                    position will be used to define a sequence of images
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_LoadImage (const char* filepath, 
										    unsigned short RAMPosition);


  /**
   * @brief load data from a drive or network resource
   *
   * unicode version
   *
   * Commonly this function is used to load bitmap data. The data translates to pixel voltages.
   * The input data is intensity depending on the characteristic of the MMA device. 
   *
   * @param filepath	absolute path to source file
   * @param RAMPosition position in AE memory where data is stored\n
   *                    position will be used to define a sequence of images
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_LoadImageU (const wchar_t *filepath, 
										     unsigned short RAMPosition);

  
  /**
   * @brief load data direct from an application
   *
   * Data in the range of signed short -1000 ... 15000 �rad depending on 
   * calibration min and max
   *
   * @param buffer		pointer to data to be loaded
   * @param type	    3 for unsigned short buffer (only 12Bit used max. value 4095)
   * @param cols_width  2048
   * @param rows_height 512
   * @param RAMPosition 1..511
   * @return SUCCESS(0), \n
   *		 FAIL(AE_E_DLL_STR_UTF8_FAIL)\n
   *		 FAIL(-1)
   */
  __declspec (dllexport) int AE_LoadData (void* buffer, 
											unsigned short type, 
												unsigned short cols_width, 
													unsigned short rows_height, 
														unsigned short RAMPosition );


  /**
   *
   * @brief Function is to be used to setup a sequence of pictures to display
   *
   * RAM areas must already be filled with data, for ex. \n
   *
   *		to display picture out of order 1,3,2
   *
   *			AE_SetPictureSequence ("1", 1, 0) keep LastImage 0
   *			AE_SetPictureSequence ("3", 1, 0) keep LastImage 0
   *			AE_SetPictureSequence ("2", 1, 1) set LastImage 1
   * 
   * @param RAMPosition			 a RAM position filled with valid data
   * @param ReadyOutSignalEn	 1 generate ReadyOut at SMB connector for synchronization\n
   * @param LastImage		     0 starts a sequence redefinition (used with single image selection)\n
   *							 1 ends an sequence definition (used with single image selection and range)\n
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_SetPictureSequence (int RAMPosition,
													 int ReadyOutSignalEn,
												      int LastImage );

  
  /**
   * @brief change timing of deflected mirror state
   *
   * AE system comes with a default installed setup. The user can adjust the values\
   * for the deflected mirror state after the programming cycle. In this time the\n
   * pixels are moved to nominal deflection. (In programming time the pixels are held \n
   * to nearly none defelcted state)
   *
   * The values below are related to full programming speed in a 2kHz cycle.
   *
   * @param DeflectionDelay_us		delay to deflection period 0.1 .. 15�s	
   * @param DeflectionWidth_us		width of deflection period 0.1 .. 50�s
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_SetDeflectionPhase (float DeflectionDelay_us,
								   		             float DeflectionWidth_us );


  /**
   * @brief read actual timing values for deflected mirror state
   *
   * The values below are related to full programming speed in a 2kHz cycle.
   *
   * @param DeflectionDelay_us		returns the delay value in �s
   * @param DeflectionWidth_us		returns the width value in �s
   *
   * @return SUCCESS(0), FAIL(-1)
   */
    __declspec (dllexport) int AE_GetDeflectionPhase (float &DeflectionDelay_us,
								   		             float &DeflectionWidth_us );



  /**
   * @brief adjustment for synchronization signal Ready on SMB connector of AE
   *
   * It is possible to generate a signal after each programming cycle. This\n
   * signal can be used to derive trigger pulses to illumination sources.\n
   * The signal should be shifted inside the deflection period (see above).\n
   * It is a good choice to add a couple of microseconds from begin of \n
   * deflection period to ensure that mirrors are stable deflected before\n
   * image aquisition.
   *
   * @param UserReadyDelay_us	delay of ready pulse 0.1..50�s
   * @param UserReadyWidth_us   width of ready pulse 0.1..10�s
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_SetReadySignal (float UserReadyDelay_us,
								   		         float UserReadyWidth_us );


  /**
   * @brief read actual timing values for signal Ready on SMB connector of AE
   *
   * @param UserReadyDelay_us	returns actual delay of ready pulse
   * @param UserReadyWidth_us   returns actual width of ready pulse 
   *
   * @return SUCCESS(0), FAIL(-1)
   */

  __declspec (dllexport) int AE_GetReadySignal (float &UserReadyDelay_us,
								   		         float &UserReadyWidth_us );

  
  /**
   * @brief turn on MMA power supply
   *
   * The MMA power supplies are enabled and checked.
   *
    * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_SLM_PowerOn ();


   /**
   * @brief turn off MMA power supply
   *
   * The MMA power supplies are enabled and checked.
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_SLM_PowerOff ();


  /**
   * @brief enable external trigger input SMB <Start>
   *
   * This signal must be set before AE_SLM_Start () command. \n
   * The input is expected 3.3V , active HIGH.
   *
   * @param enable 1 = enable, 0 = disable   
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_EnableExternStart (bool enable);


  /**
   * @brief start the MMA addressing
   *
   * The data is programmed / written to the MMA pixel cells and after that
   * deflection period will be generated with the user settings.
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_SLM_Start ();


  /**
   * @brief stop the MMA addressing
   *
   * The actual picture sequence will be finished and after that 
   * MMA programming is stopped.
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_SLM_Stop ();


  /**
   * @brief enable the onboard peltier controller
   * 
   * Current AE has an installed peltier controller of about max. 20W.
   * (max. current 4A, direction : cooling only )
   *
   * @param  enable		1 = enable, 0 = disable
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_PeltierControlEnable (bool enable);

  
  /**
   * @brief setup peltier temperature
   *
   * Setup the target temperature for cooling the MMA. 15.0� .. 40.0�
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_PeltierSetTemp (float Temperature);


   /**
   * @brief read current peltier temperature
   *
   * This reads the current temperature. Therefore the controller must\n
   * be enabled.
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_PeltierGetTemp (float &Temperature);


  /**
   * @brief readout temperature sensors of AE
   *
   * This functions gives the value of the AE temperature sensors.\n
   * It can be used to monitor AE health.
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_ReadTempSensorValues (AETempSensors_t &TempValues);
  

  /**
   * @brief register a callback function
   *
   * The callback will receive error information like temperature errors of the AE.
   *
   * @param  pCallback			reference to the callback
   * @param  ERR_NOTIFY_MASK	select errors to receive information
   *
   * @return SUCCESS(0), FAIL(-1)
   */
  __declspec (dllexport) int AE_RegisterCallback (AE_CALLBACK pCallback, unsigned int ERR_NOTIFY_MASK);


    /**
	 * @brief reset and reboot the AE
	 *
	 * @return SUCCESS(0), FAIL(-1)
	 */ 
  __declspec (dllexport) int AE_Reset ();


    /**
	 * @brief setup a deflection range for the mirrors in mrad
	 *
	 * This function will only work if there is already a valid calibration file loaded. 
	 *
	 * User can setup an inividual range for scaling / limitation.
	 *
	 * @return SUCCESS(0), FAIL(-1)
	 */ 

  __declspec (dllexport) int AE_SetDeflectionRange (float min, float max);

  
  /**
	 * @brief get the max valus of the deflection range for the mirrors in mrad
	 *
	 * This function will only work if there is already a valid calibration file loaded. 
	 *
	 * @return SUCCESS(0), FAIL(-1)
	 */ 

  __declspec (dllexport) int AE_GetDeflectionRange (float &min, float &max);

  }
