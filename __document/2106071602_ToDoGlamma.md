<h2>ToDo Glamma</h2>

<h4>2106070530 - Ergänzungen/Korrekturen</h4>

<b>X</b> Glamma-Gui Visualisierung: TabSheets von unten nach oben verlegen.
<b>X</b> Glamma-Gui Visualisierung: Bedienungs-Buttons von unter dem Image nach links neben dem Image.
<b>X</b> "CutFrame" bzw. "Load": Hintergrundfarbe default weiss, wählbar schwarz, beliebig.
<b>X</b> Gui-Row "Convert to File 2048x256" herausnehmen.
<b>X</b> top! "ShowFileIndex".checked -> unchecked bei anderer Aktion!.


<h4>2106021000 - Besprechung Ergänzungen/Korrekturen PSimon/FKleinwort/OM</h4>

<b>Korrekturen:</b>
<b>X</b> Bilder-Sequenz speichern: letztes Bild fehlt.
<b>O</b> Laden von PNG-Files: Transparent-Color: immer zuerst Frame mit Hintergrundfarbe beschreiben.

<b>Ergänzungen:</b>
- Glamma-Gui Visualisierung: TabSheets von unten nach oben verlegen.
- Glamma-Gui Visualisierung: Bedienungs-Buttons von unter dem Image nach links neben dem Image.
- "CutFrame" bzw. "Load": Hintergrundfarbe default weiss, wählbar schwarz, beliebig.
<b>O</b> "Save": Modaler Dialog Zielordner/Name.
<b>O</b> "Save": Info-Button mit (default-)Format.
<b>O</b> Gamma-Faktor Color: $C_{out} = C_{in} ^ \gamma$ mit $\gamma \in (0, 2) \in \mathbb{R}$
<b>O</b> Gamma-Faktor Color: über Histogramm vorgebbar.
- Gui-Row "Convert to File 2048x256" herausnehmen.
<b>O</b> Initdata für alle Prozessgrössen verwalten.

<br>
<h4>2106010230 - Auslieferung Glamma_00V11 PSimon/FKleinwort</h4>

<b>X</b> Teams-Sendung an PSimon und FKleinwort: <b>"2106011135_Glamma_00V11_DeliveryPSFK_exe.zip"</b>
<b>X</b> Laden von BitmapSources vom "source"-Ordner als SubOrdner des Working-Directory.
<b>X</b> Speichern der SequenceColumnBitmaps im "result"-Ordner als SubOrdner des Working-Directory.
<b>X</b> Schnelle PreVisualisierung aller SequenceColumnBitmaps über State-Checkbox "Show FileIndex" mit Scrollbar und NumericUpdown.
<b>X</b> Damit 1. Aufgabe: <b>"Glamma - ImageProcessing"</b> erledigt!

<img src="./_image/2106011520_Glamma_00V11.png" width="60%">
<br>

<h4>210531xxxx - Aktuelle Aufgaben</h4>

<b>X</b> Image2048x256 aus k-ter Spalte BitmapNormalized berechnet.
<b>X</b> Image2048x512 aus k-ter Spalte BitmapNormalized berechnet.
<b>X</b> für alle Spalten Folge Image2048x256_000.bmp .. Image2048x256_255.bmp mit StartIndex, EndIndex und IndexInterval berechnet und gespeichert.
<b>X</b> für alle Spalten Folge Image2048x512_000.bmp .. Image2048x512_255.bmp  mit StartIndex, EndIndex und IndexInterval berechnet und gespeichert.
<br>

<h4>2105280630 - Aktuelle Aufgaben</h4>
<b>X</b> Aus Bild mit 256/512 Spalten 256 Einzelbilder berechnen.

<h4>2105270830 - Aktuelle Aufgaben</h4>
Lösung:<br>
<b>X</b> Scale: um Center der BitmapSource scalen.<br>
<b>X</b> Rotate: um Center der BitmapSource rotieren.<br>
<b>X</b> Translate: Offset X/Y für Center der BitmapSource.<br>
<b>X</b> CutFrame: <i>BitmapSource -(Transformation)-> BitmapNormalized -(CutFrame)-> BitmapProjection</i>.
<br>

<h4>2105260830 - Aktuelle Aufgaben</h4>
Lösung:<br>
<b>X</b> Graphics.DrawImage OHNE Interpolation pixelgenau darstellen.<br>
<br>

<h4>2105250630 - Aktuelle Aufgaben</h4>

Aufgaben:

<b>///</b> Aus jeder Spalte SourceBitmap 256x256 ein Einzelbild berechnen:
  Column $C_k$ mit $k \in [0, 1, 255]$ 256-fach duplizieren,
  jede $C_k$ ergibt neues Einzelbild $P_k$ im Format $2048 \cdot C_k$, daher 256 Einzelbilder.
<b>///</b> In $P_k$ jede Zeile $R_l, l \in [0, 1, 255]$ duplizieren,
  ergibt aus $P_k$ 256-mal das Format $Q_k$ mit $2048 \cdot 512$.
<br>

<h4>2105191100 - Besprechung PSimon/FKleinwort Image-Preprocessing</h4>

 <b>/</b> Drei Tasten: Load, Convert, Save $\rarr$ Scrollbars
 <b>X</b> Vor Convert: Frame256x256 fixed auf 512x512PictureBox
 <b>X</b> Linke Maustaste: MoveXY $\rarr$ Scrollbars
 <b>X</b> Mausrad: ZoomIn/Out $\rarr$ Scrollbars
 <b>X</b> Convert: Aktuelle Sicht im Frame $\rarr$ resultierende (Source-)Bitmaps
 <b>O</b> weisse Ränder: als weisse Pixel konvertieren == keine Bearbeitung!
<br>

<h4>2105190800 - Gui : Image24b-Dll</h4>
 <b>X</b> Converting to: 256x256 pixel2, 512x256 pixel2, 4096x256 pixel2<br>
 <b>X</b> Converting to: 256x512 pixel2, 512x512 pixel2, 4096x512 pixel2<br>
 <b>O</b> Buttons Column2048x256 und Column2048x512: mit Scrollbar Column0..255 auswählen und anzeigen!<br>
<br>

<h4>2105180700 - Gui : Image24b-Dll</h4>
 <b>X</b> Laden von beliebigen Bitmaps,<br>
 <b>X</b> XY-Sizing auf 256x256 pixel2, 24bit RGB<br>
<br>

<h4>2105110916 - Gui : Module-Main/Dlls</h4>
Basic-Module:<br>
 <b>X</b> Eigenschaften der Initdata-Dll aus aktuellen C#-Projekten übernehmen.<br>
 <b>X</b> Eigenschaften der Programdata-Dll aus aktuellen C#-Projekten übernehmen.<br>
<br>
Helper-Module:<br>
 <b>X</b> Eigenschaften der Task-Dll aus aktuellen C#-Projekten übernehmen.<br>
 <b>X</b> Eigenschaften der TextFile-Dll aus aktuellen C#-Projekten übernehmen.<br>
 <b>/</b> Neu-Generierung der Image24b-Dll speziell für Glamma-Funktionen.<br>
<br>
UCBasic-Module:<br>
<b>X</b> Eigenschaften der UCNotifier-Dll aus aktuellen C#-Projekten übernehmen.<br>
<b>X</b> Eigenschaften der UCSerialNumber-Dll aus aktuellen C#-Projekten übernehmen.<br>
<b>X</b> Eigenschaften der UCTextEdito-Dll aus aktuellen C#-Projekten übernehmen.<br>
<br>
UCGlamma-Module:<br>
<b>/</b> Neu-Generierung der UCImageProcessing-Dll speziell für Glamma-Funktionen.<br>
<b>/</b> Neu-Generierung der UCLaserAreaScanner-Dll speziell für Glamma-Funktionen.<br>
<b>/</b> Neu-Generierung der UCLowLevelMMA-Dll speziell für Glamma-Funktionen.<br>
<b>/</b> Neu-Generierung der UCHighLevelMMA-Dll speziell für Glamma-Funktionen.<br>
<b>/</b> Neugenerierung des Main-Module Glamma.Exe<br>
<br>

<h4>2105071000 - Gerüst der Glamma-Software</h4>

Rahmen:
 - programmiert mit Visual Studio 2019 in C# als Exe-File,
 - programmiert für alle Windows10 Betriebssysteme,
 - alle Sources sind offengelegt,
 - modulare Top-Down-Architektur,
 - alle Module als Dlls,
<br>

<h4>2105061400 - Spezifikation der Rahmenbedingungen</h4>

WebMeeting mit PK (Peter Simon), FK (Frederick Kleinworth) und OM (Olaf Maediger) über die Machbarkeit der Glamma-Software.

Ergebnisse:
 - Die vier von FK aufgeführten Punkte werden als Punkt1 und Punkt3 zuerst allein von OM programmiert.
 - Die Punkte 2 und 3 unterstützt OM mit Code-Fragmenten, welche von einer weiteren Person (hier genannt WP) WP im Labor zusammengeführt, unter Laborbedingungen ergänzt, getestet und verifiziert werden. Dabei besteht ein aktiver Gedankenaustausch zwischen OM und WP über WebMeeting.
<br>













<!--
<h3>   </h3>
<h4>   </h4>
<h5>   </h5>
<h6>   </h6>
<h7>   </h7>
<b>   </b>
<i>   </i>
-->
