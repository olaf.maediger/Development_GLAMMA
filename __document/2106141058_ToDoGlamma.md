<h2>ToDo Glamma</h2>

<h4>2106111052 - Auslieferung "Glamma_00V16"</h4>

<b>X</b> "2106111130_Glamma_00V16_DeliveryPSJK.zip" nach Teams/Glamma speichern.
<b>X</b> "2106111130_Glamma_00V16_DeliveryPSJK_exe.zip" nach Teams/Glamma speichern.
<b>X</b> Aktuelle ToDo Datei nach Teams/Glamma speichern.

<img src="./_image/2106111130_Glamma_00V16.png" width="76%">


<br><h4>2106110530 - Korrekturen</h4>

<b>X</b> Initdata für alle Prozessgrössen verwalten.
<b>X</b> Initdata SelectFileDirectory last Directory speichern
<b>X</b> "Load Image" : nur Image-Files (bmp/png/gif/jpg) und Separation jedes Typs.


<br><h4>2106100600 - Korrekturen</h4>

<b>X</b> Seitenverhältnis beim Laden beibehalten, kürzere Seite bstimmt 256-Skalierung anpassen
<b>X</b> Seitenverhältnis beim Laden: Zusatz: Auswahl ShortSide / LongSide
<b>X</b> nudShowFileIndex enabled
<b>X</b> CutFrame GRÜN am Anfang und bei Änderung, nach Cut grau
<b>X</b> Button: File GammaFactor-User-Histogram laden (Xml)
<b>X</b> RadioButton: User-Histogram auswählen


<br><h4>2106091150 - Besprechung PSimon, FKleinwort, OMaediger</h4>

Ergänzungen/Korrekturen:
<b>X</b> Initdata für alle Prozessgrössen verwalten.
<b>X</b> Initdata SelectFileDirectory last Directory speichern
<b>X</b> "Load Image" : nur Image-Files (bmp/png/gif/jpg) und Separation jedes Typs.
<b>X</b> Seitenverhältnis beim Laden beibehalten, kürzere Seite bstimmt 256-Skalierung anpassen
<b>X</b> Seitenverhältnis beim Laden: Zusatz: Auswahl ShortSide / LongSide
<b>X</b> Scrollbars im Hauptfenster bzw. im ImageProcessing-TabSheet
<b>X</b> SelectFileDialog verschwindet, Funktion nach ConvertToFile verschieben
<b>X</b> nudShowFileIndex enabled
<b>X</b> CutFrame GRÜN am Anfang und bei Änderung, nach Cut grau
<b>X</b> Button: File GammaFactor-User-Histogram laden (Xml)
<b>X</b> RadioButton: User-Histogram auswählen


<br><h4>2106091116 - Ergänzungen/Korrekturen</h4>

<b>X</b> Auslieferung <b>210609????_Glamma_00V16</b>
<b>X</b> Scrollbars im Hauptfenster bzw. im ImageProcessing-TabSheet
<b>X</b> Sicherung "2106091114_Glamma_00V15_pngpreinitdata"
<b>X</b> Laden von PNG-Files: Transparent-Color: immer zuerst Frame mit Hintergrundfarbe beschreiben.


<br><h4>2106080445 - Ergänzungen/Korrekturen</h4>

<b>X</b> Auslieferung <b>2106082047_Glamma_00V14</b>
<b>X</b> Auslieferung <b>2106082047_Glamma_00V14_exe</b>

<img src="./_image/2106082116_Glamma_00V14.png" width="70%">

<b>X</b> BackColor bei FileIndex-Aktivierung auf aktuelle BackColor einstellen!!!
<b>X</b> "Save": Modaler Dialog Zielordner/Name.
<b>X</b> "Save": Info-Button mit (default-)Format.
<b>X</b> Gamma-Faktor: über Histogramm vorgebbar und als Xml-File ladbar und speicherbar.
<b>X</b> Fehler Graustufen-Konvertierung beseitigt (jetzt Mittelwert über alle Colors).
<b>X</b> BackColor Frame: Hintergrundfarbe zwischen weiss, schwarz und UserColor wählbar.
<b>X</b> Gamma-Faktor:
&nbsp;&nbsp;&nbsp; Linear $[0..255] \rarr [0..255]$
&nbsp;&nbsp;&nbsp; Inverse $[0..255] \rarr [255..0]$


<br><h4>2106070530 - Ergänzungen/Korrekturen</h4>

<b>X</b> Glamma-Gui Visualisierung: TabSheets von unten nach oben verlegen.
<b>X</b> Glamma-Gui Visualisierung: Bedienungs-Buttons von unter dem Image nach links neben dem Image.
<b>X</b> "CutFrame" bzw. "Load": Hintergrundfarbe default weiss, wählbar schwarz, beliebig.
<b>X</b> Gui-Row "Convert to File 2048x256" herausnehmen.
<b>X</b> top! "ShowFileIndex".checked -> unchecked bei anderer Aktion!.


<br><h4>2106021000 - Besprechung Ergänzungen/Korrekturen PSimon/FKleinwort/OM</h4>

<b>Korrekturen:</b>
<b>X</b> Bilder-Sequenz speichern: letztes Bild fehlt.
<b>X</b> Laden von PNG-Files: Transparent-Color: immer zuerst Frame mit Hintergrundfarbe beschreiben.

<b>Ergänzungen:</b>
<b>X</b> Glamma-Gui Visualisierung: TabSheets von unten nach oben verlegen.
<b>X</b> Glamma-Gui Visualisierung: Bedienungs-Buttons von unter dem Image nach links neben dem Image.
<b>X</b> "CutFrame" bzw. "Load": Hintergrundfarbe default weiss, wählbar schwarz, beliebig.
<b>X</b> "Save": Modaler Dialog Zielordner/Name.
<b>X</b> "Save": Info-Button mit (default-)Format.
<b>X</b> Gamma-Faktor Color: $C_{out} = C_{in} ^ \gamma$ mit $\gamma \in (0, 2) \in \mathbb{R}$
<b>X</b> Gamma-Faktor Color: über Histogramm vorgebbar.
<b>X</b> Gui-Row "Convert to File 2048x256" herausnehmen.


<br><h4>2106010230 - Auslieferung Glamma_00V11 PSimon/FKleinwort</h4>

<b>X</b> Teams-Sendung an PSimon und FKleinwort: <b>"2106011135_Glamma_00V11_DeliveryPSFK_exe.zip"</b>
<b>X</b> Laden von BitmapSources vom "source"-Ordner als SubOrdner des Working-Directory.
<b>X</b> Speichern der SequenceColumnBitmaps im "result"-Ordner als SubOrdner des Working-Directory.
<b>X</b> Schnelle PreVisualisierung aller SequenceColumnBitmaps über State-Checkbox "Show FileIndex" mit Scrollbar und NumericUpdown.
<b>X</b> Damit 1. Aufgabe: <b>"Glamma - ImageProcessing"</b> erledigt!

<img src="./_image/2106011520_Glamma_00V11.png" width="70%">


<br><h4>210531xxxx - Aktuelle Aufgaben</h4>

<b>X</b> Image2048x256 aus k-ter Spalte BitmapNormalized berechnet.
<b>X</b> Image2048x512 aus k-ter Spalte BitmapNormalized berechnet.
<b>X</b> für alle Spalten Folge Image2048x256_000.bmp .. Image2048x256_255.bmp mit StartIndex, EndIndex und IndexInterval berechnet und gespeichert.
<b>X</b> für alle Spalten Folge Image2048x512_000.bmp .. Image2048x512_255.bmp  mit StartIndex, EndIndex und IndexInterval berechnet und gespeichert.


<br><h4>2105280630 - Aktuelle Aufgaben</h4>
<b>X</b> Aus Bild mit 256/512 Spalten 256 Einzelbilder berechnen.


<br><h4>2105270830 - Aktuelle Aufgaben</h4>
Lösung:
<b>X</b> Scale: um Center der BitmapSource scalen.
<b>X</b> Rotate: um Center der BitmapSource rotieren.
<b>X</b> Translate: Offset X/Y für Center der BitmapSource.
<b>X</b> CutFrame: <i>BitmapSource -(Transformation)-> BitmapNormalized -(CutFrame)-> BitmapProjection</i>.


<br><h4>2105260830 - Aktuelle Aufgaben</h4>

Lösung:
<b>X</b> Graphics.DrawImage OHNE Interpolation pixelgenau darstellen.


<br><h4>2105250630 - Aktuelle Aufgaben</h4>

Aufgaben:

<b>X</b> Aus jeder Spalte SourceBitmap 256x256 ein Einzelbild berechnen:
&nbsp;&nbsp;&nbsp;Column $C_k$ mit $k \in [0, 1, 255]$ 256-fach duplizieren,
  jede $C_k$ ergibt neues Einzelbild $P_k$ im Format $2048 \cdot C_k$, daher 256 Einzelbilder.
<b>X</b> In $P_k$ jede Zeile $R_l, l \in [0, 1, 255]$ duplizieren,
&nbsp;&nbsp;&nbsp;ergibt aus $P_k$ 256-mal das Format $Q_k$ mit $2048 \cdot 512$.


<br><h4>2105191100 - Besprechung PSimon/FKleinwort Image-Preprocessing</h4>

 <b>X</b> Drei Tasten: Load, Convert, Save $\rarr$ Scrollbars
 <b>X</b> Vor Convert: Frame256x256 fixed auf 512x512PictureBox
 <b>X</b> Linke Maustaste: MoveXY $\rarr$ Scrollbars
 <b>X</b> Mausrad: ZoomIn/Out $\rarr$ Scrollbars
 <b>X</b> Convert: Aktuelle Sicht im Frame $\rarr$ resultierende (Source-)Bitmaps
 <b>X</b> weisse Ränder: als weisse Pixel konvertieren == keine Bearbeitung!


<br><h4>2105190800 - Gui : Image24b-Dll</h4>

 <b>X</b> Converting to: 256x256 pixel2, 512x256 pixel2, 4096x256 pixel2.
 <b>X</b> Converting to: 256x512 pixel2, 512x512 pixel2, 4096x512 pixel2.
 <b>X</b> Buttons Column2048x256 und Column2048x512: mit Scrollbar Column0..255 auswählen und anzeigen!


<br><h4>2105180700 - Gui : Image24b-Dll</h4>

 <b>X</b> Laden von beliebigen Bitmaps.
 <b>X</b> XY-Sizing auf 256x256 pixel2, 24bit RGB.


<br><h4>2105110916 - Gui : Module-Main/Dlls</h4>

Basic-Module:
 <b>X</b> Eigenschaften der Initdata-Dll aus aktuellen C#-Projekten übernehmen.
 <b>X</b> Eigenschaften der Programdata-Dll aus aktuellen C#-Projekten übernehmen.

Helper-Module:
 <b>X</b> Eigenschaften der Task-Dll aus aktuellen C#-Projekten übernehmen.
 <b>X</b> Eigenschaften der TextFile-Dll aus aktuellen C#-Projekten übernehmen.
 <b>X</b> Neu-Generierung der Image24b-Dll speziell für Glamma-Funktionen.

UCBasic-Module:
<b>X</b> Eigenschaften der UCNotifier-Dll aus aktuellen C#-Projekten übernehmen.
<b>X</b> Eigenschaften der UCSerialNumber-Dll aus aktuellen C#-Projekten übernehmen.
<b>X</b> Eigenschaften der UCTextEdito-Dll aus aktuellen C#-Projekten übernehmen.

UCGlamma-Module:
<b>/</b> Neu-Generierung der UCImageProcessing-Dll speziell für Glamma-Funktionen.
<b>/</b> Neu-Generierung der UCLaserAreaScanner-Dll speziell für Glamma-Funktionen.
<b>/</b> Neu-Generierung der UCLowLevelMMA-Dll speziell für Glamma-Funktionen.
<b>/</b> Neu-Generierung der UCHighLevelMMA-Dll speziell für Glamma-Funktionen.
<b>/</b> Neugenerierung des Main-Module Glamma.Exe .


<br><h4>2105071000 - Gerüst der Glamma-Software</h4>

Rahmen:
 - programmiert mit Visual Studio 2019 in C# als Exe-File,
 - programmiert für alle Windows10 Betriebssysteme,
 - alle Sources sind offengelegt,
 - modulare Top-Down-Architektur,
 - alle Module als Dlls,


<br><h4>2105061400 - Spezifikation der Rahmenbedingungen</h4>

WebMeeting mit PK (Peter Simon), FK (Frederick Kleinworth) und OM (Olaf Maediger) über die Machbarkeit der Glamma-Software.

Ergebnisse:
 - Die vier von FK aufgeführten Punkte werden als Punkt1 und Punkt3 zuerst allein von OM programmiert.
 - Die Punkte 2 und 3 unterstützt OM mit Code-Fragmenten, welche von einer weiteren Person (hier genannt WP) WP im Labor zusammengeführt, unter Laborbedingungen ergänzt, getestet und verifiziert werden. Dabei besteht ein aktiver Gedankenaustausch zwischen OM und WP über WebMeeting.



<!--
<h3>   </h3>
<h4>   </h4>
<h5>   </h5>
<h6>   </h6>
<h7>   </h7>
<b>   </b>
<i>   </i>
-->
