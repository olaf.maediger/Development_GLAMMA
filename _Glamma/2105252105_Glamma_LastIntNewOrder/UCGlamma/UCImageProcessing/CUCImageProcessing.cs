﻿using System;
using System.Windows.Forms;
//
using Image24b;
//
namespace UCImageProcessing
{
  public partial class CUCImageProcessing : UserControl
  {
    private CImage24b FImage24b;
    public CUCImageProcessing()
    {
      InitializeComponent();
      //
      FImage24b = new CImage24b();
    }

    private void btnLoadFromFile_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        FImage24b.LoadSourceFromFile(DialogLoadImage.FileName);
        FImage24b.NormalizeSource(cbxGrayScale.Checked);
        pbxImage.Image = FImage24b.GetBitmapNormalized();
      }
    }


  }
}
