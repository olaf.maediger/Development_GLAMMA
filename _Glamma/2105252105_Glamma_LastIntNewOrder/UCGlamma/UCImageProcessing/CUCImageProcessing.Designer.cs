﻿namespace UCImageProcessing
{
    partial class CUCImageProcessing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.pnlImageScrollZoom = new System.Windows.Forms.Panel();
      this.pbxImage = new System.Windows.Forms.PictureBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.scbScale = new System.Windows.Forms.HScrollBar();
      this.scbMoveH = new System.Windows.Forms.HScrollBar();
      this.pnlLS = new System.Windows.Forms.Panel();
      this.scbMoveV = new System.Windows.Forms.VScrollBar();
      this.scbRotate = new System.Windows.Forms.VScrollBar();
      this.pnlLB = new System.Windows.Forms.Panel();
      this.pnlUser = new System.Windows.Forms.Panel();
      this.scbColumn2048x512 = new System.Windows.Forms.HScrollBar();
      this.nudColumn2048x512 = new System.Windows.Forms.NumericUpDown();
      this.scbColumn2048x256 = new System.Windows.Forms.HScrollBar();
      this.nudColumn2048x256 = new System.Windows.Forms.NumericUpDown();
      this.btnColumn2048x256 = new System.Windows.Forms.Button();
      this.btnColumn2048x512 = new System.Windows.Forms.Button();
      this.nudRotate = new System.Windows.Forms.NumericUpDown();
      this.nudScale = new System.Windows.Forms.NumericUpDown();
      this.nudMoveV = new System.Windows.Forms.NumericUpDown();
      this.btnRescale = new System.Windows.Forms.Button();
      this.nudMoveH = new System.Windows.Forms.NumericUpDown();
      this.btnLoadFromFile = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.DialogLoadImage = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveImage = new System.Windows.Forms.SaveFileDialog();
      this.cbxGrayScale = new System.Windows.Forms.CheckBox();
      this.pnlImageScrollZoom.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
      this.panel1.SuspendLayout();
      this.pnlLS.SuspendLayout();
      this.pnlUser.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x512)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x256)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudRotate)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScale)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMoveV)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMoveH)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlImageScrollZoom
      // 
      this.pnlImageScrollZoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlImageScrollZoom.Controls.Add(this.pbxImage);
      this.pnlImageScrollZoom.Controls.Add(this.panel1);
      this.pnlImageScrollZoom.Controls.Add(this.pnlLS);
      this.pnlImageScrollZoom.Location = new System.Drawing.Point(0, 0);
      this.pnlImageScrollZoom.Name = "pnlImageScrollZoom";
      this.pnlImageScrollZoom.Size = new System.Drawing.Size(645, 556);
      this.pnlImageScrollZoom.TabIndex = 1;
      // 
      // pbxImage
      // 
      this.pbxImage.BackColor = System.Drawing.Color.LightSalmon;
      this.pbxImage.Location = new System.Drawing.Point(35, 0);
      this.pbxImage.Name = "pbxImage";
      this.pbxImage.Size = new System.Drawing.Size(518, 518);
      this.pbxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pbxImage.TabIndex = 6;
      this.pbxImage.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.scbScale);
      this.panel1.Controls.Add(this.scbMoveH);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(35, 518);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(608, 36);
      this.panel1.TabIndex = 5;
      // 
      // scbScale
      // 
      this.scbScale.Dock = System.Windows.Forms.DockStyle.Top;
      this.scbScale.Location = new System.Drawing.Point(0, 17);
      this.scbScale.Maximum = 109;
      this.scbScale.Minimum = -100;
      this.scbScale.Name = "scbScale";
      this.scbScale.Size = new System.Drawing.Size(608, 17);
      this.scbScale.TabIndex = 3;
      // 
      // scbMoveH
      // 
      this.scbMoveH.Dock = System.Windows.Forms.DockStyle.Top;
      this.scbMoveH.Location = new System.Drawing.Point(0, 0);
      this.scbMoveH.Maximum = 109;
      this.scbMoveH.Minimum = -100;
      this.scbMoveH.Name = "scbMoveH";
      this.scbMoveH.Size = new System.Drawing.Size(608, 17);
      this.scbMoveH.TabIndex = 2;
      // 
      // pnlLS
      // 
      this.pnlLS.Controls.Add(this.scbMoveV);
      this.pnlLS.Controls.Add(this.scbRotate);
      this.pnlLS.Controls.Add(this.pnlLB);
      this.pnlLS.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLS.Location = new System.Drawing.Point(0, 0);
      this.pnlLS.Name = "pnlLS";
      this.pnlLS.Size = new System.Drawing.Size(35, 554);
      this.pnlLS.TabIndex = 4;
      // 
      // scbMoveV
      // 
      this.scbMoveV.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbMoveV.Location = new System.Drawing.Point(17, 0);
      this.scbMoveV.Maximum = 109;
      this.scbMoveV.Minimum = -100;
      this.scbMoveV.Name = "scbMoveV";
      this.scbMoveV.Size = new System.Drawing.Size(17, 516);
      this.scbMoveV.TabIndex = 4;
      // 
      // scbRotate
      // 
      this.scbRotate.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbRotate.Location = new System.Drawing.Point(0, 0);
      this.scbRotate.Maximum = 109;
      this.scbRotate.Minimum = -100;
      this.scbRotate.Name = "scbRotate";
      this.scbRotate.Size = new System.Drawing.Size(17, 516);
      this.scbRotate.TabIndex = 3;
      // 
      // pnlLB
      // 
      this.pnlLB.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlLB.Location = new System.Drawing.Point(0, 516);
      this.pnlLB.Name = "pnlLB";
      this.pnlLB.Size = new System.Drawing.Size(35, 38);
      this.pnlLB.TabIndex = 1;
      // 
      // pnlUser
      // 
      this.pnlUser.Controls.Add(this.cbxGrayScale);
      this.pnlUser.Controls.Add(this.button1);
      this.pnlUser.Controls.Add(this.scbColumn2048x512);
      this.pnlUser.Controls.Add(this.nudColumn2048x512);
      this.pnlUser.Controls.Add(this.scbColumn2048x256);
      this.pnlUser.Controls.Add(this.nudColumn2048x256);
      this.pnlUser.Controls.Add(this.btnColumn2048x256);
      this.pnlUser.Controls.Add(this.btnColumn2048x512);
      this.pnlUser.Controls.Add(this.nudRotate);
      this.pnlUser.Controls.Add(this.nudScale);
      this.pnlUser.Controls.Add(this.nudMoveV);
      this.pnlUser.Controls.Add(this.btnRescale);
      this.pnlUser.Controls.Add(this.nudMoveH);
      this.pnlUser.Controls.Add(this.btnLoadFromFile);
      this.pnlUser.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlUser.Location = new System.Drawing.Point(0, 556);
      this.pnlUser.Name = "pnlUser";
      this.pnlUser.Size = new System.Drawing.Size(763, 158);
      this.pnlUser.TabIndex = 2;
      // 
      // scbColumn2048x512
      // 
      this.scbColumn2048x512.Location = new System.Drawing.Point(269, 115);
      this.scbColumn2048x512.Maximum = 264;
      this.scbColumn2048x512.Name = "scbColumn2048x512";
      this.scbColumn2048x512.Size = new System.Drawing.Size(182, 20);
      this.scbColumn2048x512.TabIndex = 34;
      // 
      // nudColumn2048x512
      // 
      this.nudColumn2048x512.Location = new System.Drawing.Point(213, 116);
      this.nudColumn2048x512.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudColumn2048x512.Name = "nudColumn2048x512";
      this.nudColumn2048x512.Size = new System.Drawing.Size(51, 20);
      this.nudColumn2048x512.TabIndex = 33;
      this.nudColumn2048x512.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // scbColumn2048x256
      // 
      this.scbColumn2048x256.Location = new System.Drawing.Point(269, 90);
      this.scbColumn2048x256.Maximum = 264;
      this.scbColumn2048x256.Name = "scbColumn2048x256";
      this.scbColumn2048x256.Size = new System.Drawing.Size(182, 20);
      this.scbColumn2048x256.TabIndex = 32;
      // 
      // nudColumn2048x256
      // 
      this.nudColumn2048x256.Location = new System.Drawing.Point(213, 91);
      this.nudColumn2048x256.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudColumn2048x256.Name = "nudColumn2048x256";
      this.nudColumn2048x256.Size = new System.Drawing.Size(51, 20);
      this.nudColumn2048x256.TabIndex = 31;
      this.nudColumn2048x256.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnColumn2048x256
      // 
      this.btnColumn2048x256.Location = new System.Drawing.Point(84, 88);
      this.btnColumn2048x256.Name = "btnColumn2048x256";
      this.btnColumn2048x256.Size = new System.Drawing.Size(123, 23);
      this.btnColumn2048x256.TabIndex = 30;
      this.btnColumn2048x256.Text = "Column 2048x256";
      this.btnColumn2048x256.UseVisualStyleBackColor = true;
      // 
      // btnColumn2048x512
      // 
      this.btnColumn2048x512.Location = new System.Drawing.Point(84, 115);
      this.btnColumn2048x512.Name = "btnColumn2048x512";
      this.btnColumn2048x512.Size = new System.Drawing.Size(123, 23);
      this.btnColumn2048x512.TabIndex = 29;
      this.btnColumn2048x512.Text = "Column 2048x512";
      this.btnColumn2048x512.UseVisualStyleBackColor = true;
      // 
      // nudRotate
      // 
      this.nudRotate.Location = new System.Drawing.Point(142, 62);
      this.nudRotate.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
      this.nudRotate.Name = "nudRotate";
      this.nudRotate.Size = new System.Drawing.Size(51, 20);
      this.nudRotate.TabIndex = 28;
      this.nudRotate.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // nudScale
      // 
      this.nudScale.Location = new System.Drawing.Point(85, 62);
      this.nudScale.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
      this.nudScale.Name = "nudScale";
      this.nudScale.Size = new System.Drawing.Size(51, 20);
      this.nudScale.TabIndex = 27;
      this.nudScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // nudMoveV
      // 
      this.nudMoveV.Location = new System.Drawing.Point(142, 36);
      this.nudMoveV.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
      this.nudMoveV.Name = "nudMoveV";
      this.nudMoveV.Size = new System.Drawing.Size(51, 20);
      this.nudMoveV.TabIndex = 26;
      this.nudMoveV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnRescale
      // 
      this.btnRescale.Location = new System.Drawing.Point(3, 34);
      this.btnRescale.Name = "btnRescale";
      this.btnRescale.Size = new System.Drawing.Size(75, 24);
      this.btnRescale.TabIndex = 25;
      this.btnRescale.Text = "Rescale";
      this.btnRescale.UseVisualStyleBackColor = true;
      // 
      // nudMoveH
      // 
      this.nudMoveH.Location = new System.Drawing.Point(85, 36);
      this.nudMoveH.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
      this.nudMoveH.Name = "nudMoveH";
      this.nudMoveH.Size = new System.Drawing.Size(51, 20);
      this.nudMoveH.TabIndex = 24;
      this.nudMoveH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnLoadFromFile
      // 
      this.btnLoadFromFile.Location = new System.Drawing.Point(3, 4);
      this.btnLoadFromFile.Name = "btnLoadFromFile";
      this.btnLoadFromFile.Size = new System.Drawing.Size(75, 24);
      this.btnLoadFromFile.TabIndex = 23;
      this.btnLoadFromFile.Text = "Load";
      this.btnLoadFromFile.UseVisualStyleBackColor = true;
      this.btnLoadFromFile.Click += new System.EventHandler(this.btnLoadFromFile_Click);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(3, 87);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 24);
      this.button1.TabIndex = 35;
      this.button1.Text = "Sequence";
      this.button1.UseVisualStyleBackColor = true;
      // 
      // DialogLoadImage
      // 
      this.DialogLoadImage.DefaultExt = "bmp";
      this.DialogLoadImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogLoadImage.Title = "Load Image";
      // 
      // DialogSaveImage
      // 
      this.DialogSaveImage.DefaultExt = "bmp";
      this.DialogSaveImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogSaveImage.Title = "Save Image";
      // 
      // cbxGrayScale
      // 
      this.cbxGrayScale.AutoSize = true;
      this.cbxGrayScale.Location = new System.Drawing.Point(85, 8);
      this.cbxGrayScale.Name = "cbxGrayScale";
      this.cbxGrayScale.Size = new System.Drawing.Size(75, 17);
      this.cbxGrayScale.TabIndex = 36;
      this.cbxGrayScale.Text = "GrayScale";
      this.cbxGrayScale.UseVisualStyleBackColor = true;
      // 
      // CUCImageProcessing
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlUser);
      this.Controls.Add(this.pnlImageScrollZoom);
      this.Name = "CUCImageProcessing";
      this.Size = new System.Drawing.Size(763, 714);
      this.pnlImageScrollZoom.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
      this.panel1.ResumeLayout(false);
      this.pnlLS.ResumeLayout(false);
      this.pnlUser.ResumeLayout(false);
      this.pnlUser.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x512)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x256)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudRotate)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScale)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMoveV)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMoveH)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    private System.Windows.Forms.Panel pnlImageScrollZoom;
    private System.Windows.Forms.PictureBox pbxImage;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.HScrollBar scbScale;
    private System.Windows.Forms.HScrollBar scbMoveH;
    private System.Windows.Forms.Panel pnlLS;
    private System.Windows.Forms.VScrollBar scbMoveV;
    private System.Windows.Forms.VScrollBar scbRotate;
    private System.Windows.Forms.Panel pnlLB;
    private System.Windows.Forms.Panel pnlUser;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.HScrollBar scbColumn2048x512;
    private System.Windows.Forms.NumericUpDown nudColumn2048x512;
    private System.Windows.Forms.HScrollBar scbColumn2048x256;
    private System.Windows.Forms.NumericUpDown nudColumn2048x256;
    private System.Windows.Forms.Button btnColumn2048x256;
    private System.Windows.Forms.Button btnColumn2048x512;
    private System.Windows.Forms.NumericUpDown nudRotate;
    private System.Windows.Forms.NumericUpDown nudScale;
    private System.Windows.Forms.NumericUpDown nudMoveV;
    private System.Windows.Forms.Button btnRescale;
    private System.Windows.Forms.NumericUpDown nudMoveH;
    private System.Windows.Forms.Button btnLoadFromFile;
    private System.Windows.Forms.OpenFileDialog DialogLoadImage;
    private System.Windows.Forms.SaveFileDialog DialogSaveImage;
    private System.Windows.Forms.CheckBox cbxGrayScale;
  }
}
