﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
//
namespace UCMultiColorText
{
  //
  //##################################################
  //  Segment - Main - CUCMultiColorText
  //##################################################
  //
  public partial class CUCMultiColorText : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    // Color Background
    private const Byte COLOR_BACKGROUND_A = 0xFF;
    private const Byte COLOR_BACKGROUND_R = 0xF6;
    private const Byte COLOR_BACKGROUND_G = 0xF7;
    private const Byte COLOR_BACKGROUND_B = 0xF8;
    // Color Focus
    private const Byte COLOR_FOCUS_A = 0xFF;
    private const Byte COLOR_FOCUS_R = 0xAA;
    private const Byte COLOR_FOCUS_G = 0xFF;
    private const Byte COLOR_FOCUS_B = 0xAA;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private RichTextBox FRichTextBox;
    // NC private List<Int32> FStartIndices;
    // NC private List<Int32> FStopIndices;

    static readonly object FLock = new object();
    //
    //-------------------------------
    //  Segment - Constructor
    //-------------------------------
    //
    public CUCMultiColorText()
    {
      InitializeComponent();
      //
      FRichTextBox = new RichTextBox();
      this.Controls.Add(FRichTextBox);
      FRichTextBox.ContextMenuStrip = cmsMultiColorText;
      //
      FRichTextBox.BackColor = Color.FromArgb(COLOR_BACKGROUND_A,
                                         COLOR_BACKGROUND_R,
                                         COLOR_BACKGROUND_G,
                                         COLOR_BACKGROUND_B);
      //
      FRichTextBox.SelectionColor = Color.FromArgb(COLOR_FOCUS_A,
                                              COLOR_FOCUS_R,
                                              COLOR_FOCUS_G,
                                              COLOR_FOCUS_B);
      //
      FRichTextBox.Text = "";
      FRichTextBox.Dock = DockStyle.Fill;
      //
      mitEnableEditing.Checked = false;
      FRichTextBox.Clear();
      FRichTextBox.ReadOnly = true;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }
    //
    //-------------------------------
    //  Segment - Event
    //-------------------------------
    //

    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
    private void ClearAll()
    {
      FRichTextBox.Clear();
    }

    private delegate void CBAddColoredText(Color color, String text);
    private void AddColoredText(Color color, String text)
    {
      if (this.InvokeRequired)
      {
        CBAddColoredText CB = new CBAddColoredText(AddColoredText);
        Invoke(CB, new object[] { color, text });
      }
      else
      {
        // only black text!FRichTextBox.AppendText(text);
        try
        {
          lock (FLock)
          {
            FRichTextBox.SelectionColor = color;
            FRichTextBox.SelectedText = text;
            FRichTextBox.SelectionStart = FRichTextBox.Text.Length;
            FRichTextBox.ScrollToCaret();
          }
        }
        catch (Exception)
        {
        }
        // NC FStopIndices.Add(rtbView.TextLength - 1);
      }
    }

    //
    //-------------------------------
    //  Segment - Helper
    //-------------------------------
    //
 
    //
    //-------------------------------
    //  Segment - Menu
    //-------------------------------
    //
    private void mitClearAllLines_Click(object sender, EventArgs e)
    {
      ClearAll();
    }

    private void mitSaveProtocol_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogSaveProtocol.ShowDialog())
      {
        FRichTextBox.SaveFile(DialogSaveProtocol.FileName);
      }
    }

    private void mitLoadProtocol_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadProtocol.ShowDialog())
      {
        FRichTextBox.LoadFile(DialogLoadProtocol.FileName);
      }
    }

    private void mitAddSpaceline_Click(object sender, EventArgs e)
    {
      Color C = Color.FromArgb(COLOR_BACKGROUND_A, COLOR_BACKGROUND_R,
                               COLOR_BACKGROUND_G, COLOR_BACKGROUND_B);
      AddColoredText(C, "\r\n");
      FRichTextBox.SelectionStart = FRichTextBox.Text.Length;
      FRichTextBox.ScrollToCaret();
    }

    private void mitEnableEditing_Click(object sender, EventArgs e)
    {
      FRichTextBox.ReadOnly = !mitEnableEditing.Checked;
    }

    //
    //-------------------------------
    //  Segment - Management
    //-------------------------------
    //
    public void Clear()
    {
      ClearAll();
    }

    public void Add(Color color, String text)
    {
      AddColoredText(color, text);
    }

    public void EnableText(Boolean enable)
    {
      FRichTextBox.Enabled = enable;
    } 

  }
}
