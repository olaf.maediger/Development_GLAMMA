﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
//
using XmlFile;
//
namespace Image24b
{
  public class CGammaHistogram
  {
    public const int SIZE_HISTOGRAM = 256;
    private const String INIT_SECTION = "GammaFactor";
    private const Byte INIT_VALUE = 0x00;
    //
    private Byte[] FData;
    private CXmlFile FXmlFile;
    public CGammaHistogram()
    {
      FData = new Byte[SIZE_HISTOGRAM];
      FXmlFile = new CXmlFile();
      SetLinear();
    }
    public Byte Get(Byte index)
    {
      return FData[index];
    }
    public Boolean SetLinear()
    {
      for (int CI = 0; CI < SIZE_HISTOGRAM; CI++)
      {
        FData[CI] = (byte)CI;
      }
      return true;
    }
    public Boolean SetInverse()
    {
      for (int CI = 0; CI < SIZE_HISTOGRAM; CI++)
      {
        FData[CI] = (byte)(255 - CI);
      }
      return true;
    }
    public Boolean LoadFromFile(String filename)
    {
      try
      {
        if (FXmlFile.ReadOpen(filename, INIT_SECTION))
        {
          Byte Value = 0x00;
          for (int CI = 0; CI < SIZE_HISTOGRAM; CI++)
          {
            String NodeName = String.Format("I{0:000}", CI);
            if (FXmlFile.ReadByte(NodeName, out Value, INIT_VALUE))
            { // debug Console.WriteLine(Value);
              FData[CI] = Value;
            }
          }
          return true;
        }
        FXmlFile.ReadClose();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean SaveToFile(String filename)
    {
      try
      {
        Boolean Result;
        Result = FXmlFile.WriteOpen(filename, INIT_SECTION);
        if (Result)
        {
          for (int CI = 0; CI < SIZE_HISTOGRAM; CI++)
          {
            String NodeName = String.Format("I{0:000}", CI);
            Result &= FXmlFile.WriteByte(NodeName, (byte)FData[CI]);
          }
          Result &= FXmlFile.WriteClose();
        }
        return Result;
      }
      catch (Exception)
      {
        return false;
      }
    }

  }


}
