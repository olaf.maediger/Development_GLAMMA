﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
// no Hardware here! only Event from Controls! 
using Uart;
//
namespace UCUartOpenClose
{ //
  public partial class CUCUartOpenClose : UserControl
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    public const String INIT_PORTNAME = "";
    public const EComPort INIT_COMPORT = EComPort.cp1;
    public const EBaudrate INIT_BAUDRATE = EBaudrate.br9600;
    public const EParity INIT_PARITY = EParity.paNone;
    public const EDatabits INIT_DATABITS = EDatabits.db8;
    public const EStopbits INIT_STOPBITS = EStopbits.sb1;
    public const EHandshake INIT_HANDSHAKE = EHandshake.hsNone;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnUCOpenCloseChanged FOnOpenCloseChanged;
    //
    //------------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------------
    //
    public CUCUartOpenClose()
    {
      InitializeComponent();
      //
      FUCOpenClose.SetOnUCOpenCloseChanged(UCOpenCloseOnUCOpenCloseChanged);
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
      FUCOpenClose.SetNotifier(FNotifier);
      FUCBaudrate.SetNotifier(FNotifier);
      FUCDatabits.SetNotifier(FNotifier);
      FUCHandshake.SetNotifier(FNotifier);
      FUCParity.SetNotifier(FNotifier);
      FUCStopbits.SetNotifier(FNotifier);
      FUCPortName.SetNotifier(FNotifier);
      FUCPortsInstalled.SetNotifier(FNotifier);
      FUCPortsSelectable.SetNotifier(FNotifier);
    }

    public void SetOpened(Boolean open)
    {
      FUCOpenClose.SetStateOpened(open);
      pnlControls.Enabled = !open;
    }

    public String GetName()
    {
      return FUCPortName.GetPortName();
    }
    public EComPort GetComPort()
    {
      return FUCPortsSelectable.GetPortSelected();
    }
    public void SetComPort(EComPort comport)
    {
      FUCPortsSelectable.SetPortSelected(comport);
    }

    public EBaudrate GetBaudrate()
    {
      return FUCBaudrate.GetBaudrate();
    }
    public EParity GetParity()
    {
      return FUCParity.GetParity();
    }
    public EDatabits GetDatabits()
    {
      return FUCDatabits.GetDatabits();
    }
    public EStopbits GetStopbits()
    {
      return FUCStopbits.GetStopbits();
    }
    public EHandshake GetHandshake()
    {
      return FUCHandshake.GetHandshake();
    }

    public void SetOnUCOpenCloseChanged(DOnUCOpenCloseChanged value)
    {
      FOnOpenCloseChanged = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //
    private void EnableComPortControls(Boolean enable)
    {
      FUCPortName.Enabled = enable;
      FUCPortsSelectable.Enabled = enable;
      FUCBaudrate.Enabled = enable;
      FUCParity.Enabled = enable;
      FUCDatabits.Enabled = enable;
      FUCStopbits.Enabled = enable;
      FUCHandshake.Enabled = enable;
      btnRefresh.Enabled = enable;
    }
    //
    //--------------------------------------------------------------------
    //  Segment - UartBase - Callback
    //--------------------------------------------------------------------
    //
    private void UCOpenCloseOnUCOpenCloseChanged(Boolean opened)
    { // Callback from Child UCOpenClose:
      if (FOnOpenCloseChanged is DOnUCOpenCloseChanged)
      {
        FOnOpenCloseChanged(opened);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event - Control
    //------------------------------------------------------------------------
    //
    private void btnRefresh_Click(object sender, EventArgs e)
    {
      RefreshComPortControls();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Public Management
    //------------------------------------------------------------------------
    //    
    public void RefreshComPortControls()
    {
      FUCPortsInstalled.RefreshPortsInstalled();
      FUCPortsSelectable.RefreshPortsSelectable();
    }

    public void RefreshOpenCloseComPort(Boolean opened)
    {
      EnableComPortControls(opened);
    }

  

  }
}
