﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
using LaserScanner;
//
namespace UCLaserAreaScanner
{
  public partial class CUCLaserAreaScannerTable : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    // 
    private DOnLoadLaserSteptable FOnLoadLaserSteptable;
    private DOnPulseLaserImage FOnPulseLaserImage;
    private DOnAbortLaserImage FOnAbortLaserImage;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCLaserAreaScannerTable()
    {
      InitializeComponent();
    }


    // LaserImage
    public void SetOnLoadLaserSteptable(DOnLoadLaserSteptable value)
    {
      FOnLoadLaserSteptable = value;
    }
    public void SetOnPulseLaserImage(DOnPulseLaserImage value)
    {
      FOnPulseLaserImage = value;
    }
    public void SetOnAbortLaserImage(DOnAbortLaserImage value)
    {
      FOnAbortLaserImage = value;
    }

    public void SetLaserStepList(CLaserStepList lasersteplist)
    {
      if (lasersteplist is CLaserStepList)
      {
        if (0 < lasersteplist.Count)
        {
          FUCLaserStepTable.SetLaserStepList(lasersteplist);
        }
      }
    }

    public void SelectLaserStep(UInt32 index)
    {
      FUCLaserStepTable.SelectLaserStep(index);
    }

    public Boolean SkipZeroPulses
    {
      get { return cbxSkipZeroPulses.Checked; }
      set { cbxSkipZeroPulses.Checked = value; }
    }



    private void btnLoadLaserSteptable_Click(object sender, EventArgs e)
    {
      if (FOnLoadLaserSteptable is DOnLoadLaserSteptable)
      {
        if (DialogResult.OK == DialogLoadLaserSteptable.ShowDialog())
        {
          FOnLoadLaserSteptable(DialogLoadLaserSteptable.FileName);
        }
      }
    }

    private void btnPulseLaserImage_Click(object sender, EventArgs e)
    {
      if (FOnPulseLaserImage is DOnPulseLaserImage)
      {
        FOnPulseLaserImage();
      }
    }

    private void btnAbortLaserImage_Click(object sender, EventArgs e)
    {
      if (FOnAbortLaserImage is DOnAbortLaserImage)
      {
        FOnAbortLaserImage();
      }
    }

  }
}
