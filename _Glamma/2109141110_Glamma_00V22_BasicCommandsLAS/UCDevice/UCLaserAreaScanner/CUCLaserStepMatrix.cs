﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using BitmapImage;
//
namespace UCLaserAreaScanner
{
  public partial class CUCLaserStepMatrix : UserControl
  {
    // Mirror!!! private UInt16[,] FLaserPoints;
    private CBitmap FBitmapSource = new CBitmap(1, 1);
    private CBitmap FBitmapTarget;
    private UInt16 FScaleFactor = 10;

    public CUCLaserStepMatrix()
    {
      InitializeComponent();
    }

    public void SetScaleFactor(UInt16 scalefactor)
    {
      FScaleFactor = scalefactor;
      if (FBitmapSource is CBitmap)
      {
        FBitmapTarget = new CBitmap(FBitmapSource.GetBitmap(), ref FScaleFactor);
        pbxImage.Image = FBitmapTarget.GetBitmap();
        pbxImage.Invalidate();
      }
    }
//    public unsafe Boolean SetLaserPoints(UInt32 rowcount, UInt32 colcount, UInt16[,] laserpoints,
//                                         UInt16 pulsecountminimum, UInt16 pulsecountmaximum)
//    {
//      try
//      {
//        FBitmapSource = new CBitmap((int)colcount, (int)rowcount);
//        Int32 BW = FBitmapSource.GetWidth();
//        Int32 BH = FBitmapSource.GetHeight();
//        PixelFormat PF = FBitmapSource.GetPixelFormat();
//        Rectangle R = new Rectangle(0, 0, BW, BH);
//        BitmapData BD = FBitmapSource.LockBits(R, ImageLockMode.ReadWrite, PF);
//        //
//        Int32 SI = 0;
////        Int32 YI = 0;
//        Int32 RC = BH;
//        Int32 CC = BW;
//        for (Int32 RI = 0; RI < RC; RI++)
//        {
//          Byte* PBD = (Byte*)(BD.Scan0 + RI * BD.Stride);
//          for (Int32 CI = 0; CI < CC; CI++)
//          {
//            UInt16 XL = laserpoints[SI, CUCLaserStepTable.XLOCATION];
//            UInt16 YL = laserpoints[SI, CUCLaserStepTable.YLOCATION];
//            UInt16 PP = laserpoints[SI, CUCLaserStepTable.PULSEPERIODMS];
//            UInt16 PC = laserpoints[SI, CUCLaserStepTable.PULSECOUNT];
//            //
//            Byte PCBlue = (Byte)((UInt32)(PC - pulsecountminimum) *
//                                 (UInt32)(255 - 0) /
//                                 (UInt32)(pulsecountmaximum - pulsecountminimum));
//            Byte PCRed = PCBlue;
//            Byte PCGreen = PCBlue;
//            //
//            *PBD = PCBlue;
//            PBD++;
//            // G
//            *PBD = PCGreen;
//            PBD++;
//            // R
//            *PBD = PCRed;
//            PBD++;
//            // A
//            *PBD = 0xFF;
//            PBD++;
//            //
//            SI++;
//          }
//        }
//        FBitmapSource.UnlockBits(BD);        
//        FBitmapTarget = new CBitmap(FBitmapSource.GetBitmap(), ref FScaleFactor);
//        pbxImage.Image = FBitmapTarget.GetBitmap();
//        pbxImage.Invalidate();
//        return true;
//      }
//      catch (Exception e)
//      {
//        Console.WriteLine(e);
//        return false;
//      }
//    }

    public void SelectLaserStep(UInt32 index)
    {

    }

    public void SetZoom(Boolean zoom)
    {
      if (zoom)
      {
        pbxImage.Dock = DockStyle.Fill;
        pbxImage.SizeMode = PictureBoxSizeMode.Zoom;
      }
      else
      {
        pbxImage.Dock = DockStyle.None;
        pbxImage.SizeMode = PictureBoxSizeMode.AutoSize;
      }
    }

  }
}
