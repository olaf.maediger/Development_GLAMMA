﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserStepTable
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
      this.dgvSteptable = new System.Windows.Forms.DataGridView();
      this.Index = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.DelayMotion = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.XPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
      this.YPosition = new System.Windows.Forms.DataGridViewTextBoxColumn();
      ((System.ComponentModel.ISupportInitialize)(this.dgvSteptable)).BeginInit();
      this.SuspendLayout();
      // 
      // dgvSteptable
      // 
      this.dgvSteptable.AllowUserToAddRows = false;
      this.dgvSteptable.AllowUserToDeleteRows = false;
      this.dgvSteptable.AllowUserToResizeColumns = false;
      this.dgvSteptable.AllowUserToResizeRows = false;
      this.dgvSteptable.BorderStyle = System.Windows.Forms.BorderStyle.None;
      this.dgvSteptable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
      this.dgvSteptable.Dock = System.Windows.Forms.DockStyle.Fill;
      this.dgvSteptable.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
      this.dgvSteptable.EnableHeadersVisualStyles = false;
      this.dgvSteptable.Location = new System.Drawing.Point(0, 0);
      this.dgvSteptable.MultiSelect = false;
      this.dgvSteptable.Name = "dgvSteptable";
      this.dgvSteptable.ReadOnly = true;
      dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
      dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
      dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
      dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Red;
      dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
      dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
      this.dgvSteptable.RowHeadersDefaultCellStyle = dataGridViewCellStyle1;
      this.dgvSteptable.RowHeadersVisible = false;
      this.dgvSteptable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
      this.dgvSteptable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
      this.dgvSteptable.ShowEditingIcon = false;
      this.dgvSteptable.Size = new System.Drawing.Size(580, 231);
      this.dgvSteptable.TabIndex = 0;
      // 
      // Index
      // 
      this.Index.HeaderText = "Index";
      this.Index.Name = "Index";
      this.Index.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // DelayMotion
      // 
      this.DelayMotion.HeaderText = "Column1";
      this.DelayMotion.Name = "DelayMotion";
      // 
      // XPosition
      // 
      this.XPosition.HeaderText = "XPosition";
      this.XPosition.Name = "XPosition";
      this.XPosition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // YPosition
      // 
      this.YPosition.HeaderText = "YPosition";
      this.YPosition.Name = "YPosition";
      this.YPosition.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
      // 
      // CUCLaserStepTable
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.dgvSteptable);
      this.Name = "CUCLaserStepTable";
      this.Size = new System.Drawing.Size(580, 231);
      ((System.ComponentModel.ISupportInitialize)(this.dgvSteptable)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.DataGridView dgvSteptable;
    private System.Windows.Forms.DataGridViewTextBoxColumn Index;
    private System.Windows.Forms.DataGridViewTextBoxColumn DelayMotion;
    private System.Windows.Forms.DataGridViewTextBoxColumn XPosition;
    private System.Windows.Forms.DataGridViewTextBoxColumn YPosition;
  }
}
