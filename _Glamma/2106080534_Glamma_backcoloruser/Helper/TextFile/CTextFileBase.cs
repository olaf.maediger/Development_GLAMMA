﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
//
namespace TextFile
{ //
	//------------------------------------------------------
	//  Section - Error
	//------------------------------------------------------
	//	
	public enum EErrorCode
	{
		None = 0,
		InvalidAccess = 1,
		Invalid
	};
	//
	//--------------------------------------------------
	//	Types
	//--------------------------------------------------
	//
	//
	//******************************************************
	//  MainSection - CUCMeasurementCollector
	//******************************************************
	//	
	public class CTextFileBase
	{ //
		//-----------------------------------------------
		//  Section - Constant Error
		//-----------------------------------------------
		//
		public static readonly String[] ERRORS = 
    { 
      "None",
			"Invalid Access",
  	  "Invalid"
    };
		//	
		//--------------------------------------------------
		//	Section - Constant
		//--------------------------------------------------
		//
		public static String HEADER_LIBRARY = "TextFile";
		public static String SECTION_LIBRARY = HEADER_LIBRARY;
		//
		protected const String INIT_DRIVESEPARATOR = ":";
		protected const String INIT_HEADERSEPARATOR = ".";
		protected const String INIT_DRIVE = "C";
		protected const String INIT_PATH = INIT_HEADERSEPARATOR;
		protected const String INIT_HEADER = "filename";
		protected const String INIT_EXTENSION = "txt";
		protected const String INIT_ENTRY = INIT_HEADER + INIT_HEADERSEPARATOR + INIT_EXTENSION;
		protected const String INIT_FORMATSTRING = "{0}";
		protected const String INIT_FORMATCHARACTER = "{0}";
    protected const String INIT_FORMATBYTE = "{0}";//"{0:X}";
		protected const String INIT_FORMATINT16 = "{0}";
		protected const String INIT_FORMATUINT16 = "{0}";
		protected const String INIT_FORMATINT32 = "{0}";
		protected const String INIT_FORMATUINT32 = "{0}";
		protected const String INIT_FORMATDECIMAL = "{0}";
		protected const String INIT_FORMATSINGLE = "{0}";
		protected const String INIT_FORMATDOUBLE = "{0:0.000000000}";
    // 130412 
    protected readonly String[] INIT_TOKENDELIMITERS = new String[] { "\t", " " };
    protected readonly String[] INIT_LINEDELIMITERS = new String[] { "\r\n", "\r", "\n" }; 
    //
		protected const String INIT_STRING = "";
		protected const Char INIT_CHAR = (Char)0x00;
		protected const Byte INIT_BYTE = 0x00;
		protected const Int16 INIT_INT16 = 0;
		protected const UInt16 INIT_UINT16 = 0;
		protected const Int32 INIT_INT32 = 0;
		protected const UInt32 INIT_UINT32 = 0;
		protected const Single INIT_SINGLE = 0;
		protected const Double INIT_DOUBLE = 0;
    protected const Decimal INIT_DECIMAL = 0;
    protected DateTime INIT_DATETIME = DateTime.MinValue;
    protected TimeSpan INIT_TIMESPAN = TimeSpan.MinValue;
    //
		//-----------------------------------------------
		//  Section - Variable
		//-----------------------------------------------
		//
		// ??? protected CNotifier FNotifier;
		private RTextFileData FData;
		private StreamWriter FStreamWriter;
		private StreamReader FStreamReader;
		private String[][] FTokenArray;	// Row, Col
		private Int32 FRowIndex;
		private Int32 FColIndex;
		//
		//--------------------------------------------------
		//	Section - Constructor
		//--------------------------------------------------
		//
		public CTextFileBase()
		{ // DecimalSeparator
      //! !!!!!!!!!!CInitdata.Init();
			FData = new RTextFileData();
			InitData();
		}

		protected void InitData()
		{
			FData.Entry = INIT_ENTRY;
			FData.Drive = INIT_DRIVE;
			FData.Path = INIT_PATH;
			FData.Name = INIT_HEADER;
			FData.Extension = INIT_EXTENSION;
			FData.FormatString = INIT_FORMATSTRING;
			FData.FormatCharacter = INIT_FORMATCHARACTER;
			FData.FormatByte = INIT_FORMATBYTE;
			FData.FormatInt16 = INIT_FORMATINT16;
			FData.FormatUInt16 = INIT_FORMATUINT16;
			FData.FormatInt32 = INIT_FORMATINT32;
			FData.FormatUInt32 = INIT_FORMATUINT32;
			FData.FormatDecimal = INIT_FORMATDECIMAL;
			FData.FormatSingle = INIT_FORMATSINGLE;
			FData.FormatDouble = INIT_FORMATDOUBLE;
			FData.TokenDelimiters = INIT_TOKENDELIMITERS;
			FData.LineDelimiters = INIT_LINEDELIMITERS;
		}
		//
		//--------------------------------------------------
		//	Section - Property
		//--------------------------------------------------
		//
		public Int32 RowCount
		{
			get { return FTokenArray.Length; }
		}

		public Int32 ColCount(Int32 rowindex)
		{
			return FTokenArray[rowindex].Length; 
		}

		public Int32 RowIndex
		{
			get { return FRowIndex; }
		}

		public Int32 ColIndex
		{
			get { return FColIndex; }
		}

    public Boolean IsEndOfFile()
    {
      return !IsTokenAvailable();
    }
		//
		//------------------------------------------------------
		//  Section - Protocol
		//------------------------------------------------------
		//
    protected String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }
		//
		//--------------------------------------------------
		//	Section - Get/SetData
		//--------------------------------------------------
		//
		protected Boolean GetData(ref RTextFileData data)
		{
			data = FData;
			return true;
		}

		protected Boolean SetData(RTextFileData data)
		{
			FData = data;
			return true;
		}
    //
    //--------------------------------------------------
    //	Section - Text-Management
    //--------------------------------------------------
    //
    protected Boolean TokenizeText(String text)
    {
      String ReadBuffer = text;
      String[] RowLines = ReadBuffer.Split(FData.LineDelimiters, StringSplitOptions.RemoveEmptyEntries);
      FTokenArray = new String[RowLines.Length][];
      for (Int32 RI = 0; RI < RowLines.Length; RI++)
      {
        String RowLine = RowLines[RI];
        String[] Tokens = RowLine.Split(FData.TokenDelimiters, StringSplitOptions.RemoveEmptyEntries);
        FTokenArray[RI] = new String[Tokens.Length];
        for (Int32 CI = 0; CI < Tokens.Length; CI++)
        {
          FTokenArray[RI][CI] = Tokens[CI];
        }
      }
      FRowIndex = 0;
      FColIndex = 0;
      return (0 < RowCount);
    }
    //
    //--------------------------------------------------
    //	Section - File-Management
    //--------------------------------------------------
    //
    protected Boolean OpenRead(String fileentry)
		{
			if (!(FStreamReader is StreamReader))
			{
        if (File.Exists(fileentry))
        {
          FStreamReader = new StreamReader(fileentry, Encoding.ASCII, false);
          String ReadBuffer = FStreamReader.ReadToEnd();
          String[] RowLines = ReadBuffer.Split(FData.LineDelimiters, StringSplitOptions.RemoveEmptyEntries);
          FTokenArray = new String[RowLines.Length][];
          for (Int32 RI = 0; RI < RowLines.Length; RI++)
          {
            String RowLine = RowLines[RI];
            String[] Tokens = RowLine.Split(FData.TokenDelimiters, StringSplitOptions.RemoveEmptyEntries);
            FTokenArray[RI] = new String[Tokens.Length];
            for (Int32 CI = 0; CI < Tokens.Length; CI++)
            {
              FTokenArray[RI][CI] = Tokens[CI];
            }
          }
          FRowIndex = 0;
          FColIndex = 0;
          return (0 < RowCount);
        }
			}
			return false;
		}

    public Boolean OpenWrite(String fileentry)
		{
			if (!(FStreamWriter is StreamWriter))
			{
				FStreamWriter = new StreamWriter(fileentry, false, Encoding.ASCII);
				return true;
			}
			return false;
		}

		public Boolean Close()
		{
			if (FStreamWriter is StreamWriter)
			{
				FStreamWriter.Close();
				FStreamWriter = null;
			}
			if (FStreamReader is StreamReader)
			{
				FStreamReader.Close();
				FStreamReader = null;
			}
			return true;
		}

		private Int32 RIH()
		{
			return FTokenArray.Length - 1;
		}

		private Int32 CIH(Int32 rowindex)
		{
			return FTokenArray[rowindex].Length - 1;
		}


		protected Boolean IsTokenAvailable()
		{	// Endposition reached?
			if ((FRowIndex < 0) || (FColIndex < 0))
			{ // no more Token available!
				return false;
			}
			//
      Int32 RowIndexHigh = RIH();
      Int32 ColIndexHigh = CIH(FRowIndex);
			return ((FRowIndex <= RowIndexHigh) && (FColIndex <= ColIndexHigh));
		}

    private void IncrementTokenIndices()
    {
      if (FColIndex < CIH(FRowIndex))
      {
        FColIndex++;
        return;
      }
      if (FRowIndex < RIH())
      {
        FColIndex = 0;
        FRowIndex++;
        return;
      }
      FColIndex = -1;
      FRowIndex = -1;
    }


		protected Boolean ReadRowIndex(out Int32 rowindex)
		{
			if (IsTokenAvailable())
			{
				rowindex = FRowIndex;
				return true;
			}
			rowindex = -1;
			return false;
		}

		protected Boolean ReadColIndex(out Int32 colindex)
		{
			if (IsTokenAvailable())
			{
				colindex = FColIndex;
				return true;
			}
			colindex = -1;
			return false;
		}

		private Boolean ReadToken(out String token)
		{	// Read actual Token
			if (IsTokenAvailable())
			{
				token = FTokenArray[FRowIndex][FColIndex];
				IncrementTokenIndices();
				return true;
			}
			token = "";
			return false;
		}
    
    private Boolean ReadTokensFromLine(out String[] tokens)
    {	// read all tokens from line rowindex
      tokens = null;
      if (IsTokenAvailable())
      {
        Int32 TokenCount = ColCount(RowIndex) - ColIndex;
        tokens = new String[TokenCount];
        Int32 TokenIndex = 0;
        Int32 LineIndex = RowIndex;
        while (LineIndex == RowIndex)
        {
				  String Token = FTokenArray[RowIndex][ColIndex];
          tokens[TokenIndex] = Token;
          TokenIndex++;
          IncrementTokenIndices();
        }
        return true;
      }    
      return false;
    }
    //
		//--------------------------------------------------
		//	Section - Write-Methods
		//--------------------------------------------------
		//
    protected Boolean WriteTokenDelimiter()
    {
      if (FStreamWriter is StreamWriter)
      {
        FStreamWriter.Write(FData.TokenDelimiters[0]);
        return true;
      }
      return false;
    }

    protected Boolean WriteLineDelimiter()
    {
      if (FStreamWriter is StreamWriter)
      {
        FStreamWriter.Write(FData.LineDelimiters[0]);
        return true;
      }
      return false;
    }

    protected Boolean Write(String value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatString, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

		protected Boolean Write(Char value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatCharacter, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Byte value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatByte, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Int16 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatInt16, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(UInt16 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatUInt16, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Int32 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatInt32, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(UInt32 value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatUInt32, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Single value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatSingle, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

		protected Boolean Write(Double value)
		{
			if (FStreamWriter is StreamWriter)
			{
				String Buffer = String.Format(FData.FormatDouble, value);
				FStreamWriter.Write(Buffer);
				return true;
			}
			return false;
		}

    protected Boolean Write(Decimal value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = String.Format(FData.FormatDecimal, value);
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(DateTime value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = value.ToString();
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }

    protected Boolean Write(TimeSpan value)
    {
      if (FStreamWriter is StreamWriter)
      {
        String Buffer = value.ToString();
        FStreamWriter.Write(Buffer);
        return true;
      }
      return false;
    }
    //
		//--------------------------------------------------
		//	Section - Read-Methods
		//--------------------------------------------------
		//
		/*protected Boolean Unread(Int32 row)
		{
			if (0 < FTokenIndex)
			{
				FTokenIndex--;
				return true;
			}
			return false;
		}*/

		protected Boolean Read(out String value)
		{
			value = INIT_STRING;
			return ReadToken(out value);
		}

		protected Boolean Read(out Char value)
		{
			value = INIT_CHAR;
			String Token;
			if (ReadToken(out Token))
			{
				return Char.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out Byte value)
		{
			value = INIT_BYTE;
			String Token;
			if (ReadToken(out Token))
			{
				return Byte.TryParse(Token, out value);
			}
			return false;
		}

    protected Boolean Read(out Byte value, out String text)
    {
      value = INIT_BYTE;
      text = INIT_STRING;
      String Token;
      if (ReadToken(out Token))
      {
        // error return Byte.TryParse(Token, out value);
        value = (Byte)Convert.ToUInt32(Token, 16);
        return true;
      }
      text = Token;
      return false;
    }

		public Boolean Read(out Int16 value)
		{
			value = INIT_INT16;
			String Token;
			if (ReadToken(out Token))
			{
				return Int16.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out UInt16 value)
		{
			value = INIT_UINT16;
			String Token;
			if (ReadToken(out Token))
			{
				return UInt16.TryParse(Token, out value);
			}
			return false;
		}

    protected Boolean Read(out UInt16 value, out String text)
    {
			value = INIT_UINT16;
      text = INIT_STRING;
      String Token;
			if (ReadToken(out Token))
			{
        if (UInt16.TryParse(Token, out value))
        {
          return true;
        }
			}
      text = Token;
      return false;
		}

		protected Boolean Read(out Int32 value)
		{
			value = INIT_INT32;
			String Token;
			if (ReadToken(out Token))
			{
				return Int32.TryParse(Token, out value);
			}
			return false;
		}

		protected Boolean Read(out UInt32 value)
		{
			value = INIT_UINT32;
			String Token;
			if (ReadToken(out Token))
			{
				return UInt32.TryParse(Token, out value);
			}
			return false;
		}

    protected Boolean Read(out Single value)
    {
      value = INIT_SINGLE;
      String Token;
      if (ReadToken(out Token))
      {
        Token = Token.Replace(',', '.');
        return Single.TryParse(Token, out value);
      }
      return false;
    }

    protected Boolean Read(out Double value)
    {
      value = INIT_DOUBLE;
      String Token;
      if (ReadToken(out Token))
      {
        Token = Token.Replace(',', '.');
        return Double.TryParse(Token, out value);
      }
      return false;
    }

		protected Boolean Read(out Double value, out String text)
		{
			value = INIT_DOUBLE;
      text = INIT_STRING;
      String Token;
			if (ReadToken(out Token))
			{
        Token = Token.Replace(',', '.');
        if (Double.TryParse(Token, out value))
        {
          return true;
        }
			}
      text = Token;
      return false;
		}

    protected Boolean Read(out Decimal value)
    {
      value = INIT_DECIMAL;
      String Token;
      if (ReadToken(out Token))
      {
        Token = Token.Replace(',', '.');
        return Decimal.TryParse(Token, out value);
      }
      return false;
    }

    protected Boolean Read(out DateTime value)
    {
      value = INIT_DATETIME;
      String Token;
      if (ReadToken(out Token))
      {
        return DateTime.TryParse(Token, out value);
      }
      return false;
    }

    protected Boolean Read(out TimeSpan value)
    {
      value = INIT_TIMESPAN;
      String Token;
      if (ReadToken(out Token))
      {
        return TimeSpan.TryParse(Token, out value);
      }
      return false;
    }

    protected Boolean Read(out Double[] values, out String[] tabbedtext)
    {
      values = null;
      tabbedtext = null;
      String[] Tokens;
      if (ReadTokensFromLine(out Tokens))
      {
        values = new Double[Tokens.Length];
        for (Int32 TI = 0; TI < Tokens.Length; TI++)
        {
          //!!!!!!!!!!!!!!!!!!!!! debug String Token = Tokens[TI].Replace(',', '.');
          String Token = Tokens[TI];
          Double Value;
          Token = Token.Replace(',', '.');
          if (!Double.TryParse(Token, out Value))
          {
            values = null;
            tabbedtext = Tokens;
            return true;
          }
          values[TI] = Value;
        }
        return true;
      }
      return false;
    }


    private Boolean AnalyseDoubleValues(String[] tokens, 
                                        out Double[] doublevalues)
    {
      doublevalues = new Double[tokens.Length];
      for (Int32 TI = 0; TI < tokens.Length; TI++)
      {
        String Token = tokens[TI];
        Token = Token.Replace(',', '.');
        Double DValue;
        if (!Double.TryParse(Token, out DValue))
        {
          doublevalues = null;
          return false;
        }
        doublevalues[TI] = DValue;
      }
      return true;
  }

    
    private Boolean AnalyseTextValues(String[] tokens, 
                                      out String[] textvalues)
    {
      textvalues = null;
      if (0 < tokens.Length)
      {
        textvalues = new String[tokens.Length];
        for (Int32 TI = 0; TI < tokens.Length; TI++)
        {
          String Token = tokens[TI];
          textvalues[TI] = Token;
        }
        return true;
      }
      return false;
    }            
  

    protected Boolean Read(out Double[][] values)
    {
      values = null;
      String[] Tokens;
      while (ReadTokensFromLine(out Tokens))
      {
        Double[] Array = new Double[Tokens.Length];
        for (Int32 TI = 0; TI < Tokens.Length; TI++)
        {
          String Token = Tokens[TI].Replace(',', '.');
          Double Value;
          if (!Double.TryParse(Token, out Value))
          {
            values = null;
            return true;
          }
          values[TI] = Array;
        }
        return true;
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Write-Methods
    //--------------------------------------------------
    //
    public Boolean Write(Byte[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("Byte");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatByte, matrix[RI, CI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        return true;
      }
      return false;
    }

    public Boolean Write(UInt16[,] matrix)
    {
      if (FStreamWriter is StreamWriter)
      {
        Int32 RC = matrix.GetLength(0);
        Int32 CC = matrix.GetLength(1);
        String Buffer = String.Format(FData.FormatUInt32, RC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        Buffer = String.Format(FData.FormatUInt32, CC);
        FStreamWriter.Write(Buffer);
        WriteTokenDelimiter();
        FStreamWriter.Write("UInt16");
        WriteLineDelimiter();
        for (Int32 RI = 0; RI < RC; RI++)
        {
          for (Int32 CI = 0; CI < CC; CI++)
          {
            Buffer = String.Format(FData.FormatUInt16, matrix[RI, CI]);
            FStreamWriter.Write(Buffer);
            if (CI < CC - 1)
            {
              WriteTokenDelimiter();
            }
          }
          WriteLineDelimiter();
        }
        return true;
      }
      return false;
    }
    //
    //--------------------------------------------------
    //	Segment - HighLevel - Read-Methods
    //--------------------------------------------------
    //
    protected Boolean Read(out Byte[,] matrix)
    {
      matrix = null;
      if (FStreamReader is StreamReader)
      {
        UInt32 RC, CC;
        String ElementType;
        Boolean Result = Read(out RC);
        Result &= Read(out CC);
        Result &= Read(out ElementType);
        if ("Byte" == ElementType)
        {
          matrix = new Byte[CC, RC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              Byte UValue;
              Result &= Read(out UValue);
              matrix[RI, CI] = UValue;
            }
          }
          return Result;
        }
      }
      return false;
    }

    protected Boolean Read(out UInt16[,] matrix)
    {
      matrix = null;
      if (FStreamReader is StreamReader)
      {
        UInt32 RC, CC;
        String ElementType;
        Boolean Result = Read(out RC);
        Result &= Read(out CC);
        Result &= Read(out ElementType);
        if ("UInt16" == ElementType)
        {
          matrix = new UInt16[RC, CC];
          for (Int32 RI = 0; RI < RC; RI++)
          {
            for (Int32 CI = 0; CI < CC; CI++)
            {
              UInt16 UValue;
              Result &= Read(out UValue);
              matrix[RI, CI] = UValue;
            }
          }
          return Result;
        }
      }
      return false;
    }

	}
}
