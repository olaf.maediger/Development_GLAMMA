﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Programdata;
using UCNotifier;
//
namespace UCTextEditor
{
  public partial class CUCEditTextEditor
  {
    //
    //------------------------------------------------------
    //  Segment - Field
    //------------------------------------------------------
    //
    private RTextEditorData FData;
    private CNotifier FNotifier;      // only Reference!
    //
    //-------------------------------
    //	Segment - Constructor
    //-------------------------------
    //
    public CUCEditTextEditor()
    {
      InitializeComponent();
      //
      Initialize();
    }
    //
    //-------------------------------
    //  Segment - Property - Common
    //-------------------------------
    //
    public Boolean SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      // FChild.SetNotifier(notifier);
      return true;
    }
    //
    //  BorderStyle
    public void SetBorderStyle(BorderStyle value)
    {
      FRTBEditor.BorderStyle = value;
    }
    //
    //	Property - WorkingDirectory
    private String GetWorkingDirectory()
    {
      return FData.WorkingDirectory;
    }
    private void SetWorkingDirectory(String value)
    {
      if (value != WorkingDirectory)
      {
        FData.WorkingDirectory = value;
        stsWorkingDirectory.Text = HEADER_WORKINGDIRECTORY + FData.WorkingDirectory;
      }
    }
    public String WorkingDirectory
    {
      get { return GetWorkingDirectory(); }
      set { SetWorkingDirectory(value); }
    }
    //
    //	Property - FileName
    private String GetFileName()
    {
      return FData.FileName;
    }
    private void SetFileName(String value)
    {
      if (value != FileName)
      {
        FData.FileName = Path.GetFileName(value);
        stsFileName.Text = HEADER_FILENAME + FData.FileName;
        if (1 < value.Length)
        {
          if (!cbxLoadFromFile.Items.Contains(value))
          {
            cbxLoadFromFile.Items.Add(value);
            cbxLoadFromFile.Text = value;
          }
          if (!cbxSaveToFile.Items.Contains(value))
          {
            cbxSaveToFile.Items.Add(value);
            cbxSaveToFile.Text = value;
          }
        }
      }
    }
    public String FileName
    {
      get { return GetFileName(); }
      set { SetFileName(value); }
    }
    //
    //  Text
    public String GetText()
    {
      return FRTBEditor.Text;
    }
    public void SetText(String text)
    {
      FRTBEditor.Text = text;
    }
    //
    //-------------------------------
    //  Segment - Property - Load
    //-------------------------------
    //
    //  ShowPanelLoad
    public void ShowPanelLoad(Boolean value)
    {
      FData.ShowPanelLoad = value;
      pnlLoad.Visible = FData.ShowPanelLoad;
    }
    //
    //  IsPanelLoadVisible
    public Boolean IsPanelLoadVisible
    {
      get { return FData.ShowPanelLoad; }
    }
    //
    public String LoadFileName
    {
      get { return DialogLoadText.FileName; }
      set { DialogLoadText.FileName = value; }
    }
    public String LoadFileExtension
    {
      get { return DialogLoadText.DefaultExt; }
      set { DialogLoadText.DefaultExt = value; }
    }
    public String LoadFileFilter
    {
      get { return DialogLoadText.Filter; }
      set { DialogLoadText.Filter = value; }
    }
    //
    //-------------------------------
    //  Segment - Property - Save
    //-------------------------------
    //
    //  ShowPanelSave
    public void ShowPanelSave(Boolean value)
    {
      FData.ShowPanelSave = value;
      pnlSave.Visible = FData.ShowPanelSave;
    }
    //
    //  IsPanelSaveVisible
    public Boolean IsPanelSaveVisible
    {
      get { return FData.ShowPanelSave; }
    }
    //
    public String SaveFileName
    {
      get { return DialogSaveText.FileName; }
      set { DialogSaveText.FileName = value; }
    }
    public String SaveFileExtension
    {
      get { return DialogSaveText.DefaultExt; }
      set { DialogSaveText.DefaultExt = value; }
    }
    public String SaveFileFilter
    {
      get { return DialogSaveText.Filter; }
      set { DialogSaveText.Filter = value; }
    }
    //
    //-------------------------------
    //  Segment - Property - Parameter
    //-------------------------------
    //
    //  ShowPanelParameter
    public void ShowPanelParameter(Boolean value)
    {
      FData.ShowPanelParameter = value;
      pnlParameter.Visible = FData.ShowPanelParameter;
    }
    //
    //  IsPanelParameterVisible
    public Boolean IsPanelParameterVisible
    {
      get { return FData.ShowPanelParameter; }
    }
    //
    //  ShowLegend
    public Boolean IsLegendVisible
    {
      get { return FData.ShowLegend; }
    }
    public void ShowLegend(Boolean value)
    {
      FData.ShowLegend = value;
      stsLegend.Visible = FData.ShowLegend;
      if (FData.ShowLegend != cbxShowLegend.Checked)
      {
        cbxShowLegend.Checked = FData.ShowLegend;
      }
    }
    //??????????????????INITDATA
    //  Property - IsReadOnly????????????????????????????????????????????????????????????????????????????????????
    private void SetIsReadOnly(Boolean value)
    {
      if (value != IsReadOnly)
      {
        FData.IsReadOnly = value;
        FRTBEditor.ReadOnly = value;
        if (value)
        {
          FRTBEditor.BackColor = Color.FromArgb(255, 230, 230, 230);
        }
        else
        {
          FRTBEditor.BackColor = Color.White;
        }
        cbxIsReadOnly.Checked = value;
      }
    }
    public Boolean IsReadOnly
    {
      get { return FRTBEditor.ReadOnly; }
      set { SetIsReadOnly(value); }
    }
    //
    //  Property - IsChanged
    private Boolean GetIsChanged()
    {
      return FData.IsChanged;
    }
    private void SetIsChanged(Boolean value)
    {
      if (value != IsChanged)
      {
        FData.IsChanged = value;
        mitSave.Enabled = IsChanged;
      }
    }
    public Boolean IsChanged
    {
      get { return GetIsChanged(); }
    }
    //
    //  Property - TabulatorWidth
    private Int32 GetTabulatorWidth()
    {
      return FData.TabulatorWidth;
    }
    private void SetTabulatorWidth(Int32 value)
    {
      if (value != TabulatorWidth)
      {
        FData.TabulatorWidth = value;
        Int32 Value = Math.Max(scbTabulator.Minimum, value);
        Value = Math.Min(scbTabulator.Maximum, Value);
        scbTabulator.Value = Value;
        FRTBEditor.AcceptsTab = true;

        SizeF S = new Size();
        Graphics G = Graphics.FromHwnd(FRTBEditor.Handle);
        S = G.MeasureString(HEADER_TEXTMEASURE, FRTBEditor.Font);
        Int32 CW = (Int32)(S.Width / 12.0f);

        Int32[] Tabulators = new Int32[10];
        for (Int32 TI = 0; TI < 10; TI++)
        {
          Tabulators[TI] = (1 + TI) * Value * CW;
        }
        FRTBEditor.SelectAll();
        FRTBEditor.SelectionTabs = Tabulators;
        FRTBEditor.Select(0, 0);
        lblTabulator.Text = String.Format(FORMAT_TABULATOR, Value);
      }
    }
    private Int32 TabulatorWidth
    {
      get { return GetTabulatorWidth(); }
      set { SetTabulatorWidth(value); }
    }
    //
    //-------------------------------
    //  Segment - Property - Font
    //-------------------------------
    //
    //  ShowPanelFont
    public void ShowPanelFont(Boolean value)
    {
      FData.ShowPanelFont = value;
      pnlFont.Visible = FData.ShowPanelFont;
    }
    //
    //  IsPanelFontVisible
    public Boolean IsPanelFontVisible
    {
      get { return FData.ShowPanelFont; }
    }
    //
    //  Font
    public Font GetFont()
    {
      return FRTBEditor.Font;
    }

    public void SetFont(Font value)
    {
      FRTBEditor.Font = value;
      FData.Font = GetFont();
      if (FData.OnDataChanged is DOnDataChanged)
      {
        FData.OnDataChanged(FData);
      }
    }
    //
    //  FontColor
    public Color GetFontColor()
    {
      Color Result = Color.FromArgb(0xFF,
                                    FRTBEditor.ForeColor.R,
                                    FRTBEditor.ForeColor.G,
                                    FRTBEditor.ForeColor.B);
      return Result;
    }

    public void SetFontColor(Color value)
    {
      FRTBEditor.ForeColor = Color.FromArgb(0xFF, value.R, value.G, value.B);
      FData.FontColor = GetFontColor();
      if (FData.OnDataChanged is DOnDataChanged)
      {
        FData.OnDataChanged(FData);
      }
    }
    //
    //  FontPositionX (0..100%)
    public Byte GetFontPositionX()
    {
      return (Byte)nudFontPositionX.Value;
    }

    public void SetFontPositionX(Byte value)
    {
      nudFontPositionX.Value = value;
      FData.FontPositionX = GetFontPositionX();
      if (FData.OnDataChanged is DOnDataChanged)
      {
        FData.OnDataChanged(FData);
      }
    }
    //
    //  FontPositionY (0..100%)
    public Byte GetFontPositionY()
    {
      return (Byte)nudFontPositionY.Value;
    }

    public void SetFontPositionY(Byte value)
    {
      nudFontPositionY.Value = value;
      FData.FontPositionY = GetFontPositionY();
      if (FData.OnDataChanged is DOnDataChanged)
      {
        FData.OnDataChanged(FData);
      }
    }
    //
    //  FontOpacity (0..100%) - Absolute (0..255)
    public Byte GetFontOpacityAbsolute()
    {
      return (Byte)(255 * nudFontOpacity.Value / 100);
    }
    public Byte GetFontOpacity()
    {
      return (Byte)nudFontOpacity.Value;
    }

    public void SetFontOpacity(Byte value)
    {
      Byte Opacity = (Byte)Math.Min((Byte)100, value);
      nudFontOpacity.Value = Opacity;
      FData.FontOpacity = GetFontOpacity();
      if (FData.OnDataChanged is DOnDataChanged)
      {
        FData.OnDataChanged(FData);
      }
    }
    //
    //----------------------------
    //  Segment - Helper - Common
    //----------------------------
    public Boolean ClearText()
    {
      try
      {
        FRTBEditor.Clear();
        return true;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }

    private Boolean CopyToClipboard()
    {
      try
      {
        FRTBEditor.SelectAll();
        Clipboard.SetData(DataFormats.Text, FRTBEditor.SelectedText);
        FRTBEditor.Select(0, 0);
        return true;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Segment - Helper - Load
    //---------------------------------------------------------------------
    //
    public Boolean LoadFromFileModal()
    {
      try
      {
        String FileEntry = Path.Combine(WorkingDirectory, FileName);
        DialogLoadText.FileName = FileEntry;
        if (DialogResult.OK == DialogLoadText.ShowDialog())
        {
          FileName = Path.GetFileName(DialogLoadText.FileName);
          WorkingDirectory = Path.GetDirectoryName(DialogLoadText.FileName);
          FileEntry = Path.Combine(WorkingDirectory, FileName);
          FRTBEditor.LoadFile(FileEntry, RichTextBoxStreamType.PlainText);
          SetIsChanged(false);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }

    public  Boolean LoadFromFile()
    {
      try
      {
        String FileEntry = Path.Combine(WorkingDirectory, FileName);
        if (File.Exists(FileEntry))
        {
          FRTBEditor.LoadFile(FileEntry, RichTextBoxStreamType.PlainText);
          SetIsChanged(false);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }

    public Boolean LoadFromFile(String fileentry)
    {
      try
      {
        FileName = Path.GetFileName(fileentry);
        WorkingDirectory = Path.GetDirectoryName(fileentry);
        String FileEntry = Path.Combine(WorkingDirectory, FileName);
        FRTBEditor.LoadFile(FileEntry, RichTextBoxStreamType.PlainText);
        SetIsChanged(false);
        return true;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Segment - Helper - Save
    //---------------------------------------------------------------------
    //
    private Boolean SaveToFileModal()
    {
      try
      {
        DialogSaveText.FileName = FileName;
        if (DialogResult.OK == DialogSaveText.ShowDialog())
        {
          FileName = Path.GetFileName(DialogSaveText.FileName);
          WorkingDirectory = Path.GetDirectoryName(DialogSaveText.FileName);
          String FileEntry = Path.Combine(WorkingDirectory, FileName);
          FRTBEditor.SaveFile(FileEntry, RichTextBoxStreamType.PlainText);
          SetIsChanged(false);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }

    private Boolean SaveToFile()
    {
      try
      {
        FileName = Path.GetFileName(DialogSaveText.FileName);
        WorkingDirectory = Path.GetDirectoryName(DialogSaveText.FileName);
        String FileEntry = Path.Combine(WorkingDirectory, FileName);
        FRTBEditor.SaveFile(FileEntry, RichTextBoxStreamType.PlainText);
        SetIsChanged(false);
        return true;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }

    public Boolean SaveToFile(String fileentry)
    {
      try
      {
        String Text = FRTBEditor.Text; // Necessary, but why????
        FileName = Path.GetFileName(fileentry);
        WorkingDirectory = Path.GetDirectoryName(fileentry);
        String FileEntry = Path.Combine(WorkingDirectory, FileName);
        FRTBEditor.Text = Text;
        SaveToFile();
        return true;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }
    //
    //---------------------------------------------------------------------
    //  Segment - Helper - Parameter
    //---------------------------------------------------------------------
    //

    //
    //---------------------------------------------------------------------
    //  Segment - Helper - Font
    //---------------------------------------------------------------------
    //
    private Boolean SelectFontModal()
    {
      try
      {
        DialogSelectFont.Font = FRTBEditor.Font;
        if (DialogResult.OK == DialogSelectFont.ShowDialog())
        {
          SetFont(DialogSelectFont.Font);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }

    private Boolean SelectFontColorModal()
    {
      try
      {
        DialogSelectFontColor.Color = FRTBEditor.ForeColor;
        if (DialogResult.OK == DialogSelectFontColor.ShowDialog())
        {
          SetFontColor(DialogSelectFontColor.Color);
          return true;
        }
        return false;
      }
      catch (Exception)
      {
        // FNotifier.Write(e);
        return false;
      }
    }

  }
}