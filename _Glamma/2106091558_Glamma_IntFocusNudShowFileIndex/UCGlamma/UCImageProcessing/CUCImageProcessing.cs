﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
//
using Image24b;
//
namespace UCImageProcessing
{
  public partial class CUCImageProcessing : UserControl
  {
    private const String INIT_DEFAULTFILEMASK = "Bitmap2048x512_{0:000}.bmp";


    //
    //---------------------------------------------------------------
    //  Field
    //---------------------------------------------------------------
    private CImage24b FImage24b;
    private Color FBackColor;
    private Boolean FNudShowIndexIsEntered;
    //
    //---------------------------------------------------------------
    //  Constructor
    //---------------------------------------------------------------
    public CUCImageProcessing()
    {
      InitializeComponent();
      //
      FImage24b = new CImage24b();
      //
      DialogSelectFileDirectory.SelectedPath = Directory.GetCurrentDirectory() + "\\result\\";
      tbxFileName512Convert.Text = INIT_DEFAULTFILEMASK;
      //
      cbxConvertGreyScale.Checked = true;
      cbxShowFileIndex.Checked = false;
      cbxShowCutFrame.Checked = true;
      //
      FNudShowIndexIsEntered = false;
      FBackColor = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
      //rbtBackColorWhite.Checked = true;
      //rbtBackColorBlack.Checked = true;
      rbtBackColorUser.Checked = true;
    }
    //
    //---------------------------------------------------------------
    //  Property
    //---------------------------------------------------------------
    public float ScaleX
    {
      get { return (float)nudScaleX.Value; }
      set { nudScaleX.Value = (decimal)value; }
    }
    public float ScaleY
    {
      get { return (float)nudScaleY.Value; }
      set { nudScaleY.Value = (decimal)value; }
    }
    public float RotateAngle
    {
      get { return (float)nudRotateAngle.Value; }
      set { nudRotateAngle.Value = (decimal)value; }
    }
    public float TranslateX
    {
      get { return (float)nudTranslateX.Value; }
      set { nudTranslateX.Value = (decimal)value; }
    }
    public float TranslateY
    {
      get { return (float)nudTranslateY.Value; }
      set { nudTranslateY.Value = (decimal)value; }
    }







    //
    //---------------------------------------------------------------
    //  Helper
    //---------------------------------------------------------------
    public Boolean LoadSourceFromFile(String filename)
    {
      Boolean Result = FImage24b.LoadSourceFromFile(filename);
      Result &= FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);
      if (Result)
      {
        TransformProjection();
        // ??? NC !!! pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    public Boolean NormalizeSource()
    {
      Boolean Result = FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);
      if (Result)
      {
        TransformProjection();
        // ??? pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    public Boolean TransformProjection(float scalex, float scaley, float angledeg, float dx, float dy)
    {
      Boolean Result = FImage24b.TransformProjection(scalex, scaley, angledeg, dx, dy, cbxShowCutFrame.Checked);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    public Boolean TransformProjection()
    {
      Boolean Result = FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle,
                                                     TranslateX, TranslateY, cbxShowCutFrame.Checked);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    //
    //---------------------------------------------------------------
    //  Event - LoadFromFile
    //---------------------------------------------------------------
    private void btnLoadFromFile_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        LoadSourceFromFile(DialogLoadImage.FileName);
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - CutFrame
    //---------------------------------------------------------------
    private void cbxShowCutFrame_CheckedChanged(object sender, EventArgs e)
    {
      TransformProjection();
    }
    private void btnCutFrame_Click(object sender, EventArgs e)
    {
      if (FImage24b.CutFrame(ScaleX, ScaleY, RotateAngle,
                             TranslateX, TranslateY, cbxShowCutFrame.Checked))
      {
        ScaleX = 1.0f;
        ScaleY = 1.0f;
        RotateAngle = 0.0f;
        TranslateX = 0.0f;
        TranslateY = 0.0f;
        if (FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle,
                                          TranslateX, TranslateY, cbxShowCutFrame.Checked))
        {
          pbxImage.Image = FImage24b.GetBitmapProjection();
        }
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - RotateAngle
    //---------------------------------------------------------------
    private void SetRotateAngle(float value)
    {
      if (value != (float)(scbRotateAngle.Value / 10.0))
      {
        scbRotateAngle.Value = (int)(10.0 * value);
      }
      if (value != (float)nudRotateAngle.Value)
      {
        nudRotateAngle.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbRotateAngle_ValueChanged(object sender, EventArgs e)
    {
      SetRotateAngle((float)(scbRotateAngle.Value / 10.0));
    }
    private void nudRotateAngle_ValueChanged(object sender, EventArgs e)
    {
      SetRotateAngle((float)(nudRotateAngle.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - TranslateX
    //---------------------------------------------------------------
    private void SetTranslateX(float value)
    {
      if (value != (float)(scbTranslateX.Value / 10.0))
      {
        scbTranslateX.Value = (int)(10.0 * value);
      }
      if (value != (float)nudTranslateX.Value)
      {
        nudTranslateX.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbTranslateX_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateX((float)(scbTranslateX.Value / 10.0));
    }
    private void nudTranslateX_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateX((float)(nudTranslateX.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - TranslateY
    //---------------------------------------------------------------
    private void SetTranslateY(float value)
    {
      if (value != (float)(scbTranslateY.Value / 10.0))
      {
        scbTranslateY.Value = (int)(10.0 * value);
      }
      if (value != (float)nudTranslateY.Value)
      {
        nudTranslateY.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbTranslateY_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateY((float)(scbTranslateY.Value / 10.0));
    }
    private void nudTranslateY_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateY((float)(nudTranslateY.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - ScaleX
    //---------------------------------------------------------------
    private void SetScaleX(float value)
    {
      float SBF = 1.0f;
      if (0 < scbScaleX.Value)
      {
        SBF = (float)(1.0 + scbScaleX.Value / 100.0);
      }
      else
      if (scbScaleX.Value < 0)
      {
        SBF = (float)(1.0 + scbScaleX.Value / 100.0);
      } // else 1.0
      if (value != SBF)
      {
        if (1.0f < value)
        {
          scbScaleX.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        if (1.0f < value)
        {
          scbScaleX.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        { // 1.0
          scbScaleX.Value = 0;
        }
      }
      if (value != (float)nudScaleX.Value)
      {
        nudScaleX.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbScaleX_ValueChanged(object sender, EventArgs e)
    {
      if (0 < scbScaleX.Value)
      {
        SetScaleX((float)(1.0 + scbScaleX.Value / 100.0));
      }
      else
      if (scbScaleX.Value < 0)
      {
        SetScaleX((float)(1.0 + scbScaleX.Value / 100.0));
      }
      else
      {
        SetScaleX(1.0f);
      }
    }
    private void nudScaleX_ValueChanged(object sender, EventArgs e)
    {
      SetScaleX((float)(nudScaleX.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - ScaleY
    //---------------------------------------------------------------
    private void SetScaleY(float value)
    {
      float SBF = 1.0f;
      if (0 < scbScaleY.Value)
      {
        SBF = (float)(1.0 - scbScaleY.Value / 100.0);
      }
      else
      if (scbScaleY.Value < 0)
      {
        SBF = (float)(1.0 - scbScaleY.Value / 100.0);
      } // else 1.0
      if (value != SBF)
      {
        if (1.0f < value)
        {
          scbScaleY.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        if (1.0f < value)
        {
          scbScaleY.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        { // 1.0
          scbScaleY.Value = 0;
        }
      }
      if (value != (float)nudScaleY.Value)
      {
        nudScaleY.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbScaleY_ValueChanged(object sender, EventArgs e)
    {
      if (0 < scbScaleY.Value)
      {
        SetScaleY((float)(1.0 - scbScaleY.Value / 100.0));
      }
      else
      if (scbScaleY.Value < 0)
      {
        SetScaleY((float)(1.0 - scbScaleY.Value / 100.0));
      }
      else
      {
        SetScaleY(1.0f);
      }
    }
    private void nudScaleY_ValueChanged(object sender, EventArgs e)
    {
      SetScaleY((float)(nudScaleY.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - GreyScale
    //---------------------------------------------------------------
    private void cbxConvertGreyScale_CheckedChanged(object sender, EventArgs e)
    {
      FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);
      TransformProjection();
    }
    //
    //---------------------------------------------------------------
    //  Event - Show FileIndex
    //---------------------------------------------------------------
    private void nudShowIndex_Enter(object sender, EventArgs e)
    {
      //FNudShowIndexIsEntered = true;
    }
    private void nudShowIndex_Leave(object sender, EventArgs e)
    {
      //FNudShowIndexIsEntered = false;
      //cbxShowFileIndex.CheckedChanged -= cbxShowFileIndex_CheckedChanged;
      cbxShowFileIndex.Checked = false;
      //cbxShowFileIndex.CheckedChanged += cbxShowFileIndex_CheckedChanged;
    }
    private void cbxShowFileIndex_Leave(object sender, EventArgs e)
    {
      //if (FNudShowIndexIsEntered) return;
      cbxShowFileIndex.CheckedChanged -= cbxShowFileIndex_CheckedChanged;
      cbxShowFileIndex.Checked = false;
      //nudShowIndex.Enabled = false;
      //scbShowIndex.Enabled = false;
      cbxShowFileIndex.CheckedChanged += cbxShowFileIndex_CheckedChanged;
    }
    private void cbxShowFileIndex_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxShowFileIndex.Checked)
      { // Mode: Show FileIndex Enabled
        nudShowIndex.Enabled = true;
        scbShowIndex.Enabled = true;
        SetFileIndex(scbShowIndex.Value);
      }
      else
      { // Mode: Show FileIndex Disabled
        nudShowIndex.Enabled = false;
        scbShowIndex.Enabled = false;
        TransformProjection();
      }
    }
    private void SetFileIndex(int value)
    {
      if (value != scbShowIndex.Value)
      {
        scbShowIndex.Value = value;
        // debug Console.WriteLine(String.Format("scb {0} {1}", value, (int)nudShowIndex.Value));
      }
      if (value != nudShowIndex.Value)
      {
        nudShowIndex.Value = value;
        // debug Console.WriteLine(String.Format("nud {0} {1}", value, (int)nudShowIndex.Value));
      }
      FImage24b.ConvertShow256x256(scbShowIndex.Value, cbxShowCutFrame.Checked, FBackColor);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void nudShowIndex_ValueChanged(object sender, EventArgs e)
    {
      SetFileIndex((int)nudShowIndex.Value);
    }
    private void scbShowIndex_ValueChanged(object sender, EventArgs e)
    {
      SetFileIndex(scbShowIndex.Value);
    }
    //
    //---------------------------------------------------------------
    //  Event - BackColor
    //---------------------------------------------------------------
    private void rbtBackColorWhite_CheckedChanged(object sender, EventArgs e)
    {
      FBackColor = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
      FImage24b.SetFrameColor(FBackColor);
      TransformProjection();
    }
    private void rbtBackColorBlack_CheckedChanged(object sender, EventArgs e)
    {
      FBackColor = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
      FImage24b.SetFrameColor(FBackColor);
      TransformProjection();
    }
    private void rbtBackColorUser_CheckedChanged(object sender, EventArgs e)
    {
      FBackColor = lblBackColorUser.BackColor;
      FImage24b.SetFrameColor(FBackColor);
      TransformProjection();
    }
    private void lblBackColorUser_Click(object sender, EventArgs e)
    {
      DialogColor.Color = lblBackColorUser.BackColor;
      if (DialogResult.OK == DialogColor.ShowDialog())
      {
        lblBackColorUser.BackColor = DialogColor.Color;
        if (rbtBackColorUser.Checked)
        {
          FBackColor = lblBackColorUser.BackColor;
          FImage24b.SetFrameColor(FBackColor);
          TransformProjection();
        }
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - GammaFactor
    //---------------------------------------------------------------
    private void rbtGammaLinear_CheckedChanged(object sender, EventArgs e)
    {
      FImage24b.SetGammaHistogramLinear();
      FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);
      TransformProjection();
    }

    private void rbtGammaInverse_CheckedChanged(object sender, EventArgs e)
    {
      FImage24b.SetGammaHistogramInverse();
      FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);
      TransformProjection();
    }

    private void rbtGammaHistogram_CheckedChanged(object sender, EventArgs e)
    {
      FImage24b.LoadGammaHistogramFromFile(".//source//GammaFactor.xml");
      FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);
      TransformProjection();
    }

    private void btnSelectGammaHistogram_Click(object sender, EventArgs e)
    {
      
    }

    private void rbtGammaLinear_KeyDown(object sender, KeyEventArgs e)
    {
      FImage24b.SetGammaHistogramLinear();
      FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);
      TransformProjection();
      FImage24b.SaveGammaHistogramToFile(".\\source\\HistogramLinear.xml");
    }

    private void rbtGammaInverse_KeyDown(object sender, KeyEventArgs e)
    {
      FImage24b.SetGammaHistogramInverse();
      FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);
      TransformProjection();
      FImage24b.SaveGammaHistogramToFile(".\\source\\HistogramInverse.xml");
    }




    //
    //---------------------------------------------------------------
    //  Event - Convert to File
    //---------------------------------------------------------------
    private void btnSelectFileDirectory_Click(object sender, EventArgs e)
    {
      //DialogSelectFileDirectory.SelectedPath = Directory.GetCurrentDirectory() + "\\result";
      //if (DialogResult.OK == DialogSelectFileDirectory.ShowDialog())
      //{
      //  Console.WriteLine(DialogSelectFileDirectory.SelectedPath);
      //}
    }
    private void btnDefaultFileMask_Click(object sender, EventArgs e)
    {
      tbxFileName512Convert.Text = INIT_DEFAULTFILEMASK;
    }
    private void btnConvert512_Click(object sender, EventArgs e)
    {
      DialogSelectFileDirectory.SelectedPath = Directory.GetCurrentDirectory() + "\\result";
      if (DialogResult.OK == DialogSelectFileDirectory.ShowDialog())
      { // Console.WriteLine(DialogSelectFileDirectory.SelectedPath);
        String FileMask = DialogSelectFileDirectory.SelectedPath + "\\" + tbxFileName512Convert.Text;
        FImage24b.ConvertToFile2048x512(FileMask, (int)nudFile512IndexStart.Value,
                                        (int)nudFile512IndexEnd.Value, (int)nudFile512IndexStep.Value);
      }
    }









  }
}
