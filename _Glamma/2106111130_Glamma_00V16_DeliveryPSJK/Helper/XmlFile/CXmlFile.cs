﻿using System;
using System.Drawing;
using System.Xml;
//
namespace XmlFile
{
  public class CXmlFile
  { //
    //------------------------------------------------------------------------------
    //  Constant
    //------------------------------------------------------------------------------
    protected const String SECTION_COLOR = "Color";
    protected const String NAME_ALPHA = "Alpha";
    protected const String NAME_RED = "Red";
    protected const String NAME_GREEN = "Green";
    protected const String NAME_BLUE = "Blue";
    protected const String NAME_OPACITY = "Opacity";
    //
    protected const Byte INIT_ALPHA = 0xFF;
    protected const Byte INIT_RED = 0xFF;
    protected const Byte INIT_GREEN = 0xFF;
    protected const Byte INIT_BLUE = 0xFF;
    protected const Byte INIT_OPACITY = 100;

    protected const String SECTION_PEN = "Pen";
    protected const String NAME_COLOR = "Color";
    protected const String NAME_WIDTH = "Width";
    //
    protected Color INIT_COLOR = Color.Black;
    protected const float INIT_WIDTH = 1.0f;
    //
    protected const String SECTION_SOLIDBRUSH = "SolidBrush";
    //
    protected const String SECTION_FONT = "Font";
    protected const String NAME_NAME = "Name";
    protected const String NAME_SIZE = "Size";
    protected const String NAME_BOLD = "Bold";
    protected const String NAME_ITALIC = "Italic";
    protected const String NAME_STRIKEOUT = "Strikeout";
    protected const String NAME_UNDERLINE = "Underline";
    //
    //
    //------------------------------------------------------------------------------
    //  Field
    //------------------------------------------------------------------------------
    protected XmlDocument FXmlDocument;
    protected XmlNode FNodeBase;
    protected XmlNode FNodeParent;
    protected String FFileName;
    //
    //------------------------------------------------------------------------------
    //  Constructor
    //------------------------------------------------------------------------------
    public CXmlFile()
    {
      FFileName = "dummy.xml";
    }
    //
    //------------------------------------------------------------------------------
    //  Read
    //------------------------------------------------------------------------------
    public Boolean ReadOpen(String filename, String section)
    {
      try
      {
        FFileName = filename;
        FXmlDocument = new XmlDocument();
        FXmlDocument.Load(FFileName);
        FNodeBase = FXmlDocument.SelectSingleNode(section);
        FNodeParent = FNodeBase;
        return (FNodeBase is XmlNode);
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean ReadClose()
    {
      if (FNodeBase is XmlNode)
      {
        FNodeBase = null;
        return !(FNodeBase is XmlNode);
      }
      return false;
    }
    public Boolean ReadOpenSection(String section)
    {
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(section);
        if (NodeChild is XmlNode)
        {
          FNodeParent = NodeChild;
          return (FNodeParent is XmlNode);
        }
      }
      return false;
    }
    public Boolean ReadCloseSection()
    {
      if (FNodeParent is XmlNode)
      {
        FNodeParent = FNodeParent.ParentNode;
      }
      return (FNodeParent is XmlNode);
    }
    public Boolean ReadByte(String name,
                            out Byte value,
                            Byte preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return Byte.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadUInt16(String name,
                              out UInt16 value,
                              UInt16 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return UInt16.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadUInt32(String name,
                              out UInt32 value,
                              UInt32 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return UInt32.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadInt32(String name,
                             out Int32 value,
                             Int32 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return Int32.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadBoolean(String name,
                               out Boolean value,
                               Boolean preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return Boolean.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadString(String name,
                              out String value,
                              String preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          value = NodeChild.InnerText;
          return true;
        }
      }
      return false;
    }
    public Boolean ReadFloat(String name,
                             out float value,
                             float preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return float.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadDouble(String name,
                             out Double value,
                             Double preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return Double.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadEnumeration(String name,
                             out String value,
                             String preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          value = NodeChild.InnerText;
          return (0 < value.Length);
        }
      }
      return false;
    }
    public Boolean ReadColor(String name,
                             out Color value,
                             Color preset)
    {
      ReadOpenSection(SECTION_COLOR + name);
      //
      Boolean Result = true;
      value = preset;
      Byte Alpha = INIT_ALPHA;
      Result &= ReadByte(NAME_ALPHA, out Alpha, Alpha);
      Byte Red = INIT_RED;
      Result &= ReadByte(NAME_RED, out Red, Red);
      Byte Green = INIT_GREEN;
      Result &= ReadByte(NAME_GREEN, out Green, Green);
      Byte Blue = INIT_BLUE;
      Result &= ReadByte(NAME_BLUE, out Blue, Blue);
      //
      if (!Result)
      {
        value = preset;
      }
      else
      {
        value = Color.FromArgb(Alpha, Red, Green, Blue);
      }
      //
      ReadCloseSection();
      return Result;
    }
    public Boolean ReadColorOpacity(String name,
                                    out Color colorvalue,
                                    Color colorpreset,
                                    out Byte opacityvalue,
                                    Byte opacitypreset)
    {
      ReadOpenSection(SECTION_COLOR + name);
      //
      Boolean Result = true;
      colorvalue = colorpreset;
      opacityvalue = opacitypreset;
      //
      Byte Red = INIT_RED;
      Result &= ReadByte(NAME_RED, out Red, Red);
      Byte Green = INIT_GREEN;
      Result &= ReadByte(NAME_GREEN, out Green, Green);
      Byte Blue = INIT_BLUE;
      Result &= ReadByte(NAME_BLUE, out Blue, Blue);
      Byte Opacity = INIT_OPACITY;
      Result &= ReadByte(NAME_OPACITY, out Opacity, Opacity);
      Opacity = (Byte)Math.Min(100, (int)Opacity);
      Opacity = (Byte)Math.Max(0, (int)Opacity);
      //
      if (Result)
      {
        colorvalue = Color.FromArgb(INIT_ALPHA, Red, Green, Blue);
        opacityvalue = Opacity;
      }
      //
      ReadCloseSection();
      return Result;
    }
    public Boolean ReadGuid(String name,
                            out Guid value,
                            Guid preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return Guid.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadDateTime(String name,
                                out DateTime value,
                                DateTime preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          return DateTime.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    public Boolean ReadPen(String name,
                           out Pen value,
                           Pen preset)
    {
      const String SECTION_PEN = "Pen";
      const String NAME_COLOR = "Color";
      const String NAME_WIDTH = "Width";
      //
      Color INIT_COLOR = Color.Black;
      const float INIT_WIDTH = 1.0f;
      //
      ReadOpenSection(SECTION_PEN + name);
      //
      Boolean Result = true;
      value = preset;
      Color CValue = INIT_COLOR;
      Result &= ReadColor(NAME_COLOR, out CValue, CValue);
      float WValue = INIT_WIDTH;
      Result &= ReadFloat(NAME_WIDTH, out WValue, WValue);
      //
      value = new Pen(CValue, WValue);
      //
      ReadCloseSection();
      return Result;
    }
    public Boolean ReadSolidBrush(String name,
                                  out SolidBrush value,
                                  SolidBrush preset)
    {
      const String SECTION_SOLIDBRUSH = "SolidBrush";
      const String NAME_COLOR = "Color";
      //
      Color INIT_COLOR = Color.White;
      //
      ReadOpenSection(SECTION_SOLIDBRUSH + name);
      //
      Boolean Result = true;
      value = preset;
      Color CValue = INIT_COLOR;
      Result &= ReadColor(NAME_COLOR, out CValue, CValue);
      //
      value = new SolidBrush(CValue);
      //
      ReadCloseSection();
      return Result;
    }
    public Boolean ReadFont(String headeroffset,
                            out Font value,
                            Font preset)
    {
      const String SECTION_FONT = "Font";
      const String NAME_NAME = "Name";
      const String NAME_SIZE = "Size";
      const String NAME_BOLD = "Bold";
      const String NAME_ITALIC = "Italic";
      const String NAME_STRIKEOUT = "Strikeout";
      const String NAME_UNDERLINE = "Underline";
      //
      const String INIT_HEADER = "Arial";
      const float INIT_SIZE = 10.0f;
      const Boolean INIT_BOLD = false;
      const Boolean INIT_ITALIC = false;
      const Boolean INIT_STRIKEOUT = false;
      const Boolean INIT_UNDERLINE = false;
      //
      ReadOpenSection(SECTION_FONT + headeroffset);
      //
      Boolean Result = true;
      value = preset;
      String Name = INIT_HEADER;
      Result &= ReadString(NAME_NAME, out Name, Name);
      float Size = INIT_SIZE;
      Result &= ReadFloat(NAME_SIZE, out Size, Size);
      Boolean Bold = INIT_BOLD;
      Result &= ReadBoolean(NAME_BOLD, out Bold, Bold);
      Boolean Italic = INIT_ITALIC;
      Result &= ReadBoolean(NAME_ITALIC, out Italic, Italic);
      Boolean Strikeout = INIT_STRIKEOUT;
      Result &= ReadBoolean(NAME_STRIKEOUT, out Strikeout, Strikeout);
      Boolean Underline = INIT_UNDERLINE;
      Result &= ReadBoolean(NAME_UNDERLINE, out Underline, Underline);
      //
      FontStyle FS = new FontStyle();
      if (Bold)
      {
        FS |= FontStyle.Bold;
      }
      if (Italic)
      {
        FS |= FontStyle.Italic;
      }
      if (Strikeout)
      {
        FS |= FontStyle.Strikeout;
      }
      if (Underline)
      {
        FS |= FontStyle.Underline;
      }
      value = new Font(Name, Size, FS);
      //
      ReadCloseSection();
      return Result;
    }
    //
    //------------------------------------------------------------------------------
    //  Write
    //------------------------------------------------------------------------------
    public Boolean WriteOpen(String filename, String section)
    {
      try
      {
        FFileName = filename;
        FXmlDocument = new XmlDocument();
        FNodeBase = FXmlDocument.CreateElement(section);
        FNodeParent = FNodeBase;
        FXmlDocument.AppendChild(FNodeBase);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean WriteClose()
    {
      try
      {
        FXmlDocument.Save(FFileName);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean WriteOpenSection(String section)
    {
      XmlNode Node = FXmlDocument.CreateElement(section);
      if (Node is XmlNode)
      {
        FNodeParent.AppendChild(Node);
        FNodeParent = Node;
        return true;
      }
      return false;
    }
    public Boolean WriteCloseSection()
    {
      FNodeParent = FNodeParent.ParentNode;
      return (FNodeParent is XmlNode);
    }
    private Boolean WriteCommon(String name,
                                object value)
    {
      try
      {
        XmlNode Node = FXmlDocument.CreateElement(name);
        Node.InnerText = value.ToString();
        FNodeParent.AppendChild(Node);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean WriteByte(String name,
                             Byte value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteUInt16(String name,
                               UInt16 value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteUInt32(String name,
                               UInt32 value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteInt32(String name,
                              Int32 value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteBoolean(String name,
                                Boolean value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteString(String name,
                              String value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteFloat(String name,
                              float value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteDouble(String name,
                             Double value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteEnumeration(String name,
                                    String value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteGuid(String name,
                             Guid value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteDateTime(String name,
                                 DateTime value)
    {
      return WriteCommon(name, value);
    }
    public Boolean WriteColor(String name,
                              Color value)
    {
      Boolean Result = true;
      Result &= WriteOpenSection(SECTION_COLOR + name);
      Result &= WriteByte(NAME_ALPHA, value.A);
      Result &= WriteByte(NAME_RED, value.R);
      Result &= WriteByte(NAME_GREEN, value.G);
      Result &= WriteByte(NAME_BLUE, value.B);
      Result &= WriteCloseSection();
      return Result;
    }
    public Boolean WriteColorOpacity(String name,
                                     Color color, Byte opacity)
    {
      Boolean Result = true;
      Result &= WriteOpenSection(SECTION_COLOR + name);
      Result &= WriteByte(NAME_RED, color.R);
      Result &= WriteByte(NAME_GREEN, color.G);
      Result &= WriteByte(NAME_BLUE, color.B);
      Result &= WriteByte(NAME_OPACITY, opacity);
      Result &= WriteCloseSection();
      return Result;
    }
    public Boolean WritePen(String name,
                            Pen value)
    {
      Boolean Result = true;
      Result &= WriteOpenSection(SECTION_COLOR + name);
      Result &= WriteColor(NAME_COLOR, value.Color);
      Result &= WriteFloat(NAME_WIDTH, value.Width);
      WriteCloseSection();
      return Result;
    }
    public Boolean WriteSolidBrush(String name,
                                   SolidBrush value)
    {
      Boolean Result = true;
      Result &= WriteOpenSection(SECTION_COLOR + name);
      Result &= WriteColor(NAME_COLOR, value.Color);
      WriteCloseSection();
      return Result;
    }
    public Boolean WriteFont(String headeroffset,
                             Font value)
    {
      WriteOpenSection(SECTION_FONT + headeroffset);
      Boolean Result = true;
      Result &= WriteString(NAME_NAME, value.Name);
      Result &= WriteFloat(NAME_SIZE, value.Size);
      Result &= WriteBoolean(NAME_BOLD, value.Bold);
      Result &= WriteBoolean(NAME_ITALIC, value.Italic);
      Result &= WriteBoolean(NAME_STRIKEOUT, value.Strikeout);
      Result &= WriteBoolean(NAME_UNDERLINE, value.Underline);
      WriteCloseSection();
      return Result;
    }




    //
    //------------------------------------------------------------------------------
    //  Handler
    //------------------------------------------------------------------------------
    //public Boolean Load(String filename, String segment)
    //{
    //  try
    //  {
    //    if (ReadOpen(filename, segment))
    //    {
    //      Byte Value = 0x00;
    //      for (int CI = 0; CI < 256; CI++)
    //      {
    //        String NodeName = String.Format("I{0:000}", CI);
    //        if (ReadByte(NodeName, out Value, 0x12))
    //        {
    //          Console.WriteLine(Value);
    //        }
    //      }
    //      return true;
    //    }
    //    ReadClose();
    //    return false;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}
    //public Boolean Save(String filename, String segment)
    //{
    //  try
    //  {
    //    //if (WriteOpen(filename, segment))
    //    //{
    //    //  Byte Value = 0x00;
    //    //  for (int CI = 0; CI < 256; CI++)
    //    //  {
    //    //    String NodeName = String.Format("I{0:000}", CI);
    //    //    if (ReadByte(NodeName, out Value, 0x12))
    //    //    {
    //    //      Console.WriteLine(Value);
    //    //    }
    //    //  }
    //    //  return true;
    //    //}
    //    //WriteClose();
    //    return false;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}



  }
}
