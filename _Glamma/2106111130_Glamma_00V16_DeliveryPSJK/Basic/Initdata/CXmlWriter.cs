﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Drawing;
//
namespace Initdata
{
  public class CXmlWriter : CXmlBase
  {
    //
    //---------------------------------------------------
    //  Section - Constructor
    //---------------------------------------------------
    //
    public CXmlWriter(String filename)
    {
      FFileName = filename;
      FNotify = new CNotify();
    }
    //
    //---------------------------------------------------
    //  Section - Property
    //---------------------------------------------------
    //
    public void SetOnNotify(DOnNotify value)
    {
      FNotify.SetOnNotify(value);
    }
    //
    //---------------------------------------------------
    //  Section - Management
    //---------------------------------------------------
    //
    public Boolean Open(String section)
    {
      try
      {
        FXmlDocument = new XmlDocument();
        FNodeBase = FXmlDocument.CreateElement(section);
        FNodeParent = FNodeBase;
        FXmlDocument.AppendChild(FNodeBase);
        String Line = String.Format("XmlWriter: Open file \"{0}\"", FFileName);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        Line = String.Format("<{0}>", section);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        FNotify.IncrementLeadCount();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    public Boolean Close()
    {
      try
      {
        FNotify.DecrementLeadCount();
        String Line = String.Format("<{0}>", FNodeBase.Name);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        Line = String.Format("XmlWriter: Close file \"{0}\"", FFileName);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        FXmlDocument.Save(FFileName);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean CreateSection(String section)
    {
      XmlNode Node = FXmlDocument.CreateElement(section);
      if (Node is XmlNode)
      {
        FNodeParent.AppendChild(Node);
        FNodeParent = Node;
        String Line = String.Format("<{0}>", section);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        FNotify.IncrementLeadCount();        
        return true;
      }
      return false;
    }
    public Boolean CloseSection()
    {
      FNotify.DecrementLeadCount();
      String Line = String.Format("</{0}>", FNodeParent.Name);
      FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
      FNodeParent = FNodeParent.ParentNode;
      return (FNodeParent is XmlNode);
    }
    //
    //---------------------------------------
    //  Segment - CommonBase
    //---------------------------------------
    //      
    private Boolean WriteCommon(String name,
                                object value)
    {
      try
      {
        XmlNode Node = FXmlDocument.CreateElement(name);
        Node.InnerText = value.ToString();
        FNodeParent.AppendChild(Node);
        String Line = String.Format("<{0}>{1}</{0}>", name, Node.InnerText);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //---------------------------------------
    //  Segment - Int32
    //---------------------------------------
    //      
    public Boolean WriteInt32(String name,
                              Int32 value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Boolean
    //---------------------------------------
    //      
    public Boolean WriteBoolean(String name,
                                Boolean value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - String
    //---------------------------------------
    //      
    public Boolean WriteString(String name,
                              String value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Float
    //---------------------------------------
    //      
    public Boolean WriteFloat32(String name,
                                float value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Double
    //---------------------------------------
    //      
    public Boolean WriteDouble64(String name,
                                 Double value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Enumeration
    //---------------------------------------
    //      
    public Boolean WriteEnumeration(String name,
                                    String value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Guid
    //---------------------------------------
    //      
    public Boolean WriteGuid(String name,
                             Guid value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - DateTime
    //---------------------------------------
    //      
    public Boolean WriteDateTime(String name,
                                 DateTime value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Byte
    //---------------------------------------
    //      
    public Boolean WriteByte(String name,
                             Byte value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - UInt16
    //---------------------------------------
    //      
    public Boolean WriteUInt16(String name,
                               UInt16 value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - UInt32
    //---------------------------------------
    //      
    public Boolean WriteUInt32(String name,
                               UInt32 value)
    {
      return WriteCommon(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Color
    //---------------------------------------
    //      
    public Boolean WriteColor(String name,
                              Color value)
    {
      Boolean Result = true;
      Result &= CreateSection(SECTION_COLOR + name);
      //
      Result &= WriteByte(NAME_ALPHA, value.A);
      Result &= WriteByte(NAME_RED, value.R);
      Result &= WriteByte(NAME_GREEN, value.G);
      Result &= WriteByte(NAME_BLUE, value.B);
      //
      Result &= CloseSection();
      return Result;
    }
    //
    //---------------------------------------
    //  Segment - ColorOpacity
    //---------------------------------------
    //      
    public Boolean WriteColorOpacity(String name,
                                     Color color, Byte opacity)
    {
      Boolean Result = true;
      Result &= CreateSection(SECTION_COLOR + name);
      //
      Result &= WriteByte(NAME_RED, color.R);
      Result &= WriteByte(NAME_GREEN, color.G);
      Result &= WriteByte(NAME_BLUE, color.B);
      Result &= WriteByte(NAME_OPACITY, opacity);
      //
      Result &= CloseSection();
      return Result;
    }
    //
    //---------------------------------------
    //  Segment - Pen
    //---------------------------------------
    //      
    public Boolean WritePen(String name,
                            Pen value)
    {
      Boolean Result = true;
      Result &= CreateSection(SECTION_COLOR + name);
      //
      Result &= WriteColor(NAME_COLOR, value.Color);
      Result &= WriteFloat32(NAME_WIDTH, value.Width);
      //
      CloseSection();
      return Result;
    }
    //
    //---------------------------------------
    //  Segment - Brush
    //---------------------------------------
    //      
    public Boolean WriteSolidBrush(String name,
                                   SolidBrush value)
    {
      Boolean Result = true;
      Result &= CreateSection(SECTION_COLOR + name);
      //
      Result &= WriteColor(NAME_COLOR, value.Color);
      //
      CloseSection();
      return Result;
    }
    //
    //---------------------------------------
    //  Segment - Font
    //---------------------------------------
    //      
    public Boolean WriteFont(String headeroffset,
                             Font value)
    {
      const String SECTION_FONT = "Font";
      const String NAME_NAME = "Name";
      const String NAME_SIZE = "Size";
      const String NAME_BOLD = "Bold";
      const String NAME_ITALIC = "Italic";
      const String NAME_STRIKEOUT = "Strikeout";
      const String NAME_UNDERLINE = "Underline";
      //
      CreateSection(SECTION_FONT + headeroffset);
      //
      Boolean Result = true;
      Result &= WriteString(NAME_NAME, value.Name);
      Result &= WriteFloat32(NAME_SIZE, value.Size);
      Result &= WriteBoolean(NAME_BOLD, value.Bold);
      Result &= WriteBoolean(NAME_ITALIC, value.Italic);
      Result &= WriteBoolean(NAME_STRIKEOUT, value.Strikeout);
      Result &= WriteBoolean(NAME_UNDERLINE, value.Underline);
      //
      CloseSection();
      return Result;
    }





  }
}
