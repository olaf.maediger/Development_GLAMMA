﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace Initdata
{
  public class CInitdataReader : CInitdataBase
  {
    private CXmlReader FXmlReader;

    public CInitdataReader()
    {
      FXmlReader = null;
    }


    public Boolean Open(String filename, 
                        String mainsection,
                        DOnNotify onnotify)
    {
      FXmlReader = new CXmlReader(filename);
      if (FXmlReader is CXmlReader)
      {
        FXmlReader.SetOnNotify(onnotify);
        return FXmlReader.Open(mainsection);
      }
      return false;
    }
    public Boolean Close()
    {
      return FXmlReader.Close();
    }

    public Boolean OpenSection(String section)
    {
      return FXmlReader.OpenSection(section);
    }
    public Boolean CloseSection()
    {
      return FXmlReader.CloseSection();
    }
    //
    //---------------------------------------
    //  Segment - Int32
    //---------------------------------------
    //      
    public Boolean ReadInt32(String name, 
                             out Int32 value, 
                             Int32 preset)
    {
      return FXmlReader.ReadInt32(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - Boolean
    //---------------------------------------
    //      
    public Boolean ReadBoolean(String name,
                               out Boolean value,
                               Boolean preset)
    {
      return FXmlReader.ReadBoolean(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - String
    //---------------------------------------
    //      
    public Boolean ReadString(String name,
                              out String value,
                              String preset)
    {
      return FXmlReader.ReadString(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - Float
    //---------------------------------------
    //      
    public Boolean ReadFloat32(String name,
                               out float value,
                               float preset)
    {
      return FXmlReader.ReadFloat32(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - Double
    //---------------------------------------
    //      
    public Boolean ReadDouble64(String name,
                                out Double value,
                                Double preset)
    {
      return FXmlReader.ReadDouble64(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - Enumeration
    //---------------------------------------
    //      
    public Boolean ReadEnumeration(String name,
                                   out String value,
                                   String preset)
    {
      return FXmlReader.ReadEnumeration(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - Guid
    //---------------------------------------
    //      
    public Boolean ReadGuid(String name,
                            out Guid value,
                            Guid preset)
    {
      return FXmlReader.ReadGuid(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - DateTime
    //---------------------------------------
    //      
    public Boolean ReadDateTime(String name,
                                out DateTime value,
                                DateTime preset)
    {
      return FXmlReader.ReadDateTime(name, out value, preset);
    } 
    //
    //---------------------------------------
    //  Segment - Byte
    //---------------------------------------
    //      
    public Boolean ReadByte(String name,
                            out Byte value,
                            Byte preset)
    {
      return FXmlReader.ReadByte(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - UInt16
    //---------------------------------------
    //      
    public Boolean ReadUInt16(String name,
                              out UInt16 value,
                              UInt16 preset)
    {
      return FXmlReader.ReadUInt16(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - UInt32
    //---------------------------------------
    //      
    public Boolean ReadUInt32(String name,
                              out UInt32 value,
                              UInt32 preset)
    {
      return FXmlReader.ReadUInt32(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - Color
    //---------------------------------------
    //      
    public Boolean ReadColor(String name,
                             out Color value,
                             Color preset)
    {
      return FXmlReader.ReadColor(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - ColorOpacity
    //---------------------------------------
    //      
    public Boolean ReadColorOpacity(String name,
                                    out Color colorvalue, Color colorpreset,
                                    out Byte opacityvalue, Byte opacitypreset)
    {
      return FXmlReader.ReadColorOpacity(name, out colorvalue, colorpreset,
                                               out opacityvalue, opacitypreset);
    }
    //
    //---------------------------------------
    //  Segment - Pen
    //---------------------------------------
    //      
    public Boolean ReadPen(String name,
                           out Pen value,
                           Pen preset)
    {
      return FXmlReader.ReadPen(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - Brush
    //---------------------------------------
    //      
    public Boolean ReadSolidBrush(String name,
                                  out SolidBrush value,
                                  SolidBrush preset)
    {
      return FXmlReader.ReadSolidBrush(name, out value, preset);
    }
    //
    //---------------------------------------
    //  Segment - Font
    //---------------------------------------
    //      
    public Boolean ReadFont(String headeroffset,
                            out Font value,
                            Font preset)
    {
      return FXmlReader.ReadFont(headeroffset, out value, preset);
    }

  }
}
