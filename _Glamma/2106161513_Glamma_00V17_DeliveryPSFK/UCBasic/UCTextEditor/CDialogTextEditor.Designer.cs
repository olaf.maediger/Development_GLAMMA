﻿namespace UCTextEditor
{
  partial class CDialogTextEditor
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlKey = new System.Windows.Forms.Panel();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnOk = new System.Windows.Forms.Button();
      this.FUCTextEditor = new UCTextEditor.CUCEditTextEditor();
      this.pnlKey.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlKey
      // 
      this.pnlKey.Controls.Add(this.btnCancel);
      this.pnlKey.Controls.Add(this.btnOk);
      this.pnlKey.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlKey.Location = new System.Drawing.Point(0, 450);
      this.pnlKey.Name = "pnlKey";
      this.pnlKey.Size = new System.Drawing.Size(514, 32);
      this.pnlKey.TabIndex = 3;
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(261, 5);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 2;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // btnOk
      // 
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.Location = new System.Drawing.Point(178, 5);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(75, 23);
      this.btnOk.TabIndex = 0;
      this.btnOk.Text = "OK";
      this.btnOk.UseVisualStyleBackColor = true;
      // 
      // FUCTextEditor
      // 
      this.FUCTextEditor.FileName = "";
      this.FUCTextEditor.Location = new System.Drawing.Point(45, 68);
      this.FUCTextEditor.Name = "FUCTextEditor";
      this.FUCTextEditor.Size = new System.Drawing.Size(424, 347);
      this.FUCTextEditor.TabIndex = 4;
      // 
      // CDialogTextEditor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(514, 482);
      this.Controls.Add(this.FUCTextEditor);
      this.Controls.Add(this.pnlKey);
      this.Name = "CDialogTextEditor";
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Edit Text";
      this.Resize += new System.EventHandler(this.CDialogTextEditor_Resize);
      this.pnlKey.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlKey;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnOk;
    private CUCEditTextEditor FUCTextEditor;
  }
}