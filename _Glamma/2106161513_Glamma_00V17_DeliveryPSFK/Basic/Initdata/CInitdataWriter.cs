﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
//
namespace Initdata
{
  public class CInitdataWriter : CInitdataBase
  {
    private CXmlWriter FXmlWriter;

    public CInitdataWriter()
    {
      FXmlWriter = null;
    }

    public Boolean Open(String filename, 
                        String mainsection,
                        DOnNotify onnotify)
    {
      FXmlWriter = new CXmlWriter(filename);
      if (FXmlWriter is CXmlWriter)
      {
        FXmlWriter.SetOnNotify(onnotify);
        return FXmlWriter.Open(mainsection);
      }
      return false;
    }
    public Boolean Close()
    {
      return FXmlWriter.Close();
    }

    public Boolean CreateSection(String section)
    {
      return FXmlWriter.CreateSection(section);
    }
    public Boolean CloseSection()
    {
      return FXmlWriter.CloseSection();
    }
    //
    //---------------------------------------
    //  Segment - Int32
    //---------------------------------------
    //      
    public Boolean WriteInt32(String name,
                              Int32 value)
    {
      return FXmlWriter.WriteInt32(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Boolean
    //---------------------------------------
    //      
    public Boolean WriteBoolean(String name,
                                Boolean value)
    {
      return FXmlWriter.WriteBoolean(name, value);
    }
    //
    //---------------------------------------
    //  Segment - String
    //---------------------------------------
    //      
    public Boolean WriteString(String name,
                               String value)
    {
      return FXmlWriter.WriteString(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Float
    //---------------------------------------
    //      
    public Boolean WriteFloat32(String name,
                                float value)
    {
      return FXmlWriter.WriteFloat32(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Double
    //---------------------------------------
    //      
    public Boolean WriteDouble64(String name,
                                Double value)
    {
      return FXmlWriter.WriteDouble64(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Enumeration
    //---------------------------------------
    //      
    public Boolean WriteEnumeration(String name,
                                    String value)
    {
      return FXmlWriter.WriteEnumeration(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Guid
    //---------------------------------------
    //      
    public Boolean WriteGuid(String name,
                             Guid value)
    {
      return FXmlWriter.WriteGuid(name, value);
    }
    //
    //---------------------------------------
    //  Segment - DateTime
    //---------------------------------------
    //      
    public Boolean WriteDateTime(String name,
                                 DateTime value)
    {
      return FXmlWriter.WriteDateTime(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Byte
    //---------------------------------------
    //      
    public Boolean WriteByte(String name,
                             Byte value)
    {
      return FXmlWriter.WriteByte(name, value);
    }
    //
    //---------------------------------------
    //  Segment - UInt16
    //---------------------------------------
    //      
    public Boolean WriteUInt16(String name,
                               UInt16 value)
    {
      return FXmlWriter.WriteUInt16(name, value);
    }
    //
    //---------------------------------------
    //  Segment - UInt32
    //---------------------------------------
    //      
    public Boolean WriteUInt32(String name,
                               UInt32 value)
    {
      return FXmlWriter.WriteUInt32(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Color
    //---------------------------------------
    //      
    public Boolean WriteColor(String name,
                              Color value)
    {
      return FXmlWriter.WriteColor(name, value);
    }
    //
    //---------------------------------------
    //  Segment - ColorOpacity
    //---------------------------------------
    //      
    public Boolean WriteColorOpacity(String name,
                                     Color colorvalue,
                                     Byte opacityvalue)
    {
      return FXmlWriter.WriteColorOpacity(name, colorvalue, opacityvalue);
    }
    //
    //---------------------------------------
    //  Segment - Pen
    //---------------------------------------
    //      
    public Boolean WritePen(String name,
                            Pen value)
    {
      return FXmlWriter.WritePen(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Brush
    //---------------------------------------
    //      
    public Boolean WriteSolidBrush(String name,
                                   SolidBrush value)
    {
      return FXmlWriter.WriteSolidBrush(name, value);
    }
    //
    //---------------------------------------
    //  Segment - Font
    //---------------------------------------
    //      
    public Boolean WriteFont(String headeroffset,
                             Font value)
    {
      return FXmlWriter.WriteFont(headeroffset, value);
    }

  }
}
