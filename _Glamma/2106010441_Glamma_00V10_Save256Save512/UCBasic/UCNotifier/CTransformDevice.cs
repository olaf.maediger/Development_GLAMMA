﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCNotifier
{ //
  //--------------------------------------------------------
  //  Segment - Type
  //--------------------------------------------------------
  //
  public struct RDeviceData
  {
    public RTransformData TransformData;
    public String ApplicationKey;
    public Int32 SerialNumber;
    public RDeviceData(Int32 index)
    {
      TransformData = new RTransformData(0);
      ApplicationKey = "0-0-0-0";
      SerialNumber = 0;
    }
  };
  //
  //#########################################################
  //  Segment - CTransformDevice
  //#########################################################
  //
  public class CTransformDevice : CTransformBase
  { //
    //--------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------
    //
    private RDeviceData FData;
    //
    //--------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------
    //
    public CTransformDevice(String name,
                            String version,
                            String date,
                            Boolean discountuser,
                            String user,
                            Int32 serialnumber,
                            Int32 productnumber,
                            String productkey)
      : base(name, version, date, discountuser, user, productnumber, productkey)
    {
      FData.SerialNumber = serialnumber;
      SetData(name, version, date,
              discountuser, user, serialnumber,
              productnumber, productkey);
    }

    public CTransformDevice(RDeviceData data)
      : base(data.TransformData.Name,
             data.TransformData.Version,
             data.TransformData.Date,
             data.TransformData.DiscountUser,
             data.TransformData.User,
             data.TransformData.ProductNumber,
             data.TransformData.ProductKey)
    {
      FData.SerialNumber = data.SerialNumber;
      SetData(data);
    }
    //
    //--------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------
    //
    public Int32 SerialNumber
    {
      get { return FData.SerialNumber; }
    }
    //
    //--------------------------------------------------------
    //  Segment - Get/SetData
    //--------------------------------------------------------
    //
    public Boolean SetData(RDeviceData data)
    {
      if (base.SetData(data.TransformData))
      {
        FData.SerialNumber = data.SerialNumber;
        return base.GetData(out FData.TransformData);
      }
      return false;
    }
    
    public Boolean SetData(String name,
                           String version,
                           String date,
                           Boolean discountuser,
                           String user,
                           Int32 serialnumber,
                           Int32 productnumber,
                           String productkey)
    {
      if (base.SetData(name, version, date,
                       discountuser, user,
                       productnumber, productkey))
      {
        FData.SerialNumber = serialnumber;
        return base.GetData(out FData.TransformData);
      }
      return false;
    }

    public Boolean GetData(out RDeviceData data)
    {
      Boolean Result = base.GetData(out data.TransformData);
      data.SerialNumber = FData.SerialNumber;
      data = FData;
      return Result;
    }

  }
}
