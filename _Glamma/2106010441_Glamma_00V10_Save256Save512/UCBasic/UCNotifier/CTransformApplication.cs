﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCNotifier
{ //
  //--------------------------------------------------------
  //  Segment - Type
  //--------------------------------------------------------
  //
  public struct RApplicationData
  {
    public RTransformData TransformData;
    public String Company;
    public String Street;
    public String StreetNumber;
    public String PostalCode;
    public String City;
    public RApplicationData(Int32 index)
    {
      TransformData = new RTransformData(0);
      Company = "";
      Street = "";
      StreetNumber = "";
      PostalCode = "";
      City = "";
    }
  };
  //
  //#########################################################
  //  Segment - CTransformApplication
  //#########################################################
  //
  public class CTransformApplication : CTransformBase
  { //
    //--------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------
    //
    private RApplicationData FData;
    //
    //--------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------
    //
    public CTransformApplication(String name,                                 
                                 String version,
                                 String date,
                                 String company,
                                 String street,
                                 String streetnumber,
                                 String postalcode,
                                 String city,
                                 Boolean discountuser,
                                 String user,
                                 Int32 productnumber,
                                 String productkey)
      : base(name, version, date, discountuser, user, productnumber, productkey)
    {
      SetData(name, version, date,
              company, street, streetnumber,
              postalcode, city, 
              discountuser, user,
              productnumber, productkey);
    }

    public CTransformApplication(RApplicationData data)
      : base(data.TransformData.Name, 
             data.TransformData.Version,
             data.TransformData.Date,
             data.TransformData.DiscountUser,
             data.TransformData.User,
             data.TransformData.ProductNumber, 
             data.TransformData.ProductKey)
    {
      SetData(data);
    }
    //
    //--------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------
    //
    public String Company
    {
      get { return FData.Company; }
    }
    public String Street
    {
      get { return FData.Street; }
    }
    public String StreetNumber
    {
      get { return FData.StreetNumber; }
    }
    public String PostalCode
    {
      get { return FData.PostalCode; }
    }
    public String City
    {
      get { return FData.City; }
    }
    //
    //--------------------------------------------------------
    //  Segment - Get/SetData
    //--------------------------------------------------------
    //
    public Boolean SetData(RApplicationData data)
    {
      FData.Company = data.Company;
      FData.Street = data.Street;
      FData.StreetNumber = data.StreetNumber;
      FData.PostalCode = data.PostalCode;
      FData.City = data.City;
      if (base.SetData(data.TransformData))
      {
        return base.GetData(out FData.TransformData);
      }
      return false;
    }

    public Boolean SetData(String name,
                           String version,
                           String date,
                           String company,
                           String street,
                           String streetnumber,
                           String postalcode,
                           String city,
                           Boolean discountuser,
                           String user,
                           Int32 productnumber,
                           String productkey)
    {
      FData.Company = company;
      FData.Street = street;
      FData.StreetNumber = streetnumber;
      FData.PostalCode = postalcode;
      FData.City = city;
      if (base.SetData(name, version, date, 
                       discountuser, user,
                       productnumber, productkey))
      {
        return base.GetData(out FData.TransformData);
      }
      return false;
    }

    public Boolean GetData(out RApplicationData data)
    {
      Boolean Result = base.GetData(out data.TransformData);
      data = FData;
      return Result;
    }

  }
}
