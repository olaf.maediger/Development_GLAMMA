﻿using System;
using System.Windows.Forms;
//
using Image24b;
//
namespace UCImageProcessing
{
  public partial class CUCImageProcessing : UserControl
  {
    private CImage24b FImage24b;
    public CUCImageProcessing()
    {
      InitializeComponent();
      //
      FImage24b = new CImage24b();
    }

    private void btnLoadFromFile_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        LoadSourceFromFile(DialogLoadImage.FileName);
      }
    }
    private void btnRotate_Click(object sender, EventArgs e)
    {
      RotateProjection((float)nudRotate.Value);
    }


    public Boolean LoadSourceFromFile(String filename)
    {
      Boolean Result = FImage24b.LoadSourceFromFile(filename);
      Result &= FImage24b.NormalizeSource(cbxGrayScale.Checked);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapNormalized();
      }
      return Result;
    }

    public Boolean NormalizeSource()
    {
      Boolean Result = FImage24b.NormalizeSource(cbxGrayScale.Checked);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapNormalized();
      }
      return Result;
    }

    public Boolean TranslateProjection(Int32 dx, Int32 dy)
    {
      Boolean Result = FImage24b.TranslateProjection(dx, dy);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    public Boolean RotateProjection(float angledeg)
    {
      Boolean Result = FImage24b.RotateProjection(angledeg);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }





  }
}
