﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCNotifier
{
  public partial class CUCListBox : ListBox
  {
    public CUCListBox()
    {
      InitializeComponent();
      ResizeRedraw = true;
      DoubleBuffered = true;
      BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      IntegralHeight = false;
      Font = new Font("Courier New", 12, FontStyle.Regular);
      // ... DrawMode = DrawMode.OwnerDrawVariable;
      // ... ItemHeight = 13;
      //
      // debug Items.Add("IT01-A|9111218|1MX3-KN0E6B-09A9C-Y6RW");
      // debug Items.Add("IT01-B|9111219|8QZS-PHDC7U-6WY63-KZS8");
      // debug Items.Add("IT01-C|9111220|ZS8Q-9A9A0Q-TX31S-31MX");
    }
    /*
    protected override void OnDrawItem(DrawItemEventArgs e)
    { 
      if ((0 <= e.Index) && (e.Index < Items.Count))
      {
        Object Line = Items[e.Index];
        e.DrawBackground();
        Int32 L = e.Bounds.Location.X;
        Int32 T = e.Bounds.Location.Y;
        e.Graphics.DrawString((String)Line, e.Font, new SolidBrush(e.ForeColor), L, T);
        L += e.Bounds.Width / 3;
        e.Graphics.DrawString((String)Line, e.Font, new SolidBrush(e.ForeColor), L, T);
        L += e.Bounds.Width / 3;
        e.Graphics.DrawString((String)Line, e.Font, new SolidBrush(e.ForeColor), L, T);
        // NC e.DrawFocusRectangle();
      }
    }*/
    /*
    protected override void OnSelectedIndexChanged(EventArgs e)
    {
      //base.OnSelectedIndexChanged(e);
      Refresh();
    }*/

    /*
    protected override void  OnMeasureItem(MeasureItemEventArgs e)
    {
      if (e.Index == SelectedIndex)
      {
        e.ItemHeight = this.ItemHeight;
      } else
      {
        e.ItemHeight = this.ItemHeight;
      }
    }*/

  }
}
