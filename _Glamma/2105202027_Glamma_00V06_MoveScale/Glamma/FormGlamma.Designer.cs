﻿namespace Glamma
{
  partial class FormClient
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlProtocol = new System.Windows.Forms.Panel();
      this.FUCSerialNumber = new UCSerialNumber.CUCSerialNumber();
      this.splProtocol = new System.Windows.Forms.Splitter();
      this.tmrStartup = new System.Windows.Forms.Timer(this.components);
      this.FHelpProvider = new System.Windows.Forms.HelpProvider();
      this.DialogSaveInitdata = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadInitdata = new System.Windows.Forms.OpenFileDialog();
      this.mstMain = new System.Windows.Forms.MenuStrip();
      this.mitSystem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSQuit = new System.Windows.Forms.ToolStripMenuItem();
      this.mitConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCDefault = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCResetToDefaultConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveAutomaticAtProgramEnd = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCStartup = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditStartupConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem7 = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCLoadSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCSaveSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCShowEditSelectableConfiguration = new System.Windows.Forms.ToolStripMenuItem();
      this.mitElevator = new System.Windows.Forms.ToolStripMenuItem();
      this.mitStartSimulation = new System.Windows.Forms.ToolStripMenuItem();
      this.mitProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPEditParameter = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.diagnosticToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPLoadDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitPSaveDiagnosticFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSendDiagnosticEmail = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHelp = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHContents = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHTopic = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHSearch = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterApplicationProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHEnterDeviceProductKey = new System.Windows.Forms.ToolStripMenuItem();
      this.mitHAbout = new System.Windows.Forms.ToolStripMenuItem();
      this.cbxAutomate = new System.Windows.Forms.CheckBox();
      this.tmrAutomation = new System.Windows.Forms.Timer(this.components);
      this.DialogLoadImage = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveImage = new System.Windows.Forms.SaveFileDialog();
      this.cbxDebug = new System.Windows.Forms.CheckBox();
      this.tbcGlamma = new System.Windows.Forms.TabControl();
      this.tbpTest = new System.Windows.Forms.TabPage();
      this.nudMoveV = new System.Windows.Forms.NumericUpDown();
      this.btnRescale = new System.Windows.Forms.Button();
      this.nudMoveH = new System.Windows.Forms.NumericUpDown();
      this.btnLoadFromFile = new System.Windows.Forms.Button();
      this.pnlImageScrollZoom = new System.Windows.Forms.Panel();
      this.pbxTest = new System.Windows.Forms.PictureBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.scbScale = new System.Windows.Forms.HScrollBar();
      this.scbMoveH = new System.Windows.Forms.HScrollBar();
      this.pnlLS = new System.Windows.Forms.Panel();
      this.scbMoveV = new System.Windows.Forms.VScrollBar();
      this.vScrollBar1 = new System.Windows.Forms.VScrollBar();
      this.pnlLB = new System.Windows.Forms.Panel();
      this.tbpImageProcessing = new System.Windows.Forms.TabPage();
      this.pnlImage = new System.Windows.Forms.Panel();
      this.pbxImage = new System.Windows.Forms.PictureBox();
      this.pnlInput = new System.Windows.Forms.Panel();
      this.scbColumn2048x512 = new System.Windows.Forms.HScrollBar();
      this.nudColumn2048x512 = new System.Windows.Forms.NumericUpDown();
      this.scbColumn2048x256 = new System.Windows.Forms.HScrollBar();
      this.nudColumn2048x256 = new System.Windows.Forms.NumericUpDown();
      this.btnColumn2048x256 = new System.Windows.Forms.Button();
      this.btnColumn2048x512 = new System.Windows.Forms.Button();
      this.cbxGrayScale = new System.Windows.Forms.CheckBox();
      this.btnConvert256x512 = new System.Windows.Forms.Button();
      this.btnConvert512x512 = new System.Windows.Forms.Button();
      this.btnConvert4096x256 = new System.Windows.Forms.Button();
      this.btnConvert4096x512 = new System.Windows.Forms.Button();
      this.btnConvert512x256 = new System.Windows.Forms.Button();
      this.btnConvert256x256 = new System.Windows.Forms.Button();
      this.btnLoadImage = new System.Windows.Forms.Button();
      this.tbpLaserAreaScanner = new System.Windows.Forms.TabPage();
      this.tbpLowLevelGlamma = new System.Windows.Forms.TabPage();
      this.tbpHighLevelGlamma = new System.Windows.Forms.TabPage();
      this.nudScale = new System.Windows.Forms.NumericUpDown();
      this.pnlProtocol.SuspendLayout();
      this.mstMain.SuspendLayout();
      this.tbcGlamma.SuspendLayout();
      this.tbpTest.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudMoveV)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMoveH)).BeginInit();
      this.pnlImageScrollZoom.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxTest)).BeginInit();
      this.panel1.SuspendLayout();
      this.pnlLS.SuspendLayout();
      this.tbpImageProcessing.SuspendLayout();
      this.pnlImage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
      this.pnlInput.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x512)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x256)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScale)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlProtocol
      // 
      this.pnlProtocol.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlProtocol.Controls.Add(this.FUCSerialNumber);
      this.pnlProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlProtocol.Location = new System.Drawing.Point(0, 778);
      this.pnlProtocol.Name = "pnlProtocol";
      this.pnlProtocol.Size = new System.Drawing.Size(808, 92);
      this.pnlProtocol.TabIndex = 138;
      // 
      // FUCSerialNumber
      // 
      this.FUCSerialNumber.Dock = System.Windows.Forms.DockStyle.Top;
      this.FUCSerialNumber.Location = new System.Drawing.Point(0, 0);
      this.FUCSerialNumber.Name = "FUCSerialNumber";
      this.FUCSerialNumber.Size = new System.Drawing.Size(806, 64);
      this.FUCSerialNumber.TabIndex = 113;
      this.FUCSerialNumber.Visible = false;
      // 
      // splProtocol
      // 
      this.splProtocol.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.splProtocol.Location = new System.Drawing.Point(0, 775);
      this.splProtocol.Name = "splProtocol";
      this.splProtocol.Size = new System.Drawing.Size(808, 3);
      this.splProtocol.TabIndex = 139;
      this.splProtocol.TabStop = false;
      // 
      // tmrStartup
      // 
      this.tmrStartup.Interval = 1;
      // 
      // DialogSaveInitdata
      // 
      this.DialogSaveInitdata.DefaultExt = "ini.xml";
      this.DialogSaveInitdata.FileName = "Initdata";
      this.DialogSaveInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogSaveInitdata.Title = "MDesign: Save Initdata";
      // 
      // DialogLoadInitdata
      // 
      this.DialogLoadInitdata.DefaultExt = "ini.xml";
      this.DialogLoadInitdata.FileName = "Initdata";
      this.DialogLoadInitdata.Filter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      this.DialogLoadInitdata.Title = "MDesign: Load Initdata";
      // 
      // mstMain
      // 
      this.mstMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSystem,
            this.mitConfiguration,
            this.mitElevator,
            this.mitProtocol,
            this.mitHelp});
      this.mstMain.Location = new System.Drawing.Point(0, 0);
      this.mstMain.Name = "mstMain";
      this.mstMain.Size = new System.Drawing.Size(808, 24);
      this.mstMain.TabIndex = 140;
      this.mstMain.Text = "System";
      // 
      // mitSystem
      // 
      this.mitSystem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSQuit});
      this.mitSystem.Name = "mitSystem";
      this.mitSystem.Size = new System.Drawing.Size(57, 20);
      this.mitSystem.Text = "&System";
      // 
      // mitSQuit
      // 
      this.mitSQuit.Name = "mitSQuit";
      this.mitSQuit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
      this.mitSQuit.Size = new System.Drawing.Size(140, 22);
      this.mitSQuit.Text = "&Quit";
      // 
      // mitConfiguration
      // 
      this.mitConfiguration.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitCDefault,
            this.mitCResetToDefaultConfiguration,
            this.mitCSaveAutomaticAtProgramEnd,
            this.mitCStartup,
            this.mitCLoadStartupConfiguration,
            this.mitCSaveStartupConfiguration,
            this.mitCShowEditStartupConfiguration,
            this.toolStripMenuItem7,
            this.mitCLoadSelectableConfiguration,
            this.mitCSaveSelectableConfiguration,
            this.mitCShowEditSelectableConfiguration});
      this.mitConfiguration.Name = "mitConfiguration";
      this.mitConfiguration.Size = new System.Drawing.Size(93, 20);
      this.mitConfiguration.Text = "&Configuration";
      // 
      // mitCDefault
      // 
      this.mitCDefault.Enabled = false;
      this.mitCDefault.Name = "mitCDefault";
      this.mitCDefault.Size = new System.Drawing.Size(300, 22);
      this.mitCDefault.Text = "- Default -";
      // 
      // mitCResetToDefaultConfiguration
      // 
      this.mitCResetToDefaultConfiguration.Name = "mitCResetToDefaultConfiguration";
      this.mitCResetToDefaultConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Alt) 
            | System.Windows.Forms.Keys.R)));
      this.mitCResetToDefaultConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCResetToDefaultConfiguration.Text = "Reset to Default-Configuration";
      // 
      // mitCSaveAutomaticAtProgramEnd
      // 
      this.mitCSaveAutomaticAtProgramEnd.Name = "mitCSaveAutomaticAtProgramEnd";
      this.mitCSaveAutomaticAtProgramEnd.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveAutomaticAtProgramEnd.Text = "Save automatic at program end";
      // 
      // mitCStartup
      // 
      this.mitCStartup.Enabled = false;
      this.mitCStartup.Name = "mitCStartup";
      this.mitCStartup.Size = new System.Drawing.Size(300, 22);
      this.mitCStartup.Text = "- Startup -";
      // 
      // mitCLoadStartupConfiguration
      // 
      this.mitCLoadStartupConfiguration.Name = "mitCLoadStartupConfiguration";
      this.mitCLoadStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.L)));
      this.mitCLoadStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadStartupConfiguration.Text = "Load Startup-Configuration";
      // 
      // mitCSaveStartupConfiguration
      // 
      this.mitCSaveStartupConfiguration.Name = "mitCSaveStartupConfiguration";
      this.mitCSaveStartupConfiguration.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
      this.mitCSaveStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveStartupConfiguration.Text = "Save Startup-Configuration";
      // 
      // mitCShowEditStartupConfiguration
      // 
      this.mitCShowEditStartupConfiguration.Name = "mitCShowEditStartupConfiguration";
      this.mitCShowEditStartupConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditStartupConfiguration.Text = "Show / Edit Startup-Configuration";
      // 
      // toolStripMenuItem7
      // 
      this.toolStripMenuItem7.Enabled = false;
      this.toolStripMenuItem7.Name = "toolStripMenuItem7";
      this.toolStripMenuItem7.Size = new System.Drawing.Size(300, 22);
      this.toolStripMenuItem7.Text = "- Named -";
      // 
      // mitCLoadSelectableConfiguration
      // 
      this.mitCLoadSelectableConfiguration.Name = "mitCLoadSelectableConfiguration";
      this.mitCLoadSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCLoadSelectableConfiguration.Text = "Load Selectable-Configuration ";
      // 
      // mitCSaveSelectableConfiguration
      // 
      this.mitCSaveSelectableConfiguration.Name = "mitCSaveSelectableConfiguration";
      this.mitCSaveSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCSaveSelectableConfiguration.Text = "Save Selectable-Configuration";
      // 
      // mitCShowEditSelectableConfiguration
      // 
      this.mitCShowEditSelectableConfiguration.Name = "mitCShowEditSelectableConfiguration";
      this.mitCShowEditSelectableConfiguration.Size = new System.Drawing.Size(300, 22);
      this.mitCShowEditSelectableConfiguration.Text = "Show / Edit Selectable-Configuration";
      // 
      // mitElevator
      // 
      this.mitElevator.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitStartSimulation});
      this.mitElevator.Name = "mitElevator";
      this.mitElevator.Size = new System.Drawing.Size(106, 20);
      this.mitElevator.Text = "Communication";
      // 
      // mitStartSimulation
      // 
      this.mitStartSimulation.Name = "mitStartSimulation";
      this.mitStartSimulation.Size = new System.Drawing.Size(98, 22);
      this.mitStartSimulation.Text = "Start";
      // 
      // mitProtocol
      // 
      this.mitProtocol.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitPEditParameter,
            this.mitPClearProtocol,
            this.mitPCopyToClipboard,
            this.diagnosticToolStripMenuItem,
            this.mitPLoadDiagnosticFile,
            this.mitPSaveDiagnosticFile,
            this.mitSendDiagnosticEmail});
      this.mitProtocol.Name = "mitProtocol";
      this.mitProtocol.Size = new System.Drawing.Size(64, 20);
      this.mitProtocol.Text = "&Protocol";
      // 
      // mitPEditParameter
      // 
      this.mitPEditParameter.Name = "mitPEditParameter";
      this.mitPEditParameter.Size = new System.Drawing.Size(171, 22);
      this.mitPEditParameter.Text = "Edit Parameter";
      // 
      // mitPClearProtocol
      // 
      this.mitPClearProtocol.Name = "mitPClearProtocol";
      this.mitPClearProtocol.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
      this.mitPClearProtocol.Size = new System.Drawing.Size(171, 22);
      this.mitPClearProtocol.Text = "Clear";
      // 
      // mitPCopyToClipboard
      // 
      this.mitPCopyToClipboard.Name = "mitPCopyToClipboard";
      this.mitPCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitPCopyToClipboard.Text = "Copy to Clipboard";
      // 
      // diagnosticToolStripMenuItem
      // 
      this.diagnosticToolStripMenuItem.Enabled = false;
      this.diagnosticToolStripMenuItem.Name = "diagnosticToolStripMenuItem";
      this.diagnosticToolStripMenuItem.Size = new System.Drawing.Size(171, 22);
      this.diagnosticToolStripMenuItem.Text = "- Diagnostic -";
      // 
      // mitPLoadDiagnosticFile
      // 
      this.mitPLoadDiagnosticFile.Name = "mitPLoadDiagnosticFile";
      this.mitPLoadDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPLoadDiagnosticFile.Text = "Load File";
      // 
      // mitPSaveDiagnosticFile
      // 
      this.mitPSaveDiagnosticFile.Name = "mitPSaveDiagnosticFile";
      this.mitPSaveDiagnosticFile.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
      this.mitPSaveDiagnosticFile.Size = new System.Drawing.Size(171, 22);
      this.mitPSaveDiagnosticFile.Text = "Save File";
      // 
      // mitSendDiagnosticEmail
      // 
      this.mitSendDiagnosticEmail.Name = "mitSendDiagnosticEmail";
      this.mitSendDiagnosticEmail.Size = new System.Drawing.Size(171, 22);
      this.mitSendDiagnosticEmail.Text = "Send Email";
      // 
      // mitHelp
      // 
      this.mitHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitHContents,
            this.mitHTopic,
            this.mitHSearch,
            this.mitHEnterApplicationProductKey,
            this.mitHEnterDeviceProductKey,
            this.mitHAbout});
      this.mitHelp.Name = "mitHelp";
      this.mitHelp.Size = new System.Drawing.Size(44, 20);
      this.mitHelp.Text = "Help";
      // 
      // mitHContents
      // 
      this.mitHContents.Name = "mitHContents";
      this.mitHContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.mitHContents.Size = new System.Drawing.Size(234, 22);
      this.mitHContents.Text = "Contents";
      // 
      // mitHTopic
      // 
      this.mitHTopic.Name = "mitHTopic";
      this.mitHTopic.Size = new System.Drawing.Size(234, 22);
      this.mitHTopic.Text = "Topic";
      // 
      // mitHSearch
      // 
      this.mitHSearch.Name = "mitHSearch";
      this.mitHSearch.Size = new System.Drawing.Size(234, 22);
      this.mitHSearch.Text = "Search";
      // 
      // mitHEnterApplicationProductKey
      // 
      this.mitHEnterApplicationProductKey.Name = "mitHEnterApplicationProductKey";
      this.mitHEnterApplicationProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterApplicationProductKey.Text = "Enter Application Product-Key";
      // 
      // mitHEnterDeviceProductKey
      // 
      this.mitHEnterDeviceProductKey.Name = "mitHEnterDeviceProductKey";
      this.mitHEnterDeviceProductKey.Size = new System.Drawing.Size(234, 22);
      this.mitHEnterDeviceProductKey.Text = "Enter Device Product-Key";
      // 
      // mitHAbout
      // 
      this.mitHAbout.Name = "mitHAbout";
      this.mitHAbout.ShortcutKeys = System.Windows.Forms.Keys.F2;
      this.mitHAbout.Size = new System.Drawing.Size(234, 22);
      this.mitHAbout.Text = "About";
      // 
      // cbxAutomate
      // 
      this.cbxAutomate.AutoSize = true;
      this.cbxAutomate.Location = new System.Drawing.Point(376, 5);
      this.cbxAutomate.Name = "cbxAutomate";
      this.cbxAutomate.Size = new System.Drawing.Size(71, 17);
      this.cbxAutomate.TabIndex = 142;
      this.cbxAutomate.Text = "Automate";
      this.cbxAutomate.UseVisualStyleBackColor = true;
      // 
      // tmrAutomation
      // 
      this.tmrAutomation.Interval = 1000;
      // 
      // DialogLoadImage
      // 
      this.DialogLoadImage.DefaultExt = "bmp";
      this.DialogLoadImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogLoadImage.Title = "Load Image";
      // 
      // DialogSaveImage
      // 
      this.DialogSaveImage.DefaultExt = "bmp";
      this.DialogSaveImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogSaveImage.Title = "Save Image";
      // 
      // cbxDebug
      // 
      this.cbxDebug.AutoSize = true;
      this.cbxDebug.Location = new System.Drawing.Point(453, 5);
      this.cbxDebug.Name = "cbxDebug";
      this.cbxDebug.Size = new System.Drawing.Size(58, 17);
      this.cbxDebug.TabIndex = 143;
      this.cbxDebug.Text = "Debug";
      this.cbxDebug.UseVisualStyleBackColor = true;
      // 
      // tbcGlamma
      // 
      this.tbcGlamma.Alignment = System.Windows.Forms.TabAlignment.Bottom;
      this.tbcGlamma.Controls.Add(this.tbpTest);
      this.tbcGlamma.Controls.Add(this.tbpImageProcessing);
      this.tbcGlamma.Controls.Add(this.tbpLaserAreaScanner);
      this.tbcGlamma.Controls.Add(this.tbpLowLevelGlamma);
      this.tbcGlamma.Controls.Add(this.tbpHighLevelGlamma);
      this.tbcGlamma.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tbcGlamma.Location = new System.Drawing.Point(0, 24);
      this.tbcGlamma.Name = "tbcGlamma";
      this.tbcGlamma.SelectedIndex = 0;
      this.tbcGlamma.Size = new System.Drawing.Size(808, 751);
      this.tbcGlamma.TabIndex = 144;
      // 
      // tbpTest
      // 
      this.tbpTest.BackColor = System.Drawing.Color.Linen;
      this.tbpTest.Controls.Add(this.nudScale);
      this.tbpTest.Controls.Add(this.nudMoveV);
      this.tbpTest.Controls.Add(this.btnRescale);
      this.tbpTest.Controls.Add(this.nudMoveH);
      this.tbpTest.Controls.Add(this.btnLoadFromFile);
      this.tbpTest.Controls.Add(this.pnlImageScrollZoom);
      this.tbpTest.Location = new System.Drawing.Point(4, 4);
      this.tbpTest.Name = "tbpTest";
      this.tbpTest.Size = new System.Drawing.Size(800, 725);
      this.tbpTest.TabIndex = 4;
      this.tbpTest.Text = "Test";
      // 
      // nudMoveV
      // 
      this.nudMoveV.Location = new System.Drawing.Point(241, 670);
      this.nudMoveV.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
      this.nudMoveV.Name = "nudMoveV";
      this.nudMoveV.Size = new System.Drawing.Size(51, 20);
      this.nudMoveV.TabIndex = 14;
      this.nudMoveV.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMoveV.ValueChanged += new System.EventHandler(this.nudMoveV_ValueChanged);
      // 
      // btnRescale
      // 
      this.btnRescale.Location = new System.Drawing.Point(103, 668);
      this.btnRescale.Name = "btnRescale";
      this.btnRescale.Size = new System.Drawing.Size(75, 24);
      this.btnRescale.TabIndex = 13;
      this.btnRescale.Text = "Rescale";
      this.btnRescale.UseVisualStyleBackColor = true;
      this.btnRescale.Click += new System.EventHandler(this.btnRescale_Click);
      // 
      // nudMoveH
      // 
      this.nudMoveH.Location = new System.Drawing.Point(184, 670);
      this.nudMoveH.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
      this.nudMoveH.Name = "nudMoveH";
      this.nudMoveH.Size = new System.Drawing.Size(51, 20);
      this.nudMoveH.TabIndex = 12;
      this.nudMoveH.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudMoveH.ValueChanged += new System.EventHandler(this.nudMoveH_ValueChanged);
      // 
      // btnLoadFromFile
      // 
      this.btnLoadFromFile.Location = new System.Drawing.Point(20, 668);
      this.btnLoadFromFile.Name = "btnLoadFromFile";
      this.btnLoadFromFile.Size = new System.Drawing.Size(75, 24);
      this.btnLoadFromFile.TabIndex = 1;
      this.btnLoadFromFile.Text = "Load";
      this.btnLoadFromFile.UseVisualStyleBackColor = true;
      this.btnLoadFromFile.Click += new System.EventHandler(this.btnLoadFromFile_Click);
      // 
      // pnlImageScrollZoom
      // 
      this.pnlImageScrollZoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlImageScrollZoom.Controls.Add(this.pbxTest);
      this.pnlImageScrollZoom.Controls.Add(this.panel1);
      this.pnlImageScrollZoom.Controls.Add(this.pnlLS);
      this.pnlImageScrollZoom.Location = new System.Drawing.Point(18, 13);
      this.pnlImageScrollZoom.Name = "pnlImageScrollZoom";
      this.pnlImageScrollZoom.Size = new System.Drawing.Size(645, 641);
      this.pnlImageScrollZoom.TabIndex = 0;
      // 
      // pbxTest
      // 
      this.pbxTest.BackColor = System.Drawing.Color.LightSalmon;
      this.pbxTest.Location = new System.Drawing.Point(35, 0);
      this.pbxTest.Name = "pbxTest";
      this.pbxTest.Size = new System.Drawing.Size(518, 518);
      this.pbxTest.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
      this.pbxTest.TabIndex = 6;
      this.pbxTest.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.scbScale);
      this.panel1.Controls.Add(this.scbMoveH);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(35, 603);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(608, 36);
      this.panel1.TabIndex = 5;
      // 
      // scbScale
      // 
      this.scbScale.Dock = System.Windows.Forms.DockStyle.Top;
      this.scbScale.Location = new System.Drawing.Point(0, 17);
      this.scbScale.Maximum = 109;
      this.scbScale.Minimum = -100;
      this.scbScale.Name = "scbScale";
      this.scbScale.Size = new System.Drawing.Size(608, 17);
      this.scbScale.TabIndex = 3;
      this.scbScale.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scbScale_Scroll);
      // 
      // scbMoveH
      // 
      this.scbMoveH.Dock = System.Windows.Forms.DockStyle.Top;
      this.scbMoveH.Location = new System.Drawing.Point(0, 0);
      this.scbMoveH.Maximum = 109;
      this.scbMoveH.Minimum = -100;
      this.scbMoveH.Name = "scbMoveH";
      this.scbMoveH.Size = new System.Drawing.Size(608, 17);
      this.scbMoveH.TabIndex = 2;
      this.scbMoveH.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scbMoveH_Scroll);
      // 
      // pnlLS
      // 
      this.pnlLS.Controls.Add(this.scbMoveV);
      this.pnlLS.Controls.Add(this.vScrollBar1);
      this.pnlLS.Controls.Add(this.pnlLB);
      this.pnlLS.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLS.Location = new System.Drawing.Point(0, 0);
      this.pnlLS.Name = "pnlLS";
      this.pnlLS.Size = new System.Drawing.Size(35, 639);
      this.pnlLS.TabIndex = 4;
      // 
      // scbMoveV
      // 
      this.scbMoveV.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbMoveV.Location = new System.Drawing.Point(17, 0);
      this.scbMoveV.Maximum = 109;
      this.scbMoveV.Minimum = -100;
      this.scbMoveV.Name = "scbMoveV";
      this.scbMoveV.Size = new System.Drawing.Size(17, 601);
      this.scbMoveV.TabIndex = 4;
      this.scbMoveV.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scbMoveV_Scroll);
      // 
      // vScrollBar1
      // 
      this.vScrollBar1.Dock = System.Windows.Forms.DockStyle.Left;
      this.vScrollBar1.Location = new System.Drawing.Point(0, 0);
      this.vScrollBar1.Name = "vScrollBar1";
      this.vScrollBar1.Size = new System.Drawing.Size(17, 601);
      this.vScrollBar1.TabIndex = 3;
      // 
      // pnlLB
      // 
      this.pnlLB.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlLB.Location = new System.Drawing.Point(0, 601);
      this.pnlLB.Name = "pnlLB";
      this.pnlLB.Size = new System.Drawing.Size(35, 38);
      this.pnlLB.TabIndex = 1;
      // 
      // tbpImageProcessing
      // 
      this.tbpImageProcessing.Controls.Add(this.pnlImage);
      this.tbpImageProcessing.Controls.Add(this.pnlInput);
      this.tbpImageProcessing.Location = new System.Drawing.Point(4, 4);
      this.tbpImageProcessing.Name = "tbpImageProcessing";
      this.tbpImageProcessing.Padding = new System.Windows.Forms.Padding(3);
      this.tbpImageProcessing.Size = new System.Drawing.Size(800, 725);
      this.tbpImageProcessing.TabIndex = 0;
      this.tbpImageProcessing.Text = "ImageProcessing";
      this.tbpImageProcessing.UseVisualStyleBackColor = true;
      // 
      // pnlImage
      // 
      this.pnlImage.AutoScroll = true;
      this.pnlImage.BackColor = System.Drawing.Color.AntiqueWhite;
      this.pnlImage.Controls.Add(this.pbxImage);
      this.pnlImage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlImage.Location = new System.Drawing.Point(3, 3);
      this.pnlImage.Name = "pnlImage";
      this.pnlImage.Size = new System.Drawing.Size(794, 631);
      this.pnlImage.TabIndex = 5;
      // 
      // pbxImage
      // 
      this.pbxImage.BackColor = System.Drawing.Color.Gainsboro;
      this.pbxImage.Location = new System.Drawing.Point(5, 5);
      this.pbxImage.Name = "pbxImage";
      this.pbxImage.Size = new System.Drawing.Size(512, 512);
      this.pbxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbxImage.TabIndex = 1;
      this.pbxImage.TabStop = false;
      // 
      // pnlInput
      // 
      this.pnlInput.Controls.Add(this.scbColumn2048x512);
      this.pnlInput.Controls.Add(this.nudColumn2048x512);
      this.pnlInput.Controls.Add(this.scbColumn2048x256);
      this.pnlInput.Controls.Add(this.nudColumn2048x256);
      this.pnlInput.Controls.Add(this.btnColumn2048x256);
      this.pnlInput.Controls.Add(this.btnColumn2048x512);
      this.pnlInput.Controls.Add(this.cbxGrayScale);
      this.pnlInput.Controls.Add(this.btnConvert256x512);
      this.pnlInput.Controls.Add(this.btnConvert512x512);
      this.pnlInput.Controls.Add(this.btnConvert4096x256);
      this.pnlInput.Controls.Add(this.btnConvert4096x512);
      this.pnlInput.Controls.Add(this.btnConvert512x256);
      this.pnlInput.Controls.Add(this.btnConvert256x256);
      this.pnlInput.Controls.Add(this.btnLoadImage);
      this.pnlInput.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlInput.Location = new System.Drawing.Point(3, 634);
      this.pnlInput.Name = "pnlInput";
      this.pnlInput.Size = new System.Drawing.Size(794, 88);
      this.pnlInput.TabIndex = 4;
      // 
      // scbColumn2048x512
      // 
      this.scbColumn2048x512.Location = new System.Drawing.Point(592, 59);
      this.scbColumn2048x512.Maximum = 264;
      this.scbColumn2048x512.Name = "scbColumn2048x512";
      this.scbColumn2048x512.Size = new System.Drawing.Size(182, 20);
      this.scbColumn2048x512.TabIndex = 14;
      this.scbColumn2048x512.ValueChanged += new System.EventHandler(this.scbColumn2048x512_ValueChanged);
      // 
      // nudColumn2048x512
      // 
      this.nudColumn2048x512.Location = new System.Drawing.Point(536, 60);
      this.nudColumn2048x512.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudColumn2048x512.Name = "nudColumn2048x512";
      this.nudColumn2048x512.Size = new System.Drawing.Size(51, 20);
      this.nudColumn2048x512.TabIndex = 13;
      this.nudColumn2048x512.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudColumn2048x512.ValueChanged += new System.EventHandler(this.nudColumn2048x512_ValueChanged);
      // 
      // scbColumn2048x256
      // 
      this.scbColumn2048x256.Location = new System.Drawing.Point(592, 34);
      this.scbColumn2048x256.Maximum = 264;
      this.scbColumn2048x256.Name = "scbColumn2048x256";
      this.scbColumn2048x256.Size = new System.Drawing.Size(182, 20);
      this.scbColumn2048x256.TabIndex = 12;
      this.scbColumn2048x256.Scroll += new System.Windows.Forms.ScrollEventHandler(this.scbColumn2048x256_Scroll);
      // 
      // nudColumn2048x256
      // 
      this.nudColumn2048x256.Location = new System.Drawing.Point(536, 35);
      this.nudColumn2048x256.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudColumn2048x256.Name = "nudColumn2048x256";
      this.nudColumn2048x256.Size = new System.Drawing.Size(51, 20);
      this.nudColumn2048x256.TabIndex = 11;
      this.nudColumn2048x256.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudColumn2048x256.ValueChanged += new System.EventHandler(this.nudColumn2048x256_ValueChanged);
      // 
      // btnColumn2048x256
      // 
      this.btnColumn2048x256.Location = new System.Drawing.Point(407, 32);
      this.btnColumn2048x256.Name = "btnColumn2048x256";
      this.btnColumn2048x256.Size = new System.Drawing.Size(123, 23);
      this.btnColumn2048x256.TabIndex = 10;
      this.btnColumn2048x256.Text = "Column 2048x256";
      this.btnColumn2048x256.UseVisualStyleBackColor = true;
      this.btnColumn2048x256.Click += new System.EventHandler(this.btnColumn2048x256_Click);
      // 
      // btnColumn2048x512
      // 
      this.btnColumn2048x512.Location = new System.Drawing.Point(407, 59);
      this.btnColumn2048x512.Name = "btnColumn2048x512";
      this.btnColumn2048x512.Size = new System.Drawing.Size(123, 23);
      this.btnColumn2048x512.TabIndex = 9;
      this.btnColumn2048x512.Text = "Column 2048x512";
      this.btnColumn2048x512.UseVisualStyleBackColor = true;
      this.btnColumn2048x512.Click += new System.EventHandler(this.btnColumn2048x512_Click);
      // 
      // cbxGrayScale
      // 
      this.cbxGrayScale.AutoSize = true;
      this.cbxGrayScale.Location = new System.Drawing.Point(135, 9);
      this.cbxGrayScale.Name = "cbxGrayScale";
      this.cbxGrayScale.Size = new System.Drawing.Size(75, 17);
      this.cbxGrayScale.TabIndex = 8;
      this.cbxGrayScale.Text = "GrayScale";
      this.cbxGrayScale.UseVisualStyleBackColor = true;
      // 
      // btnConvert256x512
      // 
      this.btnConvert256x512.Location = new System.Drawing.Point(5, 59);
      this.btnConvert256x512.Name = "btnConvert256x512";
      this.btnConvert256x512.Size = new System.Drawing.Size(123, 23);
      this.btnConvert256x512.TabIndex = 7;
      this.btnConvert256x512.Text = "Convert 256x512";
      this.btnConvert256x512.UseVisualStyleBackColor = true;
      this.btnConvert256x512.Click += new System.EventHandler(this.btnConvert256x512_Click);
      // 
      // btnConvert512x512
      // 
      this.btnConvert512x512.Location = new System.Drawing.Point(134, 59);
      this.btnConvert512x512.Name = "btnConvert512x512";
      this.btnConvert512x512.Size = new System.Drawing.Size(123, 23);
      this.btnConvert512x512.TabIndex = 6;
      this.btnConvert512x512.Text = "Convert 512x512";
      this.btnConvert512x512.UseVisualStyleBackColor = true;
      this.btnConvert512x512.Click += new System.EventHandler(this.btnConvert512x512_Click);
      // 
      // btnConvert4096x256
      // 
      this.btnConvert4096x256.Location = new System.Drawing.Point(263, 32);
      this.btnConvert4096x256.Name = "btnConvert4096x256";
      this.btnConvert4096x256.Size = new System.Drawing.Size(123, 23);
      this.btnConvert4096x256.TabIndex = 5;
      this.btnConvert4096x256.Text = "Convert 4096x256";
      this.btnConvert4096x256.UseVisualStyleBackColor = true;
      this.btnConvert4096x256.Click += new System.EventHandler(this.btnConvert4096x256_Click);
      // 
      // btnConvert4096x512
      // 
      this.btnConvert4096x512.Location = new System.Drawing.Point(263, 59);
      this.btnConvert4096x512.Name = "btnConvert4096x512";
      this.btnConvert4096x512.Size = new System.Drawing.Size(123, 23);
      this.btnConvert4096x512.TabIndex = 4;
      this.btnConvert4096x512.Text = "Convert 4096x512";
      this.btnConvert4096x512.UseVisualStyleBackColor = true;
      this.btnConvert4096x512.Click += new System.EventHandler(this.btnConvert4096x512_Click);
      // 
      // btnConvert512x256
      // 
      this.btnConvert512x256.Location = new System.Drawing.Point(134, 32);
      this.btnConvert512x256.Name = "btnConvert512x256";
      this.btnConvert512x256.Size = new System.Drawing.Size(123, 23);
      this.btnConvert512x256.TabIndex = 3;
      this.btnConvert512x256.Text = "Convert 512x256";
      this.btnConvert512x256.UseVisualStyleBackColor = true;
      this.btnConvert512x256.Click += new System.EventHandler(this.btnConvert512x256_Click);
      // 
      // btnConvert256x256
      // 
      this.btnConvert256x256.Location = new System.Drawing.Point(5, 32);
      this.btnConvert256x256.Name = "btnConvert256x256";
      this.btnConvert256x256.Size = new System.Drawing.Size(123, 23);
      this.btnConvert256x256.TabIndex = 2;
      this.btnConvert256x256.Text = "Convert 256x256";
      this.btnConvert256x256.UseVisualStyleBackColor = true;
      this.btnConvert256x256.Click += new System.EventHandler(this.btnConvert256x256_Click);
      // 
      // btnLoadImage
      // 
      this.btnLoadImage.Location = new System.Drawing.Point(5, 6);
      this.btnLoadImage.Name = "btnLoadImage";
      this.btnLoadImage.Size = new System.Drawing.Size(123, 23);
      this.btnLoadImage.TabIndex = 1;
      this.btnLoadImage.Text = "Load Image";
      this.btnLoadImage.UseVisualStyleBackColor = true;
      this.btnLoadImage.Click += new System.EventHandler(this.btnLoadImage_Click);
      // 
      // tbpLaserAreaScanner
      // 
      this.tbpLaserAreaScanner.Location = new System.Drawing.Point(4, 4);
      this.tbpLaserAreaScanner.Name = "tbpLaserAreaScanner";
      this.tbpLaserAreaScanner.Padding = new System.Windows.Forms.Padding(3);
      this.tbpLaserAreaScanner.Size = new System.Drawing.Size(800, 725);
      this.tbpLaserAreaScanner.TabIndex = 1;
      this.tbpLaserAreaScanner.Text = "LaserAreaScanner";
      this.tbpLaserAreaScanner.UseVisualStyleBackColor = true;
      // 
      // tbpLowLevelGlamma
      // 
      this.tbpLowLevelGlamma.Location = new System.Drawing.Point(4, 4);
      this.tbpLowLevelGlamma.Name = "tbpLowLevelGlamma";
      this.tbpLowLevelGlamma.Size = new System.Drawing.Size(800, 725);
      this.tbpLowLevelGlamma.TabIndex = 2;
      this.tbpLowLevelGlamma.Text = "LowLevelGlamma";
      this.tbpLowLevelGlamma.UseVisualStyleBackColor = true;
      // 
      // tbpHighLevelGlamma
      // 
      this.tbpHighLevelGlamma.Location = new System.Drawing.Point(4, 4);
      this.tbpHighLevelGlamma.Name = "tbpHighLevelGlamma";
      this.tbpHighLevelGlamma.Size = new System.Drawing.Size(800, 725);
      this.tbpHighLevelGlamma.TabIndex = 3;
      this.tbpHighLevelGlamma.Text = "HighLevelGlamma";
      this.tbpHighLevelGlamma.UseVisualStyleBackColor = true;
      // 
      // nudScale
      // 
      this.nudScale.Location = new System.Drawing.Point(315, 670);
      this.nudScale.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
      this.nudScale.Name = "nudScale";
      this.nudScale.Size = new System.Drawing.Size(51, 20);
      this.nudScale.TabIndex = 15;
      this.nudScale.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudScale.ValueChanged += new System.EventHandler(this.nudScale_ValueChanged);
      // 
      // FormClient
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(808, 870);
      this.Controls.Add(this.tbcGlamma);
      this.Controls.Add(this.cbxDebug);
      this.Controls.Add(this.cbxAutomate);
      this.Controls.Add(this.mstMain);
      this.Controls.Add(this.splProtocol);
      this.Controls.Add(this.pnlProtocol);
      this.Name = "FormClient";
      this.Text = "Form1";
      this.pnlProtocol.ResumeLayout(false);
      this.mstMain.ResumeLayout(false);
      this.mstMain.PerformLayout();
      this.tbcGlamma.ResumeLayout(false);
      this.tbpTest.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.nudMoveV)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudMoveH)).EndInit();
      this.pnlImageScrollZoom.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbxTest)).EndInit();
      this.panel1.ResumeLayout(false);
      this.pnlLS.ResumeLayout(false);
      this.tbpImageProcessing.ResumeLayout(false);
      this.pnlImage.ResumeLayout(false);
      this.pnlImage.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
      this.pnlInput.ResumeLayout(false);
      this.pnlInput.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x512)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x256)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScale)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Panel pnlProtocol;
    private UCSerialNumber.CUCSerialNumber FUCSerialNumber;
    private System.Windows.Forms.Splitter splProtocol;
    private System.Windows.Forms.Timer tmrStartup;
    private System.Windows.Forms.HelpProvider FHelpProvider;
    private System.Windows.Forms.SaveFileDialog DialogSaveInitdata;
    private System.Windows.Forms.OpenFileDialog DialogLoadInitdata;
    private System.Windows.Forms.MenuStrip mstMain;
    private System.Windows.Forms.ToolStripMenuItem mitSystem;
    private System.Windows.Forms.ToolStripMenuItem mitSQuit;
    private System.Windows.Forms.ToolStripMenuItem mitConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCDefault;
    private System.Windows.Forms.ToolStripMenuItem mitCResetToDefaultConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveAutomaticAtProgramEnd;
    private System.Windows.Forms.ToolStripMenuItem mitCStartup;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditStartupConfiguration;
    private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem7;
    private System.Windows.Forms.ToolStripMenuItem mitCLoadSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCSaveSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitCShowEditSelectableConfiguration;
    private System.Windows.Forms.ToolStripMenuItem mitElevator;
    private System.Windows.Forms.ToolStripMenuItem mitStartSimulation;
    private System.Windows.Forms.ToolStripMenuItem mitProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPEditParameter;
    private System.Windows.Forms.ToolStripMenuItem mitPClearProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitPCopyToClipboard;
    private System.Windows.Forms.ToolStripMenuItem diagnosticToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem mitPLoadDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitPSaveDiagnosticFile;
    private System.Windows.Forms.ToolStripMenuItem mitSendDiagnosticEmail;
    private System.Windows.Forms.ToolStripMenuItem mitHelp;
    private System.Windows.Forms.ToolStripMenuItem mitHContents;
    private System.Windows.Forms.ToolStripMenuItem mitHTopic;
    private System.Windows.Forms.ToolStripMenuItem mitHSearch;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterApplicationProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHEnterDeviceProductKey;
    private System.Windows.Forms.ToolStripMenuItem mitHAbout;
    private System.Windows.Forms.CheckBox cbxAutomate;
    private System.Windows.Forms.Timer tmrAutomation;
    private System.Windows.Forms.OpenFileDialog DialogLoadImage;
    private System.Windows.Forms.SaveFileDialog DialogSaveImage;
    private System.Windows.Forms.CheckBox cbxDebug;
    private System.Windows.Forms.TabControl tbcGlamma;
    private System.Windows.Forms.TabPage tbpImageProcessing;
    private System.Windows.Forms.TabPage tbpLaserAreaScanner;
    private System.Windows.Forms.TabPage tbpLowLevelGlamma;
    private System.Windows.Forms.TabPage tbpHighLevelGlamma;
    private System.Windows.Forms.Panel pnlInput;
    private System.Windows.Forms.Button btnConvert4096x256;
    private System.Windows.Forms.Button btnConvert4096x512;
    private System.Windows.Forms.Button btnConvert512x256;
    private System.Windows.Forms.Button btnConvert256x256;
    private System.Windows.Forms.Button btnLoadImage;
    private System.Windows.Forms.Button btnConvert512x512;
    private System.Windows.Forms.Button btnConvert256x512;
    private System.Windows.Forms.Panel pnlImage;
    private System.Windows.Forms.PictureBox pbxImage;
    private System.Windows.Forms.CheckBox cbxGrayScale;
    private System.Windows.Forms.Button btnColumn2048x256;
    private System.Windows.Forms.Button btnColumn2048x512;
    private System.Windows.Forms.HScrollBar scbColumn2048x256;
    private System.Windows.Forms.NumericUpDown nudColumn2048x256;
    private System.Windows.Forms.HScrollBar scbColumn2048x512;
    private System.Windows.Forms.NumericUpDown nudColumn2048x512;
    private System.Windows.Forms.TabPage tbpTest;
    private System.Windows.Forms.Panel pnlImageScrollZoom;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.HScrollBar scbScale;
    private System.Windows.Forms.HScrollBar scbMoveH;
    private System.Windows.Forms.Panel pnlLS;
    private System.Windows.Forms.VScrollBar scbMoveV;
    private System.Windows.Forms.VScrollBar vScrollBar1;
    private System.Windows.Forms.Panel pnlLB;
    private System.Windows.Forms.PictureBox pbxTest;
    private System.Windows.Forms.Button btnLoadFromFile;
    private System.Windows.Forms.NumericUpDown nudMoveH;
    private System.Windows.Forms.Button btnRescale;
    private System.Windows.Forms.NumericUpDown nudMoveV;
    private System.Windows.Forms.NumericUpDown nudScale;
    //private UCLaserAreaScanner.CUCLaserStepMatrix cucLaserStepMatrix1;
    //private UCLaserAreaScanner.CUCLaserAreaScannerCommand FUCLaserAreaScannerCommand;
    //private UCLaserAreaScanner.CUCLaserAreaScannerTable FUCLaserAreaScannerTable;
    //private System.Windows.Forms.TabPage tbpLaserstepMatrix;
    //private UCLaserAreaScanner.CUCLaserAreaScannerMatrix FUCLaserAreaScannerMatrix;
    //private System.Windows.Forms.TabPage tbpLaserStepLine;
    //private UCUartTerminal.CUCUartTerminal FUCUartTerminal;
    //private System.Windows.Forms.Label lblHeader;
    //private UCLaserAreaScanner.CUCLaserStepLine FUCLaserStepLine;
  }
}

