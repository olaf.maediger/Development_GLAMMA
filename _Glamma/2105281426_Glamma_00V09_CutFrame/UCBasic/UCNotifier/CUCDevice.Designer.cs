﻿namespace UCNotifier
{
  partial class CUCDevice
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label7 = new System.Windows.Forms.Label();
      this.label11 = new System.Windows.Forms.Label();
      this.tbxProductKey = new System.Windows.Forms.TextBox();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnConfirm = new System.Windows.Forms.Button();
      this.cbxDiscountUser = new System.Windows.Forms.CheckBox();
      this.lblTargetUser = new System.Windows.Forms.Label();
      this.btnCopyTargetUser = new System.Windows.Forms.Button();
      this.btnPasteTargetUser = new System.Windows.Forms.Button();
      this.btnCopyProductKey = new System.Windows.Forms.Button();
      this.btnPasteProductKey = new System.Windows.Forms.Button();
      this.btnCopySerialNumber = new System.Windows.Forms.Button();
      this.btnPasteSerialNumber = new System.Windows.Forms.Button();
      this.nudSerialNumber = new System.Windows.Forms.NumericUpDown();
      this.lblBackground = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.tbxName = new System.Windows.Forms.TextBox();
      this.btnCopyApplicationName = new System.Windows.Forms.Button();
      this.btnPasteApplicationName = new System.Windows.Forms.Button();
      this.gbxDevice = new System.Windows.Forms.GroupBox();
      this.tbxUser = new System.Windows.Forms.TextBox();
      ((System.ComponentModel.ISupportInitialize)(this.nudSerialNumber)).BeginInit();
      this.gbxDevice.SuspendLayout();
      this.SuspendLayout();
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(309, 73);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(65, 13);
      this.label7.TabIndex = 19;
      this.label7.Text = "Product-Key";
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(309, 28);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(73, 13);
      this.label11.TabIndex = 20;
      this.label11.Text = "Serial-Number";
      // 
      // tbxProductKey
      // 
      this.tbxProductKey.BackColor = System.Drawing.SystemColors.Window;
      this.tbxProductKey.Location = new System.Drawing.Point(308, 89);
      this.tbxProductKey.Name = "tbxProductKey";
      this.tbxProductKey.Size = new System.Drawing.Size(266, 20);
      this.tbxProductKey.TabIndex = 22;
      this.tbxProductKey.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnCancel
      // 
      this.btnCancel.Location = new System.Drawing.Point(226, 129);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(76, 23);
      this.btnCancel.TabIndex = 23;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // btnConfirm
      // 
      this.btnConfirm.Location = new System.Drawing.Point(307, 129);
      this.btnConfirm.Name = "btnConfirm";
      this.btnConfirm.Size = new System.Drawing.Size(75, 23);
      this.btnConfirm.TabIndex = 24;
      this.btnConfirm.Text = "<confirm>";
      this.btnConfirm.UseVisualStyleBackColor = true;
      this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
      // 
      // cbxDiscountUser
      // 
      this.cbxDiscountUser.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxDiscountUser.FlatStyle = System.Windows.Forms.FlatStyle.System;
      this.cbxDiscountUser.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
      this.cbxDiscountUser.Location = new System.Drawing.Point(56, 69);
      this.cbxDiscountUser.Name = "cbxDiscountUser";
      this.cbxDiscountUser.Size = new System.Drawing.Size(73, 18);
      this.cbxDiscountUser.TabIndex = 114;
      this.cbxDiscountUser.Text = "Discount";
      this.cbxDiscountUser.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxDiscountUser.UseVisualStyleBackColor = true;
      this.cbxDiscountUser.CheckedChanged += new System.EventHandler(this.cbxDiscountUser_CheckedChanged);
      // 
      // lblTargetUser
      // 
      this.lblTargetUser.AutoSize = true;
      this.lblTargetUser.Location = new System.Drawing.Point(23, 73);
      this.lblTargetUser.Name = "lblTargetUser";
      this.lblTargetUser.Size = new System.Drawing.Size(29, 13);
      this.lblTargetUser.TabIndex = 115;
      this.lblTargetUser.Text = "User";
      // 
      // btnCopyTargetUser
      // 
      this.btnCopyTargetUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnCopyTargetUser.Location = new System.Drawing.Point(216, 69);
      this.btnCopyTargetUser.Name = "btnCopyTargetUser";
      this.btnCopyTargetUser.Size = new System.Drawing.Size(36, 18);
      this.btnCopyTargetUser.TabIndex = 117;
      this.btnCopyTargetUser.Text = "Copy";
      this.btnCopyTargetUser.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnCopyTargetUser.UseVisualStyleBackColor = true;
      this.btnCopyTargetUser.Click += new System.EventHandler(this.btnCopyUser_Click);
      // 
      // btnPasteTargetUser
      // 
      this.btnPasteTargetUser.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnPasteTargetUser.Location = new System.Drawing.Point(252, 69);
      this.btnPasteTargetUser.Name = "btnPasteTargetUser";
      this.btnPasteTargetUser.Size = new System.Drawing.Size(36, 18);
      this.btnPasteTargetUser.TabIndex = 118;
      this.btnPasteTargetUser.Text = "Paste";
      this.btnPasteTargetUser.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnPasteTargetUser.UseVisualStyleBackColor = true;
      this.btnPasteTargetUser.Click += new System.EventHandler(this.btnPasteUser_Click);
      // 
      // btnCopyProductKey
      // 
      this.btnCopyProductKey.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnCopyProductKey.Location = new System.Drawing.Point(501, 69);
      this.btnCopyProductKey.Name = "btnCopyProductKey";
      this.btnCopyProductKey.Size = new System.Drawing.Size(36, 18);
      this.btnCopyProductKey.TabIndex = 123;
      this.btnCopyProductKey.Text = "Copy";
      this.btnCopyProductKey.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnCopyProductKey.UseVisualStyleBackColor = true;
      this.btnCopyProductKey.Click += new System.EventHandler(this.btnCopyProductKey_Click);
      // 
      // btnPasteProductKey
      // 
      this.btnPasteProductKey.Enabled = false;
      this.btnPasteProductKey.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnPasteProductKey.Location = new System.Drawing.Point(537, 69);
      this.btnPasteProductKey.Name = "btnPasteProductKey";
      this.btnPasteProductKey.Size = new System.Drawing.Size(36, 18);
      this.btnPasteProductKey.TabIndex = 124;
      this.btnPasteProductKey.Text = "Paste";
      this.btnPasteProductKey.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnPasteProductKey.UseVisualStyleBackColor = true;
      this.btnPasteProductKey.Click += new System.EventHandler(this.btnPasteProductKey_Click);
      // 
      // btnCopySerialNumber
      // 
      this.btnCopySerialNumber.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnCopySerialNumber.Location = new System.Drawing.Point(501, 24);
      this.btnCopySerialNumber.Name = "btnCopySerialNumber";
      this.btnCopySerialNumber.Size = new System.Drawing.Size(36, 18);
      this.btnCopySerialNumber.TabIndex = 125;
      this.btnCopySerialNumber.Text = "Copy";
      this.btnCopySerialNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnCopySerialNumber.UseVisualStyleBackColor = true;
      this.btnCopySerialNumber.Click += new System.EventHandler(this.btnCopySerialNumber_Click);
      // 
      // btnPasteSerialNumber
      // 
      this.btnPasteSerialNumber.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnPasteSerialNumber.Location = new System.Drawing.Point(537, 24);
      this.btnPasteSerialNumber.Name = "btnPasteSerialNumber";
      this.btnPasteSerialNumber.Size = new System.Drawing.Size(36, 18);
      this.btnPasteSerialNumber.TabIndex = 126;
      this.btnPasteSerialNumber.Text = "Paste";
      this.btnPasteSerialNumber.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnPasteSerialNumber.UseVisualStyleBackColor = true;
      this.btnPasteSerialNumber.Click += new System.EventHandler(this.btnPasteSerialNumber_Click);
      // 
      // nudSerialNumber
      // 
      this.nudSerialNumber.Location = new System.Drawing.Point(308, 44);
      this.nudSerialNumber.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
      this.nudSerialNumber.Name = "nudSerialNumber";
      this.nudSerialNumber.Size = new System.Drawing.Size(267, 20);
      this.nudSerialNumber.TabIndex = 128;
      this.nudSerialNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudSerialNumber.Value = new decimal(new int[] {
            9111288,
            0,
            0,
            0});
      this.nudSerialNumber.ValueChanged += new System.EventHandler(this.tbxTextBoxInput_TextChanged);
      // 
      // lblBackground
      // 
      this.lblBackground.BackColor = System.Drawing.SystemColors.Info;
      this.lblBackground.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lblBackground.Location = new System.Drawing.Point(11, 19);
      this.lblBackground.Name = "lblBackground";
      this.lblBackground.Size = new System.Drawing.Size(575, 102);
      this.lblBackground.TabIndex = 141;
      this.lblBackground.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Location = new System.Drawing.Point(24, 27);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(72, 13);
      this.label12.TabIndex = 130;
      this.label12.Text = "Name Device";
      // 
      // tbxName
      // 
      this.tbxName.Location = new System.Drawing.Point(22, 43);
      this.tbxName.Multiline = true;
      this.tbxName.Name = "tbxName";
      this.tbxName.Size = new System.Drawing.Size(267, 20);
      this.tbxName.TabIndex = 129;
      this.tbxName.Text = "IT-01";
      this.tbxName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.tbxName.TextChanged += new System.EventHandler(this.tbxTextBoxInput_TextChanged);
      // 
      // btnCopyApplicationName
      // 
      this.btnCopyApplicationName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnCopyApplicationName.Location = new System.Drawing.Point(216, 23);
      this.btnCopyApplicationName.Name = "btnCopyApplicationName";
      this.btnCopyApplicationName.Size = new System.Drawing.Size(36, 18);
      this.btnCopyApplicationName.TabIndex = 135;
      this.btnCopyApplicationName.Text = "Copy";
      this.btnCopyApplicationName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnCopyApplicationName.UseVisualStyleBackColor = true;
      this.btnCopyApplicationName.Click += new System.EventHandler(this.btnCopyName_Click);
      // 
      // btnPasteApplicationName
      // 
      this.btnPasteApplicationName.Font = new System.Drawing.Font("Arial Rounded MT Bold", 6F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnPasteApplicationName.Location = new System.Drawing.Point(252, 23);
      this.btnPasteApplicationName.Name = "btnPasteApplicationName";
      this.btnPasteApplicationName.Size = new System.Drawing.Size(36, 18);
      this.btnPasteApplicationName.TabIndex = 136;
      this.btnPasteApplicationName.Text = "Paste";
      this.btnPasteApplicationName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
      this.btnPasteApplicationName.UseVisualStyleBackColor = true;
      this.btnPasteApplicationName.Click += new System.EventHandler(this.btnPasteName_Click);
      // 
      // gbxDevice
      // 
      this.gbxDevice.Controls.Add(this.btnPasteApplicationName);
      this.gbxDevice.Controls.Add(this.btnCopyApplicationName);
      this.gbxDevice.Controls.Add(this.tbxName);
      this.gbxDevice.Controls.Add(this.label12);
      this.gbxDevice.Controls.Add(this.nudSerialNumber);
      this.gbxDevice.Controls.Add(this.btnPasteSerialNumber);
      this.gbxDevice.Controls.Add(this.btnCopySerialNumber);
      this.gbxDevice.Controls.Add(this.btnPasteProductKey);
      this.gbxDevice.Controls.Add(this.btnCopyProductKey);
      this.gbxDevice.Controls.Add(this.btnPasteTargetUser);
      this.gbxDevice.Controls.Add(this.btnCopyTargetUser);
      this.gbxDevice.Controls.Add(this.tbxUser);
      this.gbxDevice.Controls.Add(this.lblTargetUser);
      this.gbxDevice.Controls.Add(this.cbxDiscountUser);
      this.gbxDevice.Controls.Add(this.btnConfirm);
      this.gbxDevice.Controls.Add(this.btnCancel);
      this.gbxDevice.Controls.Add(this.tbxProductKey);
      this.gbxDevice.Controls.Add(this.label11);
      this.gbxDevice.Controls.Add(this.label7);
      this.gbxDevice.Controls.Add(this.lblBackground);
      this.gbxDevice.Dock = System.Windows.Forms.DockStyle.Fill;
      this.gbxDevice.Location = new System.Drawing.Point(0, 0);
      this.gbxDevice.Name = "gbxDevice";
      this.gbxDevice.Size = new System.Drawing.Size(597, 161);
      this.gbxDevice.TabIndex = 29;
      this.gbxDevice.TabStop = false;
      this.gbxDevice.Text = " <title> ";
      // 
      // tbxUser
      // 
      this.tbxUser.Location = new System.Drawing.Point(22, 89);
      this.tbxUser.Name = "tbxUser";
      this.tbxUser.Size = new System.Drawing.Size(267, 20);
      this.tbxUser.TabIndex = 116;
      this.tbxUser.Text = "Ingo Gehne";
      this.tbxUser.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.tbxUser.TextChanged += new System.EventHandler(this.tbxTextBoxInput_TextChanged);
      // 
      // CUCDevice
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.gbxDevice);
      this.Name = "CUCDevice";
      this.Size = new System.Drawing.Size(597, 161);
      ((System.ComponentModel.ISupportInitialize)(this.nudSerialNumber)).EndInit();
      this.gbxDevice.ResumeLayout(false);
      this.gbxDevice.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.TextBox tbxProductKey;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnConfirm;
    private System.Windows.Forms.CheckBox cbxDiscountUser;
    private System.Windows.Forms.Label lblTargetUser;
    private System.Windows.Forms.Button btnCopyTargetUser;
    private System.Windows.Forms.Button btnPasteTargetUser;
    private System.Windows.Forms.Button btnCopyProductKey;
    private System.Windows.Forms.Button btnPasteProductKey;
    private System.Windows.Forms.Button btnCopySerialNumber;
    private System.Windows.Forms.Button btnPasteSerialNumber;
    private System.Windows.Forms.NumericUpDown nudSerialNumber;
    private System.Windows.Forms.Label lblBackground;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.TextBox tbxName;
    private System.Windows.Forms.Button btnCopyApplicationName;
    private System.Windows.Forms.Button btnPasteApplicationName;
    private System.Windows.Forms.GroupBox gbxDevice;
    private System.Windows.Forms.TextBox tbxUser;

  }
}
