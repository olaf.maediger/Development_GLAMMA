﻿using System;
using System.Windows.Forms;
//
using Image24b;
//
namespace UCImageProcessing
{
  public partial class CUCImageProcessing : UserControl
  { //
    //---------------------------------------------------------------
    //  Field
    //---------------------------------------------------------------
    private CImage24b FImage24b;
    //
    //---------------------------------------------------------------
    //  Constructor
    //---------------------------------------------------------------
    public CUCImageProcessing()
    {
      InitializeComponent();
      //
      FImage24b = new CImage24b();
    }
    //
    //---------------------------------------------------------------
    //  Property
    //---------------------------------------------------------------
    public float ScaleX
    {
      get { return (float)nudScaleX.Value; }
      set { nudScaleX.Value = (decimal)value; }
    }
    public float ScaleY
    {
      get { return (float)nudScaleY.Value; }
      set { nudScaleY.Value = (decimal)value; }
    }
    public float RotateAngle
    {
      get { return (float)nudRotateAngle.Value; }
      set { nudRotateAngle.Value = (decimal)value; }
    }
    public float TranslateX
    {
      get { return (float)nudTranslateX.Value; }
      set { nudTranslateX.Value = (decimal)value; }
    }
    public float TranslateY
    {
      get { return (float)nudTranslateY.Value; }
      set { nudTranslateY.Value = (decimal)value; }
    }
    //
    //---------------------------------------------------------------
    //  Helper
    //---------------------------------------------------------------
    public Boolean LoadSourceFromFile(String filename)
    {
      Boolean Result = FImage24b.LoadSourceFromFile(filename);
      Result &= FImage24b.NormalizeSource(cbxGrayScale.Checked);
      if (Result)
      {
        TransformProjection();
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    public Boolean NormalizeSource()
    {
      Boolean Result = FImage24b.NormalizeSource(cbxGrayScale.Checked);
      if (Result)
      {
        TransformProjection();
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    public Boolean TransformProjection(float scalex, float scaley, float angledeg, float dx, float dy)
    {
      Boolean Result = FImage24b.TransformProjection(scalex, scaley, angledeg, dx, dy, cbxShowCutFrame.Checked);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    public Boolean TransformProjection()
    {
      Boolean Result = FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle,
                                                     TranslateX, TranslateY, cbxShowCutFrame.Checked);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    //
    //---------------------------------------------------------------
    //  Event - LoadFromFile
    //---------------------------------------------------------------
    private void btnLoadFromFile_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        LoadSourceFromFile(DialogLoadImage.FileName);
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - CutFrame
    //---------------------------------------------------------------
    private void cbxShowCutFrame_CheckedChanged(object sender, EventArgs e)
    {
      TransformProjection();
    }
    private void btnCutFrame_Click(object sender, EventArgs e)
    {
      if (FImage24b.CutFrame(ScaleX, ScaleY, RotateAngle,
                             TranslateX, TranslateY, cbxShowCutFrame.Checked))
      {
        ScaleX = 1.0f;
        ScaleY = 1.0f;
        RotateAngle = 0.0f;
        TranslateX = 0.0f;
        TranslateY = 0.0f;
        if (FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle,
                                          TranslateX, TranslateY, cbxShowCutFrame.Checked))
        {
          pbxImage.Image = FImage24b.GetBitmapProjection();
        }
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - RotateAngle
    //---------------------------------------------------------------
    private void SetRotateAngle(float value)
    {
      if (value != (float)(scbRotateAngle.Value / 10.0))
      {
        scbRotateAngle.Value = (int)(10.0 * value);
      }
      if (value != (float)nudRotateAngle.Value)
      {
        nudRotateAngle.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbRotateAngle_ValueChanged(object sender, EventArgs e)
    {
      SetRotateAngle((float)(scbRotateAngle.Value / 10.0));
    }
    private void nudRotateAngle_ValueChanged(object sender, EventArgs e)
    {
      SetRotateAngle((float)(nudRotateAngle.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - TranslateX
    //---------------------------------------------------------------
    private void SetTranslateX(float value)
    {
      if (value != (float)(scbTranslateX.Value / 10.0))
      {
        scbTranslateX.Value = (int)(10.0 * value);
      }
      if (value != (float)nudTranslateX.Value)
      {
        nudTranslateX.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbTranslateX_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateX((float)(scbTranslateX.Value / 10.0));
    }
    private void nudTranslateX_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateX((float)(nudTranslateX.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - TranslateY
    //---------------------------------------------------------------
    private void SetTranslateY(float value)
    {
      if (value != (float)(scbTranslateY.Value / 10.0))
      {
        scbTranslateY.Value = (int)(10.0 * value);
      }
      if (value != (float)nudTranslateY.Value)
      {
        nudTranslateY.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbTranslateY_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateY((float)(scbTranslateY.Value / 10.0));
    }
    private void nudTranslateY_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateY((float)(nudTranslateY.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - ScaleX
    //---------------------------------------------------------------
    private void SetScaleX(float value)
    {
      float SBF = 1.0f;
      if (0 < scbScaleX.Value)
      {
        SBF = (float)(1.0 + scbScaleX.Value / 100.0);
      }
      else
      if (scbScaleX.Value < 0)
      {
        SBF = (float)(1.0 + scbScaleX.Value / 100.0);
      } // else 1.0
      if (value != SBF)
      {
        if (1.0f < value)
        {
          scbScaleX.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        if (1.0f < value)
        {
          scbScaleX.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        { // 1.0
          scbScaleX.Value = 0;
        }
      }
      if (value != (float)nudScaleX.Value)
      {
        nudScaleX.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbScaleX_ValueChanged(object sender, EventArgs e)
    {
      if (0 < scbScaleX.Value)
      {
        SetScaleX((float)(1.0 + scbScaleX.Value / 100.0));
      }
      else
      if (scbScaleX.Value < 0)
      {
        SetScaleX((float)(1.0 + scbScaleX.Value / 100.0));
      }
      else
      {
        SetScaleX(1.0f);
      }
    }
    private void nudScaleX_ValueChanged(object sender, EventArgs e)
    {
      SetScaleX((float)(nudScaleX.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - ScaleY
    //---------------------------------------------------------------
    private void SetScaleY(float value)
    {
      float SBF = 1.0f;
      if (0 < scbScaleY.Value)
      {
        SBF = (float)(1.0 - scbScaleY.Value / 100.0);
      }
      else
      if (scbScaleY.Value < 0)
      {
        SBF = (float)(1.0 - scbScaleY.Value / 100.0);
      } // else 1.0
      if (value != SBF)
      {
        if (1.0f < value)
        {
          scbScaleY.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        if (1.0f < value)
        {
          scbScaleY.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        { // 1.0
          scbScaleY.Value = 0;
        }
      }
      if (value != (float)nudScaleY.Value)
      {
        nudScaleY.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void scbScaleY_ValueChanged(object sender, EventArgs e)
    {
      if (0 < scbScaleY.Value)
      {
        SetScaleY((float)(1.0 - scbScaleY.Value / 100.0));
      }
      else
      if (scbScaleY.Value < 0)
      {
        SetScaleY((float)(1.0 - scbScaleY.Value / 100.0));
      }
      else
      {
        SetScaleY(1.0f);
      }
    }
    private void nudScaleY_ValueChanged(object sender, EventArgs e)
    {
      SetScaleY((float)(nudScaleY.Value));
    }


  }
}
