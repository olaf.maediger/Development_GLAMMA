﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Drawing;
//
namespace Initdata
{
  public class CXmlReader : CXmlBase
  {
    //
    //---------------------------------------------------
    //  Section - Constructor
    //---------------------------------------------------
    //
    public CXmlReader(String filename)
    {
      FFileName = filename;
      FNotify = new CNotify();
    }
    //
    //---------------------------------------------------
    //  Section - Property
    //---------------------------------------------------
    //
    public void SetOnNotify(DOnNotify value)
    {
      FNotify.SetOnNotify(value);
    }
    //
    //---------------------------------------------------
    //  Section - Management
    //---------------------------------------------------
    //
    public Boolean Open(String section)
    {
      try
      {
        FXmlDocument = new XmlDocument();
        FXmlDocument.Load(FFileName);
        FNodeBase = FXmlDocument.SelectSingleNode(section);
        FNodeParent = FNodeBase;
        String Line = String.Format("XmlReader: Open file \"{0}\"", FFileName);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        Line = String.Format("<{0}>", section);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        FNotify.IncrementLeadCount();
        return (FNodeBase is XmlNode);
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean Close()
    {
      if (FNodeBase is XmlNode)
      {
        FNotify.DecrementLeadCount();
        String Line = String.Format("<{0}>", FNodeBase.Name);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        Line = String.Format("XmlReader: Close file \"{0}\"", FFileName);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        FNodeBase = null;
        return !(FNodeBase is XmlNode);
      }
      return false;
    }


    public Boolean OpenSection(String section)
    {
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(section);
        if (NodeChild is XmlNode)
        {
          FNodeParent = NodeChild;
          String Line = String.Format("<{0}>", section);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          FNotify.IncrementLeadCount();
          return (FNodeParent is XmlNode);
        }
      }
      return false;
    }

    public Boolean CloseSection()
    {
      if (FNodeParent is XmlNode)
      {
        FNotify.DecrementLeadCount();
        String Line = String.Format("</{0}>", FNodeParent.Name);
        FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
        FNodeParent = FNodeParent.ParentNode;
      }
      return (FNodeParent is XmlNode);
    }
    //
    //---------------------------------------
    //  Segment - Int32
    //---------------------------------------
    //      
    public Boolean ReadInt32(String name, 
                             out Int32 value, 
                             Int32 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return Int32.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Boolean
    //---------------------------------------
    //      
    public Boolean ReadBoolean(String name, 
                               out Boolean value, 
                               Boolean preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return Boolean.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - String
    //---------------------------------------
    //      
    public Boolean ReadString(String name, 
                              out String value, 
                              String preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          value = NodeChild.InnerText;
          return true;
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Float
    //---------------------------------------
    //      
    public Boolean ReadFloat(String name, 
                             out float value, 
                             float preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return float.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Double
    //---------------------------------------
    //      
    public Boolean ReadDouble(String name, 
                             out Double value, 
                             Double preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return Double.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Enumeration
    //---------------------------------------
    //      
    public Boolean ReadEnumeration(String name, 
                             out String value, 
                             String preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          value = NodeChild.InnerText;
          return (0 < value.Length);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Guid
    //---------------------------------------
    //      
    public Boolean ReadGuid(String name, 
                            out Guid value, 
                            Guid preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return Guid.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - DateTime
    //---------------------------------------
    //      
    public Boolean ReadDateTime(String name, 
                                out DateTime value, 
                                DateTime preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return DateTime.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Byte
    //---------------------------------------
    //      
    public Boolean ReadByte(String name, 
                            out Byte value, 
                            Byte preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return Byte.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - UInt16
    //---------------------------------------
    //      
    public Boolean ReadUInt16(String name, 
                              out UInt16 value, 
                              UInt16 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return UInt16.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - UInt32
    //---------------------------------------
    //      
    public Boolean ReadUInt32(String name, 
                              out UInt32 value, 
                              UInt32 preset)
    {
      value = preset;
      if (FNodeParent is XmlNode)
      {
        XmlNode NodeChild = FNodeParent.SelectSingleNode(name);
        if (NodeChild is XmlNode)
        {
          String Line = String.Format("<{0}>{1}</{0}>", name, NodeChild.InnerText);
          FNotify.Write(CInitdataBase.LIBRARY_HEADER, Line);
          return UInt32.TryParse(NodeChild.InnerText, out value);
        }
      }
      return false;
    }
    //
    //---------------------------------------
    //  Segment - Color
    //---------------------------------------
    //      
    public Boolean ReadColor(String name,
                             out Color value,
                             Color preset)
    {
      const String SECTION_COLOR = "Color";
      const String NAME_ALPHA = "Alpha";
      const String NAME_RED = "Red";
      const String NAME_GREEN = "Green";
      const String NAME_BLUE = "Blue";
      //
      const Byte INIT_ALPHA = 0xFF;
      const Byte INIT_RED = 0x80;
      const Byte INIT_GREEN = 0x80;
      const Byte INIT_BLUE = 0x80;
      //
      OpenSection(SECTION_COLOR + name);
      //
      Boolean Result = true;
      value = preset;
      Byte Alpha = INIT_ALPHA;
      Result &= ReadByte(NAME_ALPHA, out Alpha, Alpha);
      Byte Red = INIT_RED;
      Result &= ReadByte(NAME_RED, out Red, Red);
      Byte Green = INIT_GREEN;
      Result &= ReadByte(NAME_GREEN, out Green, Green);
      Byte Blue = INIT_BLUE;
      Result &= ReadByte(NAME_BLUE, out Blue, Blue);
      //
      if (!Result)
      {
        value = preset;
      }
      else
      {
        value = Color.FromArgb(Alpha, Red, Green, Blue);
      }
      //
      CloseSection();
      return Result;
    }
    //
    //---------------------------------------
    //  Segment - ColorOpacity
    //---------------------------------------
    //      
    public Boolean ReadColorOpacity(String name,
                                    out Color colorvalue,
                                    Color colorpreset,
                                    out Byte opacityvalue,
                                    Byte opacitypreset)
    {
      OpenSection(SECTION_COLOR + name);
      //
      Boolean Result = true;
      colorvalue = colorpreset;
      opacityvalue = opacitypreset;
      //
      Byte Red = INIT_RED;
      Result &= ReadByte(NAME_RED, out Red, Red);
      Byte Green = INIT_GREEN;
      Result &= ReadByte(NAME_GREEN, out Green, Green);
      Byte Blue = INIT_BLUE;
      Result &= ReadByte(NAME_BLUE, out Blue, Blue);
      Byte Opacity = INIT_OPACITY;
      Result &= ReadByte(NAME_OPACITY, out Opacity, Opacity);
      Opacity = (Byte)Math.Min(100, (int)Opacity);
      Opacity = (Byte)Math.Max(0, (int)Opacity);
      //
      if (Result)
      {
        colorvalue = Color.FromArgb(INIT_ALPHA, Red, Green, Blue);
        opacityvalue = Opacity;
      }
      //
      CloseSection();
      return Result;
    }
    //
    //---------------------------------------
    //  Segment - Pen
    //---------------------------------------
    //      
    public Boolean ReadPen(String name, 
                           out Pen value, 
                           Pen preset)
    {
      const String SECTION_PEN = "Pen";
      const String NAME_COLOR = "Color";
      const String NAME_WIDTH = "Width";
      //
      Color INIT_COLOR = Color.Black;
      const float INIT_WIDTH = 1.0f;
      //
      OpenSection(SECTION_PEN + name);
      //
      Boolean Result = true;
      value = preset;
      Color CValue = INIT_COLOR;
      Result &= ReadColor(NAME_COLOR, out CValue, CValue);
      float WValue = INIT_WIDTH;
      Result &= ReadFloat(NAME_WIDTH, out WValue, WValue);
      //
      value = new Pen(CValue, WValue);
      //
      CloseSection();
      return Result;
    }
    //
    //---------------------------------------
    //  Segment - Brush
    //---------------------------------------
    //      
    public Boolean ReadSolidBrush(String name, 
                                  out SolidBrush value, 
                                  SolidBrush preset)
    {
      const String SECTION_SOLIDBRUSH = "SolidBrush";
      const String NAME_COLOR = "Color";
      //
      Color INIT_COLOR = Color.White;
      //
      OpenSection(SECTION_SOLIDBRUSH + name);
      //
      Boolean Result = true;
      value = preset;
      Color CValue = INIT_COLOR;
      Result &= ReadColor(NAME_COLOR, out CValue, CValue);
      //
      value = new SolidBrush(CValue);
      //
      CloseSection();
      return Result;
    }
    //
    //---------------------------------------
    //  Segment - Font
    //---------------------------------------
    //      
    public Boolean ReadFont(String headeroffset,
                            out Font value,
                            Font preset)
    {
      const String SECTION_FONT = "Font";
      const String NAME_NAME = "Name";
      const String NAME_SIZE = "Size";
      const String NAME_BOLD = "Bold";
      const String NAME_ITALIC = "Italic";
      const String NAME_STRIKEOUT = "Strikeout";
      const String NAME_UNDERLINE = "Underline";
      //
      const String INIT_HEADER = "Arial";
      const float INIT_SIZE = 10.0f;
      const Boolean INIT_BOLD = false;
      const Boolean INIT_ITALIC = false;
      const Boolean INIT_STRIKEOUT = false;
      const Boolean INIT_UNDERLINE = false;
      //
      OpenSection(SECTION_FONT + headeroffset);
      //
      Boolean Result = true;
      value = preset;
      String Name = INIT_HEADER;
      Result &= ReadString(NAME_NAME, out Name, Name);
      float Size = INIT_SIZE;
      Result &= ReadFloat(NAME_SIZE, out Size, Size);
      Boolean Bold = INIT_BOLD;
      Result &= ReadBoolean(NAME_BOLD, out Bold, Bold);
      Boolean Italic = INIT_ITALIC;
      Result &= ReadBoolean(NAME_ITALIC, out Italic, Italic);
      Boolean Strikeout = INIT_STRIKEOUT;
      Result &= ReadBoolean(NAME_STRIKEOUT, out Strikeout, Strikeout);
      Boolean Underline = INIT_UNDERLINE;
      Result &= ReadBoolean(NAME_UNDERLINE, out Underline, Underline);
      //
      FontStyle FS = new FontStyle();
      if (Bold)
      {
        FS |= FontStyle.Bold;
      }
      if (Italic)
      {
        FS |= FontStyle.Italic;
      }
      if (Strikeout)
      {
        FS |= FontStyle.Strikeout;
      }
      if (Underline)
      {
        FS |= FontStyle.Underline;
      }
      value = new Font(Name, Size, FS);
      //
      CloseSection();
      return Result;
    }








  }
}
