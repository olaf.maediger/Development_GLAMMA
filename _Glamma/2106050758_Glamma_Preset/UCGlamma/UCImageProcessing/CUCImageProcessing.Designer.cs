﻿namespace UCImageProcessing
{
    partial class CUCImageProcessing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.pnlUser = new System.Windows.Forms.Panel();
      this.cbxShowFileIndex = new System.Windows.Forms.CheckBox();
      this.tbxFileName512Convert = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.nudFile512IndexStep = new System.Windows.Forms.NumericUpDown();
      this.label8 = new System.Windows.Forms.Label();
      this.nudFile512IndexEnd = new System.Windows.Forms.NumericUpDown();
      this.label9 = new System.Windows.Forms.Label();
      this.nudFile512IndexStart = new System.Windows.Forms.NumericUpDown();
      this.btnConvert512 = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.lblOffsetY = new System.Windows.Forms.Label();
      this.lblOffsetX = new System.Windows.Forms.Label();
      this.cbxShowCutFrame = new System.Windows.Forms.CheckBox();
      this.nudScaleY = new System.Windows.Forms.NumericUpDown();
      this.cbxConvertGreyScale = new System.Windows.Forms.CheckBox();
      this.scbShowIndex = new System.Windows.Forms.HScrollBar();
      this.nudShowIndex = new System.Windows.Forms.NumericUpDown();
      this.nudTranslateY = new System.Windows.Forms.NumericUpDown();
      this.nudScaleX = new System.Windows.Forms.NumericUpDown();
      this.nudTranslateX = new System.Windows.Forms.NumericUpDown();
      this.btnCutFrame = new System.Windows.Forms.Button();
      this.nudRotateAngle = new System.Windows.Forms.NumericUpDown();
      this.btnLoadFromFile = new System.Windows.Forms.Button();
      this.DialogLoadImage = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveImage = new System.Windows.Forms.SaveFileDialog();
      this.btnFileSequence = new System.Windows.Forms.Button();
      this.pnlImageScrollZoom = new System.Windows.Forms.Panel();
      this.pbxImage = new System.Windows.Forms.PictureBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.scbScaleX = new System.Windows.Forms.HScrollBar();
      this.scbTranslateX = new System.Windows.Forms.HScrollBar();
      this.pnlLS = new System.Windows.Forms.Panel();
      this.scbTranslateY = new System.Windows.Forms.VScrollBar();
      this.scbScaleY = new System.Windows.Forms.VScrollBar();
      this.scbRotateAngle = new System.Windows.Forms.VScrollBar();
      this.pnlLB = new System.Windows.Forms.Panel();
      this.pnlUser.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudFile512IndexStep)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudFile512IndexEnd)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudFile512IndexStart)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudShowIndex)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTranslateY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTranslateX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudRotateAngle)).BeginInit();
      this.pnlImageScrollZoom.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
      this.panel1.SuspendLayout();
      this.pnlLS.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlUser
      // 
      this.pnlUser.Controls.Add(this.btnFileSequence);
      this.pnlUser.Controls.Add(this.cbxShowFileIndex);
      this.pnlUser.Controls.Add(this.tbxFileName512Convert);
      this.pnlUser.Controls.Add(this.label7);
      this.pnlUser.Controls.Add(this.nudFile512IndexStep);
      this.pnlUser.Controls.Add(this.label8);
      this.pnlUser.Controls.Add(this.nudFile512IndexEnd);
      this.pnlUser.Controls.Add(this.label9);
      this.pnlUser.Controls.Add(this.nudFile512IndexStart);
      this.pnlUser.Controls.Add(this.btnConvert512);
      this.pnlUser.Controls.Add(this.label1);
      this.pnlUser.Controls.Add(this.label3);
      this.pnlUser.Controls.Add(this.label2);
      this.pnlUser.Controls.Add(this.lblOffsetY);
      this.pnlUser.Controls.Add(this.lblOffsetX);
      this.pnlUser.Controls.Add(this.cbxShowCutFrame);
      this.pnlUser.Controls.Add(this.nudScaleY);
      this.pnlUser.Controls.Add(this.cbxConvertGreyScale);
      this.pnlUser.Controls.Add(this.scbShowIndex);
      this.pnlUser.Controls.Add(this.nudShowIndex);
      this.pnlUser.Controls.Add(this.nudTranslateY);
      this.pnlUser.Controls.Add(this.nudScaleX);
      this.pnlUser.Controls.Add(this.nudTranslateX);
      this.pnlUser.Controls.Add(this.btnCutFrame);
      this.pnlUser.Controls.Add(this.nudRotateAngle);
      this.pnlUser.Controls.Add(this.btnLoadFromFile);
      this.pnlUser.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlUser.Location = new System.Drawing.Point(0, 0);
      this.pnlUser.Name = "pnlUser";
      this.pnlUser.Size = new System.Drawing.Size(329, 569);
      this.pnlUser.TabIndex = 2;
      // 
      // cbxShowFileIndex
      // 
      this.cbxShowFileIndex.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxShowFileIndex.Location = new System.Drawing.Point(6, 226);
      this.cbxShowFileIndex.Name = "cbxShowFileIndex";
      this.cbxShowFileIndex.Size = new System.Drawing.Size(136, 24);
      this.cbxShowFileIndex.TabIndex = 63;
      this.cbxShowFileIndex.Text = "Show FileIndex";
      this.cbxShowFileIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxShowFileIndex.UseVisualStyleBackColor = true;
      this.cbxShowFileIndex.CheckedChanged += new System.EventHandler(this.cbxShowFileIndex_CheckedChanged);
      // 
      // tbxFileName512Convert
      // 
      this.tbxFileName512Convert.Location = new System.Drawing.Point(150, 273);
      this.tbxFileName512Convert.Name = "tbxFileName512Convert";
      this.tbxFileName512Convert.Size = new System.Drawing.Size(163, 20);
      this.tbxFileName512Convert.TabIndex = 60;
      this.tbxFileName512Convert.Text = "Bitmap2048x512_{0:000}.bmp";
      this.tbxFileName512Convert.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(203, 303);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(29, 13);
      this.label7.TabIndex = 59;
      this.label7.Text = "Step";
      // 
      // nudFile512IndexStep
      // 
      this.nudFile512IndexStep.Location = new System.Drawing.Point(233, 301);
      this.nudFile512IndexStep.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudFile512IndexStep.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudFile512IndexStep.Name = "nudFile512IndexStep";
      this.nudFile512IndexStep.Size = new System.Drawing.Size(43, 20);
      this.nudFile512IndexStep.TabIndex = 58;
      this.nudFile512IndexStep.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudFile512IndexStep.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(130, 304);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(26, 13);
      this.label8.TabIndex = 57;
      this.label8.Text = "End";
      // 
      // nudFile512IndexEnd
      // 
      this.nudFile512IndexEnd.Location = new System.Drawing.Point(157, 301);
      this.nudFile512IndexEnd.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudFile512IndexEnd.Name = "nudFile512IndexEnd";
      this.nudFile512IndexEnd.Size = new System.Drawing.Size(43, 20);
      this.nudFile512IndexEnd.TabIndex = 56;
      this.nudFile512IndexEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudFile512IndexEnd.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(8, 303);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(74, 13);
      this.label9.TabIndex = 55;
      this.label9.Text = "FileIndex Start";
      // 
      // nudFile512IndexStart
      // 
      this.nudFile512IndexStart.Location = new System.Drawing.Point(84, 301);
      this.nudFile512IndexStart.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudFile512IndexStart.Name = "nudFile512IndexStart";
      this.nudFile512IndexStart.Size = new System.Drawing.Size(43, 20);
      this.nudFile512IndexStart.TabIndex = 54;
      this.nudFile512IndexStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnConvert512
      // 
      this.btnConvert512.Location = new System.Drawing.Point(5, 326);
      this.btnConvert512.Name = "btnConvert512";
      this.btnConvert512.Size = new System.Drawing.Size(135, 24);
      this.btnConvert512.TabIndex = 53;
      this.btnConvert512.Text = "Convert to File2048x512";
      this.btnConvert512.UseVisualStyleBackColor = true;
      this.btnConvert512.Click += new System.EventHandler(this.btnConvert512_Click);
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(117, 122);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(53, 13);
      this.label1.TabIndex = 43;
      this.label1.Text = "ScaleY[1]";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(6, 122);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(53, 13);
      this.label3.TabIndex = 42;
      this.label3.Text = "ScaleX[1]";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(6, 95);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(70, 13);
      this.label2.TabIndex = 41;
      this.label2.Text = "RotateA[deg]";
      // 
      // lblOffsetY
      // 
      this.lblOffsetY.AutoSize = true;
      this.lblOffsetY.Location = new System.Drawing.Point(123, 70);
      this.lblOffsetY.Name = "lblOffsetY";
      this.lblOffsetY.Size = new System.Drawing.Size(61, 13);
      this.lblOffsetY.TabIndex = 40;
      this.lblOffsetY.Text = "OffsetY[pxl]";
      // 
      // lblOffsetX
      // 
      this.lblOffsetX.AutoSize = true;
      this.lblOffsetX.Location = new System.Drawing.Point(7, 70);
      this.lblOffsetX.Name = "lblOffsetX";
      this.lblOffsetX.Size = new System.Drawing.Size(61, 13);
      this.lblOffsetX.TabIndex = 39;
      this.lblOffsetX.Text = "OffsetX[pxl]";
      // 
      // cbxShowCutFrame
      // 
      this.cbxShowCutFrame.AutoSize = true;
      this.cbxShowCutFrame.Location = new System.Drawing.Point(138, 35);
      this.cbxShowCutFrame.Name = "cbxShowCutFrame";
      this.cbxShowCutFrame.Size = new System.Drawing.Size(101, 17);
      this.cbxShowCutFrame.TabIndex = 38;
      this.cbxShowCutFrame.Text = "Show CutFrame";
      this.cbxShowCutFrame.UseVisualStyleBackColor = true;
      this.cbxShowCutFrame.CheckedChanged += new System.EventHandler(this.cbxShowCutFrame_CheckedChanged);
      // 
      // nudScaleY
      // 
      this.nudScaleY.DecimalPlaces = 2;
      this.nudScaleY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.nudScaleY.Location = new System.Drawing.Point(171, 119);
      this.nudScaleY.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            65536});
      this.nudScaleY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
      this.nudScaleY.Name = "nudScaleY";
      this.nudScaleY.Size = new System.Drawing.Size(51, 20);
      this.nudScaleY.TabIndex = 37;
      this.nudScaleY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudScaleY.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
      this.nudScaleY.ValueChanged += new System.EventHandler(this.nudScaleY_ValueChanged);
      // 
      // cbxConvertGreyScale
      // 
      this.cbxConvertGreyScale.AutoSize = true;
      this.cbxConvertGreyScale.Location = new System.Drawing.Point(6, 34);
      this.cbxConvertGreyScale.Name = "cbxConvertGreyScale";
      this.cbxConvertGreyScale.Size = new System.Drawing.Size(127, 17);
      this.cbxConvertGreyScale.TabIndex = 36;
      this.cbxConvertGreyScale.Text = "Convert to GrayScale";
      this.cbxConvertGreyScale.UseVisualStyleBackColor = true;
      this.cbxConvertGreyScale.CheckedChanged += new System.EventHandler(this.cbxConvertGreyScale_CheckedChanged);
      // 
      // scbShowIndex
      // 
      this.scbShowIndex.Enabled = false;
      this.scbShowIndex.Location = new System.Drawing.Point(5, 202);
      this.scbShowIndex.Maximum = 264;
      this.scbShowIndex.Name = "scbShowIndex";
      this.scbShowIndex.Size = new System.Drawing.Size(308, 20);
      this.scbShowIndex.TabIndex = 32;
      this.scbShowIndex.ValueChanged += new System.EventHandler(this.scbShowIndex_ValueChanged);
      // 
      // nudShowIndex
      // 
      this.nudShowIndex.Enabled = false;
      this.nudShowIndex.Location = new System.Drawing.Point(149, 228);
      this.nudShowIndex.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudShowIndex.Name = "nudShowIndex";
      this.nudShowIndex.Size = new System.Drawing.Size(51, 20);
      this.nudShowIndex.TabIndex = 31;
      this.nudShowIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudShowIndex.ValueChanged += new System.EventHandler(this.nudShowIndex_ValueChanged);
      // 
      // nudTranslateY
      // 
      this.nudTranslateY.Location = new System.Drawing.Point(185, 67);
      this.nudTranslateY.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
      this.nudTranslateY.Minimum = new decimal(new int[] {
            256,
            0,
            0,
            -2147483648});
      this.nudTranslateY.Name = "nudTranslateY";
      this.nudTranslateY.Size = new System.Drawing.Size(51, 20);
      this.nudTranslateY.TabIndex = 28;
      this.nudTranslateY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTranslateY.ValueChanged += new System.EventHandler(this.nudTranslateY_ValueChanged);
      // 
      // nudScaleX
      // 
      this.nudScaleX.DecimalPlaces = 2;
      this.nudScaleX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.nudScaleX.Location = new System.Drawing.Point(60, 119);
      this.nudScaleX.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            65536});
      this.nudScaleX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
      this.nudScaleX.Name = "nudScaleX";
      this.nudScaleX.Size = new System.Drawing.Size(51, 20);
      this.nudScaleX.TabIndex = 27;
      this.nudScaleX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudScaleX.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
      this.nudScaleX.ValueChanged += new System.EventHandler(this.nudScaleX_ValueChanged);
      // 
      // nudTranslateX
      // 
      this.nudTranslateX.Location = new System.Drawing.Point(68, 67);
      this.nudTranslateX.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
      this.nudTranslateX.Minimum = new decimal(new int[] {
            256,
            0,
            0,
            -2147483648});
      this.nudTranslateX.Name = "nudTranslateX";
      this.nudTranslateX.Size = new System.Drawing.Size(51, 20);
      this.nudTranslateX.TabIndex = 26;
      this.nudTranslateX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTranslateX.ValueChanged += new System.EventHandler(this.nudTranslateX_ValueChanged);
      // 
      // btnCutFrame
      // 
      this.btnCutFrame.Location = new System.Drawing.Point(6, 151);
      this.btnCutFrame.Name = "btnCutFrame";
      this.btnCutFrame.Size = new System.Drawing.Size(135, 24);
      this.btnCutFrame.TabIndex = 25;
      this.btnCutFrame.Text = "Cut Frame";
      this.btnCutFrame.UseVisualStyleBackColor = true;
      this.btnCutFrame.Click += new System.EventHandler(this.btnCutFrame_Click);
      // 
      // nudRotateAngle
      // 
      this.nudRotateAngle.DecimalPlaces = 1;
      this.nudRotateAngle.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.nudRotateAngle.Location = new System.Drawing.Point(76, 92);
      this.nudRotateAngle.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
      this.nudRotateAngle.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
      this.nudRotateAngle.Name = "nudRotateAngle";
      this.nudRotateAngle.Size = new System.Drawing.Size(51, 20);
      this.nudRotateAngle.TabIndex = 24;
      this.nudRotateAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudRotateAngle.ValueChanged += new System.EventHandler(this.nudRotateAngle_ValueChanged);
      // 
      // btnLoadFromFile
      // 
      this.btnLoadFromFile.Location = new System.Drawing.Point(6, 4);
      this.btnLoadFromFile.Name = "btnLoadFromFile";
      this.btnLoadFromFile.Size = new System.Drawing.Size(135, 24);
      this.btnLoadFromFile.TabIndex = 23;
      this.btnLoadFromFile.Text = "Load";
      this.btnLoadFromFile.UseVisualStyleBackColor = true;
      this.btnLoadFromFile.Click += new System.EventHandler(this.btnLoadFromFile_Click);
      // 
      // DialogLoadImage
      // 
      this.DialogLoadImage.DefaultExt = "bmp";
      this.DialogLoadImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogLoadImage.Title = "Load Image";
      // 
      // DialogSaveImage
      // 
      this.DialogSaveImage.DefaultExt = "bmp";
      this.DialogSaveImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogSaveImage.Title = "Save Image";
      // 
      // btnFileSequence
      // 
      this.btnFileSequence.Location = new System.Drawing.Point(5, 271);
      this.btnFileSequence.Name = "btnFileSequence";
      this.btnFileSequence.Size = new System.Drawing.Size(135, 24);
      this.btnFileSequence.TabIndex = 64;
      this.btnFileSequence.Text = "File Sequence";
      this.btnFileSequence.UseVisualStyleBackColor = true;
      // 
      // pnlImageScrollZoom
      // 
      this.pnlImageScrollZoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlImageScrollZoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlImageScrollZoom.Controls.Add(this.pbxImage);
      this.pnlImageScrollZoom.Controls.Add(this.panel1);
      this.pnlImageScrollZoom.Controls.Add(this.pnlLS);
      this.pnlImageScrollZoom.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlImageScrollZoom.Location = new System.Drawing.Point(329, 0);
      this.pnlImageScrollZoom.Name = "pnlImageScrollZoom";
      this.pnlImageScrollZoom.Size = new System.Drawing.Size(590, 569);
      this.pnlImageScrollZoom.TabIndex = 3;
      // 
      // pbxImage
      // 
      this.pbxImage.BackColor = System.Drawing.Color.LightSalmon;
      this.pbxImage.Location = new System.Drawing.Point(66, 6);
      this.pbxImage.Name = "pbxImage";
      this.pbxImage.Size = new System.Drawing.Size(512, 512);
      this.pbxImage.TabIndex = 6;
      this.pbxImage.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.scbScaleX);
      this.panel1.Controls.Add(this.scbTranslateX);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(59, 529);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(529, 38);
      this.panel1.TabIndex = 5;
      // 
      // scbScaleX
      // 
      this.scbScaleX.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.scbScaleX.Location = new System.Drawing.Point(0, 19);
      this.scbScaleX.Maximum = 108;
      this.scbScaleX.Minimum = -99;
      this.scbScaleX.Name = "scbScaleX";
      this.scbScaleX.Size = new System.Drawing.Size(529, 19);
      this.scbScaleX.TabIndex = 3;
      this.scbScaleX.ValueChanged += new System.EventHandler(this.scbScaleX_ValueChanged);
      // 
      // scbTranslateX
      // 
      this.scbTranslateX.Dock = System.Windows.Forms.DockStyle.Top;
      this.scbTranslateX.Location = new System.Drawing.Point(0, 0);
      this.scbTranslateX.Maximum = 2569;
      this.scbTranslateX.Minimum = -2560;
      this.scbTranslateX.Name = "scbTranslateX";
      this.scbTranslateX.Size = new System.Drawing.Size(529, 19);
      this.scbTranslateX.SmallChange = 10;
      this.scbTranslateX.TabIndex = 2;
      this.scbTranslateX.ValueChanged += new System.EventHandler(this.scbTranslateX_ValueChanged);
      // 
      // pnlLS
      // 
      this.pnlLS.Controls.Add(this.scbTranslateY);
      this.pnlLS.Controls.Add(this.scbScaleY);
      this.pnlLS.Controls.Add(this.scbRotateAngle);
      this.pnlLS.Controls.Add(this.pnlLB);
      this.pnlLS.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLS.Location = new System.Drawing.Point(0, 0);
      this.pnlLS.Name = "pnlLS";
      this.pnlLS.Size = new System.Drawing.Size(59, 567);
      this.pnlLS.TabIndex = 4;
      // 
      // scbTranslateY
      // 
      this.scbTranslateY.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbTranslateY.Location = new System.Drawing.Point(38, 0);
      this.scbTranslateY.Maximum = 2569;
      this.scbTranslateY.Minimum = -2560;
      this.scbTranslateY.Name = "scbTranslateY";
      this.scbTranslateY.Size = new System.Drawing.Size(19, 529);
      this.scbTranslateY.SmallChange = 10;
      this.scbTranslateY.TabIndex = 8;
      this.scbTranslateY.ValueChanged += new System.EventHandler(this.scbTranslateY_ValueChanged);
      // 
      // scbScaleY
      // 
      this.scbScaleY.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbScaleY.Location = new System.Drawing.Point(19, 0);
      this.scbScaleY.Maximum = 108;
      this.scbScaleY.Minimum = -99;
      this.scbScaleY.Name = "scbScaleY";
      this.scbScaleY.Size = new System.Drawing.Size(19, 529);
      this.scbScaleY.TabIndex = 7;
      this.scbScaleY.ValueChanged += new System.EventHandler(this.scbScaleY_ValueChanged);
      // 
      // scbRotateAngle
      // 
      this.scbRotateAngle.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbRotateAngle.Location = new System.Drawing.Point(0, 0);
      this.scbRotateAngle.Maximum = 1809;
      this.scbRotateAngle.Minimum = -1800;
      this.scbRotateAngle.Name = "scbRotateAngle";
      this.scbRotateAngle.Size = new System.Drawing.Size(19, 529);
      this.scbRotateAngle.TabIndex = 6;
      this.scbRotateAngle.ValueChanged += new System.EventHandler(this.scbRotateAngle_ValueChanged);
      // 
      // pnlLB
      // 
      this.pnlLB.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlLB.Location = new System.Drawing.Point(0, 529);
      this.pnlLB.Name = "pnlLB";
      this.pnlLB.Size = new System.Drawing.Size(59, 38);
      this.pnlLB.TabIndex = 1;
      // 
      // CUCImageProcessing
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlImageScrollZoom);
      this.Controls.Add(this.pnlUser);
      this.Name = "CUCImageProcessing";
      this.Size = new System.Drawing.Size(923, 569);
      this.pnlUser.ResumeLayout(false);
      this.pnlUser.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudFile512IndexStep)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudFile512IndexEnd)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudFile512IndexStart)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudShowIndex)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTranslateY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTranslateX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudRotateAngle)).EndInit();
      this.pnlImageScrollZoom.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
      this.panel1.ResumeLayout(false);
      this.pnlLS.ResumeLayout(false);
      this.ResumeLayout(false);

        }

    #endregion
    private System.Windows.Forms.Panel pnlUser;
    private System.Windows.Forms.HScrollBar scbShowIndex;
    private System.Windows.Forms.NumericUpDown nudShowIndex;
    private System.Windows.Forms.NumericUpDown nudTranslateY;
    private System.Windows.Forms.NumericUpDown nudScaleX;
    private System.Windows.Forms.NumericUpDown nudTranslateX;
    private System.Windows.Forms.Button btnCutFrame;
    private System.Windows.Forms.NumericUpDown nudRotateAngle;
    private System.Windows.Forms.Button btnLoadFromFile;
    private System.Windows.Forms.OpenFileDialog DialogLoadImage;
    private System.Windows.Forms.SaveFileDialog DialogSaveImage;
    private System.Windows.Forms.CheckBox cbxConvertGreyScale;
    private System.Windows.Forms.NumericUpDown nudScaleY;
    private System.Windows.Forms.CheckBox cbxShowCutFrame;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lblOffsetY;
    private System.Windows.Forms.Label lblOffsetX;
    private System.Windows.Forms.TextBox tbxFileName512Convert;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown nudFile512IndexStep;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudFile512IndexEnd;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.NumericUpDown nudFile512IndexStart;
    private System.Windows.Forms.Button btnConvert512;
    private System.Windows.Forms.CheckBox cbxShowFileIndex;
    private System.Windows.Forms.Button btnFileSequence;
    private System.Windows.Forms.Panel pnlImageScrollZoom;
    private System.Windows.Forms.PictureBox pbxImage;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.HScrollBar scbScaleX;
    private System.Windows.Forms.HScrollBar scbTranslateX;
    private System.Windows.Forms.Panel pnlLS;
    private System.Windows.Forms.VScrollBar scbTranslateY;
    private System.Windows.Forms.VScrollBar scbScaleY;
    private System.Windows.Forms.VScrollBar scbRotateAngle;
    private System.Windows.Forms.Panel pnlLB;
  }
}
