﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace Image24b
{
  public class CImage24b
  {
    const int BITMAP_WIDTH = 256;
    const int BITMAP_HEIGHT = 256;
    const int PICTURE_WIDTH = 512;
    const int PICTURE_HEIGHT = 512;
    //
    private Bitmap FBitmap256x256;
    private Bitmap FBitmapConverted;
    private Bitmap FBitmapSource;
    private Bitmap FBitmapPicture;
    //
    public CImage24b()
    {
      FBitmap256x256 = new Bitmap(BITMAP_WIDTH, BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
      FBitmapSource = new Bitmap(BITMAP_WIDTH, BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
      FBitmapPicture = new Bitmap(PICTURE_WIDTH, PICTURE_HEIGHT, PixelFormat.Format24bppRgb);
    }


    public Bitmap GetBitmap256x256()
    {
      return FBitmap256x256;
    }
    public Bitmap GetBitmapConverted()
    {
      return FBitmapConverted;
    }
    public Bitmap GetBitmapSource()
    {
      return FBitmapSource;
    }
    public Bitmap GetBitmapPicture()
    {
      return FBitmapPicture;
    }
    public int GetWidthSource()
    {
      return FBitmapSource.Width;
    }
    public int GetHeightSource()
    {
      return FBitmapSource.Height;
    }

    //public Boolean LoadFromFile(String filename)
    //{
    //  try
    //  {
    //    Bitmap BF = new Bitmap(filename);
    //    int BFW = BF.Width;
    //    int BFH = BF.Height;
    //    //
    //    // NC FBitmap = new Bitmap(BITMAP_WIDTH, BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
    //    Graphics G = Graphics.FromImage(FBitmap256x256);
    //    //
    //    G.InterpolationMode = InterpolationMode.High;
    //    G.CompositingQuality = CompositingQuality.HighQuality;
    //    G.SmoothingMode = SmoothingMode.AntiAlias;
    //    G.DrawImage(BF, 0, 0, BITMAP_WIDTH, BITMAP_HEIGHT);
    //    //
    //    G.Dispose();
    //    BF.Dispose();
    //    //
    //    Rectangle RS = new Rectangle(0, 0, BITMAP_WIDTH, BITMAP_HEIGHT);
    //    FBitmapConverted = FBitmap256x256.Clone(RS, PixelFormat.Format24bppRgb);
    //    //
    //    Rectangle RP = new Rectangle(0, 0, PICTURE_WIDTH, PICTURE_HEIGHT);
    //    FBitmapPicture = new Bitmap(RP.Width, RP.Height, PixelFormat.Format24bppRgb);
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}

    unsafe public Boolean ConvertGrayScale()
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(1 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 256;
        Int32 BID0 = (Int32)PBD;
        Int32 BID1 = (Int32)PBD + SCD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          byte GV = (byte)(0.0 + (B0 + B1 + B2) / 3);
          *((byte*)(BID0 + 0)) = GV;
          *((byte*)(BID0 + 1)) = GV;
          *((byte*)(BID0 + 2)) = GV;
          CID += 3;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 1 * RID * SCD;
            BID1 = BID0 + SCD;
          }
          else
          {
            BID0 += 3;
            BID1 += 3;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //----------------------------------------------------------
    //  256 x ...
    //----------------------------------------------------------
    unsafe public Boolean Convert256x256(Boolean grayscale)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(1 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 256;
        Int32 BID0 = (Int32)PBD;
        Int32 BID1 = (Int32)PBD + SCD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          if (grayscale)
          {
            byte GS = (byte)((B0 + B1 + B2) / 3);
            B0 = GS;
            B1 = GS;
            B2 = GS;
          }
          *((byte*)(BID0 + 0)) = B0;
          *((byte*)(BID0 + 1)) = B1;
          *((byte*)(BID0 + 2)) = B2;
          CID += 3;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 1 * RID * SCD;
            BID1 = BID0 + SCD;
          }
          else
          {
            BID0 += 3;
            BID1 += 3;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean Convert512x256(Boolean grayscale)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(2 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 512;
        Int32 BID0 = (Int32)PBD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          if (grayscale)
          {
            byte GS = (byte)((B0 + B1 + B2) / 3);
            B0 = GS;
            B1 = GS;
            B2 = GS;
          }
          *((byte*)(BID0 + 0)) = B0;
          *((byte*)(BID0 + 1)) = B1;
          *((byte*)(BID0 + 2)) = B2;
          *((byte*)(BID0 + 3)) = B0;
          *((byte*)(BID0 + 4)) = B1;
          *((byte*)(BID0 + 5)) = B2;
          CID += 6;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 1 * RID * SCD;
          }
          else
          {
            BID0 += 6;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean Convert4096x256(Boolean grayscale)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(16 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 4096;
        Int32 BID0 = (Int32)PBD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          if (grayscale)
          {
            byte GS = (byte)((B0 + B1 + B2) / 3);
            B0 = GS;
            B1 = GS;
            B2 = GS;
          }
          //-------------------------
          *((byte*)(BID0 + 0)) = B0;
          *((byte*)(BID0 + 1)) = B1;
          *((byte*)(BID0 + 2)) = B2;
          *((byte*)(BID0 + 3)) = B0;
          *((byte*)(BID0 + 4)) = B1;
          *((byte*)(BID0 + 5)) = B2;
          *((byte*)(BID0 + 6)) = B0;
          *((byte*)(BID0 + 7)) = B1;
          *((byte*)(BID0 + 8)) = B2;
          *((byte*)(BID0 + 9)) = B0;
          *((byte*)(BID0 + 10)) = B1;
          *((byte*)(BID0 + 11)) = B2;
          *((byte*)(BID0 + 12)) = B0;
          *((byte*)(BID0 + 13)) = B1;
          *((byte*)(BID0 + 14)) = B2;
          *((byte*)(BID0 + 15)) = B0;
          *((byte*)(BID0 + 16)) = B1;
          *((byte*)(BID0 + 17)) = B2;
          *((byte*)(BID0 + 18)) = B0;
          *((byte*)(BID0 + 19)) = B1;
          *((byte*)(BID0 + 20)) = B2;
          *((byte*)(BID0 + 21)) = B0;
          *((byte*)(BID0 + 22)) = B1;
          *((byte*)(BID0 + 23)) = B2;
          *((byte*)(BID0 + 24)) = B0;
          *((byte*)(BID0 + 25)) = B1;
          *((byte*)(BID0 + 26)) = B2;
          *((byte*)(BID0 + 27)) = B0;
          *((byte*)(BID0 + 28)) = B1;
          *((byte*)(BID0 + 29)) = B2;
          *((byte*)(BID0 + 30)) = B0;
          *((byte*)(BID0 + 31)) = B1;
          *((byte*)(BID0 + 32)) = B2;
          *((byte*)(BID0 + 33)) = B0;
          *((byte*)(BID0 + 34)) = B1;
          *((byte*)(BID0 + 35)) = B2;
          *((byte*)(BID0 + 36)) = B0;
          *((byte*)(BID0 + 37)) = B1;
          *((byte*)(BID0 + 38)) = B2;
          *((byte*)(BID0 + 39)) = B0;
          *((byte*)(BID0 + 40)) = B1;
          *((byte*)(BID0 + 41)) = B2;
          *((byte*)(BID0 + 42)) = B0;
          *((byte*)(BID0 + 43)) = B1;
          *((byte*)(BID0 + 44)) = B2;
          *((byte*)(BID0 + 45)) = B0;
          *((byte*)(BID0 + 46)) = B1;
          *((byte*)(BID0 + 47)) = B2;
          //--------------------------
          CID += 48;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 1 * RID * SCD;
          }
          else
          {
            BID0 += 48;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean Column2048x256(Boolean grayscale)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(8 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 2048;
        Int32 BID0 = (Int32)PBD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          if (grayscale)
          {
            byte GS = (byte)((B0 + B1 + B2) / 3);
            B0 = GS;
            B1 = GS;
            B2 = GS;
          }
          //-------------------------
          *((byte*)(BID0 + 0)) = B0;
          *((byte*)(BID0 + 1)) = B1;
          *((byte*)(BID0 + 2)) = B2;
          *((byte*)(BID0 + 3)) = B0;
          *((byte*)(BID0 + 4)) = B1;
          *((byte*)(BID0 + 5)) = B2;
          *((byte*)(BID0 + 6)) = B0;
          *((byte*)(BID0 + 7)) = B1;
          *((byte*)(BID0 + 8)) = B2;
          *((byte*)(BID0 + 9)) = B0;
          *((byte*)(BID0 + 10)) = B1;
          *((byte*)(BID0 + 11)) = B2;
          *((byte*)(BID0 + 12)) = B0;
          *((byte*)(BID0 + 13)) = B1;
          *((byte*)(BID0 + 14)) = B2;
          *((byte*)(BID0 + 15)) = B0;
          *((byte*)(BID0 + 16)) = B1;
          *((byte*)(BID0 + 17)) = B2;
          *((byte*)(BID0 + 18)) = B0;
          *((byte*)(BID0 + 19)) = B1;
          *((byte*)(BID0 + 20)) = B2;
          *((byte*)(BID0 + 21)) = B0;
          *((byte*)(BID0 + 22)) = B1;
          *((byte*)(BID0 + 23)) = B2;
          //--------------------------
          CID += 24;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 1 * RID * SCD;
          }
          else
          {
            BID0 += 24;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }
    //
    //----------------------------------------------------------
    //  512 x ...
    //----------------------------------------------------------
    unsafe public Boolean Convert256x512(Boolean grayscale)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(1 * BITMAP_WIDTH, 2 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 256;
        Int32 BID0 = (Int32)PBD;
        Int32 BID1 = (Int32)PBD + SCD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          if (grayscale)
          {
            byte GS = (byte)((B0 + B1 + B2) / 3);
            B0 = GS;
            B1 = GS;
            B2 = GS;
          }
          *((byte*)(BID0 + 0)) = B0;
          *((byte*)(BID0 + 1)) = B1;
          *((byte*)(BID0 + 2)) = B2;
          *((byte*)(BID1 + 0)) = B0;
          *((byte*)(BID1 + 1)) = B1;
          *((byte*)(BID1 + 2)) = B2;
          CID += 3;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 2 * RID * SCD;
            BID1 = BID0 + SCD;
          }
          else
          {
            BID0 += 3;
            BID1 += 3;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean Convert512x512(Boolean grayscale)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(2 * BITMAP_WIDTH, 2 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 512;// 2 * Math.Abs(BDS.Stride); 
        Int32 BID0 = (Int32)PBD;
        Int32 BID1 = (Int32)PBD + SCD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          if (grayscale)
          {
            byte GS = (byte)((B0 + B1 + B2) / 3);
            B0 = GS;
            B1 = GS;
            B2 = GS;
          }
          *((byte*)(BID0 + 0)) = B0;
          *((byte*)(BID0 + 1)) = B1;
          *((byte*)(BID0 + 2)) = B2;
          *((byte*)(BID0 + 3)) = B0;
          *((byte*)(BID0 + 4)) = B1;
          *((byte*)(BID0 + 5)) = B2;
          *((byte*)(BID1 + 0)) = B0;
          *((byte*)(BID1 + 1)) = B1;
          *((byte*)(BID1 + 2)) = B2;
          *((byte*)(BID1 + 3)) = B0;
          *((byte*)(BID1 + 4)) = B1;
          *((byte*)(BID1 + 5)) = B2;
          CID += 6;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 2 * RID * SCD;
            BID1 = BID0 + SCD;
          }
          else
          {
            BID0 += 6;
            BID1 += 6;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean Convert4096x512(Boolean grayscale)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(16 * BITMAP_WIDTH, 2 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 4096;// 
        Int32 BID0 = (Int32)PBD;
        Int32 BID1 = (Int32)PBD + SCD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          if (grayscale)
          {
            byte GS = (byte)((B0 + B1 + B2) / 3);
            B0 = GS;
            B1 = GS;
            B2 = GS;
          }
          //-------------------------
          *((byte*)(BID0 + 0)) = B0;
          *((byte*)(BID0 + 1)) = B1;
          *((byte*)(BID0 + 2)) = B2;
          *((byte*)(BID0 + 3)) = B0;
          *((byte*)(BID0 + 4)) = B1;
          *((byte*)(BID0 + 5)) = B2;
          *((byte*)(BID0 + 6)) = B0;
          *((byte*)(BID0 + 7)) = B1;
          *((byte*)(BID0 + 8)) = B2;
          *((byte*)(BID0 + 9)) = B0;
          *((byte*)(BID0 + 10)) = B1;
          *((byte*)(BID0 + 11)) = B2;
          *((byte*)(BID0 + 12)) = B0;
          *((byte*)(BID0 + 13)) = B1;
          *((byte*)(BID0 + 14)) = B2;
          *((byte*)(BID0 + 15)) = B0;
          *((byte*)(BID0 + 16)) = B1;
          *((byte*)(BID0 + 17)) = B2;
          *((byte*)(BID0 + 18)) = B0;
          *((byte*)(BID0 + 19)) = B1;
          *((byte*)(BID0 + 20)) = B2;
          *((byte*)(BID0 + 21)) = B0;
          *((byte*)(BID0 + 22)) = B1;
          *((byte*)(BID0 + 23)) = B2;
          *((byte*)(BID0 + 24)) = B0;
          *((byte*)(BID0 + 25)) = B1;
          *((byte*)(BID0 + 26)) = B2;
          *((byte*)(BID0 + 27)) = B0;
          *((byte*)(BID0 + 28)) = B1;
          *((byte*)(BID0 + 29)) = B2;
          *((byte*)(BID0 + 30)) = B0;
          *((byte*)(BID0 + 31)) = B1;
          *((byte*)(BID0 + 32)) = B2;
          *((byte*)(BID0 + 33)) = B0;
          *((byte*)(BID0 + 34)) = B1;
          *((byte*)(BID0 + 35)) = B2;
          *((byte*)(BID0 + 36)) = B0;
          *((byte*)(BID0 + 37)) = B1;
          *((byte*)(BID0 + 38)) = B2;
          *((byte*)(BID0 + 39)) = B0;
          *((byte*)(BID0 + 40)) = B1;
          *((byte*)(BID0 + 41)) = B2;
          *((byte*)(BID0 + 42)) = B0;
          *((byte*)(BID0 + 43)) = B1;
          *((byte*)(BID0 + 44)) = B2;
          *((byte*)(BID0 + 45)) = B0;
          *((byte*)(BID0 + 46)) = B1;
          *((byte*)(BID0 + 47)) = B2;
          //-------------------------
          *((byte*)(BID1 + 0)) = B0;
          *((byte*)(BID1 + 1)) = B1;
          *((byte*)(BID1 + 2)) = B2;
          *((byte*)(BID1 + 3)) = B0;
          *((byte*)(BID1 + 4)) = B1;
          *((byte*)(BID1 + 5)) = B2;
          *((byte*)(BID1 + 6)) = B0;
          *((byte*)(BID1 + 7)) = B1;
          *((byte*)(BID1 + 8)) = B2;
          *((byte*)(BID1 + 9)) = B0;
          *((byte*)(BID1 + 10)) = B1;
          *((byte*)(BID1 + 11)) = B2;
          *((byte*)(BID1 + 12)) = B0;
          *((byte*)(BID1 + 13)) = B1;
          *((byte*)(BID1 + 14)) = B2;
          *((byte*)(BID1 + 15)) = B0;
          *((byte*)(BID1 + 16)) = B1;
          *((byte*)(BID1 + 17)) = B2;
          *((byte*)(BID1 + 18)) = B0;
          *((byte*)(BID1 + 19)) = B1;
          *((byte*)(BID1 + 20)) = B2;
          *((byte*)(BID1 + 21)) = B0;
          *((byte*)(BID1 + 22)) = B1;
          *((byte*)(BID1 + 23)) = B2;
          *((byte*)(BID1 + 24)) = B0;
          *((byte*)(BID1 + 25)) = B1;
          *((byte*)(BID1 + 26)) = B2;
          *((byte*)(BID1 + 27)) = B0;
          *((byte*)(BID1 + 28)) = B1;
          *((byte*)(BID1 + 29)) = B2;
          *((byte*)(BID1 + 30)) = B0;
          *((byte*)(BID1 + 31)) = B1;
          *((byte*)(BID1 + 32)) = B2;
          *((byte*)(BID1 + 33)) = B0;
          *((byte*)(BID1 + 34)) = B1;
          *((byte*)(BID1 + 35)) = B2;
          *((byte*)(BID1 + 36)) = B0;
          *((byte*)(BID1 + 37)) = B1;
          *((byte*)(BID1 + 38)) = B2;
          *((byte*)(BID1 + 39)) = B0;
          *((byte*)(BID1 + 40)) = B1;
          *((byte*)(BID1 + 41)) = B2;
          *((byte*)(BID1 + 42)) = B0;
          *((byte*)(BID1 + 43)) = B1;
          *((byte*)(BID1 + 44)) = B2;
          *((byte*)(BID1 + 45)) = B0;
          *((byte*)(BID1 + 46)) = B1;
          *((byte*)(BID1 + 47)) = B2;
          //--------------------------
          CID += 48;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 2 * RID * SCD;
            BID1 = BID0 + SCD;
          }
          else
          {
            BID0 += 48;
            BID1 += 48;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean Column2048x512(Boolean grayscale)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
        BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        //
        FBitmapConverted = new Bitmap(8 * BITMAP_WIDTH, 2 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
        BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        IntPtr PBD = BDD.Scan0;
        //
        int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
        byte[] BValues = new byte[ByteCountSource];
        Marshal.Copy(PBS, BValues, 0, ByteCountSource);
        //
        FBitmap256x256.UnlockBits(BDS);
        //
        Int32 SCD = 3 * 2048;// 
        Int32 BID0 = (Int32)PBD;
        Int32 BID1 = (Int32)PBD + SCD;
        Int32 CID = 0;
        Int32 RID = 0;
        for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
        {
          byte B0 = BValues[BIS + 0];
          byte B1 = BValues[BIS + 1];
          byte B2 = BValues[BIS + 2];
          if (grayscale)
          {
            byte GS = (byte)((B0 + B1 + B2) / 3);
            B0 = GS;
            B1 = GS;
            B2 = GS;
          }
          //-------------------------
          *((byte*)(BID0 + 0)) = B0;
          *((byte*)(BID0 + 1)) = B1;
          *((byte*)(BID0 + 2)) = B2;
          *((byte*)(BID0 + 3)) = B0;
          *((byte*)(BID0 + 4)) = B1;
          *((byte*)(BID0 + 5)) = B2;
          *((byte*)(BID0 + 6)) = B0;
          *((byte*)(BID0 + 7)) = B1;
          *((byte*)(BID0 + 8)) = B2;
          *((byte*)(BID0 + 9)) = B0;
          *((byte*)(BID0 + 10)) = B1;
          *((byte*)(BID0 + 11)) = B2;
          *((byte*)(BID0 + 12)) = B0;
          *((byte*)(BID0 + 13)) = B1;
          *((byte*)(BID0 + 14)) = B2;
          *((byte*)(BID0 + 15)) = B0;
          *((byte*)(BID0 + 16)) = B1;
          *((byte*)(BID0 + 17)) = B2;
          *((byte*)(BID0 + 18)) = B0;
          *((byte*)(BID0 + 19)) = B1;
          *((byte*)(BID0 + 20)) = B2;
          *((byte*)(BID0 + 21)) = B0;
          *((byte*)(BID0 + 22)) = B1;
          *((byte*)(BID0 + 23)) = B2;
          //-------------------------
          *((byte*)(BID1 + 0)) = B0;
          *((byte*)(BID1 + 1)) = B1;
          *((byte*)(BID1 + 2)) = B2;
          *((byte*)(BID1 + 3)) = B0;
          *((byte*)(BID1 + 4)) = B1;
          *((byte*)(BID1 + 5)) = B2;
          *((byte*)(BID1 + 6)) = B0;
          *((byte*)(BID1 + 7)) = B1;
          *((byte*)(BID1 + 8)) = B2;
          *((byte*)(BID1 + 9)) = B0;
          *((byte*)(BID1 + 10)) = B1;
          *((byte*)(BID1 + 11)) = B2;
          *((byte*)(BID1 + 12)) = B0;
          *((byte*)(BID1 + 13)) = B1;
          *((byte*)(BID1 + 14)) = B2;
          *((byte*)(BID1 + 15)) = B0;
          *((byte*)(BID1 + 16)) = B1;
          *((byte*)(BID1 + 17)) = B2;
          *((byte*)(BID1 + 18)) = B0;
          *((byte*)(BID1 + 19)) = B1;
          *((byte*)(BID1 + 20)) = B2;
          *((byte*)(BID1 + 21)) = B0;
          *((byte*)(BID1 + 22)) = B1;
          *((byte*)(BID1 + 23)) = B2;
          //--------------------------
          CID += 24;
          if (SCD <= CID)
          {
            CID = 0;
            RID++;
            BID0 = (Int32)PBD + 2 * RID * SCD;
            BID1 = BID0 + SCD;
          }
          else
          {
            BID0 += 24;
            BID1 += 24;
          }
        }
        //
        FBitmapConverted.UnlockBits(BDD);
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }




    //##############################################################################################

    public Boolean LoadFromFile(String filename)
    {
      try
      {
        FBitmapSource = new Bitmap(filename);
        RescaleSourceToPicture(0, 0, 1.0, 0.0); // Center!
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean CopySourceToPicture()
    {
      try
      {
        Graphics G = Graphics.FromImage(FBitmapPicture);
        int WS = GetWidthSource();
        int HS = GetHeightSource();
        //
        G.InterpolationMode = InterpolationMode.NearestNeighbor;
        G.CompositingQuality = CompositingQuality.AssumeLinear;
        G.SmoothingMode = SmoothingMode.None;
        //
        G.DrawImage(FBitmapSource, 0, 0, WS, HS);
        G.Dispose();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean RescaleSourceToPicture(double foffsetx, double foffsety, 
                                                 double fscale, double frotate)
    {
      try
      {
        Graphics G = Graphics.FromImage(FBitmapPicture);
        G.FillRectangle(new SolidBrush(Color.FromArgb(0xFF, 0x88, 0x99, 0xAA)), 0, 0, 512, 512);
        //
        int WS = GetWidthSource();
        int HS = GetHeightSource();
        int OX = (int)(foffsetx * PICTURE_WIDTH);
        int OY = (int)(foffsety * PICTURE_HEIGHT);
        int SX = (int)(fscale * WS);
        int SY = (int)(fscale * HS);
        //
        Matrix M = new Matrix();
        // Scale
        M.Translate(-SX / 2, -SY / 2);
        M.Scale((float)fscale, (float)fscale);
        // Rotate
        float AR = (float)frotate;
        // M.RotateAt(AR, new PointF(PICTURE_WIDTH / 2 + WS / 2, PICTURE_HEIGHT / 2 + HS / 2));
        M.Translate(OX, OY);
        M.RotateAt(AR, new PointF(WS / 2, HS / 2));
        // Translate
        //M.Translate((PICTURE_WIDTH - 0) / 2, (PICTURE_HEIGHT - 0) / 2);
        G.Transform = M;
        //
        G.InterpolationMode = InterpolationMode.NearestNeighbor;
        G.CompositingQuality = CompositingQuality.AssumeLinear;
        G.SmoothingMode = SmoothingMode.None;
        //
        G.DrawImage(FBitmapSource, 0, 0);//, WS, HS);
        //
        G.Dispose();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    // 2105210902
    //unsafe public Boolean RescaleSourceToPicture(double foffsetx, double foffsety,
    //                                             double fscale, double frotate)
    //{
    //  try
    //  {
    //    Graphics G = Graphics.FromImage(FBitmapPicture);
    //    G.FillRectangle(new SolidBrush(Color.FromArgb(0xFF, 0x88, 0x99, 0xAA)), 0, 0, 512, 512);
    //    //
    //    int WS = GetWidthSource();
    //    int HS = GetHeightSource();
    //    int OX = (int)(foffsetx * (512.0 - WS / 2) + 256 - WS / 2);
    //    int OY = (int)(foffsety * (512.0 - HS / 2) + 256 - HS / 2);
    //    Matrix M = new Matrix();
    //    // Rotate
    //    float AR = (float)frotate;
    //    M.RotateAt(AR, new PointF(WS, HS));
    //    // Translate
    //    M.Translate(OX, OY);
    //    // Scale
    //    M.Scale((float)fscale, (float)fscale);
    //    G.Transform = M;
    //    //
    //    G.DrawImage(FBitmapSource, 0, 0, WS, HS);
    //    //
    //    G.Dispose();
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}


    //Matrix MR = new Matrix();
    //float AR = 20.0f;
    //MR.RotateAt(AR, new PointF(0, 0));
    //    G.Transform = MR;
    //int SX = (int)(fscale * WS);
    //int SY = (int)(fscale * HS);
    //// muell M.Translate(OX, OY);
    //// muell M.Scale((float)fscale, (float)fscale);
    ////M.Translate(256 - WS / 2, 256 - HS / 2);// SX / 2, SY / 2); // !!!!!!!!! SX/SY ???
    //M.Translate(-256 + WS + SX/2, 256-HS);
    // G.DrawImage(FBitmapSource, OX, OY, SY, SY);


    //int BFW = FBitmapSource.Width;
    //int BFH = FBitmapSource.Height;
    //    //
    //    //Graphics G = Graphics.FromImage(FBitmap256x256);
    //    //
    //    G.InterpolationMode = InterpolationMode.NearestNeighbor;
    //    G.CompositingQuality = CompositingQuality.AssumeLinear;
    //    G.SmoothingMode = SmoothingMode.None;
    //    //
    //    //G.DrawImage(BF, 0, 0, BITMAP_WIDTH, BITMAP_HEIGHT);
    //    ////
    //    //G.Dispose();
    //    //BF.Dispose();
    //    ////
    //    //Rectangle RS = new Rectangle(0, 0, BITMAP_WIDTH, BITMAP_HEIGHT);
    //    //FBitmapConverted = FBitmap256x256.Clone(RS, PixelFormat.Format24bppRgb);
    //    ////
    //    //Rectangle RP = new Rectangle(0, 0, PICTURE_WIDTH, PICTURE_HEIGHT);
    //    //FBitmapPicture = new Bitmap(RP.Width, RP.Height, PixelFormat.Format24bppRgb);


  }
}



