﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCNotifier
{ //
  //###################################################################
  //  Applicationlist
  //###################################################################
  //
  public class CApplicationlist : CTransformBaselist
  { //
    //-----------------------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------------------
    //
    public CTransformApplication Find(String name,
                                      String version,
                                      String date,
                                      String company,
                                      String street,
                                      String streetnumber,
                                      String postalcode,
                                      String city,
                                      Boolean discountuser,
                                      String user,
                                      Int32 productnumber,
                                      String productkey)
    {
      foreach (CTransformApplication TransformApplication in this)
      {
        Boolean Result = (name == TransformApplication.Name);
        Result &= (version == TransformApplication.Version);
        Result &= (date == TransformApplication.Date);
        Result &= (company == TransformApplication.Company);
        Result &= (street == TransformApplication.Street);
        Result &= (streetnumber == TransformApplication.StreetNumber);
        Result &= (postalcode == TransformApplication.PostalCode);
        Result &= (city == TransformApplication.City);
        Result &= (discountuser == TransformApplication.DiscountUser);
        Result &= (user == TransformApplication.User);
        Result &= (productnumber == TransformApplication.ProductNumber);
        Result &= (productkey == TransformApplication.ProductKey);
        if (Result)
        {
          return TransformApplication;
        }
      }
      return null;
    }

    public Boolean Exists(String name,
                          String version,
                          String date,
                          String company,
                          String street,
                          String streetnumber,
                          String postalcode,
                          String city,
                          Boolean discountuser,
                          String user,
                          Int32 productnumber,
                          String productkey)
    {
      CTransformApplication TransformApplication = Find(name, version, date,
                                                        company, street, streetnumber,
                                                        postalcode, city, 
                                                        discountuser, user,
                                                        productnumber, productkey);
      return (TransformApplication is CTransformApplication);
    }

    public Boolean Add(String name,
                       String version,
                       String date,
                       String company,
                       String street,
                       String streetnumber,
                       String postalcode,
                       String city,
                       Boolean discountuser,
                       String user,
                       String productkey)
    {
      CTransform Transform = new CTransform();
      Int32 ProductNumber = Transform.BuildApplication(name, version, date, 
                                                       company, street, streetnumber,
                                                       postalcode, city, 
                                                       discountuser, user);
      String ProductKey = Transform.BuildProductKey(ProductNumber);
      if (ProductKey == productkey)
      {
        Add(name, version, date,
            company, street, streetnumber,
            postalcode, city, 
            discountuser, user,
            ProductNumber, productkey);
        return true;
      }
      return false;
    }

    public Boolean Add(String name,
                       String version,
                       String date,
                       String company,
                       String street,
                       String streetnumber,
                       String postalcode,
                       String city,
                       Boolean discountuser,
                       String user,
                       Int32 productnumber,
                       String productkey)
    {
      CTransformBase TransformBase = Find(name, productnumber, productkey);
      if (TransformBase is CTransformBase)
      {
        return false;
      }
      TransformBase = new CTransformApplication(name, version, date,
                                                company, street, streetnumber,
                                                postalcode, city, 
                                                discountuser, user,
                                                productnumber, productkey);
      if (TransformBase is CTransformApplication)
      {
        base.Add(TransformBase);
        return true;
      }
      return false;
    }

    public Boolean Add(RApplicationData data)
    {
      CTransformBase TransformBase = Find(data.TransformData.Name, 
                                          data.TransformData.ProductNumber, 
                                          data.TransformData.ProductKey);
      if (TransformBase is CTransformBase)
      {
        return false;
      }
      TransformBase = new CTransformApplication(data);
      if (TransformBase is CTransformApplication)
      {
        base.Add(TransformBase);
        return true;
      }
      return false;
    }

    public Boolean Change(Int32 applicationindex,
                          String name,
                          String version,
                          String date,
                          String company,
                          String street,
                          String streetnumber,
                          String postalcode,
                          String city,
                          Boolean discountuser,
                          String user,
                          Int32 productnumber,
                          String productkey)
    {
      if ((0 <= applicationindex) && (applicationindex < this.Count))
      {
        CTransformBase TransformBase = this[applicationindex];
        if (TransformBase is CTransformApplication)
        {
          CTransformApplication TransformApplication = (CTransformApplication)TransformBase;
          RApplicationData ApplicationData;
          if (TransformApplication.GetData(out ApplicationData))
          {
            ApplicationData.TransformData.Name = name;
            ApplicationData.TransformData.Version = version;
            ApplicationData.TransformData.Date = date;
            ApplicationData.Company = company;
            ApplicationData.Street = street;
            ApplicationData.StreetNumber = streetnumber;
            ApplicationData.PostalCode = postalcode;
            ApplicationData.City = city;
            ApplicationData.TransformData.DiscountUser = discountuser;
            ApplicationData.TransformData.User = user;
            ApplicationData.TransformData.ProductNumber = productnumber;
            ApplicationData.TransformData.ProductKey = productkey;
            return TransformApplication.SetData(ApplicationData);
          }
        }
      }
      return false;
    }

    public Boolean Change(Int32 applicationindex, RApplicationData data)
    {
      if ((0 <= applicationindex) && (applicationindex < this.Count))
      {
        CTransformBase TransformBase = this[applicationindex];
        if (TransformBase is CTransformApplication)
        {
          CTransformApplication TransformApplication = (CTransformApplication)TransformBase;
          RApplicationData ApplicationData;
          if (TransformApplication.GetData(out ApplicationData))
          {
            ApplicationData.TransformData.Name = data.TransformData.Name;
            ApplicationData.TransformData.Version = data.TransformData.Version;
            ApplicationData.TransformData.Date = data.TransformData.Date;
            ApplicationData.Company = data.Company;
            ApplicationData.Street = data.Street;
            ApplicationData.StreetNumber = data.StreetNumber;
            ApplicationData.PostalCode = data.PostalCode;
            ApplicationData.City = data.City;
            ApplicationData.TransformData.DiscountUser = data.TransformData.DiscountUser;
            ApplicationData.TransformData.User = data.TransformData.User;
            ApplicationData.TransformData.ProductNumber = data.TransformData.ProductNumber;
            ApplicationData.TransformData.ProductKey = data.TransformData.ProductKey;
            return TransformApplication.SetData(ApplicationData);
          }
        }
      }
      return false;
    }
  }
  //
  //
  //###################################################################
  //  Devicelist
  //###################################################################
  //
  public class CDevicelist : CTransformBaselist
  { //
    //-----------------------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------------------
    //
    public CTransformDevice Find(String name,
                                 String version,
                                 String date,
                                 Boolean discountuser,
                                 String user,
                                 Int32 serialnumber,
                                 Int32 productnumber,
                                 String productkey)
    {
      foreach (CTransformDevice TransformDevice in this)
      {
        Boolean Result = (name == TransformDevice.Name);
        Result &= (version == TransformDevice.Version);
        Result &= (date == TransformDevice.Date);
        Result &= (discountuser == TransformDevice.DiscountUser);
        Result &= (user == TransformDevice.User);
        Result &= (serialnumber == TransformDevice.SerialNumber);
        Result &= (productnumber == TransformDevice.ProductNumber);
        Result &= (productkey == TransformDevice.ProductKey);
        if (Result)
        {
          return TransformDevice;
        }
      }
      return null;
    }

    public Boolean Exists(String name,
                          String version,
                          String date,
                          Boolean discountuser,
                          String user,
                          Int32 serialnumber,
                          Int32 productnumber,
                          String productkey)
    {
      CTransformDevice TransformDevice = Find(name, version, date,
                                              discountuser, user, serialnumber,
                                              productnumber, productkey);
      return (TransformDevice is CTransformDevice);
    }

    public Boolean Add(String name,
                       String version,
                       String date,
                       Boolean discountuser,
                       String user,
                       Int32 serialnumber,
                       Int32 productnumber,
                       String productkey)
    {
      CTransformBase TransformBase = Find(name, productnumber, productkey);
      if (TransformBase is CTransformBase)
      {
        return false;
      }
      TransformBase = new CTransformDevice(name, version, date,
                                           discountuser, user, serialnumber,
                                           productnumber, productkey);
      if (TransformBase is CTransformDevice)
      {
        base.Add(TransformBase);
        return true;
      }
      return false;
    }

    public Boolean Add(RDeviceData data)
    {
      CTransformBase TransformBase = Find(data.TransformData.Name,
                                          data.TransformData.ProductNumber,
                                          data.TransformData.ProductKey);
      if (TransformBase is CTransformBase)
      {
        return false;
      }
      TransformBase = new CTransformDevice(data);
      if (TransformBase is CTransformDevice)
      {
        this.Add(TransformBase);
        return true;
      }
      return false;
    }

    public Boolean Change(Int32 applicationindex,
                          String name,
                          String version,
                          String date,
                          Boolean discountuser,
                          String user,
                          Int32 serialnumber,
                          Int32 productnumber,
                          String productkey)
    {
      if ((0 <= applicationindex) && (applicationindex < this.Count))
      {
        CTransformBase TransformBase = this[applicationindex];
        if (TransformBase is CTransformDevice)
        {
          CTransformDevice TransformDevice = (CTransformDevice)TransformBase;
          RDeviceData DeviceData;
          if (TransformDevice.GetData(out DeviceData))
          {
            DeviceData.TransformData.Name = name;
            DeviceData.TransformData.Version = version;
            DeviceData.TransformData.Date = date;
            DeviceData.TransformData.DiscountUser = discountuser;
            DeviceData.TransformData.User = user;
            DeviceData.SerialNumber = serialnumber;
            DeviceData.TransformData.ProductNumber = productnumber;
            DeviceData.TransformData.ProductKey = productkey;
            return TransformDevice.SetData(DeviceData);
          }
        }
      }
      return false;
    }

    public Boolean Change(Int32 applicationindex, RDeviceData data)
    {
      if ((0 <= applicationindex) && (applicationindex < this.Count))
      {
        CTransformBase TransformBase = this[applicationindex];
        if (TransformBase is CTransformDevice)
        {
          CTransformDevice TransformDevice = (CTransformDevice)TransformBase;
          RDeviceData DeviceData;
          if (TransformDevice.GetData(out DeviceData))
          {
            DeviceData.TransformData.Name = data.TransformData.Name;
            DeviceData.TransformData.Version = data.TransformData.Version;
            DeviceData.TransformData.Date = data.TransformData.Date;
            DeviceData.TransformData.DiscountUser = data.TransformData.DiscountUser;
            DeviceData.TransformData.User = data.TransformData.User;
            DeviceData.SerialNumber = data.SerialNumber;
            DeviceData.TransformData.ProductNumber = data.TransformData.ProductNumber;
            DeviceData.TransformData.ProductKey = data.TransformData.ProductKey;
            return TransformDevice.SetData(DeviceData);
          }
        }
      }
      return false;
    }
  }
}
