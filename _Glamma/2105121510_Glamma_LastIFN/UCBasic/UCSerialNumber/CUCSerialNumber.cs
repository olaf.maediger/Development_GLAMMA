﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Initdata;
using UCNotifier;
//
namespace UCSerialNumber
{ //
  //####################################################################
  //  Section - CUCSerialNumber
  //####################################################################
  //
  public delegate void DOnDeviceChanged(Int32 serialnumber,
                                        String productkey);

  public partial class CUCSerialNumber : UserControl
  { //
    //------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------
    //
    private const String SECTION_UCSERIALNUMBER = "UCSerialNumber";
    //
    private const String NAME_PRODUCTKEYSELECTED = "ProductKeySelected";
    private const Int32 INIT_PRODUCTKEYSELECTED = 0;
    //
    private const String FORMAT_LINE = "{0} SN[{1}] PK[{2}]";
    private static String[] SEPARATORS = { " ", "SN[", "PK[", "]" };
    //
    //------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnDeviceChanged FOnDeviceChanged;
    //
    //------------------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------------------
    //
    public CUCSerialNumber()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }


    public void SetOnDeviceChanged(DOnDeviceChanged value)
    {
      FOnDeviceChanged = value;
    }

    public void SetDeviceName(String devicename)
    {
      lblTitle.Text = String.Format("Select connected {0}-Device by SerialNumber and ProductKey:",
                                    devicename);
    }
    //
    //------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------
    //
    private void cbxSerialNumberDevices_SelectedIndexChanged(object sender, EventArgs e)
    {
      if (FOnDeviceChanged is DOnDeviceChanged)
      {
        FOnDeviceChanged(GetSerialNumberSelected(), GetProductKeySelected());
      }
    }
    //
    //------------------------------------------------------------------
    //  Section - Initdata
    //------------------------------------------------------------------
    //
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Int32 IValue;
      //
      Result &= initdata.OpenSection(SECTION_UCSERIALNUMBER);
      //
      // Autoselection Productkey:
      IValue = INIT_PRODUCTKEYSELECTED;
      initdata.ReadInt32(NAME_PRODUCTKEYSELECTED, out IValue, IValue);
      SelectProductKeyIndex(IValue);
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }

    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= initdata.CreateSection(SECTION_UCSERIALNUMBER);
      //
      initdata.WriteInt32(NAME_PRODUCTKEYSELECTED, GetProductKeyIndexSelected());
      //
      Result &= initdata.CloseSection();
      //
      return Result;
    }
    //
    //------------------------------------------------------------------
    //  Section - Public Management
    //------------------------------------------------------------------
    //
    public Boolean ClearAllDevices()
    {
      cbxSerialNumberDevices.Items.Clear();
      return (0 == cbxSerialNumberDevices.Items.Count);
    }

    public Boolean AddDevice(String devicename,
                             Int32 serialnumber,
                             String productkey)
    {
      String Line = String.Format(FORMAT_LINE,
                                  devicename, serialnumber, productkey);
      cbxSerialNumberDevices.Items.Add(Line);
      return (0 < cbxSerialNumberDevices.Items.Count);
    }


    public Int32 GetSerialNumberSelected()
    {
      Int32 SerialNumber = 0;

      String Line = cbxSerialNumberDevices.Text;
      if (0 < Line.Length)
      {
        String[] Tokens = Line.Split(SEPARATORS, StringSplitOptions.RemoveEmptyEntries);
        if (3 <= Tokens.Length)
        {
          SerialNumber = Int32.Parse(Tokens[1]);
        }
      }
      return SerialNumber;
    }

    public String GetProductKeySelected()
    {
      String ProductKey = "";

      String Line = cbxSerialNumberDevices.Text;
      if (0 < Line.Length)
      {
        String[] Tokens = Line.Split(SEPARATORS, StringSplitOptions.RemoveEmptyEntries);
        if (3 <= Tokens.Length)
        {
          ProductKey = Tokens[2];
        }
      }
      return ProductKey;
    }

    public Boolean SelectProductKeyIndex(Int32 index)
    {
      if ((0 <= index) && (index < cbxSerialNumberDevices.Items.Count))
      {
        cbxSerialNumberDevices.SelectedIndex = index;
        return true;
      }
      return false;
    }

    public Int32 GetProductKeyIndexSelected()
    {
      return cbxSerialNumberDevices.SelectedIndex;
    }

  }
}
