﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
namespace Initdata
{
  public delegate void DOnNotify(String module, String line);

  public class CNotify
  {
    private const String LEADTEXT = "  ";
    private Int32 FLeadCount;
    private DOnNotify FOnNotify;

    public void SetOnNotify(DOnNotify value)
    {
      FOnNotify = value;
    }

    public void IncrementLeadCount()
    {
      FLeadCount++;
    }
    public void DecrementLeadCount()
    {
      if (0 < FLeadCount)
      {
        FLeadCount--;
      }
    }

    private String BuildLeadText()
    {
      String Result = "";
      for (Int32 LI = 0; LI < FLeadCount; LI++)
      {
        Result += LEADTEXT;
      }
      return Result;
    }

    public void Write(String module, String value)
    {
      if (FOnNotify is DOnNotify)
      {
        String Line = BuildLeadText() + value;
        FOnNotify(module, Line);
      }
    }

  }
}
