﻿namespace UCImageProcessing
{
    partial class CUCImageProcessing
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.pnlImageScrollZoom = new System.Windows.Forms.Panel();
      this.pbxImage = new System.Windows.Forms.PictureBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.scbScaleX = new System.Windows.Forms.HScrollBar();
      this.scbTranslateX = new System.Windows.Forms.HScrollBar();
      this.pnlLS = new System.Windows.Forms.Panel();
      this.scbTranslateY = new System.Windows.Forms.VScrollBar();
      this.scbScaleY = new System.Windows.Forms.VScrollBar();
      this.scbRotateAngle = new System.Windows.Forms.VScrollBar();
      this.pnlLB = new System.Windows.Forms.Panel();
      this.pnlUser = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.lblOffsetY = new System.Windows.Forms.Label();
      this.lblOffsetX = new System.Windows.Forms.Label();
      this.cbxShowCutFrame = new System.Windows.Forms.CheckBox();
      this.nudScaleY = new System.Windows.Forms.NumericUpDown();
      this.cbxGrayScale = new System.Windows.Forms.CheckBox();
      this.button1 = new System.Windows.Forms.Button();
      this.scbColumn2048x512 = new System.Windows.Forms.HScrollBar();
      this.nudColumn2048x512 = new System.Windows.Forms.NumericUpDown();
      this.scbColumn2048x256 = new System.Windows.Forms.HScrollBar();
      this.nudColumn2048x256 = new System.Windows.Forms.NumericUpDown();
      this.btnColumn2048x256 = new System.Windows.Forms.Button();
      this.btnColumn2048x512 = new System.Windows.Forms.Button();
      this.nudTranslateY = new System.Windows.Forms.NumericUpDown();
      this.nudScaleX = new System.Windows.Forms.NumericUpDown();
      this.nudTranslateX = new System.Windows.Forms.NumericUpDown();
      this.btnCutFrame = new System.Windows.Forms.Button();
      this.nudRotateAngle = new System.Windows.Forms.NumericUpDown();
      this.btnLoadFromFile = new System.Windows.Forms.Button();
      this.DialogLoadImage = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveImage = new System.Windows.Forms.SaveFileDialog();
      this.btnSave = new System.Windows.Forms.Button();
      this.btnConvert256 = new System.Windows.Forms.Button();
      this.pnlImageScrollZoom.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
      this.panel1.SuspendLayout();
      this.pnlLS.SuspendLayout();
      this.pnlUser.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x512)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x256)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTranslateY)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTranslateX)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudRotateAngle)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlImageScrollZoom
      // 
      this.pnlImageScrollZoom.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
      this.pnlImageScrollZoom.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlImageScrollZoom.Controls.Add(this.pbxImage);
      this.pnlImageScrollZoom.Controls.Add(this.panel1);
      this.pnlImageScrollZoom.Controls.Add(this.pnlLS);
      this.pnlImageScrollZoom.Location = new System.Drawing.Point(0, 0);
      this.pnlImageScrollZoom.Name = "pnlImageScrollZoom";
      this.pnlImageScrollZoom.Size = new System.Drawing.Size(587, 564);
      this.pnlImageScrollZoom.TabIndex = 1;
      // 
      // pbxImage
      // 
      this.pbxImage.BackColor = System.Drawing.Color.LightSalmon;
      this.pbxImage.Location = new System.Drawing.Point(66, 6);
      this.pbxImage.Name = "pbxImage";
      this.pbxImage.Size = new System.Drawing.Size(512, 512);
      this.pbxImage.TabIndex = 6;
      this.pbxImage.TabStop = false;
      // 
      // panel1
      // 
      this.panel1.Controls.Add(this.scbScaleX);
      this.panel1.Controls.Add(this.scbTranslateX);
      this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.panel1.Location = new System.Drawing.Point(59, 524);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(526, 38);
      this.panel1.TabIndex = 5;
      // 
      // scbScaleX
      // 
      this.scbScaleX.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.scbScaleX.Location = new System.Drawing.Point(0, 19);
      this.scbScaleX.Maximum = 108;
      this.scbScaleX.Minimum = -99;
      this.scbScaleX.Name = "scbScaleX";
      this.scbScaleX.Size = new System.Drawing.Size(526, 19);
      this.scbScaleX.TabIndex = 3;
      this.scbScaleX.ValueChanged += new System.EventHandler(this.scbScaleX_ValueChanged);
      // 
      // scbTranslateX
      // 
      this.scbTranslateX.Dock = System.Windows.Forms.DockStyle.Top;
      this.scbTranslateX.Location = new System.Drawing.Point(0, 0);
      this.scbTranslateX.Maximum = 2569;
      this.scbTranslateX.Minimum = -2560;
      this.scbTranslateX.Name = "scbTranslateX";
      this.scbTranslateX.Size = new System.Drawing.Size(526, 19);
      this.scbTranslateX.SmallChange = 10;
      this.scbTranslateX.TabIndex = 2;
      this.scbTranslateX.ValueChanged += new System.EventHandler(this.scbTranslateX_ValueChanged);
      // 
      // pnlLS
      // 
      this.pnlLS.Controls.Add(this.scbTranslateY);
      this.pnlLS.Controls.Add(this.scbScaleY);
      this.pnlLS.Controls.Add(this.scbRotateAngle);
      this.pnlLS.Controls.Add(this.pnlLB);
      this.pnlLS.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLS.Location = new System.Drawing.Point(0, 0);
      this.pnlLS.Name = "pnlLS";
      this.pnlLS.Size = new System.Drawing.Size(59, 562);
      this.pnlLS.TabIndex = 4;
      // 
      // scbTranslateY
      // 
      this.scbTranslateY.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbTranslateY.Location = new System.Drawing.Point(38, 0);
      this.scbTranslateY.Maximum = 2569;
      this.scbTranslateY.Minimum = -2560;
      this.scbTranslateY.Name = "scbTranslateY";
      this.scbTranslateY.Size = new System.Drawing.Size(19, 524);
      this.scbTranslateY.SmallChange = 10;
      this.scbTranslateY.TabIndex = 8;
      this.scbTranslateY.ValueChanged += new System.EventHandler(this.scbTranslateY_ValueChanged);
      // 
      // scbScaleY
      // 
      this.scbScaleY.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbScaleY.Location = new System.Drawing.Point(19, 0);
      this.scbScaleY.Maximum = 108;
      this.scbScaleY.Minimum = -99;
      this.scbScaleY.Name = "scbScaleY";
      this.scbScaleY.Size = new System.Drawing.Size(19, 524);
      this.scbScaleY.TabIndex = 7;
      this.scbScaleY.ValueChanged += new System.EventHandler(this.scbScaleY_ValueChanged);
      // 
      // scbRotateAngle
      // 
      this.scbRotateAngle.Dock = System.Windows.Forms.DockStyle.Left;
      this.scbRotateAngle.Location = new System.Drawing.Point(0, 0);
      this.scbRotateAngle.Maximum = 1809;
      this.scbRotateAngle.Minimum = -1800;
      this.scbRotateAngle.Name = "scbRotateAngle";
      this.scbRotateAngle.Size = new System.Drawing.Size(19, 524);
      this.scbRotateAngle.TabIndex = 6;
      this.scbRotateAngle.ValueChanged += new System.EventHandler(this.scbRotateAngle_ValueChanged);
      // 
      // pnlLB
      // 
      this.pnlLB.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlLB.Location = new System.Drawing.Point(0, 524);
      this.pnlLB.Name = "pnlLB";
      this.pnlLB.Size = new System.Drawing.Size(59, 38);
      this.pnlLB.TabIndex = 1;
      // 
      // pnlUser
      // 
      this.pnlUser.Controls.Add(this.btnConvert256);
      this.pnlUser.Controls.Add(this.btnSave);
      this.pnlUser.Controls.Add(this.label1);
      this.pnlUser.Controls.Add(this.label3);
      this.pnlUser.Controls.Add(this.label2);
      this.pnlUser.Controls.Add(this.lblOffsetY);
      this.pnlUser.Controls.Add(this.lblOffsetX);
      this.pnlUser.Controls.Add(this.cbxShowCutFrame);
      this.pnlUser.Controls.Add(this.nudScaleY);
      this.pnlUser.Controls.Add(this.cbxGrayScale);
      this.pnlUser.Controls.Add(this.button1);
      this.pnlUser.Controls.Add(this.scbColumn2048x512);
      this.pnlUser.Controls.Add(this.nudColumn2048x512);
      this.pnlUser.Controls.Add(this.scbColumn2048x256);
      this.pnlUser.Controls.Add(this.nudColumn2048x256);
      this.pnlUser.Controls.Add(this.btnColumn2048x256);
      this.pnlUser.Controls.Add(this.btnColumn2048x512);
      this.pnlUser.Controls.Add(this.nudTranslateY);
      this.pnlUser.Controls.Add(this.nudScaleX);
      this.pnlUser.Controls.Add(this.nudTranslateX);
      this.pnlUser.Controls.Add(this.btnCutFrame);
      this.pnlUser.Controls.Add(this.nudRotateAngle);
      this.pnlUser.Controls.Add(this.btnLoadFromFile);
      this.pnlUser.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlUser.Location = new System.Drawing.Point(0, 562);
      this.pnlUser.Name = "pnlUser";
      this.pnlUser.Size = new System.Drawing.Size(763, 152);
      this.pnlUser.TabIndex = 2;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(479, 37);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(53, 13);
      this.label1.TabIndex = 43;
      this.label1.Text = "ScaleY[1]";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(368, 37);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(53, 13);
      this.label3.TabIndex = 42;
      this.label3.Text = "ScaleX[1]";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(241, 37);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(70, 13);
      this.label2.TabIndex = 41;
      this.label2.Text = "RotateA[deg]";
      // 
      // lblOffsetY
      // 
      this.lblOffsetY.AutoSize = true;
      this.lblOffsetY.Location = new System.Drawing.Point(123, 37);
      this.lblOffsetY.Name = "lblOffsetY";
      this.lblOffsetY.Size = new System.Drawing.Size(61, 13);
      this.lblOffsetY.TabIndex = 40;
      this.lblOffsetY.Text = "OffsetY[pxl]";
      // 
      // lblOffsetX
      // 
      this.lblOffsetX.AutoSize = true;
      this.lblOffsetX.Location = new System.Drawing.Point(7, 37);
      this.lblOffsetX.Name = "lblOffsetX";
      this.lblOffsetX.Size = new System.Drawing.Size(61, 13);
      this.lblOffsetX.TabIndex = 39;
      this.lblOffsetX.Text = "OffsetX[pxl]";
      // 
      // cbxShowCutFrame
      // 
      this.cbxShowCutFrame.AutoSize = true;
      this.cbxShowCutFrame.Location = new System.Drawing.Point(221, 9);
      this.cbxShowCutFrame.Name = "cbxShowCutFrame";
      this.cbxShowCutFrame.Size = new System.Drawing.Size(101, 17);
      this.cbxShowCutFrame.TabIndex = 38;
      this.cbxShowCutFrame.Text = "Show CutFrame";
      this.cbxShowCutFrame.UseVisualStyleBackColor = true;
      this.cbxShowCutFrame.CheckedChanged += new System.EventHandler(this.cbxShowCutFrame_CheckedChanged);
      // 
      // nudScaleY
      // 
      this.nudScaleY.DecimalPlaces = 2;
      this.nudScaleY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.nudScaleY.Location = new System.Drawing.Point(533, 34);
      this.nudScaleY.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            65536});
      this.nudScaleY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
      this.nudScaleY.Name = "nudScaleY";
      this.nudScaleY.Size = new System.Drawing.Size(51, 20);
      this.nudScaleY.TabIndex = 37;
      this.nudScaleY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudScaleY.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
      this.nudScaleY.ValueChanged += new System.EventHandler(this.nudScaleY_ValueChanged);
      // 
      // cbxGrayScale
      // 
      this.cbxGrayScale.AutoSize = true;
      this.cbxGrayScale.Location = new System.Drawing.Point(89, 8);
      this.cbxGrayScale.Name = "cbxGrayScale";
      this.cbxGrayScale.Size = new System.Drawing.Size(127, 17);
      this.cbxGrayScale.TabIndex = 36;
      this.cbxGrayScale.Text = "Convert to GrayScale";
      this.cbxGrayScale.UseVisualStyleBackColor = true;
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(3, 90);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(75, 24);
      this.button1.TabIndex = 35;
      this.button1.Text = "Sequence";
      this.button1.UseVisualStyleBackColor = true;
      // 
      // scbColumn2048x512
      // 
      this.scbColumn2048x512.Location = new System.Drawing.Point(269, 118);
      this.scbColumn2048x512.Maximum = 264;
      this.scbColumn2048x512.Name = "scbColumn2048x512";
      this.scbColumn2048x512.Size = new System.Drawing.Size(182, 20);
      this.scbColumn2048x512.TabIndex = 34;
      // 
      // nudColumn2048x512
      // 
      this.nudColumn2048x512.Location = new System.Drawing.Point(213, 119);
      this.nudColumn2048x512.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudColumn2048x512.Name = "nudColumn2048x512";
      this.nudColumn2048x512.Size = new System.Drawing.Size(51, 20);
      this.nudColumn2048x512.TabIndex = 33;
      this.nudColumn2048x512.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // scbColumn2048x256
      // 
      this.scbColumn2048x256.Location = new System.Drawing.Point(269, 93);
      this.scbColumn2048x256.Maximum = 264;
      this.scbColumn2048x256.Name = "scbColumn2048x256";
      this.scbColumn2048x256.Size = new System.Drawing.Size(182, 20);
      this.scbColumn2048x256.TabIndex = 32;
      // 
      // nudColumn2048x256
      // 
      this.nudColumn2048x256.Location = new System.Drawing.Point(213, 94);
      this.nudColumn2048x256.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
      this.nudColumn2048x256.Name = "nudColumn2048x256";
      this.nudColumn2048x256.Size = new System.Drawing.Size(51, 20);
      this.nudColumn2048x256.TabIndex = 31;
      this.nudColumn2048x256.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnColumn2048x256
      // 
      this.btnColumn2048x256.Location = new System.Drawing.Point(84, 91);
      this.btnColumn2048x256.Name = "btnColumn2048x256";
      this.btnColumn2048x256.Size = new System.Drawing.Size(123, 23);
      this.btnColumn2048x256.TabIndex = 30;
      this.btnColumn2048x256.Text = "Column 2048x256";
      this.btnColumn2048x256.UseVisualStyleBackColor = true;
      // 
      // btnColumn2048x512
      // 
      this.btnColumn2048x512.Location = new System.Drawing.Point(84, 118);
      this.btnColumn2048x512.Name = "btnColumn2048x512";
      this.btnColumn2048x512.Size = new System.Drawing.Size(123, 23);
      this.btnColumn2048x512.TabIndex = 29;
      this.btnColumn2048x512.Text = "Column 2048x512";
      this.btnColumn2048x512.UseVisualStyleBackColor = true;
      // 
      // nudTranslateY
      // 
      this.nudTranslateY.Location = new System.Drawing.Point(185, 34);
      this.nudTranslateY.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
      this.nudTranslateY.Minimum = new decimal(new int[] {
            256,
            0,
            0,
            -2147483648});
      this.nudTranslateY.Name = "nudTranslateY";
      this.nudTranslateY.Size = new System.Drawing.Size(51, 20);
      this.nudTranslateY.TabIndex = 28;
      this.nudTranslateY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTranslateY.ValueChanged += new System.EventHandler(this.nudTranslateY_ValueChanged);
      // 
      // nudScaleX
      // 
      this.nudScaleX.DecimalPlaces = 2;
      this.nudScaleX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.nudScaleX.Location = new System.Drawing.Point(422, 34);
      this.nudScaleX.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            65536});
      this.nudScaleX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
      this.nudScaleX.Name = "nudScaleX";
      this.nudScaleX.Size = new System.Drawing.Size(51, 20);
      this.nudScaleX.TabIndex = 27;
      this.nudScaleX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudScaleX.Value = new decimal(new int[] {
            10,
            0,
            0,
            65536});
      this.nudScaleX.ValueChanged += new System.EventHandler(this.nudScaleX_ValueChanged);
      // 
      // nudTranslateX
      // 
      this.nudTranslateX.Location = new System.Drawing.Point(68, 34);
      this.nudTranslateX.Maximum = new decimal(new int[] {
            256,
            0,
            0,
            0});
      this.nudTranslateX.Minimum = new decimal(new int[] {
            256,
            0,
            0,
            -2147483648});
      this.nudTranslateX.Name = "nudTranslateX";
      this.nudTranslateX.Size = new System.Drawing.Size(51, 20);
      this.nudTranslateX.TabIndex = 26;
      this.nudTranslateX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudTranslateX.ValueChanged += new System.EventHandler(this.nudTranslateX_ValueChanged);
      // 
      // btnCutFrame
      // 
      this.btnCutFrame.Location = new System.Drawing.Point(3, 60);
      this.btnCutFrame.Name = "btnCutFrame";
      this.btnCutFrame.Size = new System.Drawing.Size(75, 24);
      this.btnCutFrame.TabIndex = 25;
      this.btnCutFrame.Text = "Cut Frame";
      this.btnCutFrame.UseVisualStyleBackColor = true;
      this.btnCutFrame.Click += new System.EventHandler(this.btnCutFrame_Click);
      // 
      // nudRotateAngle
      // 
      this.nudRotateAngle.DecimalPlaces = 1;
      this.nudRotateAngle.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
      this.nudRotateAngle.Location = new System.Drawing.Point(311, 34);
      this.nudRotateAngle.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
      this.nudRotateAngle.Minimum = new decimal(new int[] {
            180,
            0,
            0,
            -2147483648});
      this.nudRotateAngle.Name = "nudRotateAngle";
      this.nudRotateAngle.Size = new System.Drawing.Size(51, 20);
      this.nudRotateAngle.TabIndex = 24;
      this.nudRotateAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudRotateAngle.ValueChanged += new System.EventHandler(this.nudRotateAngle_ValueChanged);
      // 
      // btnLoadFromFile
      // 
      this.btnLoadFromFile.Location = new System.Drawing.Point(3, 4);
      this.btnLoadFromFile.Name = "btnLoadFromFile";
      this.btnLoadFromFile.Size = new System.Drawing.Size(75, 24);
      this.btnLoadFromFile.TabIndex = 23;
      this.btnLoadFromFile.Text = "Load";
      this.btnLoadFromFile.UseVisualStyleBackColor = true;
      this.btnLoadFromFile.Click += new System.EventHandler(this.btnLoadFromFile_Click);
      // 
      // DialogLoadImage
      // 
      this.DialogLoadImage.DefaultExt = "bmp";
      this.DialogLoadImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogLoadImage.Title = "Load Image";
      // 
      // DialogSaveImage
      // 
      this.DialogSaveImage.DefaultExt = "bmp";
      this.DialogSaveImage.Filter = "Image files (*.bmp)|*.bmp|All files (*.*)|*.*";
      this.DialogSaveImage.Title = "Save Image";
      // 
      // btnSave
      // 
      this.btnSave.Location = new System.Drawing.Point(84, 61);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(75, 24);
      this.btnSave.TabIndex = 44;
      this.btnSave.Text = "Save Frame";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // btnConvert256
      // 
      this.btnConvert256.Location = new System.Drawing.Point(200, 61);
      this.btnConvert256.Name = "btnConvert256";
      this.btnConvert256.Size = new System.Drawing.Size(75, 24);
      this.btnConvert256.TabIndex = 45;
      this.btnConvert256.Text = "Convert 256";
      this.btnConvert256.UseVisualStyleBackColor = true;
      this.btnConvert256.Click += new System.EventHandler(this.btnConvert256_Click);
      // 
      // CUCImageProcessing
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlUser);
      this.Controls.Add(this.pnlImageScrollZoom);
      this.Name = "CUCImageProcessing";
      this.Size = new System.Drawing.Size(763, 714);
      this.pnlImageScrollZoom.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
      this.panel1.ResumeLayout(false);
      this.pnlLS.ResumeLayout(false);
      this.pnlUser.ResumeLayout(false);
      this.pnlUser.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x512)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudColumn2048x256)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTranslateY)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudTranslateX)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudRotateAngle)).EndInit();
      this.ResumeLayout(false);

        }

    #endregion

    private System.Windows.Forms.Panel pnlImageScrollZoom;
    private System.Windows.Forms.PictureBox pbxImage;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.HScrollBar scbScaleX;
    private System.Windows.Forms.HScrollBar scbTranslateX;
    private System.Windows.Forms.Panel pnlLS;
    private System.Windows.Forms.Panel pnlLB;
    private System.Windows.Forms.Panel pnlUser;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.HScrollBar scbColumn2048x512;
    private System.Windows.Forms.NumericUpDown nudColumn2048x512;
    private System.Windows.Forms.HScrollBar scbColumn2048x256;
    private System.Windows.Forms.NumericUpDown nudColumn2048x256;
    private System.Windows.Forms.Button btnColumn2048x256;
    private System.Windows.Forms.Button btnColumn2048x512;
    private System.Windows.Forms.NumericUpDown nudTranslateY;
    private System.Windows.Forms.NumericUpDown nudScaleX;
    private System.Windows.Forms.NumericUpDown nudTranslateX;
    private System.Windows.Forms.Button btnCutFrame;
    private System.Windows.Forms.NumericUpDown nudRotateAngle;
    private System.Windows.Forms.Button btnLoadFromFile;
    private System.Windows.Forms.OpenFileDialog DialogLoadImage;
    private System.Windows.Forms.SaveFileDialog DialogSaveImage;
    private System.Windows.Forms.CheckBox cbxGrayScale;
    private System.Windows.Forms.NumericUpDown nudScaleY;
    private System.Windows.Forms.VScrollBar scbTranslateY;
    private System.Windows.Forms.VScrollBar scbScaleY;
    private System.Windows.Forms.VScrollBar scbRotateAngle;
    private System.Windows.Forms.CheckBox cbxShowCutFrame;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label lblOffsetY;
    private System.Windows.Forms.Label lblOffsetX;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Button btnConvert256;
  }
}
