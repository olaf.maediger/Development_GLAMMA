﻿using System;
using System.Drawing;
using System.Xml;
//
namespace Initdata
{
  public class CXmlBase
  {
    //
    //---------------------------------------------
    //  Section - Constant
    //---------------------------------------------
    //

    //
    //---------------------------------------------
    //  Section - Constant - Color
    //---------------------------------------------
    //
    protected const String SECTION_COLOR = "Color";
    protected const String NAME_ALPHA = "Alpha";
    protected const String NAME_RED = "Red";
    protected const String NAME_GREEN = "Green";
    protected const String NAME_BLUE = "Blue";
    protected const String NAME_OPACITY = "Opacity";
    //
    protected const Byte INIT_ALPHA = 0xFF;
    protected const Byte INIT_RED = 0xFF;
    protected const Byte INIT_GREEN = 0xFF;
    protected const Byte INIT_BLUE = 0xFF;
    protected const Byte INIT_OPACITY = 100;
    //
    //---------------------------------------------
    //  Section - Constant - Pen
    //---------------------------------------------
    //
    protected const String SECTION_PEN = "Pen";
    protected const String NAME_COLOR = "Color";
    protected const String NAME_WIDTH = "Width";
    //
    protected Color INIT_COLOR = Color.Black;
    protected const float INIT_WIDTH = 1.0f;
    //
    //---------------------------------------------
    //  Section - Constant - SolidBrush
    //---------------------------------------------
    //
    protected const String SECTION_SOLIDBRUSH = "SolidBrush";
    //
    protected String FFileName;
    protected CNotify FNotify;
    protected XmlDocument FXmlDocument;
    protected XmlNode FNodeBase;
    protected XmlNode FNodeParent;

  }
}
