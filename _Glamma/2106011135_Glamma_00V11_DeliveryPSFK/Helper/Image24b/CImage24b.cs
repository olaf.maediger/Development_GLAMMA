﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Net.Http.Headers;

namespace Image24b
{
  public class CImage24b
  {
    const int NORMALIZED_WIDTH = 256;
    const int NORMALIZED_HEIGHT = 256;
    const int PROJECTION_WIDTH    = 512;
    const int PROJECTION_HEIGHT   = 512;
    //
    const byte BACKCOLOR_R        = 166;
    const byte BACKCOLOR_G        = 166;
    const byte BACKCOLOR_B        = 166;
    //
    private Bitmap FBitmapSource;
    private Bitmap FBitmapNormalized;
    private Bitmap FBitmapProjection;
    private Bitmap FBitmapFile;
    //
    public CImage24b()
    {
      FBitmapSource = new Bitmap(NORMALIZED_WIDTH, NORMALIZED_HEIGHT, PixelFormat.Format24bppRgb);
      FBitmapNormalized = new Bitmap(NORMALIZED_WIDTH, NORMALIZED_HEIGHT, PixelFormat.Format24bppRgb);
      FBitmapProjection = new Bitmap(PROJECTION_WIDTH, PROJECTION_HEIGHT, PixelFormat.Format24bppRgb);
      FBitmapFile = new Bitmap(NORMALIZED_WIDTH, NORMALIZED_HEIGHT, PixelFormat.Format24bppRgb);
    }

    public Bitmap GetBitmapSource()
    {
      return FBitmapSource;
    }
    public Bitmap GetBitmapNormalized()
    {
      return FBitmapNormalized;
    }
    public Bitmap GetBitmapProjection()
    {
      return FBitmapProjection;
    }
    public Bitmap GetBitmapFile()
    {
      return FBitmapFile;
    }


    //public 


    public Boolean LoadSourceFromFile(String filename)
    {
      try
      {
        FBitmapSource = new Bitmap(filename);
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    unsafe public Boolean NormalizeSource(Boolean greyscale)
    {
      try
      { // BitmapSource -> BitmapNormalized
        Graphics G = Graphics.FromImage(FBitmapNormalized);
        G.InterpolationMode = InterpolationMode.NearestNeighbor;
        G.CompositingQuality = CompositingQuality.AssumeLinear;
        G.SmoothingMode = SmoothingMode.None;
        G.DrawImage(FBitmapSource, 0, 0, NORMALIZED_WIDTH, NORMALIZED_HEIGHT);
        G.Dispose();
        // BitmapNormalized -> greyed
        if (greyscale)
        {          
          Rectangle R = new Rectangle(0, 0, FBitmapNormalized.Width, FBitmapNormalized.Height);
          BitmapData BD = FBitmapNormalized.LockBits(R, ImageLockMode.ReadWrite, PixelFormat.Format24bppRgb);
          IntPtr PBD = BD.Scan0;
          //
          int BC = Math.Abs(BD.Stride) * FBitmapNormalized.Height;
          Console.WriteLine(BC);
          for (int BI = 0; BI < BC; BI += 3)
          {
            byte GS = (byte)((*(byte*)(PBD + 0) + *(byte*)(PBD + 0) + *(byte*)(PBD + 0)) / 3) ;
            *(byte*)PBD = GS; PBD += 1;
            *(byte*)PBD = GS; PBD += 1;
            *(byte*)PBD = GS; PBD += 1;
          }
          //
          FBitmapNormalized.UnlockBits(BD);
        }
        //
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean TranslateProjection(Int32 dx, Int32 dy)
    {
      try
      { 
        Graphics G = Graphics.FromImage(FBitmapProjection);
        G.InterpolationMode = InterpolationMode.NearestNeighbor;
        G.CompositingQuality = CompositingQuality.AssumeLinear;
        G.SmoothingMode = SmoothingMode.None;
        //
        Int32 W = FBitmapNormalized.Width;
        Int32 H = FBitmapNormalized.Height;
        //
        Matrix M = new Matrix();
        M.Translate(dx, dy);
        G.Transform = M;
        //
        G.DrawImage(FBitmapNormalized, 0, 0, W, H);
        G.Dispose();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean RotateProjection(Double angledeg)
    {
      try
      {
        Graphics G = Graphics.FromImage(FBitmapProjection);
        G.InterpolationMode = InterpolationMode.NearestNeighbor;
        G.CompositingQuality = CompositingQuality.AssumeLinear;
        G.SmoothingMode = SmoothingMode.None;
        //
        Int32 W = FBitmapNormalized.Width;
        Int32 H = FBitmapNormalized.Height;
        //
        Matrix M = new Matrix();
        float AR = (float)angledeg;
        M.RotateAt(-AR, new PointF(0, 0));
        G.Transform = M;
        //
        G.DrawImage(FBitmapNormalized, 0, 0, W, H);
        G.Dispose();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }



    public Boolean TransformProjection(float scalex, float scaley, float angledeg, 
                                       float dx, float dy, Boolean cuttingframe)
    {
      try
      {
        Graphics G = Graphics.FromImage(FBitmapProjection);
        G.InterpolationMode = InterpolationMode.NearestNeighbor;
        G.CompositingQuality = CompositingQuality.AssumeLinear;
        G.SmoothingMode = SmoothingMode.None;
        //
        Int32 NW = FBitmapNormalized.Width;
        Int32 NH = FBitmapNormalized.Height;
        Int32 PW = FBitmapProjection.Width;
        Int32 PH = FBitmapProjection.Height;
        //
        G.FillRectangle(new SolidBrush(Color.FromArgb(255, 128, 128, 88)), 0, 0, PW, PH);
        //
        Matrix M = new Matrix();
        M.Translate(PW / 2 + dx, PH / 2 + dy);
        M.RotateAt(angledeg, new PointF(0, 0));
        M.Scale(scalex, scaley);
        M.Translate(-NW / 2, -NH / 2);
        G.Transform = M;
        //
        G.DrawImage(FBitmapNormalized, 0, 0, NW, NH);
        G.ResetTransform();
        if (cuttingframe)
        {
          G.DrawRectangle(new Pen(Color.FromArgb(0xFF, 0xFF, 0x88, 0xFF), 3), 126, 126, 259, 259);
        }
        G.Dispose();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public Boolean CutFrame(float scalex, float scaley, float angledeg,
                            float dx, float dy, Boolean showcutframe)
    {
      try
      {
        TransformProjection(scalex, scaley, angledeg, dx, dy, false);
        //
        Graphics G = Graphics.FromImage(FBitmapNormalized);
        G.InterpolationMode = InterpolationMode.NearestNeighbor;
        G.CompositingQuality = CompositingQuality.AssumeLinear;
        G.SmoothingMode = SmoothingMode.None;
        //
        Int32 NW = FBitmapNormalized.Width;
        Int32 NH = FBitmapNormalized.Height;
        Int32 PW = FBitmapProjection.Width;
        Int32 PH = FBitmapProjection.Height;
        //
        Rectangle RF = new Rectangle(0, 0, NORMALIZED_WIDTH, NORMALIZED_HEIGHT);
        Rectangle RS = new Rectangle(NORMALIZED_WIDTH / 2, NORMALIZED_HEIGHT / 2, NORMALIZED_WIDTH, NORMALIZED_HEIGHT);
        //
        G.FillRectangle(new SolidBrush(Color.FromArgb(255, 128, 88, 128)), 0, 0, NW, NH);
        G.DrawImage(FBitmapProjection, RF, RS, GraphicsUnit.Pixel);
        //
        G.Dispose();
        return true;
      }
      catch (Exception)
      {
        return false;
      }
    }

    //public Boolean SaveNormalizedToFile(String filename)
    //{
    //  try
    //  {
    //    FBitmapNormalized.Save(filename);
    //    return true;
    //  }
    //  catch (Exception)
    //  {
    //    return false;
    //  }
    //}




    unsafe public Boolean ConvertShow256x256(int indexshow, Boolean cuttingframe)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmapNormalized.Width, FBitmapNormalized.Height);
        BitmapData BDS = FBitmapNormalized.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        // (256, 256) -> (2048, 256)
        Bitmap BitmapTarget = new Bitmap(NORMALIZED_WIDTH, NORMALIZED_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RT = new Rectangle(0, 0, BitmapTarget.Width, BitmapTarget.Height);
        BitmapData BDT = BitmapTarget.LockBits(RT, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        Int32 PBT = (Int32)BDT.Scan0;
        //
        // BitmapSource to ByteVector
        int BCS = Math.Abs(BDS.Stride) * FBitmapNormalized.Height;
        //
        byte[] BVRR = new byte[NORMALIZED_HEIGHT];
        byte[] BVRG = new byte[NORMALIZED_HEIGHT];
        byte[] BVRB = new byte[NORMALIZED_HEIGHT];
        //
        // BitmapSource[column] -> BVRGB
        int RIBV = 0;
        for (int RIS = 0; RIS < NORMALIZED_HEIGHT; RIS++)
        {
          BVRR[RIBV] = *(byte*)(PBS + 0 + 3 * (indexshow + RIS * NORMALIZED_WIDTH));
          BVRG[RIBV] = *(byte*)(PBS + 1 + 3 * (indexshow + RIS * NORMALIZED_WIDTH));
          BVRB[RIBV] = *(byte*)(PBS + 2 + 3 * (indexshow + RIS * NORMALIZED_WIDTH));
          RIBV++;
        }
        // BVRGB -> BitmapTarget[all]
        RIBV = 0;
        int RLBT = Math.Abs(BDT.Stride);
        for (int RIT = 0; RIT < NORMALIZED_HEIGHT; RIT++)
        {
          for (int CIT = 0; CIT < 3 * NORMALIZED_WIDTH; CIT += 3)
          { // Col0 - the only one!
            *(byte*)(PBT + 0x00 + CIT + RIT * RLBT) = BVRR[RIBV];
            *(byte*)(PBT + 0x01 + CIT + RIT * RLBT) = BVRG[RIBV];
            *(byte*)(PBT + 0x02 + CIT + RIT * RLBT) = BVRB[RIBV];
          }
          RIBV++;
        }
        //
        FBitmapNormalized.UnlockBits(BDS);
        BitmapTarget.UnlockBits(BDT);
        //-----------------------------------------------------------------
        int BPW = FBitmapProjection.Width;
        int BPH = FBitmapProjection.Height;
        int SIX = (BPW - BitmapTarget.Width) / 2;
        int SIY = (BPH - BitmapTarget.Height) / 2;
        Graphics G = Graphics.FromImage(FBitmapProjection);
        G.InterpolationMode = InterpolationMode.NearestNeighbor;
        G.CompositingQuality = CompositingQuality.AssumeLinear;
        G.SmoothingMode = SmoothingMode.None;
        G.FillRectangle(new SolidBrush(Color.FromArgb(255, 128, 88, 128)), 0, 0, BPW, BPH);
        G.DrawImage(BitmapTarget, SIX, SIY, BitmapTarget.Width, BitmapTarget.Height);
        if (cuttingframe)
        {
          G.DrawRectangle(new Pen(Color.FromArgb(0xFF, 0xFF, 0x88, 0xFF), 3), 126, 126, 259, 259);
        }
        G.Dispose();
        //
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }








    unsafe public Boolean ConvertToFile2048x256(String filemask, int indexstart, int indexend, int indexstep)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmapNormalized.Width, FBitmapNormalized.Height);
        BitmapData BDS = FBitmapNormalized.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        // (256, 256) -> (2048, 256)
        Bitmap BitmapTarget = new Bitmap(8 * NORMALIZED_WIDTH, NORMALIZED_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RT = new Rectangle(0, 0, BitmapTarget.Width, BitmapTarget.Height);
        BitmapData BDT = BitmapTarget.LockBits(RT, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        Int32 PBT = (Int32)BDT.Scan0;
        //
        // BitmapSource to ByteVector
        int BCS = Math.Abs(BDS.Stride) * FBitmapNormalized.Height;
        //
        byte[] BVRR = new byte[NORMALIZED_HEIGHT];
        byte[] BVRG = new byte[NORMALIZED_HEIGHT];
        byte[] BVRB = new byte[NORMALIZED_HEIGHT];
        //
        for (int CIS = indexstart; CIS < indexend; CIS += indexstep)
        { // BitmapSource[column] -> BVRGB
          int RIBV = 0;
          for (int RIS = 0; RIS < NORMALIZED_HEIGHT; RIS++)
          {
            BVRR[RIBV] = *(byte*)(PBS + 0 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
            BVRG[RIBV] = *(byte*)(PBS + 1 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
            BVRB[RIBV] = *(byte*)(PBS + 2 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
            RIBV++;
          }
          // BVRGB -> BitmapTarget[all]
          RIBV = 0;
          int RLBT = Math.Abs(BDT.Stride);
          for (int RIT = 0; RIT < NORMALIZED_HEIGHT; RIT++)
          {
            for (int CIT = 0; CIT < 3 * 8 * NORMALIZED_WIDTH; CIT += 3 * 8)
            { // Col0
              *(byte*)(PBT + 0x00 + CIT + RIT * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x01 + CIT + RIT * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x02 + CIT + RIT * RLBT) = BVRB[RIBV];
              // Col1
              *(byte*)(PBT + 0x03 + CIT + RIT * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x04 + CIT + RIT * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x05 + CIT + RIT * RLBT) = BVRB[RIBV];
              // Col2
              *(byte*)(PBT + 0x06 + CIT + RIT * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x07 + CIT + RIT * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x08 + CIT + RIT * RLBT) = BVRB[RIBV];
              // Col3
              *(byte*)(PBT + 0x09 + CIT + RIT * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x0A + CIT + RIT * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x0B + CIT + RIT * RLBT) = BVRB[RIBV];
              // Col4
              *(byte*)(PBT + 0x0C + CIT + RIT * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x0D + CIT + RIT * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x0E + CIT + RIT * RLBT) = BVRB[RIBV];
              // Col5
              *(byte*)(PBT + 0x0F + CIT + RIT * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x10 + CIT + RIT * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x11 + CIT + RIT * RLBT) = BVRB[RIBV];
              // Col6
              *(byte*)(PBT + 0x12 + CIT + RIT * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x13 + CIT + RIT * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x14 + CIT + RIT * RLBT) = BVRB[RIBV];
              // Col7
              *(byte*)(PBT + 0x15 + CIT + RIT * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x16 + CIT + RIT * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x17 + CIT + RIT * RLBT) = BVRB[RIBV];
            }
            RIBV++;
          }
          //
          BitmapTarget.Save(String.Format(".\\result\\" + filemask, CIS));
        }
        //
        FBitmapNormalized.UnlockBits(BDS);
        BitmapTarget.UnlockBits(BDT);
        //
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    unsafe public Boolean ConvertToFile2048x512(String filemask, int indexstart, int indexend, int indexstep)
    {
      try
      {
        Rectangle RS = new Rectangle(0, 0, FBitmapNormalized.Width, FBitmapNormalized.Height);
        BitmapData BDS = FBitmapNormalized.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
        IntPtr PBS = BDS.Scan0;
        // (256, 256) -> (2048, 256)
        Bitmap BitmapTarget = new Bitmap(8 * NORMALIZED_WIDTH, 2 * NORMALIZED_HEIGHT, PixelFormat.Format24bppRgb);
        Rectangle RT = new Rectangle(0, 0, BitmapTarget.Width, BitmapTarget.Height);
        BitmapData BDT = BitmapTarget.LockBits(RT, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
        Int32 PBT = (Int32)BDT.Scan0;
        //
        // BitmapSource to ByteVector
        int BCS = Math.Abs(BDS.Stride) * FBitmapNormalized.Height;
        //
        byte[] BVRR = new byte[NORMALIZED_HEIGHT];
        byte[] BVRG = new byte[NORMALIZED_HEIGHT];
        byte[] BVRB = new byte[NORMALIZED_HEIGHT];
        //
        for (int CIS = indexstart; CIS < indexend; CIS += indexstep)
        { // BitmapSource[column] -> BVRGB
          int RIBV = 0;
          for (int RIS = 0; RIS < NORMALIZED_HEIGHT; RIS++)
          {
            BVRR[RIBV] = *(byte*)(PBS + 0 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
            BVRG[RIBV] = *(byte*)(PBS + 1 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
            BVRB[RIBV] = *(byte*)(PBS + 2 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
            RIBV++;
          }
          // BVRGB -> BitmapTarget[all]
          RIBV = 0;
          int RLBT = Math.Abs(BDT.Stride);
          for (int RIT = 0; RIT < 2 * NORMALIZED_HEIGHT; RIT += 2)
          { // Row0
            for (int CIT = 0; CIT < 3 * 8 * NORMALIZED_WIDTH; CIT += 3 * 8)
            { // Col0
              *(byte*)(PBT + 0x00 + CIT + (0 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x01 + CIT + (0 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x02 + CIT + (0 + RIT) * RLBT) = BVRB[RIBV];
              // Col1
              *(byte*)(PBT + 0x03 + CIT + (0 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x04 + CIT + (0 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x05 + CIT + (0 + RIT) * RLBT) = BVRB[RIBV];
              // Col2
              *(byte*)(PBT + 0x06 + CIT + (0 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x07 + CIT + (0 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x08 + CIT + (0 + RIT) * RLBT) = BVRB[RIBV];
              // Col3
              *(byte*)(PBT + 0x09 + CIT + (0 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x0A + CIT + (0 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x0B + CIT + (0 + RIT) * RLBT) = BVRB[RIBV];
              // Col4
              *(byte*)(PBT + 0x0C + CIT + (0 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x0D + CIT + (0 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x0E + CIT + (0 + RIT) * RLBT) = BVRB[RIBV];
              // Col5
              *(byte*)(PBT + 0x0F + CIT + (0 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x10 + CIT + (0 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x11 + CIT + (0 + RIT) * RLBT) = BVRB[RIBV];
              // Col6
              *(byte*)(PBT + 0x12 + CIT + (0 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x13 + CIT + (0 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x14 + CIT + (0 + RIT) * RLBT) = BVRB[RIBV];
              // Col7
              *(byte*)(PBT + 0x15 + CIT + (0 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x16 + CIT + (0 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x17 + CIT + (0 + RIT) * RLBT) = BVRB[RIBV];
            }
            // Row1
            for (int CIT = 0; CIT < 3 * 8 * NORMALIZED_WIDTH; CIT += 3 * 8)
            { // Col0
              *(byte*)(PBT + 0x00 + CIT + (1 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x01 + CIT + (1 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x02 + CIT + (1 + RIT) * RLBT) = BVRB[RIBV];
              // Col1
              *(byte*)(PBT + 0x03 + CIT + (1 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x04 + CIT + (1 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x05 + CIT + (1 + RIT) * RLBT) = BVRB[RIBV];
              // Col2
              *(byte*)(PBT + 0x06 + CIT + (1 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x07 + CIT + (1 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x08 + CIT + (1 + RIT) * RLBT) = BVRB[RIBV];
              // Col3
              *(byte*)(PBT + 0x09 + CIT + (1 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x0A + CIT + (1 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x0B + CIT + (1 + RIT) * RLBT) = BVRB[RIBV];
              // Col4
              *(byte*)(PBT + 0x0C + CIT + (1 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x0D + CIT + (1 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x0E + CIT + (1 + RIT) * RLBT) = BVRB[RIBV];
              // Col5
              *(byte*)(PBT + 0x0F + CIT + (1 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x10 + CIT + (1 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x11 + CIT + (1 + RIT) * RLBT) = BVRB[RIBV];
              // Col6
              *(byte*)(PBT + 0x12 + CIT + (1 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x13 + CIT + (1 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x14 + CIT + (1 + RIT) * RLBT) = BVRB[RIBV];
              // Col7
              *(byte*)(PBT + 0x15 + CIT + (1 + RIT) * RLBT) = BVRR[RIBV];
              *(byte*)(PBT + 0x16 + CIT + (1 + RIT) * RLBT) = BVRG[RIBV];
              *(byte*)(PBT + 0x17 + CIT + (1 + RIT) * RLBT) = BVRB[RIBV];
            }
            RIBV++;
          }
          //
          BitmapTarget.Save(String.Format(".\\result\\" + filemask, CIS));
        }
        //
        FBitmapNormalized.UnlockBits(BDS);
        BitmapTarget.UnlockBits(BDT);
        //
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }


  }
}


















// OK!!! unsafe public Boolean DuplicateColumn256()
//{
//  try
//  {
//    Rectangle RS = new Rectangle(0, 0, FBitmapNormalized.Width, FBitmapNormalized.Height);
//    BitmapData BDS = FBitmapNormalized.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//    IntPtr PBS = BDS.Scan0;
//    //
//    Bitmap BitmapTarget = new Bitmap(NORMALIZED_WIDTH, NORMALIZED_HEIGHT, PixelFormat.Format24bppRgb);
//    Rectangle RT = new Rectangle(0, 0, BitmapTarget.Width, BitmapTarget.Height);
//    BitmapData BDT = BitmapTarget.LockBits(RT, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//    Int32 PBT = (Int32)BDT.Scan0;
//    //
//    // BitmapSource to ByteVector
//    int BCS = Math.Abs(BDS.Stride) * FBitmapNormalized.Height;
//    //
//    byte[] BVRR = new byte[NORMALIZED_HEIGHT];
//    byte[] BVRG = new byte[NORMALIZED_HEIGHT];
//    byte[] BVRB = new byte[NORMALIZED_HEIGHT];
//    //
//    for (int CIS = 0; CIS < NORMALIZED_WIDTH; CIS += 20) // 
//    { // BitmapSource[column] -> BVRGB
//      int RIBV = 0;
//      for (int RIS = 0; RIS < NORMALIZED_HEIGHT; RIS++)
//      {
//        BVRR[RIBV] = *(byte*)(PBS + 0 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
//        BVRG[RIBV] = *(byte*)(PBS + 1 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
//        BVRB[RIBV] = *(byte*)(PBS + 2 + 3 * (CIS + RIS * NORMALIZED_WIDTH));
//        RIBV++;
//      }
//      // BVRGB -> BitmapTarget[all]
//      RIBV = 0;
//      int RLBT = Math.Abs(BDT.Stride);
//      for (int RIT = 0; RIT < NORMALIZED_HEIGHT; RIT++)
//      {
//        for (int CIT = 0; CIT < 3 * NORMALIZED_WIDTH; CIT += 3)
//        {
//          *(byte*)(PBT + 0 + CIT + RIT * RLBT) = BVRR[RIBV];
//          *(byte*)(PBT + 1 + CIT + RIT * RLBT) = BVRG[RIBV];
//          *(byte*)(PBT + 2 + CIT + RIT * RLBT) = BVRB[RIBV];
//        }
//        RIBV++;
//      }
//      //
//      BitmapTarget.Save(String.Format("BT{0:000}.bmp", CIS));

//    }

//    //
//    FBitmapNormalized.UnlockBits(BDS);
//    BitmapTarget.UnlockBits(BDT);
//    //
//    return true;
//  }
//  catch (Exception e)
//  {
//    Console.WriteLine(e.Message);
//    return false;
//  }
//}



//  //
//  FBitmapConverted = new Bitmap(1 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//  Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//  BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//  IntPtr PBD = BDD.Scan0;
//  //
//  int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//  byte[] BValues = new byte[ByteCountSource];
//  Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//  //
//  FBitmap256x256.UnlockBits(BDS);
//  //
//  Int32 SCD = 3 * 256;
//  Int32 BID0 = (Int32)PBD;
//  Int32 BID1 = (Int32)PBD + SCD;
//  Int32 CID = 0;
//  Int32 RID = 0;
//  for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//  {
//    byte B0 = BValues[BIS + 0];
//    byte B1 = BValues[BIS + 1];
//    byte B2 = BValues[BIS + 2];
//    if (grayscale)
//    {
//      byte GS = (byte)((B0 + B1 + B2) / 3);
//      B0 = GS;
//      B1 = GS;
//      B2 = GS;
//    }
//    *((byte*)(BID0 + 0)) = B0;
//    *((byte*)(BID0 + 1)) = B1;
//    *((byte*)(BID0 + 2)) = B2;
//    CID += 3;
//    if (SCD <= CID)
//    {
//      CID = 0;
//      RID++;
//      BID0 = (Int32)PBD + 1 * RID * SCD;
//      BID1 = BID0 + SCD;
//    }
//    else
//    {
//      BID0 += 3;
//      BID1 += 3;
//    }
//  }
//  //
//  FBitmapConverted.UnlockBits(BDD);
//  //
//  //
//  return true;
//}
//catch (Exception)
//{
//  return false;
//}









//################################################################################################################

//################################################################################################################

//################################################################################################################

//unsafe public Boolean ConvertGrayScale()
//{
//  try
//  {
//    Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//    BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//    IntPtr PBS = BDS.Scan0;
//    //
//    FBitmapConverted = new Bitmap(1 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//    Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//    BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//    IntPtr PBD = BDD.Scan0;
//    //
//    int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//    byte[] BValues = new byte[ByteCountSource];
//    Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//    //
//    FBitmap256x256.UnlockBits(BDS);
//    //
//    Int32 SCD = 3 * 256;
//    Int32 BID0 = (Int32)PBD;
//    Int32 BID1 = (Int32)PBD + SCD;
//    Int32 CID = 0;
//    Int32 RID = 0;
//    for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//    {
//      byte B0 = BValues[BIS + 0];
//      byte B1 = BValues[BIS + 1];
//      byte B2 = BValues[BIS + 2];
//      byte GV = (byte)(0.0 + (B0 + B1 + B2) / 3);
//      *((byte*)(BID0 + 0)) = GV;
//      *((byte*)(BID0 + 1)) = GV;
//      *((byte*)(BID0 + 2)) = GV;
//      CID += 3;
//      if (SCD <= CID)
//      {
//        CID = 0;
//        RID++;
//        BID0 = (Int32)PBD + 1 * RID * SCD;
//        BID1 = BID0 + SCD;
//      }
//      else
//      {
//        BID0 += 3;
//        BID1 += 3;
//      }
//    }
//    //
//    FBitmapConverted.UnlockBits(BDD);
//    //
//    //
//    return true;
//  }
//  catch (Exception)
//  {
//    return false;
//  }
//}

//unsafe public Boolean Column2048x256(Boolean grayscale)
//{
//  try
//  {
//    Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//    BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//    IntPtr PBS = BDS.Scan0;
//    //
//    FBitmapConverted = new Bitmap(8 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//    Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//    BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//    IntPtr PBD = BDD.Scan0;
//    //
//    int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//    byte[] BValues = new byte[ByteCountSource];
//    Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//    //
//    FBitmap256x256.UnlockBits(BDS);
//    //
//    Int32 SCD = 3 * 2048;
//    Int32 BID0 = (Int32)PBD;
//    Int32 CID = 0;
//    Int32 RID = 0;
//    for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//    {
//      byte B0 = BValues[BIS + 0];
//      byte B1 = BValues[BIS + 1];
//      byte B2 = BValues[BIS + 2];
//      if (grayscale)
//      {
//        byte GS = (byte)((B0 + B1 + B2) / 3);
//        B0 = GS;
//        B1 = GS;
//        B2 = GS;
//      }
//      //-------------------------
//      *((byte*)(BID0 + 0)) = B0;
//      *((byte*)(BID0 + 1)) = B1;
//      *((byte*)(BID0 + 2)) = B2;
//      *((byte*)(BID0 + 3)) = B0;
//      *((byte*)(BID0 + 4)) = B1;
//      *((byte*)(BID0 + 5)) = B2;
//      *((byte*)(BID0 + 6)) = B0;
//      *((byte*)(BID0 + 7)) = B1;
//      *((byte*)(BID0 + 8)) = B2;
//      *((byte*)(BID0 + 9)) = B0;
//      *((byte*)(BID0 + 10)) = B1;
//      *((byte*)(BID0 + 11)) = B2;
//      *((byte*)(BID0 + 12)) = B0;
//      *((byte*)(BID0 + 13)) = B1;
//      *((byte*)(BID0 + 14)) = B2;
//      *((byte*)(BID0 + 15)) = B0;
//      *((byte*)(BID0 + 16)) = B1;
//      *((byte*)(BID0 + 17)) = B2;
//      *((byte*)(BID0 + 18)) = B0;
//      *((byte*)(BID0 + 19)) = B1;
//      *((byte*)(BID0 + 20)) = B2;
//      *((byte*)(BID0 + 21)) = B0;
//      *((byte*)(BID0 + 22)) = B1;
//      *((byte*)(BID0 + 23)) = B2;
//      //--------------------------
//      CID += 24;
//      if (SCD <= CID)
//      {
//        CID = 0;
//        RID++;
//        BID0 = (Int32)PBD + 1 * RID * SCD;
//      }
//      else
//      {
//        BID0 += 24;
//      }
//    }
//    //
//    FBitmapConverted.UnlockBits(BDD);
//    //
//    return true;
//  }
//  catch (Exception)
//  {
//    return false;
//  }
//}

//unsafe public Boolean Column2048x512(Boolean grayscale)
//{
//  try
//  {
//    Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//    BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//    IntPtr PBS = BDS.Scan0;
//    //
//    FBitmapConverted = new Bitmap(8 * BITMAP_WIDTH, 2 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//    Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//    BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//    IntPtr PBD = BDD.Scan0;
//    //
//    int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//    byte[] BValues = new byte[ByteCountSource];
//    Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//    //
//    FBitmap256x256.UnlockBits(BDS);
//    //
//    Int32 SCD = 3 * 2048;// 
//    Int32 BID0 = (Int32)PBD;
//    Int32 BID1 = (Int32)PBD + SCD;
//    Int32 CID = 0;
//    Int32 RID = 0;
//    for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//    {
//      byte B0 = BValues[BIS + 0];
//      byte B1 = BValues[BIS + 1];
//      byte B2 = BValues[BIS + 2];
//      if (grayscale)
//      {
//        byte GS = (byte)((B0 + B1 + B2) / 3);
//        B0 = GS;
//        B1 = GS;
//        B2 = GS;
//      }
//      //-------------------------
//      *((byte*)(BID0 + 0)) = B0;
//      *((byte*)(BID0 + 1)) = B1;
//      *((byte*)(BID0 + 2)) = B2;
//      *((byte*)(BID0 + 3)) = B0;
//      *((byte*)(BID0 + 4)) = B1;
//      *((byte*)(BID0 + 5)) = B2;
//      *((byte*)(BID0 + 6)) = B0;
//      *((byte*)(BID0 + 7)) = B1;
//      *((byte*)(BID0 + 8)) = B2;
//      *((byte*)(BID0 + 9)) = B0;
//      *((byte*)(BID0 + 10)) = B1;
//      *((byte*)(BID0 + 11)) = B2;
//      *((byte*)(BID0 + 12)) = B0;
//      *((byte*)(BID0 + 13)) = B1;
//      *((byte*)(BID0 + 14)) = B2;
//      *((byte*)(BID0 + 15)) = B0;
//      *((byte*)(BID0 + 16)) = B1;
//      *((byte*)(BID0 + 17)) = B2;
//      *((byte*)(BID0 + 18)) = B0;
//      *((byte*)(BID0 + 19)) = B1;
//      *((byte*)(BID0 + 20)) = B2;
//      *((byte*)(BID0 + 21)) = B0;
//      *((byte*)(BID0 + 22)) = B1;
//      *((byte*)(BID0 + 23)) = B2;
//      //-------------------------
//      *((byte*)(BID1 + 0)) = B0;
//      *((byte*)(BID1 + 1)) = B1;
//      *((byte*)(BID1 + 2)) = B2;
//      *((byte*)(BID1 + 3)) = B0;
//      *((byte*)(BID1 + 4)) = B1;
//      *((byte*)(BID1 + 5)) = B2;
//      *((byte*)(BID1 + 6)) = B0;
//      *((byte*)(BID1 + 7)) = B1;
//      *((byte*)(BID1 + 8)) = B2;
//      *((byte*)(BID1 + 9)) = B0;
//      *((byte*)(BID1 + 10)) = B1;
//      *((byte*)(BID1 + 11)) = B2;
//      *((byte*)(BID1 + 12)) = B0;
//      *((byte*)(BID1 + 13)) = B1;
//      *((byte*)(BID1 + 14)) = B2;
//      *((byte*)(BID1 + 15)) = B0;
//      *((byte*)(BID1 + 16)) = B1;
//      *((byte*)(BID1 + 17)) = B2;
//      *((byte*)(BID1 + 18)) = B0;
//      *((byte*)(BID1 + 19)) = B1;
//      *((byte*)(BID1 + 20)) = B2;
//      *((byte*)(BID1 + 21)) = B0;
//      *((byte*)(BID1 + 22)) = B1;
//      *((byte*)(BID1 + 23)) = B2;
//      //--------------------------
//      CID += 24;
//      if (SCD <= CID)
//      {
//        CID = 0;
//        RID++;
//        BID0 = (Int32)PBD + 2 * RID * SCD;
//        BID1 = BID0 + SCD;
//      }
//      else
//      {
//        BID0 += 24;
//        BID1 += 24;
//      }
//    }
//    //
//    FBitmapConverted.UnlockBits(BDD);
//    //
//    return true;
//  }
//  catch (Exception)
//  {
//    return false;
//  }
//}

//public Boolean LoadFromFile(String filename)
//{
//  try
//  {
//    FBitmapSource = new Bitmap(filename);
//    RescaleSourceToPicture(0, 0, 1.0, 0.0); // Center!
//    return true;
//  }
//  catch (Exception)
//  {
//    return false;
//  }
//}


////
////----------------------------------------------------------
////  256 x ...
////----------------------------------------------------------
//unsafe public Boolean Convert256x256(Boolean grayscale)
//    {
//      //try
//      //{
//      //  Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//      //  BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBS = BDS.Scan0;
//      //  //
//      //  FBitmapConverted = new Bitmap(1 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//      //  Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//      //  BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBD = BDD.Scan0;
//      //  //
//      //  int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//      //  byte[] BValues = new byte[ByteCountSource];
//      //  Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//      //  //
//      //  FBitmap256x256.UnlockBits(BDS);
//      //  //
//      //  Int32 SCD = 3 * 256;
//      //  Int32 BID0 = (Int32)PBD;
//      //  Int32 BID1 = (Int32)PBD + SCD;
//      //  Int32 CID = 0;
//      //  Int32 RID = 0;
//      //  for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//      //  {
//      //    byte B0 = BValues[BIS + 0];
//      //    byte B1 = BValues[BIS + 1];
//      //    byte B2 = BValues[BIS + 2];
//      //    if (grayscale)
//      //    {
//      //      byte GS = (byte)((B0 + B1 + B2) / 3);
//      //      B0 = GS;
//      //      B1 = GS;
//      //      B2 = GS;
//      //    }
//      //    *((byte*)(BID0 + 0)) = B0;
//      //    *((byte*)(BID0 + 1)) = B1;
//      //    *((byte*)(BID0 + 2)) = B2;
//      //    CID += 3;
//      //    if (SCD <= CID)
//      //    {
//      //      CID = 0;
//      //      RID++;
//      //      BID0 = (Int32)PBD + 1 * RID * SCD;
//      //      BID1 = BID0 + SCD;
//      //    }
//      //    else
//      //    {
//      //      BID0 += 3;
//      //      BID1 += 3;
//      //    }
//      //  }
//      //  //
//      //  FBitmapConverted.UnlockBits(BDD);
//      //  //
//      //  //
//      //  return true;
//      //}
//      //catch (Exception)
//      //{
//      //  return false;
//      //}
//      return false;
//    }

//    unsafe public Boolean Convert512x256(Boolean grayscale)
//    {
//      //try
//      //{
//      //  Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//      //  BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBS = BDS.Scan0;
//      //  //
//      //  FBitmapConverted = new Bitmap(2 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//      //  Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//      //  BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBD = BDD.Scan0;
//      //  //
//      //  int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//      //  byte[] BValues = new byte[ByteCountSource];
//      //  Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//      //  //
//      //  FBitmap256x256.UnlockBits(BDS);
//      //  //
//      //  Int32 SCD = 3 * 512;
//      //  Int32 BID0 = (Int32)PBD;
//      //  Int32 CID = 0;
//      //  Int32 RID = 0;
//      //  for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//      //  {
//      //    byte B0 = BValues[BIS + 0];
//      //    byte B1 = BValues[BIS + 1];
//      //    byte B2 = BValues[BIS + 2];
//      //    if (grayscale)
//      //    {
//      //      byte GS = (byte)((B0 + B1 + B2) / 3);
//      //      B0 = GS;
//      //      B1 = GS;
//      //      B2 = GS;
//      //    }
//      //    *((byte*)(BID0 + 0)) = B0;
//      //    *((byte*)(BID0 + 1)) = B1;
//      //    *((byte*)(BID0 + 2)) = B2;
//      //    *((byte*)(BID0 + 3)) = B0;
//      //    *((byte*)(BID0 + 4)) = B1;
//      //    *((byte*)(BID0 + 5)) = B2;
//      //    CID += 6;
//      //    if (SCD <= CID)
//      //    {
//      //      CID = 0;
//      //      RID++;
//      //      BID0 = (Int32)PBD + 1 * RID * SCD;
//      //    }
//      //    else
//      //    {
//      //      BID0 += 6;
//      //    }
//      //  }
//      //  //
//      //  FBitmapConverted.UnlockBits(BDD);
//      //  //
//      //  //
//      //  return true;
//      //}
//      //catch (Exception)
//      //{
//      //  return false;
//      //}
//      return false;
//    }

//    unsafe public Boolean Convert4096x256(Boolean grayscale)
//    {
//      //try
//      //{
//      //  Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//      //  BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBS = BDS.Scan0;
//      //  //
//      //  FBitmapConverted = new Bitmap(16 * BITMAP_WIDTH, 1 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//      //  Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//      //  BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBD = BDD.Scan0;
//      //  //
//      //  int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//      //  byte[] BValues = new byte[ByteCountSource];
//      //  Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//      //  //
//      //  FBitmap256x256.UnlockBits(BDS);
//      //  //
//      //  Int32 SCD = 3 * 4096;
//      //  Int32 BID0 = (Int32)PBD;
//      //  Int32 CID = 0;
//      //  Int32 RID = 0;
//      //  for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//      //  {
//      //    byte B0 = BValues[BIS + 0];
//      //    byte B1 = BValues[BIS + 1];
//      //    byte B2 = BValues[BIS + 2];
//      //    if (grayscale)
//      //    {
//      //      byte GS = (byte)((B0 + B1 + B2) / 3);
//      //      B0 = GS;
//      //      B1 = GS;
//      //      B2 = GS;
//      //    }
//      //    //-------------------------
//      //    *((byte*)(BID0 + 0)) = B0;
//      //    *((byte*)(BID0 + 1)) = B1;
//      //    *((byte*)(BID0 + 2)) = B2;
//      //    *((byte*)(BID0 + 3)) = B0;
//      //    *((byte*)(BID0 + 4)) = B1;
//      //    *((byte*)(BID0 + 5)) = B2;
//      //    *((byte*)(BID0 + 6)) = B0;
//      //    *((byte*)(BID0 + 7)) = B1;
//      //    *((byte*)(BID0 + 8)) = B2;
//      //    *((byte*)(BID0 + 9)) = B0;
//      //    *((byte*)(BID0 + 10)) = B1;
//      //    *((byte*)(BID0 + 11)) = B2;
//      //    *((byte*)(BID0 + 12)) = B0;
//      //    *((byte*)(BID0 + 13)) = B1;
//      //    *((byte*)(BID0 + 14)) = B2;
//      //    *((byte*)(BID0 + 15)) = B0;
//      //    *((byte*)(BID0 + 16)) = B1;
//      //    *((byte*)(BID0 + 17)) = B2;
//      //    *((byte*)(BID0 + 18)) = B0;
//      //    *((byte*)(BID0 + 19)) = B1;
//      //    *((byte*)(BID0 + 20)) = B2;
//      //    *((byte*)(BID0 + 21)) = B0;
//      //    *((byte*)(BID0 + 22)) = B1;
//      //    *((byte*)(BID0 + 23)) = B2;
//      //    *((byte*)(BID0 + 24)) = B0;
//      //    *((byte*)(BID0 + 25)) = B1;
//      //    *((byte*)(BID0 + 26)) = B2;
//      //    *((byte*)(BID0 + 27)) = B0;
//      //    *((byte*)(BID0 + 28)) = B1;
//      //    *((byte*)(BID0 + 29)) = B2;
//      //    *((byte*)(BID0 + 30)) = B0;
//      //    *((byte*)(BID0 + 31)) = B1;
//      //    *((byte*)(BID0 + 32)) = B2;
//      //    *((byte*)(BID0 + 33)) = B0;
//      //    *((byte*)(BID0 + 34)) = B1;
//      //    *((byte*)(BID0 + 35)) = B2;
//      //    *((byte*)(BID0 + 36)) = B0;
//      //    *((byte*)(BID0 + 37)) = B1;
//      //    *((byte*)(BID0 + 38)) = B2;
//      //    *((byte*)(BID0 + 39)) = B0;
//      //    *((byte*)(BID0 + 40)) = B1;
//      //    *((byte*)(BID0 + 41)) = B2;
//      //    *((byte*)(BID0 + 42)) = B0;
//      //    *((byte*)(BID0 + 43)) = B1;
//      //    *((byte*)(BID0 + 44)) = B2;
//      //    *((byte*)(BID0 + 45)) = B0;
//      //    *((byte*)(BID0 + 46)) = B1;
//      //    *((byte*)(BID0 + 47)) = B2;
//      //    //--------------------------
//      //    CID += 48;
//      //    if (SCD <= CID)
//      //    {
//      //      CID = 0;
//      //      RID++;
//      //      BID0 = (Int32)PBD + 1 * RID * SCD;
//      //    }
//      //    else
//      //    {
//      //      BID0 += 48;
//      //    }
//      //  }
//      //  //
//      //  FBitmapConverted.UnlockBits(BDD);
//      //  //
//      //  return true;
//      //}
//      //catch (Exception)
//      //{
//      //  return false;
//      //}
//      return false;
//    }
//    //
//    //----------------------------------------------------------
//    //  512 x ...
//    //----------------------------------------------------------
//    unsafe public Boolean Convert256x512(Boolean grayscale)
//    {
//      //try
//      //{
//      //  Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//      //  BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBS = BDS.Scan0;
//      //  //
//      //  FBitmapConverted = new Bitmap(1 * BITMAP_WIDTH, 2 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//      //  Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//      //  BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBD = BDD.Scan0;
//      //  //
//      //  int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//      //  byte[] BValues = new byte[ByteCountSource];
//      //  Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//      //  //
//      //  FBitmap256x256.UnlockBits(BDS);
//      //  //
//      //  Int32 SCD = 3 * 256;
//      //  Int32 BID0 = (Int32)PBD;
//      //  Int32 BID1 = (Int32)PBD + SCD;
//      //  Int32 CID = 0;
//      //  Int32 RID = 0;
//      //  for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//      //  {
//      //    byte B0 = BValues[BIS + 0];
//      //    byte B1 = BValues[BIS + 1];
//      //    byte B2 = BValues[BIS + 2];
//      //    if (grayscale)
//      //    {
//      //      byte GS = (byte)((B0 + B1 + B2) / 3);
//      //      B0 = GS;
//      //      B1 = GS;
//      //      B2 = GS;
//      //    }
//      //    *((byte*)(BID0 + 0)) = B0;
//      //    *((byte*)(BID0 + 1)) = B1;
//      //    *((byte*)(BID0 + 2)) = B2;
//      //    *((byte*)(BID1 + 0)) = B0;
//      //    *((byte*)(BID1 + 1)) = B1;
//      //    *((byte*)(BID1 + 2)) = B2;
//      //    CID += 3;
//      //    if (SCD <= CID)
//      //    {
//      //      CID = 0;
//      //      RID++;
//      //      BID0 = (Int32)PBD + 2 * RID * SCD;
//      //      BID1 = BID0 + SCD;
//      //    }
//      //    else
//      //    {
//      //      BID0 += 3;
//      //      BID1 += 3;
//      //    }
//      //  }
//      //  //
//      //  FBitmapConverted.UnlockBits(BDD);
//      //  //
//      //  return true;
//      //}
//      //catch (Exception)
//      //{
//      //  return false;
//      //}
//      return false;
//    }

//    unsafe public Boolean Convert512x512(Boolean grayscale)
//    {
//      //try
//      //{
//      //  Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//      //  BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBS = BDS.Scan0;
//      //  //
//      //  FBitmapConverted = new Bitmap(2 * BITMAP_WIDTH, 2 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//      //  Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//      //  BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBD = BDD.Scan0;
//      //  //
//      //  int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//      //  byte[] BValues = new byte[ByteCountSource];
//      //  Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//      //  //
//      //  FBitmap256x256.UnlockBits(BDS);
//      //  //
//      //  Int32 SCD = 3 * 512;// 2 * Math.Abs(BDS.Stride); 
//      //  Int32 BID0 = (Int32)PBD;
//      //  Int32 BID1 = (Int32)PBD + SCD;
//      //  Int32 CID = 0;
//      //  Int32 RID = 0;
//      //  for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//      //  {
//      //    byte B0 = BValues[BIS + 0];
//      //    byte B1 = BValues[BIS + 1];
//      //    byte B2 = BValues[BIS + 2];
//      //    if (grayscale)
//      //    {
//      //      byte GS = (byte)((B0 + B1 + B2) / 3);
//      //      B0 = GS;
//      //      B1 = GS;
//      //      B2 = GS;
//      //    }
//      //    *((byte*)(BID0 + 0)) = B0;
//      //    *((byte*)(BID0 + 1)) = B1;
//      //    *((byte*)(BID0 + 2)) = B2;
//      //    *((byte*)(BID0 + 3)) = B0;
//      //    *((byte*)(BID0 + 4)) = B1;
//      //    *((byte*)(BID0 + 5)) = B2;
//      //    *((byte*)(BID1 + 0)) = B0;
//      //    *((byte*)(BID1 + 1)) = B1;
//      //    *((byte*)(BID1 + 2)) = B2;
//      //    *((byte*)(BID1 + 3)) = B0;
//      //    *((byte*)(BID1 + 4)) = B1;
//      //    *((byte*)(BID1 + 5)) = B2;
//      //    CID += 6;
//      //    if (SCD <= CID)
//      //    {
//      //      CID = 0;
//      //      RID++;
//      //      BID0 = (Int32)PBD + 2 * RID * SCD;
//      //      BID1 = BID0 + SCD;
//      //    }
//      //    else
//      //    {
//      //      BID0 += 6;
//      //      BID1 += 6;
//      //    }
//      //  }
//      //  //
//      //  FBitmapConverted.UnlockBits(BDD);
//      //  //
//      //  return true;
//      //}
//      //catch (Exception)
//      //{
//      //  return false;
//      //}
//      return false;
//    }

//    unsafe public Boolean Convert4096x512(Boolean grayscale)
//    {
//      //try
//      //{
//      //  Rectangle RS = new Rectangle(0, 0, FBitmap256x256.Width, FBitmap256x256.Height);
//      //  BitmapData BDS = FBitmap256x256.LockBits(RS, ImageLockMode.ReadOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBS = BDS.Scan0;
//      //  //
//      //  FBitmapConverted = new Bitmap(16 * BITMAP_WIDTH, 2 * BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//      //  Rectangle RD = new Rectangle(0, 0, FBitmapConverted.Width, FBitmapConverted.Height);
//      //  BitmapData BDD = FBitmapConverted.LockBits(RD, ImageLockMode.WriteOnly, PixelFormat.Format24bppRgb);
//      //  IntPtr PBD = BDD.Scan0;
//      //  //
//      //  int ByteCountSource = Math.Abs(BDS.Stride) * FBitmap256x256.Height;
//      //  byte[] BValues = new byte[ByteCountSource];
//      //  Marshal.Copy(PBS, BValues, 0, ByteCountSource);
//      //  //
//      //  FBitmap256x256.UnlockBits(BDS);
//      //  //
//      //  Int32 SCD = 3 * 4096;// 
//      //  Int32 BID0 = (Int32)PBD;
//      //  Int32 BID1 = (Int32)PBD + SCD;
//      //  Int32 CID = 0;
//      //  Int32 RID = 0;
//      //  for (int BIS = 0; BIS < ByteCountSource; BIS += 3)
//      //  {
//      //    byte B0 = BValues[BIS + 0];
//      //    byte B1 = BValues[BIS + 1];
//      //    byte B2 = BValues[BIS + 2];
//      //    if (grayscale)
//      //    {
//      //      byte GS = (byte)((B0 + B1 + B2) / 3);
//      //      B0 = GS;
//      //      B1 = GS;
//      //      B2 = GS;
//      //    }
//      //    //-------------------------
//      //    *((byte*)(BID0 + 0)) = B0;
//      //    *((byte*)(BID0 + 1)) = B1;
//      //    *((byte*)(BID0 + 2)) = B2;
//      //    *((byte*)(BID0 + 3)) = B0;
//      //    *((byte*)(BID0 + 4)) = B1;
//      //    *((byte*)(BID0 + 5)) = B2;
//      //    *((byte*)(BID0 + 6)) = B0;
//      //    *((byte*)(BID0 + 7)) = B1;
//      //    *((byte*)(BID0 + 8)) = B2;
//      //    *((byte*)(BID0 + 9)) = B0;
//      //    *((byte*)(BID0 + 10)) = B1;
//      //    *((byte*)(BID0 + 11)) = B2;
//      //    *((byte*)(BID0 + 12)) = B0;
//      //    *((byte*)(BID0 + 13)) = B1;
//      //    *((byte*)(BID0 + 14)) = B2;
//      //    *((byte*)(BID0 + 15)) = B0;
//      //    *((byte*)(BID0 + 16)) = B1;
//      //    *((byte*)(BID0 + 17)) = B2;
//      //    *((byte*)(BID0 + 18)) = B0;
//      //    *((byte*)(BID0 + 19)) = B1;
//      //    *((byte*)(BID0 + 20)) = B2;
//      //    *((byte*)(BID0 + 21)) = B0;
//      //    *((byte*)(BID0 + 22)) = B1;
//      //    *((byte*)(BID0 + 23)) = B2;
//      //    *((byte*)(BID0 + 24)) = B0;
//      //    *((byte*)(BID0 + 25)) = B1;
//      //    *((byte*)(BID0 + 26)) = B2;
//      //    *((byte*)(BID0 + 27)) = B0;
//      //    *((byte*)(BID0 + 28)) = B1;
//      //    *((byte*)(BID0 + 29)) = B2;
//      //    *((byte*)(BID0 + 30)) = B0;
//      //    *((byte*)(BID0 + 31)) = B1;
//      //    *((byte*)(BID0 + 32)) = B2;
//      //    *((byte*)(BID0 + 33)) = B0;
//      //    *((byte*)(BID0 + 34)) = B1;
//      //    *((byte*)(BID0 + 35)) = B2;
//      //    *((byte*)(BID0 + 36)) = B0;
//      //    *((byte*)(BID0 + 37)) = B1;
//      //    *((byte*)(BID0 + 38)) = B2;
//      //    *((byte*)(BID0 + 39)) = B0;
//      //    *((byte*)(BID0 + 40)) = B1;
//      //    *((byte*)(BID0 + 41)) = B2;
//      //    *((byte*)(BID0 + 42)) = B0;
//      //    *((byte*)(BID0 + 43)) = B1;
//      //    *((byte*)(BID0 + 44)) = B2;
//      //    *((byte*)(BID0 + 45)) = B0;
//      //    *((byte*)(BID0 + 46)) = B1;
//      //    *((byte*)(BID0 + 47)) = B2;
//      //    //-------------------------
//      //    *((byte*)(BID1 + 0)) = B0;
//      //    *((byte*)(BID1 + 1)) = B1;
//      //    *((byte*)(BID1 + 2)) = B2;
//      //    *((byte*)(BID1 + 3)) = B0;
//      //    *((byte*)(BID1 + 4)) = B1;
//      //    *((byte*)(BID1 + 5)) = B2;
//      //    *((byte*)(BID1 + 6)) = B0;
//      //    *((byte*)(BID1 + 7)) = B1;
//      //    *((byte*)(BID1 + 8)) = B2;
//      //    *((byte*)(BID1 + 9)) = B0;
//      //    *((byte*)(BID1 + 10)) = B1;
//      //    *((byte*)(BID1 + 11)) = B2;
//      //    *((byte*)(BID1 + 12)) = B0;
//      //    *((byte*)(BID1 + 13)) = B1;
//      //    *((byte*)(BID1 + 14)) = B2;
//      //    *((byte*)(BID1 + 15)) = B0;
//      //    *((byte*)(BID1 + 16)) = B1;
//      //    *((byte*)(BID1 + 17)) = B2;
//      //    *((byte*)(BID1 + 18)) = B0;
//      //    *((byte*)(BID1 + 19)) = B1;
//      //    *((byte*)(BID1 + 20)) = B2;
//      //    *((byte*)(BID1 + 21)) = B0;
//      //    *((byte*)(BID1 + 22)) = B1;
//      //    *((byte*)(BID1 + 23)) = B2;
//      //    *((byte*)(BID1 + 24)) = B0;
//      //    *((byte*)(BID1 + 25)) = B1;
//      //    *((byte*)(BID1 + 26)) = B2;
//      //    *((byte*)(BID1 + 27)) = B0;
//      //    *((byte*)(BID1 + 28)) = B1;
//      //    *((byte*)(BID1 + 29)) = B2;
//      //    *((byte*)(BID1 + 30)) = B0;
//      //    *((byte*)(BID1 + 31)) = B1;
//      //    *((byte*)(BID1 + 32)) = B2;
//      //    *((byte*)(BID1 + 33)) = B0;
//      //    *((byte*)(BID1 + 34)) = B1;
//      //    *((byte*)(BID1 + 35)) = B2;
//      //    *((byte*)(BID1 + 36)) = B0;
//      //    *((byte*)(BID1 + 37)) = B1;
//      //    *((byte*)(BID1 + 38)) = B2;
//      //    *((byte*)(BID1 + 39)) = B0;
//      //    *((byte*)(BID1 + 40)) = B1;
//      //    *((byte*)(BID1 + 41)) = B2;
//      //    *((byte*)(BID1 + 42)) = B0;
//      //    *((byte*)(BID1 + 43)) = B1;
//      //    *((byte*)(BID1 + 44)) = B2;
//      //    *((byte*)(BID1 + 45)) = B0;
//      //    *((byte*)(BID1 + 46)) = B1;
//      //    *((byte*)(BID1 + 47)) = B2;
//      //    //--------------------------
//      //    CID += 48;
//      //    if (SCD <= CID)
//      //    {
//      //      CID = 0;
//      //      RID++;
//      //      BID0 = (Int32)PBD + 2 * RID * SCD;
//      //      BID1 = BID0 + SCD;
//      //    }
//      //    else
//      //    {
//      //      BID0 += 48;
//      //      BID1 += 48;
//      //    }
//      //  }
//      //  //
//      //  FBitmapConverted.UnlockBits(BDD);
//      //  //
//      //  return true;
//      //}
//      //catch (Exception)
//      //{
//      //  return false;
//      //}
//      return false;
//    }

//public Boolean LoadFromFile(String filename)
//{
//  try
//  {
//    Bitmap BF = new Bitmap(filename);
//    int BFW = BF.Width;
//    int BFH = BF.Height;
//    //
//    // NC FBitmap = new Bitmap(BITMAP_WIDTH, BITMAP_HEIGHT, PixelFormat.Format24bppRgb);
//    Graphics G = Graphics.FromImage(FBitmap256x256);
//    //
//    G.InterpolationMode = InterpolationMode.High;
//    G.CompositingQuality = CompositingQuality.HighQuality;
//    G.SmoothingMode = SmoothingMode.AntiAlias;
//    G.DrawImage(BF, 0, 0, BITMAP_WIDTH, BITMAP_HEIGHT);
//    //
//    G.Dispose();
//    BF.Dispose();
//    //
//    Rectangle RS = new Rectangle(0, 0, BITMAP_WIDTH, BITMAP_HEIGHT);
//    FBitmapConverted = FBitmap256x256.Clone(RS, PixelFormat.Format24bppRgb);
//    //
//    Rectangle RP = new Rectangle(0, 0, PICTURE_WIDTH, PICTURE_HEIGHT);
//    FBitmapPicture = new Bitmap(RP.Width, RP.Height, PixelFormat.Format24bppRgb);
//    return true;
//  }
//  catch (Exception)
//  {
//    return false;
//  }
//}

//unsafe public Boolean CopySourceToPicture()
//{
//  try
//  {
//    Graphics G = Graphics.FromImage(FBitmapPicture);
//    int WS = GetWidthSource();
//    int HS = GetHeightSource();
//    //
//    G.InterpolationMode = InterpolationMode.NearestNeighbor;
//    G.CompositingQuality = CompositingQuality.AssumeLinear;
//    G.SmoothingMode = SmoothingMode.None;
//    //
//    G.DrawImage(FBitmapSource, 0, 0, WS, HS);
//    G.Dispose();
//    return true;
//  }
//  catch (Exception)
//  {
//    return false;
//  }
//}







