﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UCNotifier
{ //
  //--------------------------------------------------------
  //  Segment - Type
  //--------------------------------------------------------
  //
  public struct RTransformData
  {
    public String Name;
    public String Version;
    public String Date;
    public Int32 ProductNumber;
    public String ProductKey;
    public Boolean DiscountUser;
    public String User;
    public RTransformData(Int32 index)
    {
      Name = "";
      Version = "";
      Date = "";
      ProductNumber = 0;
      ProductKey = "";
      DiscountUser = false;
      User = "";
    }
  };
  //
  //#########################################################
  //  Segment - CTransformBase
  //#########################################################
  //
  public class CTransformBase
  {
    //
    //--------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------
    //
    private RTransformData FData;
    //
    //--------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------
    //

    //
    //--------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------
    //
    public String Name
    {
      get { return FData.Name; }
    }
    public String Version
    {
      get { return FData.Version; }
    }
    public String Date
    {
      get { return FData.Date; }
    }
    public Boolean DiscountUser
    {
      get { return FData.DiscountUser; }
    }
    public String User
    {
      get { return FData.User; }
    }
    public Int32 ProductNumber
    {
      get { return FData.ProductNumber; }
    }
    public String ProductKey
    {
      get { return FData.ProductKey; }
    }
    //
    //--------------------------------------------------------
    //  Segment - Get/SetData
    //--------------------------------------------------------
    //
    public CTransformBase(String name,
                          String version,
                          String date,
                          Boolean discountuser,
                          String user,
                          Int32 productnumber,
                          String productkey)
    {
      SetData(name, version, date, discountuser, user, productnumber, productkey);
    }

    public Boolean SetData(String name,
                           String version,
                           String date,
                           Boolean discountuser,
                           String user,
                           Int32 productnumber,
                           String productkey)
    {
      FData.Name = name;
      FData.Version = version;
      FData.Date = date;
      FData.DiscountUser = discountuser;
      FData.User = user;
      FData.ProductNumber = productnumber;
      FData.ProductKey = productkey;
      return true;
    }
    public Boolean SetData(RTransformData data)
    {
      FData.Name = data.Name;
      FData.Version = data.Version;
      FData.Date = data.Date;
      FData.DiscountUser = data.DiscountUser;
      FData.User = data.User;
      FData.ProductNumber = data.ProductNumber;
      FData.ProductKey = data.ProductKey;
      return true;
    }
    public Boolean GetData(out RTransformData data)
    {
      data = FData;
      return true;
    }

  }
}
