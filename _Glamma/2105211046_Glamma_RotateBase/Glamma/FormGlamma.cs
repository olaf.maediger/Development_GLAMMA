﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
using System.Threading;
//
using Initdata;
using UCNotifier;
using Task;
using TextFile;
using Image24b;
//using UCUartTerminal;
//using CommandProtocol;
//using LaserScanner;
//using CPLaserAreaScanner;
//using UCLaserAreaScanner;
//
namespace Glamma
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    //
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    private const String NAME_DEBUG = "Debug";
    private const Boolean INIT_DEBUG = true;
    private const String NAME_SHOWPAGE = "ShowPage";
    private const Int32 INIT_SHOWPAGE = 0;
    //
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    private CImage24b FImage24b;
    private CImage24b FImageSource;

    private int FScalePreset;
    private int FRotatePreset;
    // 
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //####################################################################################
      // Init - Instance - Client
      //####################################################################################
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      //!!!!!!!!!!!!!!!!!!!!!!tbcGlamma.SelectedIndex = 0;
      //
      FImage24b = new CImage24b();
      FImageSource = new CImage24b();
    }

    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    //
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      Int32 IValue = INIT_AUTOINTERVAL;
      Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      //nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      //BValue = INIT_DEBUG;
      //Result &= initdata.ReadBoolean(NAME_DEBUG, out BValue, BValue);
      //cbxDebug.Checked = BValue;
      //
      IValue = INIT_SHOWPAGE;
      Result &= initdata.ReadInt32(NAME_SHOWPAGE, out IValue, IValue);
      //!!!!!!!!!!!!!!!!!!!!!tbcGlamma.SelectedIndex = IValue;
      //
      return Result;
    }

    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      //Result &= initdata.WriteBoolean(NAME_DEBUG, cbxDebug.Checked);
      //!!!!!!!!!!!!!!Result &= initdata.WriteInt32(NAME_SHOWPAGE, tbcGlamma.SelectedIndex);
      //Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------------------------
    //

    //
    //
    //#########################################################################
    //  Segment - Callback - CommandList
    //#########################################################################
    //
    private delegate void CBCommandListOnExecutionStart(RTaskData data);
    private void CommandListOnExecutionStart(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionStart CB = new CBCommandListOnExecutionStart(CommandListOnExecutionStart);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionStart", data.Name));
        }
        //if (0 < FCommandList.Count)
        //{ 
        //  CCommand Command = FCommandList.Peek();
        //  if (Command is CCommand)
        //  {
        //    String CommandText = Command.GetCommandText();
        //    FUart.WriteLine(CommandText);
        //  }
        //}
      }
    }
    private delegate Boolean CBCommandListOnExecutionBusy(RTaskData data);
    private Boolean CommandListOnExecutionBusy(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionBusy CB = new CBCommandListOnExecutionBusy(CommandListOnExecutionBusy);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionBusy", data.Name));
        }
      }
      return false;
    }
    private delegate void CBCommandListOnExecutionEnd(RTaskData data);
    private void CommandListOnExecutionEnd(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionEnd CB = new CBCommandListOnExecutionEnd(CommandListOnExecutionEnd);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionEnd", data.Name));
        }
      }
    }
    private delegate void CBCommandListOnExecutionAbort(RTaskData data);
    private void CommandListOnExecutionAbort(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionAbort CB = new CBCommandListOnExecutionAbort(CommandListOnExecutionAbort);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          // FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionAbort", data.Name));
        }
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          tbcGlamma.SelectedIndex = 0;
          //
          FImageSource.LoadFromFile("B256x256_24b.bmp");
          // debug pbxTest.Image = ImageSource.GetBitmapSource();
          //FImageSource.CopySourceToPicture();
          //pbxTest.Image = FImageSource.GetBitmapPicture();
          FImageSource.RescaleSourceToPicture(0, 0, 1.0, 0.0); // Center!
          pbxTest.Image = FImageSource.GetBitmapPicture();
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          //FUdpTextDeviceClient.Preset("GSV");
          return true;
        case 7:
          return true;
        case 8:
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! First (Client -> Server)");
          return true;
        case 9:
          //FUdpTextDeviceClient.Preset("GHV");
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! Second (Client -> Server)");
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }

    private void btnLoadImage_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {        
        FImage24b.LoadFromFile(DialogLoadImage.FileName);
        pbxImage.Image = FImage24b.GetBitmapConverted();
      }
    }
    //
    //----------------------------------------------------------------
    //  x 256
    //----------------------------------------------------------------
    private void btnConvert256x256_Click(object sender, EventArgs e)
    {
      FImage24b.Convert256x256(cbxGrayScale.Checked);
      pbxImage.Image = FImage24b.GetBitmapConverted();
      pbxImage.Refresh();
    }

    private void btnConvert512x256_Click(object sender, EventArgs e)
    {
      FImage24b.Convert512x256(cbxGrayScale.Checked);
      pbxImage.Image = FImage24b.GetBitmapConverted();
      pbxImage.Refresh();
    }

    private void btnConvert4096x256_Click(object sender, EventArgs e)
    {
      FImage24b.Convert4096x256(cbxGrayScale.Checked);
      pbxImage.Image = FImage24b.GetBitmapConverted();
      pbxImage.Refresh();
    }
    //
    //----------------------------------------------------------------
    //  x 512
    //----------------------------------------------------------------
    private void btnConvert256x512_Click(object sender, EventArgs e)
    {
      FImage24b.Convert256x512(cbxGrayScale.Checked);
      pbxImage.Image = FImage24b.GetBitmapConverted();
      pbxImage.Refresh();
    }

    private void btnConvert512x512_Click(object sender, EventArgs e)
    {
      FImage24b.Convert512x512(cbxGrayScale.Checked);
      pbxImage.Image = FImage24b.GetBitmapConverted();
      pbxImage.Refresh();
    }

    private void btnConvert4096x512_Click(object sender, EventArgs e)
    {
      FImage24b.Convert4096x512(cbxGrayScale.Checked);
      pbxImage.Image = FImage24b.GetBitmapConverted();
      pbxImage.Refresh();
    }

    private void btnColumn2048x256_Click(object sender, EventArgs e)
    {
      FImage24b.Column2048x256(cbxGrayScale.Checked);
      pbxImage.Image = FImage24b.GetBitmapConverted();
      pbxImage.Refresh();
    }

    private void btnColumn2048x512_Click(object sender, EventArgs e)
    {
      FImage24b.Column2048x512(cbxGrayScale.Checked);
      pbxImage.Image = FImage24b.GetBitmapConverted();
      pbxImage.Refresh();
    }

    private void SetColumn2048x256(int value)
    {
      if (value != scbColumn2048x256.Value)
      {
        scbColumn2048x256.Value = value;
      }
      if (value != (int)nudColumn2048x256.Value)
      {
        nudColumn2048x256.Value = value;
      }
    }

    private void SetColumn2048x512(int value)
    {
      if (value != scbColumn2048x512.Value)
      {
        scbColumn2048x512.Value = value;
      }
      if (value != (int)nudColumn2048x512.Value)
      {
        nudColumn2048x512.Value = value;
      }
    }

    private void scbColumn2048x256_Scroll(object sender, ScrollEventArgs e)
    {
      SetColumn2048x256((int)scbColumn2048x256.Value);
    }

    private void nudColumn2048x256_ValueChanged(object sender, EventArgs e)
    {
      SetColumn2048x256((int)nudColumn2048x256.Value);
    }

    private void scbColumn2048x512_ValueChanged(object sender, EventArgs e)
    {
      SetColumn2048x512((int)scbColumn2048x512.Value);
    }

    private void nudColumn2048x512_ValueChanged(object sender, EventArgs e)
    {
      SetColumn2048x512((int)nudColumn2048x512.Value);
    }


    private void SetMoveH(int value)
    {
      if (value != scbMoveH.Value)
      {
        scbMoveH.Value = value;
      }
      if (value != (int)nudMoveH.Value)
      {
        nudMoveH.Value = value;
      }
      btnRescale_Click(this, null);
    }
    private void scbMoveH_Scroll(object sender, ScrollEventArgs e)
    {
      SetMoveH(scbMoveH.Value);
    }
    private void nudMoveH_ValueChanged(object sender, EventArgs e)
    {
      SetMoveH((int)nudMoveH.Value);
    }

    private void SetMoveV(int value)
    {
      if (-value != scbMoveV.Value)
      {
        scbMoveV.Value = -value;
      }
      if (value != (int)nudMoveV.Value)
      {
        nudMoveV.Value = value;
      }
      btnRescale_Click(this, null);
    }
    private void scbMoveV_Scroll(object sender, ScrollEventArgs e)
    {
      SetMoveV(-scbMoveV.Value);
    }
    private void nudMoveV_ValueChanged(object sender, EventArgs e)
    {
      SetMoveV((int)nudMoveV.Value);
    }

    private void SetScale(int value)
    {
      if (value != FScalePreset)
      {
        FScalePreset = value;
        if (value != scbScale.Value)
        {
          scbScale.Value = value;
        }
        if (value != (int)nudScale.Value)
        {
          nudScale.Value = value;
        }
        btnRescale_Click(this, null);
        // Console.WriteLine("...rescale...");
      }
    }
    private void scbScale_Scroll(object sender, ScrollEventArgs e)
    {
      SetScale(scbScale.Value);
    }
    private void nudScale_ValueChanged(object sender, EventArgs e)
    {
      SetScale((int)nudScale.Value);
    }

    private void SetRotate(int value)
    {
      if (value != FRotatePreset)
      {
        FRotatePreset = value;
        if (value != scbRotate.Value)
        {
          scbRotate.Value = value;
        }
        if (value != (int)nudRotate.Value)
        {
          nudRotate.Value = value;
        }
        btnRescale_Click(this, null);
      }
    }
    private void nudRotate_ValueChanged(object sender, EventArgs e)
    {
      SetRotate((int)nudRotate.Value);
    }
    private void scbRotate_ValueChanged(object sender, EventArgs e)
    {
      SetRotate(scbRotate.Value);
    }



    private void btnRescale_Click(object sender, EventArgs e)
    { // -1.0 .. 0.0 .. +1.0  ^= -W .. +W
      double FOX = (double)(nudMoveH.Value) / 100.0;
      // -1.0 .. 0.0 .. +1.0  ^= -H .. +H
      double FOY = (double)(-nudMoveV.Value) / 100.0;
      //  0.1 .. 1.0 .. +10.0 ^= 1/10 .. 10 ^= 0 .. 50 .. 100 // -100 .. 0 .. +100
      double FS = 1.0;
      if (0 < scbScale.Value)
      {
        FS = 1.0 + scbScale.Value / 20.0;
      }
      else
      if (scbScale.Value < 0)
      {
        FS = 1.0 + scbScale.Value / 20.0;
      }
      //  0.0 .. 1.0 ^= 0deg .. 360deg
      double FR = 1.80 * (double)nudRotate.Value;
      FImageSource.RescaleSourceToPicture(FOX, FOY, FS, FR);
      pbxTest.Image = FImageSource.GetBitmapPicture();
    }

    private void btnLoadFromFile_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        FImageSource.LoadFromFile(DialogLoadImage.FileName);
        // debug pbxTest.Image = ImageSource.GetBitmapSource();
        //FImageSource.CopySourceToPicture();
        //pbxTest.Image = FImageSource.GetBitmapPicture();
        FImageSource.RescaleSourceToPicture(0, 0, 1.0, 0.0); // Center!
        pbxTest.Image = FImageSource.GetBitmapPicture();
      }
    }


  }
}

