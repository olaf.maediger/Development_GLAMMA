﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using Uart;
//
namespace UCUartOpenClose
{
  public partial class CUCPortsInstalled : UserControl
  {
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;

    public CUCPortsInstalled()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void RefreshPortsInstalled()
    {
      cbxPortsInstalled.Items.Clear();
      cbxPortsInstalled.Items.AddRange(CUart.GetInstalledPorts());
      int SI = cbxPortsInstalled.Items.Count - 1;
      cbxPortsInstalled.SelectedIndex = SI;
    }

  }
}
