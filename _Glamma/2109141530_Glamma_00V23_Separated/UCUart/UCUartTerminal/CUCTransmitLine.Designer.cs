﻿namespace UCUartTerminal
{
  partial class CUCTransmitLine
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.pnlInput = new System.Windows.Forms.Panel();
      this.cbxShowOpenClose = new System.Windows.Forms.CheckBox();
      this.nudDelayLF = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.nudDelayCharacter = new System.Windows.Forms.NumericUpDown();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.nudDelayCR = new System.Windows.Forms.NumericUpDown();
      this.label3 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.cbxLF = new System.Windows.Forms.CheckBox();
      this.cbxCR = new System.Windows.Forms.CheckBox();
      this.cbxTransmitLine = new System.Windows.Forms.ComboBox();
      this.cmsTransmitLine = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitSaveTransmitLines = new System.Windows.Forms.ToolStripMenuItem();
      this.mitLoadTransmitLines = new System.Windows.Forms.ToolStripMenuItem();
      this.btnTransmitLine = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.DialogSaveTransmitLines = new System.Windows.Forms.SaveFileDialog();
      this.DialogLoadTransmitLines = new System.Windows.Forms.OpenFileDialog();
      this.pnlInput.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayLF)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayCharacter)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayCR)).BeginInit();
      this.cmsTransmitLine.SuspendLayout();
      this.SuspendLayout();
      // 
      // pnlInput
      // 
      this.pnlInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pnlInput.Controls.Add(this.cbxShowOpenClose);
      this.pnlInput.Controls.Add(this.nudDelayLF);
      this.pnlInput.Controls.Add(this.label6);
      this.pnlInput.Controls.Add(this.nudDelayCharacter);
      this.pnlInput.Controls.Add(this.label5);
      this.pnlInput.Controls.Add(this.label4);
      this.pnlInput.Controls.Add(this.nudDelayCR);
      this.pnlInput.Controls.Add(this.label3);
      this.pnlInput.Controls.Add(this.label2);
      this.pnlInput.Controls.Add(this.cbxLF);
      this.pnlInput.Controls.Add(this.cbxCR);
      this.pnlInput.Controls.Add(this.cbxTransmitLine);
      this.pnlInput.Controls.Add(this.btnTransmitLine);
      this.pnlInput.Controls.Add(this.label1);
      this.pnlInput.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlInput.Location = new System.Drawing.Point(0, 0);
      this.pnlInput.Name = "pnlInput";
      this.pnlInput.Size = new System.Drawing.Size(721, 47);
      this.pnlInput.TabIndex = 8;
      // 
      // cbxShowOpenClose
      // 
      this.cbxShowOpenClose.AutoSize = true;
      this.cbxShowOpenClose.Location = new System.Drawing.Point(4, 26);
      this.cbxShowOpenClose.Name = "cbxShowOpenClose";
      this.cbxShowOpenClose.Size = new System.Drawing.Size(113, 17);
      this.cbxShowOpenClose.TabIndex = 17;
      this.cbxShowOpenClose.Text = "Show Open/Close";
      this.cbxShowOpenClose.UseVisualStyleBackColor = true;
      this.cbxShowOpenClose.CheckedChanged += new System.EventHandler(this.cbxShowDialogOpenClose_CheckedChanged);
      // 
      // nudDelayLF
      // 
      this.nudDelayLF.Location = new System.Drawing.Point(298, 24);
      this.nudDelayLF.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudDelayLF.Name = "nudDelayLF";
      this.nudDelayLF.Size = new System.Drawing.Size(44, 20);
      this.nudDelayLF.TabIndex = 16;
      this.nudDelayLF.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(280, 27);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(19, 13);
      this.label6.TabIndex = 15;
      this.label6.Text = "LF";
      // 
      // nudDelayCharacter
      // 
      this.nudDelayCharacter.Location = new System.Drawing.Point(398, 24);
      this.nudDelayCharacter.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudDelayCharacter.Name = "nudDelayCharacter";
      this.nudDelayCharacter.Size = new System.Drawing.Size(44, 20);
      this.nudDelayCharacter.TabIndex = 14;
      this.nudDelayCharacter.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(444, 28);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(20, 13);
      this.label5.TabIndex = 13;
      this.label5.Text = "ms";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(347, 27);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(53, 13);
      this.label4.TabIndex = 12;
      this.label4.Text = "Character";
      // 
      // nudDelayCR
      // 
      this.nudDelayCR.Location = new System.Drawing.Point(232, 24);
      this.nudDelayCR.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudDelayCR.Name = "nudDelayCR";
      this.nudDelayCR.Size = new System.Drawing.Size(44, 20);
      this.nudDelayCR.TabIndex = 11;
      this.nudDelayCR.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(182, 27);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(52, 13);
      this.label3.TabIndex = 10;
      this.label3.Text = "Delay CR";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(14, 4);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(39, 13);
      this.label2.TabIndex = 9;
      this.label2.Text = "TxLine";
      // 
      // cbxLF
      // 
      this.cbxLF.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxLF.Checked = true;
      this.cbxLF.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbxLF.Location = new System.Drawing.Point(148, 23);
      this.cbxLF.Name = "cbxLF";
      this.cbxLF.Size = new System.Drawing.Size(31, 22);
      this.cbxLF.TabIndex = 8;
      this.cbxLF.Text = "LF";
      this.cbxLF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxLF.UseVisualStyleBackColor = true;
      // 
      // cbxCR
      // 
      this.cbxCR.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxCR.Checked = true;
      this.cbxCR.CheckState = System.Windows.Forms.CheckState.Checked;
      this.cbxCR.Location = new System.Drawing.Point(116, 23);
      this.cbxCR.Name = "cbxCR";
      this.cbxCR.Size = new System.Drawing.Size(31, 22);
      this.cbxCR.TabIndex = 7;
      this.cbxCR.Text = "CR";
      this.cbxCR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxCR.UseVisualStyleBackColor = true;
      // 
      // cbxTransmitLine
      // 
      this.cbxTransmitLine.BackColor = System.Drawing.SystemColors.Info;
      this.cbxTransmitLine.ContextMenuStrip = this.cmsTransmitLine;
      this.cbxTransmitLine.Dock = System.Windows.Forms.DockStyle.Fill;
      this.cbxTransmitLine.Font = new System.Drawing.Font("Courier New", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.cbxTransmitLine.ForeColor = System.Drawing.Color.Black;
      this.cbxTransmitLine.FormattingEnabled = true;
      this.cbxTransmitLine.IntegralHeight = false;
      this.cbxTransmitLine.Items.AddRange(new object[] {
            "Hello World!"});
      this.cbxTransmitLine.Location = new System.Drawing.Point(66, 0);
      this.cbxTransmitLine.Name = "cbxTransmitLine";
      this.cbxTransmitLine.Size = new System.Drawing.Size(597, 23);
      this.cbxTransmitLine.TabIndex = 4;
      this.cbxTransmitLine.Text = "BLS 200 5";
      this.cbxTransmitLine.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cbxSendData_KeyDown);
      // 
      // cmsTransmitLine
      // 
      this.cmsTransmitLine.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitSaveTransmitLines,
            this.mitLoadTransmitLines});
      this.cmsTransmitLine.Name = "cmsMultiColorText";
      this.cmsTransmitLine.Size = new System.Drawing.Size(176, 48);
      // 
      // mitSaveTransmitLines
      // 
      this.mitSaveTransmitLines.Name = "mitSaveTransmitLines";
      this.mitSaveTransmitLines.Size = new System.Drawing.Size(175, 22);
      this.mitSaveTransmitLines.Text = "Save TransmitLines";
      // 
      // mitLoadTransmitLines
      // 
      this.mitLoadTransmitLines.Name = "mitLoadTransmitLines";
      this.mitLoadTransmitLines.Size = new System.Drawing.Size(175, 22);
      this.mitLoadTransmitLines.Text = "Load TransmitLines";
      // 
      // btnTransmitLine
      // 
      this.btnTransmitLine.Dock = System.Windows.Forms.DockStyle.Right;
      this.btnTransmitLine.Location = new System.Drawing.Point(663, 0);
      this.btnTransmitLine.Name = "btnTransmitLine";
      this.btnTransmitLine.Size = new System.Drawing.Size(56, 45);
      this.btnTransmitLine.TabIndex = 3;
      this.btnTransmitLine.Text = "Transmit TxD";
      this.btnTransmitLine.UseVisualStyleBackColor = true;
      this.btnTransmitLine.Click += new System.EventHandler(this.btnTransmitLine_Click);
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Left;
      this.label1.Location = new System.Drawing.Point(0, 0);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(66, 45);
      this.label1.TabIndex = 1;
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // DialogSaveTransmitLines
      // 
      this.DialogSaveTransmitLines.DefaultExt = "txt";
      this.DialogSaveTransmitLines.FileName = "TransmitLines";
      this.DialogSaveTransmitLines.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogSaveTransmitLines.Title = "Save Transmit Lines";
      // 
      // DialogLoadTransmitLines
      // 
      this.DialogLoadTransmitLines.DefaultExt = "txt";
      this.DialogLoadTransmitLines.FileName = "TransmitLines";
      this.DialogLoadTransmitLines.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadTransmitLines.Title = "Load Transmit Lines";
      // 
      // CUCTransmitLine
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ContextMenuStrip = this.cmsTransmitLine;
      this.Controls.Add(this.pnlInput);
      this.Name = "CUCTransmitLine";
      this.Size = new System.Drawing.Size(721, 47);
      this.pnlInput.ResumeLayout(false);
      this.pnlInput.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayLF)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayCharacter)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudDelayCR)).EndInit();
      this.cmsTransmitLine.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlInput;
    private System.Windows.Forms.CheckBox cbxShowOpenClose;
    private System.Windows.Forms.NumericUpDown nudDelayLF;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown nudDelayCharacter;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown nudDelayCR;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.CheckBox cbxLF;
    private System.Windows.Forms.CheckBox cbxCR;
    private System.Windows.Forms.ComboBox cbxTransmitLine;
    private System.Windows.Forms.Button btnTransmitLine;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ContextMenuStrip cmsTransmitLine;
    private System.Windows.Forms.ToolStripMenuItem mitSaveTransmitLines;
    private System.Windows.Forms.ToolStripMenuItem mitLoadTransmitLines;
    private System.Windows.Forms.SaveFileDialog DialogSaveTransmitLines;
    private System.Windows.Forms.OpenFileDialog DialogLoadTransmitLines;
  }
}
