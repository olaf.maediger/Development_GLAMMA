﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
//
using Uart;
using UCNotifier;
using TextFile;
using UCUartOpenClose;
//
namespace UCUartTerminal
{ //
  //-------------------------------
  //  Segment - Type
  //-------------------------------
  //
  public delegate void DOnTerminalOpen(String portname, EComPort comport, EBaudrate baudrate, EParity parity,
                                       EDatabits databits, EStopbits stopbits, EHandshake handshake);
  public delegate void DOnTerminalClose(EComPort comport);
  //
  //##################################################
  //  Segment - Main - CUCUartTerminal
  //##################################################
  //
  public partial class CUCUartTerminal : UserControl
  { //
    //-------------------------------
    //  Segment - Constant
    //-------------------------------
    // Color UART - RXD 
    private Byte INIT_COLORRXD_A = 0xFF;
    private Byte INIT_COLORRXD_R = 0xD0;//0xA9;
    private Byte INIT_COLORRXD_G = 0x30;//0x22;
    private Byte INIT_COLORRXD_B = 0x30;//0xAA;
    // Color UART - TXD
    private Byte INIT_COLORTXD_A = 0xFF;
    private Byte INIT_COLORTXD_R = 0x30;//0x00;
    private Byte INIT_COLORTXD_G = 0x80;//0x88;
    private Byte INIT_COLORTXD_B = 0x30;//0xFF;
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    private CNotifier FNotifier;    
    private Color FTxdColor;
    private Color FRxdColor;
    private DOnTerminalOpen FOnTerminalOpen;
    private DOnTerminalClose FOnTerminalClose;
    private DOnTransmitText FOnTransmitText;
    //
    //------------------------------------------------------------------------
    //  Segment - Constructor
    //------------------------------------------------------------------------
    public CUCUartTerminal()
    {
      InitializeComponent();
      //
      FTxdColor = Color.FromArgb(INIT_COLORTXD_A, INIT_COLORTXD_R, INIT_COLORTXD_G, INIT_COLORTXD_B);
      FRxdColor = Color.FromArgb(INIT_COLORRXD_A, INIT_COLORRXD_R, INIT_COLORRXD_G, INIT_COLORRXD_B);
      //
      FUCTransmitLine.SetOnShowDialogOpenClose(UCTransmitLineOnShowDialogOpenClose);
      FUCTransmitLine.SetOnTransmitText(UCTransmitLineOnTransmitText);
      //
      FUCUartOpenClose.RefreshComPortControls();
      FUCUartOpenClose.SetOnUCOpenCloseChanged(UCUartOpenCloseOnUCOpenCloseChanged);
      //
      FUCMultiColorText.Dock = DockStyle.Fill;
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier notifier)
    {
      FNotifier = notifier;
      FUCMultiColorText.SetNotifier(notifier);
      FUCTransmitLine.SetNotifier(notifier);
      FUCUartOpenClose.SetNotifier(notifier);
    }

    public void SetOnTerminalOpen(DOnTerminalOpen value)
    {
      FOnTerminalOpen = value;
    }

    public void SetOnTerminalClose(DOnTerminalClose value)
    {
      FOnTerminalClose = value;
    }

    public void SetOnTransmitText(DOnTransmitText value)
    {
      FOnTransmitText = value;
    }

    public void SetUartOpened(Boolean opened)
    {
      FUCUartOpenClose.SetOpened(opened);
    }

    public EComPort GetComPort()
    {
      return FUCUartOpenClose.GetComPort();
    }
    public void SetComPort(EComPort comport)
    {
      FUCUartOpenClose.SetComPort(comport);
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Helper
    //------------------------------------------------------------------------
    //
    //------------------------------------------------------------------------
    //  Segment - Callback - UCTransmitLine
    //------------------------------------------------------------------------
    private void UCUartOpenCloseOnUCOpenCloseChanged(Boolean opened)
    {
      if (opened)
      {
        String PortName = FUCUartOpenClose.GetName();
        EComPort ComPort = FUCUartOpenClose.GetComPort();
        EBaudrate Baudrate = FUCUartOpenClose.GetBaudrate();
        EParity Parity = FUCUartOpenClose.GetParity();
        EDatabits Databits = FUCUartOpenClose.GetDatabits();
        EStopbits Stopbits = FUCUartOpenClose.GetStopbits();
        EHandshake Handshake = FUCUartOpenClose.GetHandshake();
        if (FOnTerminalOpen is DOnTerminalOpen)
        {
          FOnTerminalOpen(PortName, ComPort, Baudrate, Parity, Databits, Stopbits, Handshake);
          FUCUartOpenClose.SetOpened(true);
        }
      }
      else
      {
        if (FOnTerminalClose is DOnTerminalClose)
        {
          EComPort ComPort = FUCUartOpenClose.GetComPort();
          FOnTerminalClose(ComPort);
          FUCUartOpenClose.SetOpened(false);
        }
      }
    }
    //
    private void UCTransmitLineOnShowDialogOpenClose(Boolean visible)
    {
      FUCUartOpenClose.Visible = visible;
    }
    //
    private void UCTransmitLineOnTransmitText(String text, Int32 delaycharacter, Int32 delaycr, Int32 delaylf)
    {
      SetTxdText(text);
      if (FOnTransmitText is DOnTransmitText)
      { // Output -> Main! (because of Main-defined Colors!
        FOnTransmitText(text, delaycharacter, delaycr, delaylf);
      }
    }
    //
    //------------------------------------------------------------------------
    //  Segment - Public 
    //------------------------------------------------------------------------
    public void OpenAutomatic()
    {
      FUCUartOpenClose.SetOpened(true);
    }
    //
    // Add ColoredText/Line
    public void AddColoredText(Color color, String text)
    {
      FUCMultiColorText.Add(color, text);
    }
    public void AddColoredLine(Color color, String line)
    {
      FUCMultiColorText.Add(color, line + "\r\n");
    }
    //
    // Add RxdText/Line
    public void SetRxdText(String text)
    {
      FUCMultiColorText.Add(FRxdColor, text);
    }
    public void SetRxdLine(String line)
    {
      FUCMultiColorText.Add(FRxdColor, line + "\r\n");
    }
    //
    // Add TxdText/Line
    public void SetTxdText(String text)
    {
      FUCMultiColorText.Add(FTxdColor, text);
    }
    public void SetTxdLine(String line)
    {
      FUCMultiColorText.Add(FTxdColor, line + "\r\n");
    }

  }
}

