﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
//
namespace Uart
{
  //
  //------------------------------------------------------
  //  Segment - Type
  //------------------------------------------------------
  //
  public enum EErrorCode
  {
    None = 0,
    Child,
    OpenFailed,
    CloseFailed,
    ReadFailed,
    WriteFailed,
    AccessFailed,
    PortFailed,
    Invalid
  };

  public enum EPortSet
  {
    Possible = 0,
    Installed = 1,
    Selectable = 2
  };

  public enum EComPort
  {
    cp1 = 0,    cp2 = 1,      cp3 = 2,     cp4 = 3,
    cp5 = 4,    cp6 = 5,      cp7 = 6,     cp8 = 7,
    cp9 = 8,    cp10 = 9,     cp11 = 10,   cp12 = 11,
    cp13 = 12,  cp14 = 13,    cp15 = 14,   cp16 = 15,
    cp17 = 16,  cp18 = 17,    cp19 = 18,   cp20 = 19,
    cp21 = 20,  cp22 = 21,    cp23 = 22,   cp24 = 23,
    cp25 = 24,  cp26 = 25,    cp27 = 26,   cp28 = 27,
    cp29 = 28,  cp30 = 29,    cp31 = 30,   cp32 = 31,
    cp33 = 32,  cp34 = 33,    cp35 = 34,   cp36 = 35,
    cp37 = 36,  cp38 = 37,    cp39 = 38,   cp40 = 39,
    cp41 = 40,  cp42 = 41,    cp43 = 42,   cp44 = 43,
    cp45 = 44,  cp46 = 45,    cp47 = 46,   cp48 = 47,
    cp49 = 48,  cp50 = 49,    cp51 = 50,   cp52 = 51,
    cp53 = 52,  cp54 = 53,    cp55 = 54,   cp56 = 55,
    cp57 = 56,  cp58 = 57,    cp59 = 58,   cp60 = 59,
    cp61 = 60,  cp62 = 61,    cp63 = 62,   cp64 = 63,
    cp65 = 64,  cp66 = 65,    cp67 = 66,   cp68 = 67,
    cp69 = 68,  cp70 = 69,    cp71 = 70,   cp72 = 71,
    cp73 = 72,  cp74 = 73,    cp75 = 74,   cp76 = 75,
    cp77 = 76,  cp78 = 77,    cp79 = 78,   cp80 = 79,
    cp81 = 80,  cp82 = 81,    cp83 = 82,   cp84 = 83,
    cp85 = 84,  cp86 = 85,    cp87 = 86,   cp88 = 87,
    cp89 = 88,  cp90 = 89,    cp91 = 90,   cp92 = 91,
    cp93 = 92,  cp94 = 93,    cp95 = 94,   cp96 = 95,
    cp97 = 96,  cp98 = 97,    cp99 = 98,   cp100 = 99,
    cp101 = 100, cp102 = 101, cp103 = 102, cp104 = 103,
    cp105 = 104, cp106 = 105, cp107 = 106, cp108 = 107,
    cp109 = 108, cp110 = 109, cp111 = 100, cp112 = 111,
    cp113 = 112, cp114 = 113, cp115 = 114, cp116 = 115,
    cp117 = 116, cp118 = 117, cp119 = 118, cp120 = 119,
    cp121 = 120, cp122 = 121, cp123 = 122, cp124 = 123,
    cp125 = 124, cp126 = 125, cp127 = 126, cp128 = 127
  };

  public enum EBaudrate
  {
    br110 = 110, br300 = 300, br600 = 600, br1200 = 1200,
    br2400 = 2400, br4800 = 4800, br9600 = 9600, br19200 = 19200,
    br38400 = 38400, br57600 = 57600, br115200 = 115200,
    br230400 = 230400, br460800 = 460800, br921600 = 921600, 
    br1000000 = 1000000, br1382400 = 1382400, br2000000 = 2000000
  };

  public enum EParity
  {
    paNone = Parity.None,
    paEven = Parity.Even,
    paOdd = Parity.Odd,
    paMark = Parity.Mark,
    paSpace = Parity.Space
  };

  public enum EDatabits
  {
    db5 = 5,
    db6 = 6,
    db7 = 7,
    db8 = 8
  };

  public enum EStopbits
  {
    sb0 = StopBits.None,
    sb1 = StopBits.One,
    sb15 = StopBits.OnePointFive,
    sb2 = StopBits.Two
  };

  public enum EHandshake
  {
    hsNone = Handshake.None,                    // 0
    hsXONXOFF = Handshake.XOnXOff,              // 1
    hsRTSCTS = Handshake.RequestToSend,         // 2
    hsRTSXON = Handshake.RequestToSendXOnXOff   // 3
  };
  //
  //------------------------------------------------------
  //  Segment - Type - Callback
  //------------------------------------------------------
  //
  public delegate void DOnUartErrorDetected(EComPort comport, Int32 errorcode, String errortext);
  public delegate void DOnUartOpenCloseChanged(Guid comportid, EComPort comport, Boolean opened);
  public delegate void DOnUartTextTransmitted(EComPort comport, String text);
  public delegate void DOnUartLineTransmitted(EComPort comport, String line);
  public delegate void DOnUartTextReceived(EComPort comport, String text);
  public delegate void DOnUartLineReceived(EComPort comport, String line);
  public delegate void DOnUartPinChanged(EComPort comport, Boolean pincts, Boolean pinrts, 
                                         Boolean pindsr, Boolean pindtr, Boolean pindcd);
  //
  //------------------------------------------------------
  //  Segment - Type - CUart
  //------------------------------------------------------
  //
  public class CUart
  {
    public static readonly String HEADER_LIBRARY = "ComPort";
    //
    public const String INIT_UART_NAME = "UART1";
    public const EComPort INIT_UART_COMPORT = EComPort.cp1;
    public const EBaudrate INIT_UART_BAUDRATE = EBaudrate.br9600;
    public const EParity INIT_UART_PARITY = EParity.paNone;
    public const EDatabits INIT_UART_DATABITS = EDatabits.db8;
    public const EStopbits INIT_UART_STOPBITS = EStopbits.sb0;
    public const EHandshake INIT_UART_HANDSHAKE = EHandshake.hsNone;
    public const Int32 INIT_UART_RXDBUFFERSIZE = 4096;
    public const Int32 INIT_UART_TXDBUFFERSIZE = 4096;
    public const Int32 INIT_UART_TXDDELAYCHARACTER = 0;
    public const Int32 INIT_UART_TXDDELAYCARRIAGERETURN = 0;
    public const Int32 INIT_UART_TXDDELAYLINEFEED = 0;
    public const Int32 INIT_UART_RXDBYTESTHRESHOLD = 1; // Minimum of RXDBytes to signal Event!
    public const Boolean INIT_UART_TRANSMITENABLED = true;
    public const Boolean INIT_UART_DTRENABLE = true;
    public const Boolean INIT_UART_RTSENABLE = true;
    //
    public const Char CHARACTER_ZERO = (Char)0x00;
    public const Char CHARACTER_CR = (Char)0x0D;
    public const Char CHARACTER_LF = (Char)0x0A;
    //
    //-----------------------------------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------------------------------
    //
    private SerialPort FSerial = null;
    private Guid FID;                                     // RO
    private String FName;                                 // RW
    private EComPort FComPort;                            // RW
    private EBaudrate FBaudrate;                          // RW
    private EParity FParity;                              // RW
    private EDatabits FDatabits;                          // RW
    private EStopbits FStopbits;                          // RW
    private EHandshake FHandshake;                        // RW
    private Int32 FTXDDelayCharacter;                     // RW - [ms]
    private Int32 FTXDDelayCarriageReturn;                // RW - [ms]
    private Int32 FTXDDelayLineFeed;                      // RW - [ms]
    private Boolean FTransmitEnabled;                     // RW - Softswitch
    private DOnUartOpenCloseChanged FOnOpenCloseChanged;
    private DOnUartErrorDetected FOnErrorDetected;            // RW
    private DOnUartTextTransmitted FOnTextTransmitted;        // RW
    private DOnUartLineTransmitted FOnLineTransmitted;        // RW
    private DOnUartTextReceived FOnTextReceived;              // RW
    private DOnUartLineReceived FOnLineReceived;              // RW
    private DOnUartPinChanged FOnPinChanged;                  // RW
    private CRingBuffer FRXDRingBuffer = null;
    private CRingBuffer FTXDRingBuffer = null;
    private CRXDStreamFilter FRXDStreamFilter = null;
    private CTXDStreamFilter FTXDStreamFilter = null;
    private String FLineBuffer = "";
    //
    //-----------------------------------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------------------------------
    //
    public CUart()
    {
      FID = Guid.NewGuid();
      FName = INIT_UART_NAME; 
      FComPort = INIT_UART_COMPORT;
      FBaudrate = INIT_UART_BAUDRATE;
      FParity = INIT_UART_PARITY;
      FDatabits = INIT_UART_DATABITS;
      FStopbits = INIT_UART_STOPBITS;
      FHandshake = INIT_UART_HANDSHAKE;
      FTXDDelayCharacter = INIT_UART_TXDDELAYCHARACTER;
      FTXDDelayCarriageReturn = INIT_UART_TXDDELAYCARRIAGERETURN;
      FTXDDelayLineFeed = INIT_UART_TXDDELAYLINEFEED;
      FTransmitEnabled = INIT_UART_TRANSMITENABLED;
      FOnOpenCloseChanged = null;
      FOnErrorDetected = null;
      FOnTextTransmitted = null;
      FOnLineTransmitted = null;
      FOnTextReceived = null;
      FOnLineReceived = null;
      FOnPinChanged = null; 
      FSerial = new SerialPort();
      //
      FRXDRingBuffer = new CRingBuffer(INIT_UART_RXDBUFFERSIZE);
      FTXDRingBuffer = new CRingBuffer(INIT_UART_TXDBUFFERSIZE);
      //
      FRXDStreamFilter = new CRXDStreamFilter();
      FTXDStreamFilter = new CTXDStreamFilter();
      //
      FLineBuffer = "";
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Property
    //-----------------------------------------------------------------------------
    //
    public void SetOnErrorDetected(DOnUartErrorDetected value)
    {
      FOnErrorDetected = value;
    }
    public void SetOnOpenCloseChanged(DOnUartOpenCloseChanged value)
    {
      FOnOpenCloseChanged = value;
    }
    public void SetOnPinChanged(DOnUartPinChanged value)
    {
      FOnPinChanged = value;
    }
    public void SetOnTextTransmitted(DOnUartTextTransmitted value)
    {
      FOnTextTransmitted = value;
    }
    public void SetOnLineTransmitted(DOnUartLineTransmitted value)
    {
      FOnLineTransmitted = value;
    }
    public void SetOnTextReceived(DOnUartTextReceived value)
    {
      FOnTextReceived = value;
    }
    public void SetOnLineReceived(DOnUartLineReceived value)
    {
      FOnLineReceived = value;
    }

    public EComPort GetComPort()
    {
      return FComPort;
    }

    public void EnableTransmit(Boolean enabletransmit)
    {
      FTransmitEnabled = enabletransmit;
    }

    private Boolean GetCTS()
    {
      return FSerial.CtsHolding;
    }
    private void SetRTS(Boolean value)
    {
      FSerial.RtsEnable = value;
    }
    private Boolean GetRTS()
    {
      return FSerial.RtsEnable;
    }
    //
    //	Level-Access DSR/DTR
    //
    private Boolean GetDSR()
    {
      return FSerial.DsrHolding;
    }
    private void SetDTR(Boolean newvalue)
    {
      FSerial.DtrEnable = newvalue;
    }
    private Boolean GetDTR()
    {
      return FSerial.DtrEnable;
    }
    //
    //	Level-Access DCD
    //
    public Boolean GetDCD()
    {
      return FSerial.CDHolding;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Static Member - ComPort
    //-----------------------------------------------------------------------------
    //
    public static readonly String[] COMPORTS = 
    { 
      "COM1",	  "COM2",	  "COM3",		 "COM4",
			"COM5",		"COM6",	  "COM7",		 "COM8",
			"COM9",		"COM10",	"COM11",	"COM12",
			"COM13",	"COM14",	"COM15",	"COM16",
			"COM17",	"COM18",	"COM19",	"COM20",
      "COM21",	"COM22",	"COM23",	"COM24",
			"COM25",	"COM26",	"COM27",	"COM28",
			"COM29",	"COM30",  "COM31",	"COM32",
			"COM33",	"COM34",	"COM35",	"COM36",
			"COM37",	"COM38",	"COM39",  "COM40",
      "COM41",	"COM42",	"COM43",	"COM44",
			"COM45",	"COM46",	"COM47",	"COM48",
			"COM49",  "COM50",  "COM51",	"COM52",
			"COM53",	"COM54",	"COM55",	"COM56",
			"COM57",	"COM58",	"COM59",  "COM60",
      "COM61",	"COM62",	"COM63",	"COM64",
			"COM65",	"COM66",	"COM67",	"COM68",
			"COM69",  "COM70",  "COM71",	"COM72",
			"COM73",	"COM74",	"COM75",	"COM76",
			"COM77",	"COM78",	"COM79",  "COM80",
      "COM81",	"COM82",	"COM83",	"COM84",
			"COM85",	"COM86",	"COM87",	"COM88",
			"COM89",  "COM90",  "COM91",	"COM92",
			"COM93",	"COM94",	"COM95",	"COM96",
			"COM97",	"COM98",	"COM99",  "COM100",
      "COM101",	"COM102",	"COM103",	"COM104",
			"COM105",	"COM106", "COM107",	"COM108",
			"COM109", "COM110", "COM111",	"COM112",
			"COM113",	"COM114",	"COM115",	"COM116",
			"COM117",	"COM118",	"COM119", "COM120",
      "COM121",	"COM122",	"COM123",	"COM124",
			"COM125",	"COM126",	"COM127",	"COM128"
    };

    public static Int32 ComPortTextIndex(String value)
    {
      for (Int32 Index = 0; Index < COMPORTS.Length; Index++)
      {
        if (value == COMPORTS[Index])
        {
          return Index;
        }
      }
      return 0;
    }

    public static EComPort ComPortTextEnumerator(String value)
    {
      return (EComPort)ComPortTextIndex(value);
    }

    public static String ComPortEnumeratorName(EComPort value)
    {
      return COMPORTS[(int)value];
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Static Member - Baudrates
    //-----------------------------------------------------------------------------
    // 
    public static readonly String[] BAUDRATES = 
    { 
			"110", "300", "600", "1200",
			"2400", "4800", "9600", "19200",
			"38400", "57600", "115200",
			"230400", "460800", "921600", 
      "1000000", "1382400", "2000000"
		};

    public static readonly EBaudrate[] Baudrates = 
    { 
			EBaudrate.br110, EBaudrate.br300, EBaudrate.br600, 
      EBaudrate.br1200, EBaudrate.br2400, EBaudrate.br4800,
      EBaudrate.br9600, EBaudrate.br19200, EBaudrate.br38400, 
      EBaudrate.br57600, EBaudrate.br115200, EBaudrate.br230400,
      EBaudrate.br460800, EBaudrate.br921600, EBaudrate.br1000000, 
      EBaudrate.br1382400, EBaudrate.br2000000
		};

    public static Int32 BaudrateTextIndex(String value)
    {
      for (Int32 Index = 0; Index < BAUDRATES.Length; Index++)
      {
        if (value == BAUDRATES[Index])
        {
          return Index;
        }
      }
      return 0;
    }

    public static EBaudrate BaudrateTextEnumerator(String value)
    {
      return (EBaudrate)BaudrateTextIndex(value);
    }

    public static String BaudrateEnumeratorName(EBaudrate value)
    {
      switch (value)
      {
        case EBaudrate.br110:
          return "110";
        case EBaudrate.br300:
          return "300";
        case EBaudrate.br600:
          return "600";
        case EBaudrate.br1200:
          return "1200";
        case EBaudrate.br2400:
          return "2400";
        case EBaudrate.br4800:
          return "4800";
        case EBaudrate.br9600:
          return "9600";
        case EBaudrate.br19200:
          return "19200";
        case EBaudrate.br38400:
          return "38400";
        case EBaudrate.br57600:
          return "57600";
        case EBaudrate.br115200:
          return "115200";
        case EBaudrate.br230400:
          return "230400";
        case EBaudrate.br460800:
          return "460800";
        case EBaudrate.br921600:
          return "921600";
        case EBaudrate.br1000000:
          return "1000000";
        case EBaudrate.br1382400:
          return "1382400";
        case EBaudrate.br2000000:
          return "2000000";
        default: // unknown
          return "???";
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Static Member - Parity
    //-----------------------------------------------------------------------------
    //
    public static readonly String[] PARITIES = 
    { 
			"None",
			"Even",
			"Odd",
			"Mark",
			"Space"
		};
    public static readonly EParity[] Parities = 
    {
      EParity.paNone,
      EParity.paEven,
      EParity.paOdd,
      EParity.paMark,
      EParity.paSpace
    };

    public static Int32 ParityTextIndex(String value)
    {
      for (Int32 Index = 0; Index < PARITIES.Length; Index++)
      {
        if (value == PARITIES[Index])
        {
          return Index;
        }
      }
      return 0;
    }

    public static EParity ParityTextEnumerator(String value)
    {
      return (EParity)ParityTextIndex(value);
    }

    public static String ParityEnumeratorName(EParity value)
    {
      return PARITIES[(int)value];
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Static Member - Databits
    //-----------------------------------------------------------------------------
    //
    public static readonly String[] DATABITS = 
    { 
			"5",
			"6",
			"7",
			"8"
		};
    
    public static readonly EDatabits[] Databits = 
    { 
			EDatabits.db5,
			EDatabits.db6,
			EDatabits.db7,
			EDatabits.db8
		};

    public static Int32 DatabitsTextIndex(String value)
    {
      for (Int32 Index = 0; Index < DATABITS.Length; Index++)
      {
        if (value == DATABITS[Index])
        {
          return Index;
        }
      }
      return 0;
    }

    public static EDatabits DatabitsTextEnumerator(String value)
    {
      return (EDatabits)DatabitsTextIndex(value);
    }

    public static String DatabitsEnumeratorName(EDatabits value)
    {
      switch (value)
      {
        case EDatabits.db5:
          return "5";
        case EDatabits.db6:
          return "6";
        case EDatabits.db7:
          return "7";
        case EDatabits.db8:
          return "8";
        default: // unknown
          return "?";
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Static Member - Stopbits
    //-----------------------------------------------------------------------------
    //
    public static readonly String[] STOPBITS = 
    { 
			"0",
			"1",
			"1.5",
			"2"
		};
    public static readonly EStopbits[] Stopbits = 
    { 
			EStopbits.sb0,
			EStopbits.sb1,
			EStopbits.sb15,
			EStopbits.sb2
		};

    public static Int32 StopbitsTextIndex(String value)
    {
      for (Int32 Index = 0; Index < STOPBITS.Length; Index++)
      {
        if (value == STOPBITS[Index])
        {
          return Index;
        }
      }
      return 0;
    }

    public static EStopbits StopbitsTextEnumerator(String value)
    {
      return (EStopbits)StopbitsTextIndex(value);
    }

    public static String StopbitsEnumeratorName(EStopbits value)
    {
      return STOPBITS[(int)value];
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Static Member - Handshakes
    //-----------------------------------------------------------------------------
    //
    public static readonly String[] HANDSHAKES = 
    { 
			"None",
			"XONXOFF",
			"RTSCTS",
			"RTSXON"
		};
    public static readonly EHandshake[] Handshakes = 
    { 
			EHandshake.hsNone,
			EHandshake.hsXONXOFF,
			EHandshake.hsRTSCTS,
			EHandshake.hsRTSXON
		};

    public static Int32 HandshakeTextIndex(String value)
    {
      for (Int32 Index = 0; Index < HANDSHAKES.Length; Index++)
      {
        if (value == HANDSHAKES[Index])
        {
          return Index;
        }
      }
      return 0;
    }

    public static EHandshake HandshakeTextEnumerator(String value)
    {
      return (EHandshake)HandshakeTextIndex(value);
    }

    public static String HandshakeEnumeratorName(EHandshake value)
    {
      return HANDSHAKES[(int)value];
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Helper
    //-----------------------------------------------------------------------------
    //
    private void PurgeBuffer()
    {
      if (IsOpen())
      {
        FSerial.DiscardInBuffer();
        FSerial.DiscardOutBuffer();
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Management
    //-----------------------------------------------------------------------------
    //
    public Boolean Open(String name, EComPort comport,
                        EBaudrate baudrate, EParity parity, 
                        EDatabits databits, EStopbits stopbits, 
                        EHandshake handshake)
    {
      try
      {
        if (!FSerial.IsOpen)
        {
          FName = name;
          //
          FComPort = comport;
          FSerial.PortName = CUart.COMPORTS[(Int32)FComPort];
          FBaudrate = baudrate;
          FSerial.BaudRate = (Int32)FBaudrate;
          FDatabits = databits;
          FSerial.DataBits = (Int32)FDatabits;
          FStopbits = stopbits;
          FSerial.StopBits = (StopBits)FStopbits;
          FParity = parity;
          FSerial.Parity = (Parity)FParity;
          FHandshake = handshake;
          FSerial.Handshake = (Handshake)handshake;
          //
          FSerial.DtrEnable = INIT_UART_DTRENABLE;
          FSerial.RtsEnable = INIT_UART_RTSENABLE;
          FSerial.ReceivedBytesThreshold = INIT_UART_RXDBYTESTHRESHOLD;
          FSerial.Handshake = (Handshake)FHandshake;
          //
          FSerial.ErrorReceived += new SerialErrorReceivedEventHandler(SerialOnErrorDetected);
          FSerial.PinChanged += new SerialPinChangedEventHandler(SerialOnPinChanged);
          FSerial.DataReceived += new SerialDataReceivedEventHandler(SerialOnDataReceived);
          // 
          FSerial.Open();
          //
          if (FOnOpenCloseChanged is DOnUartOpenCloseChanged)
          {
            EComPort CP = ComPortTextEnumerator(FSerial.PortName);
            FOnOpenCloseChanged(FID, CP, IsOpen());
          }
          //
          return FSerial.IsOpen;
        }
        return false;
      }
      catch (Exception)
      {
        //_Error(EErrorCode.OpenFailed);
        return false;
      }
    }

    public Boolean Close()
    {
      try
      {
        FSerial.Close();
        //
        if (FOnOpenCloseChanged is DOnUartOpenCloseChanged)
        {
          EComPort CP = ComPortTextEnumerator(FSerial.PortName);
          FOnOpenCloseChanged(FID, CP, IsOpen());
        }
        //
        return !FSerial.IsOpen;
      }
      catch (Exception)
      {
        // _Error(EErrorCode.OpenFailed);
        return false;
      }
    }

    public Boolean IsOpen()
    {
      try
      {
        return FSerial.IsOpen;
      }
      catch (Exception)
      {
        // _Error(EErrorCode.AccessFailed);
        return false;
      }
    }

    public Boolean IsClosed()
    {
      try
      {
        return !FSerial.IsOpen;
      }
      catch (Exception)
      {
        // _Error(EErrorCode.AccessFailed);
        return false;
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Callback - Hardware - Error
    //-----------------------------------------------------------------------------
    //
    private void SerialOnErrorDetected(Object sender,
                                       SerialErrorReceivedEventArgs parameter)
    { 
       //_Error(EErrorCode.PortFailed);
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Callback - Hardware - DataReceived
    //-----------------------------------------------------------------------------
    //
    private void SerialOnDataReceived(Object sender,
                                      SerialDataReceivedEventArgs parameter)
    { 
      Int32 RXDPreset = FSerial.BytesToRead;
      if (0 < RXDPreset)
      { // No Overflow Protection!
        Char[] Buffer = new Char[RXDPreset];
        FSerial.Read(Buffer, 0, RXDPreset);
        for (Int32 CI = 0; CI < RXDPreset; CI++)
        { 
          String FilterText = FRXDStreamFilter.Execute(Buffer[CI]);
          for (Int32 FI = 0; FI < FilterText.Length; FI++)
          {
            FRXDRingBuffer.WriteCharacter(FilterText[FI]);
          }
        }
      }
      String Data = "";
      RXDPreset = FRXDRingBuffer.FillCount;
      for (Int32 CI = 0; CI < RXDPreset; CI++)
      {
        Char Character = CHARACTER_ZERO;
        if (FRXDRingBuffer.ReadCharacter(out Character))
        {
          Data += Character;
        }
      }
      int DL = Data.Length;
      if ((FOnTextReceived is DOnUartTextReceived) && (0 < DL))
      {
        FOnTextReceived(FComPort, Data);
      }
      if ((FOnLineReceived is DOnUartLineReceived) && (0 < DL))
      {
        for (Int32 DI = 0; DI < DL; DI++)
        {
          Char Character = Data[DI];
          switch (Character)
          {
            case CUart.CHARACTER_CR:
              if (0 < FLineBuffer.Length)
              {
                // ok Console.WriteLine("<<<" + FLineBuffer + ">>>");
                if (FOnLineReceived is DOnUartLineReceived)
                {
                  FOnLineReceived(FComPort, FLineBuffer);
                }
                FLineBuffer = "";
              }
              FLineBuffer = "";
              break;
            case CHARACTER_LF:
              // ignore...
              break;
            default:
              FLineBuffer += Character;
              break;
          }
        }
      }
    }
    //-----------------------------------------------------------------------------
    //  Segment - Callback - Hardware - PinChanged
    //-----------------------------------------------------------------------------
    //
    private void SerialOnPinChanged(Object sender,
                                    SerialPinChangedEventArgs parameter)
    {
      if (FOnPinChanged is DOnUartPinChanged)
      {
        FOnPinChanged(FComPort, GetCTS(), GetRTS(), GetDSR(), GetDTR(), GetDCD());
      }
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Static Management
    //-----------------------------------------------------------------------------
    //
    public static String[] GetInstalledPorts()
    { // return: sortierte Liste!
      String[] PortsInstalled = SerialPort.GetPortNames();
      String[] PortsSorted = new String[PortsInstalled.Length];
      Int32 SortIndex = 0;
      // Sortieren...(force Sortlist of COMPORTS!)
      for (Int32 PI = 0; PI < CUart.COMPORTS.Length; PI++)
      {
        for (Int32 AI = 0; AI < PortsInstalled.Length; AI++)
        { // sortieren nach CComPort.COMPORTS-Vorgabe!
          if (CUart.COMPORTS[PI] == PortsInstalled[AI])
          {
            PortsSorted[SortIndex] = PortsInstalled[AI];
            SortIndex++;
          }
        }
      }
      return PortsSorted;
    }

    public static String[] GetSelectablePorts()
    {
      String[] PortsInstalled = GetInstalledPorts();
      ArrayList PortsSelectable = new ArrayList();
      //
      SerialPort SP = new SerialPort();
      for (Int32 PI = 0; PI < PortsInstalled.Length; PI++)
      {
        try
        {
          SP.PortName = PortsInstalled[PI];
          SP.Open();
          SP.Close();
          PortsSelectable.Add(SP.PortName);
        }
        catch (Exception e)
        {
          Console.WriteLine("!!! Controlled Exception !!!");
          Console.WriteLine(e);
          Console.WriteLine("!!! Controlled Exception !!!");
        }
      }
      String[] Result = null;
      if (0 < PortsSelectable.Count)
      {
        Result = new String[PortsSelectable.Count];
        for (Int32 SI = 0; SI < PortsSelectable.Count; SI++)
        {
          Result[SI] = (String)PortsSelectable[SI];
        }
        return Result;
      }
      return Result;
    }

    public static Boolean IsPortSelectable(String portname)
    {
      String[] PortsSelectable = GetSelectablePorts();
      if (PortsSelectable is String[])
      {
        foreach (String Port in PortsSelectable)
        {
          if (portname == Port)
          {
            return true;
          }
        }
      }
      return false;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Write - Local (no filter)
    //-----------------------------------------------------------------------------
    //
    private Boolean WriteSerialTextNoCallbackFilter(String text)
    {
      if (FSerial.IsOpen)
      {
        if (FTransmitEnabled)
        {
          if ((0 == FTXDDelayCharacter) &&
              (0 == FTXDDelayLineFeed) &&
              (0 == FTXDDelayCarriageReturn))
          { // no delays -> quick out 
            // NC if (FOnLineTransmitted
            // NC FOnTextTransmitted(FID, text);
            FSerial.Write(text); // WITHOUT WriteLine!!!
          } else
          { // out with delays
            for (Int32 CI = 0; CI < text.Length; CI++)
            {
              Char Character = text[CI];
              if (0 < FTXDDelayCharacter)
              {
                // NC FOnTextTransmitted(FID, Character.ToString());
                FSerial.Write(Character.ToString());
                Thread.Sleep(FTXDDelayCharacter);
              }
              if ((CUart.CHARACTER_CR == Character) && (0 < FTXDDelayCarriageReturn))
              {
                // NC FOnTextTransmitted(FID, Character.ToString());
                FSerial.Write(Character.ToString());
                Thread.Sleep(FTXDDelayCarriageReturn);
              } else
                if ((CUart.CHARACTER_LF == Character) && (0 < FTXDDelayLineFeed))
                {
                  // NC FOnTextTransmitted(FID, Character.ToString());
                  FSerial.Write(Character.ToString());
                  Thread.Sleep(FTXDDelayLineFeed);
                }
            }
          }
          return true;
        }
      }
      return false;
    }

    private Boolean WriteSerialBufferNoCallbackFilter(Byte[] filterdata, Int32 offset, Int32 length)
    {
      if (FSerial.IsOpen)
      {
        if (FTransmitEnabled)
        {
          Int32 Size = Math.Max(0, length - offset);
          if (0 < Size)
          {
            String TXData = "";
            for (Int32 BI = offset; BI < length; BI++)
            {
              TXData += (Char)filterdata[BI];
            }
            // NC FOnTextTransmitted(FID, TXData);
            return WriteText(TXData);
          }
          return false;
        }
        return true;
      }
      return false;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Write - Public (with filter)
    //-----------------------------------------------------------------------------
    //
    public Boolean WriteByte(Byte value)
    {
      Boolean Result = false;
      if ((FSerial.IsOpen) && (FTransmitEnabled))
      {
        String FilterText = FTXDStreamFilter.Execute((char)value);
        Result = WriteSerialTextNoCallbackFilter(FilterText);
        if (FOnTextTransmitted is DOnUartTextTransmitted)
        {
          FOnTextTransmitted(FComPort, FilterText);
        }
      }
      return Result;
    }

    public Boolean WriteCharacter(Char character)
    {
      Boolean Result = false;
      if ((FSerial.IsOpen) && (FTransmitEnabled))
      { 
        String FilterText = FTXDStreamFilter.Execute(character);
        Result = WriteSerialTextNoCallbackFilter(FilterText);
        if (FOnTextTransmitted is DOnUartTextTransmitted)
        {
          FOnTextTransmitted(FComPort, FilterText);
        }
      }
      return Result;
    }

    public Boolean WriteText(String text)
    {
      Boolean Result = false;
      if ((FSerial.IsOpen) && (FTransmitEnabled))
      {
        String FilterText = FTXDStreamFilter.Execute(text);
        Result = WriteSerialTextNoCallbackFilter(FilterText);
        if (FOnTextTransmitted is DOnUartTextTransmitted)
        {
          FOnTextTransmitted(FComPort, FilterText);
        }
      }
      return Result;
    }
    public Boolean WriteLine(String line)
    {
      Boolean Result = false;
      if ((FSerial.IsOpen) && (FTransmitEnabled))
      {
        String FilterText = FTXDStreamFilter.Execute(line);
        FilterText += FTXDStreamFilter.Execute((Char)CUart.CHARACTER_CR);
        FilterText += FTXDStreamFilter.Execute((Char)CUart.CHARACTER_LF);
        Result = WriteSerialTextNoCallbackFilter(FilterText);
        if (FOnLineTransmitted is DOnUartLineTransmitted)
        {
          FOnLineTransmitted(FComPort, FilterText);
        }
      }
      return Result;
    }

    public Boolean WriteBuffer(Byte[] buffer)
    {
      Boolean Result = false;
      if ((FSerial.IsOpen) && (FTransmitEnabled))
      { 
        Byte[] FilterBuffer = FTXDStreamFilter.Execute(buffer);
        Result = WriteSerialBufferNoCallbackFilter(FilterBuffer, 0, FilterBuffer.Length);
        if (FOnTextTransmitted is DOnUartTextTransmitted)
        {
          //FOnTextTransmitted(FID, FilterBuffer);
        }
      }
      return Result;
    }

    public Boolean WriteBuffer(Byte[] buffer, Int32 offset, Int32 length)
    {
      Boolean Result = false;
      if ((FSerial.IsOpen) && (FTransmitEnabled))
      { 
        Byte[] FilterBuffer = FTXDStreamFilter.Execute(buffer);
        Result = WriteSerialBufferNoCallbackFilter(FilterBuffer, offset, length);
        if (FOnTextTransmitted is DOnUartTextTransmitted)
        {
          //FOnTextTransmitted(FID, FilterBuffer);
        }
      }
      return Result;
    }
    //
    //-----------------------------------------------------------------------------
    //  Segment - Read - Public (with filter)
    //-----------------------------------------------------------------------------
    //
    public Boolean ReadCharacter(out Char character)
    { // already filtered in RXDCallback!
      character = CUart.CHARACTER_ZERO;
      return FRXDRingBuffer.ReadCharacter(out character);
    }

    public Boolean ReadText(out String text)
    { // already filtered in RXDCallback!
      text = "";
      if (FRXDRingBuffer.ReadText(out text))
      {
        return (0 < text.Length);
      }
      return false;
    }

    public Boolean ReadLine(out String line)
    { // already filtered in RXDCallback!
      line = "";
      if (FRXDRingBuffer.ReadLine(out line))
      {
        return (0 < line.Length);
      }
      return false;
    }

  }
}

