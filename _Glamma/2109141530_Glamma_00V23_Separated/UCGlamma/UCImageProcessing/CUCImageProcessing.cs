﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
//
using Initdata;
using Image24b;
//
namespace UCImageProcessing
{
  public partial class CUCImageProcessing : UserControl
  { //
    //---------------------------------------------------------------
    //  Constant
    //---------------------------------------------------------------
    private const String SECTION_UCIMAGEPROCESSING = "UCImageProcessing";
    //
    private const String NAME_BACKCOLOR = "BackColor";
    private const Byte INIT_BACKCOLOR_ALPHA = 0xFF;
    private const Byte INIT_BACKCOLOR_RED = 0x00;
    private const Byte INIT_BACKCOLOR_GREEN = 0x00;
    private const Byte INIT_BACKCOLOR_BLUE = 0x00;
    //
    private const String NAME_BACKCOLOR_BLACK = "BackColorBlack";
    private const Boolean INIT_BACKCOLOR_BLACK = false;
    private const String NAME_BACKCOLOR_WHITE = "BackColorWhite";
    private const Boolean INIT_BACKCOLOR_WHITE = false;
    private const String NAME_BACKCOLOR_USER = "BackColorUser";
    private const Boolean INIT_BACKCOLOR_USER = true;
    //
    //private const String NAME_SHORTSIDE = "ShortSide";
    //private const Boolean INIT_SHORTSIDE = true;
    ////
    //private const String NAME_LONGSIDE = "LongSide";
    //private const Boolean INIT_LONGSIDE = true;
    ////
    private const String NAME_CONVERTGREYSCALE = "ConvertGreyScale";
    private const Boolean INIT_CONVERTGREYSCALE = true;
    //
    private const String NAME_SHOWCUTFRAME = "ShowCutFrame";
    private const Boolean INIT_SHOWCUTFRAME = true;
    //
    private const String NAME_SHOWFILEINDEX = "ShowFileIndex";
    private const Boolean INIT_SHOWFILEINDEX = false;
    //
    private const String NAME_GAMMALINEAR = "GammaLinear";
    private const Boolean INIT_GAMMALINEAR = true;
    private const String NAME_GAMMAINVERSE = "GammaInverse";
    private const Boolean INIT_GAMMAINVERSE = true;
    private const String NAME_GAMMAHISTOGRAM = "GammaHistogram";
    private const Boolean INIT_GAMMAHISTOGRAM = true;
    //
    private const String NAME_TRANSLATEX = "TranslateX";
    private const float INIT_TRANSLATEX = 0.0f;
    //
    private const String NAME_TRANSLATEY = "TranslateY";
    private const float INIT_TRANSLATEY = 0.0f;
    //
    private const String NAME_ROTATEANGLE = "RotateAngle";
    private const float INIT_ROTATEANGLE = 0.0f;
    //
    private const String NAME_SCALEX = "ScaleX";
    private const float INIT_SCALEX = 1.0f;
    //
    private const String NAME_SCALEY = "ScaleY";
    private const float INIT_SCALEY = 1.0f;
    //
    private const String NAME_SELECTFILEDIRECTORY = "SelectFileDirectory";
    private const String INIT_SELECTFILEDIRECTORY_MASK = "\\result\\";
    //
    private const String NAME_DEFAULTFILEMASK = "DefaultFileMask";
    private const String INIT_DEFAULTFILEMASK = "Bitmap2048x512_{0:000}.bmp";
    //
    private const String NAME_FILEINDEXSTART = "FileIndexStart";
    private const int INIT_FILEINDEXSTART = 0;
    //
    private const String NAME_FILEINDEXEND = "FileIndexEnd";
    private const int INIT_FILEINDEXEND = 255;
    //
    private const String NAME_FILEINDEXSTEP = "FileIndexStep";
    private const int INIT_FILEINDEXSTEP = 50;
    //
    //---------------------------------------------------------------
    //  Field
    //---------------------------------------------------------------
    private CImage24b FImage24b;
    private Color FBackColor;
    //
    //---------------------------------------------------------------
    //  Constructor
    //---------------------------------------------------------------
    public CUCImageProcessing()
    {
      InitializeComponent();
      //
      FImage24b = new CImage24b();
      //
      SetFrameBackColorActive();
      //
      cbxConvertGreyScale.Checked = INIT_CONVERTGREYSCALE;
      cbxShowCutFrame.Checked = INIT_SHOWCUTFRAME;
      cbxShowFileIndex.Checked = INIT_SHOWFILEINDEX;
      //
      FBackColor = Color.FromArgb(INIT_BACKCOLOR_ALPHA, INIT_BACKCOLOR_RED, 
                                  INIT_BACKCOLOR_GREEN, INIT_BACKCOLOR_BLUE);
      rbtBackColorBlack.Checked = INIT_BACKCOLOR_BLACK;
      rbtBackColorWhite.Checked = INIT_BACKCOLOR_WHITE;
      rbtBackColorUser.Checked = INIT_BACKCOLOR_USER;
      //
      rbtGammaLinear.Checked = true;
      //
      TranslateX = INIT_TRANSLATEX;
      TranslateY = INIT_TRANSLATEY;
      RotateAngle = INIT_ROTATEANGLE;
      ScaleX = INIT_SCALEX;
      ScaleY = INIT_SCALEY;
      //
      DialogSelectFileDirectory.SelectedPath = Directory.GetCurrentDirectory() + INIT_SELECTFILEDIRECTORY_MASK;
      tbxFileName512Convert.Text = INIT_DEFAULTFILEMASK;
      //
      nudFile512IndexStart.Value = INIT_FILEINDEXSTART;
      nudFile512IndexEnd.Value = INIT_FILEINDEXEND;
      nudFile512IndexStep.Value = INIT_FILEINDEXSTEP;
    }
    //
    //---------------------------------------------------------------
    //  Property
    //---------------------------------------------------------------
    //private Boolean IsShortSide()
    //{
    //  return rbtShortSide.Checked;
    //}
    private void SetFrameBackColorActive()
    {
      btnCutFrame.BackColor = Color.FromArgb(0xFF, 0xEE, 0xDD, 0xDD);
    }
    private void SetFrameBackColorPassive()
    {
      btnCutFrame.BackColor = Color.FromArgb(0xFF, 0xDD, 0xDD, 0xDD);
    }
    public float ScaleX
    {
      get { return (float)nudScaleX.Value; }
      set { nudScaleX.Value = (decimal)value; }
    }
    public float ScaleY
    {
      get { return (float)nudScaleY.Value; }
      set { nudScaleY.Value = (decimal)value; }
    }
    public float RotateAngle
    {
      get { return (float)nudRotateAngle.Value; }
      set { nudRotateAngle.Value = (decimal)value; }
    }
    public float TranslateX
    {
      get { return (float)nudTranslateX.Value; }
      set { nudTranslateX.Value = (decimal)value; }
    }
    public float TranslateY
    {
      get { return (float)nudTranslateY.Value; }
      set { nudTranslateY.Value = (decimal)value; }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    public Boolean LoadInitdata(CInitdataReader initdata)
    {
      Boolean Result = true;
      Result &= initdata.OpenSection(SECTION_UCIMAGEPROCESSING);
      //
      //Boolean BValue = INIT_SHORTSIDE;
      //Result &= initdata.ReadBoolean(NAME_SHORTSIDE, out BValue, BValue);
      //rbtShortSide.Checked = BValue;
      //BValue = INIT_LONGSIDE;
      //Result &= initdata.ReadBoolean(NAME_LONGSIDE, out BValue, BValue);
      //rbtLongSide.Checked = BValue;
      //
      Boolean BValue = INIT_CONVERTGREYSCALE;
      Result &= initdata.ReadBoolean(NAME_CONVERTGREYSCALE, out BValue, BValue);
      cbxConvertGreyScale.Checked = BValue;
      //
      BValue = INIT_SHOWCUTFRAME;
      Result &= initdata.ReadBoolean(NAME_SHOWCUTFRAME, out BValue, BValue);
      cbxShowCutFrame.Checked = BValue;
      //
      BValue = INIT_SHOWFILEINDEX;
      Result &= initdata.ReadBoolean(NAME_SHOWFILEINDEX, out BValue, BValue);
      // ??? cbxShowFileIndex.Checked = BValue;
      //
      BValue = INIT_GAMMALINEAR;
      Result &= initdata.ReadBoolean(NAME_GAMMALINEAR, out BValue, BValue);
      rbtGammaLinear.Checked = BValue;
      BValue = INIT_GAMMAINVERSE;
      Result &= initdata.ReadBoolean(NAME_GAMMAINVERSE, out BValue, BValue);
      rbtGammaInverse.Checked = BValue;
      BValue = INIT_GAMMAHISTOGRAM;
      Result &= initdata.ReadBoolean(NAME_GAMMAHISTOGRAM, out BValue, BValue);
      rbtGammaHistogram.Checked = BValue;
      //
      float FValue = INIT_TRANSLATEX;
      Result &= initdata.ReadFloat32(NAME_TRANSLATEX, out FValue, FValue);
      TranslateX = FValue;
      FValue = INIT_TRANSLATEY;
      Result &= initdata.ReadFloat32(NAME_TRANSLATEY, out FValue, FValue);
      TranslateY = FValue;
      FValue = INIT_ROTATEANGLE;
      Result &= initdata.ReadFloat32(NAME_ROTATEANGLE, out FValue, FValue);
      RotateAngle = FValue;
      FValue = INIT_SCALEX;
      Result &= initdata.ReadFloat32(NAME_SCALEX, out FValue, FValue);
      ScaleX = FValue;
      FValue = INIT_SCALEY;
      Result &= initdata.ReadFloat32(NAME_SCALEY, out FValue, FValue);
      ScaleY = FValue;
      //
      String SValue = Directory.GetCurrentDirectory() + INIT_SELECTFILEDIRECTORY_MASK;
      Result &= initdata.ReadString(NAME_SELECTFILEDIRECTORY, out SValue, SValue);
      if (!Directory.Exists(SValue))
      {
        SValue = Directory.GetCurrentDirectory() + INIT_SELECTFILEDIRECTORY_MASK;
      }
      DialogSelectFileDirectory.SelectedPath = SValue;
      //
      SValue = INIT_DEFAULTFILEMASK;
      Result &= initdata.ReadString(NAME_DEFAULTFILEMASK, out SValue, SValue);
      tbxFileName512Convert.Text = SValue;
      //
      Int32 IValue = INIT_FILEINDEXSTART;
      Result &= initdata.ReadInt32(NAME_FILEINDEXSTART, out IValue, IValue);
      nudFile512IndexStart.Value = IValue;
      IValue = INIT_FILEINDEXEND;
      Result &= initdata.ReadInt32(NAME_FILEINDEXEND, out IValue, IValue);
      nudFile512IndexEnd.Value = IValue;
      IValue = INIT_FILEINDEXSTEP;
      Result &= initdata.ReadInt32(NAME_FILEINDEXSTEP, out IValue, IValue);
      nudFile512IndexStep.Value = IValue;
      //
      Result &= initdata.CloseSection();
      return Result;
    }
    public Boolean SaveInitdata(CInitdataWriter initdata)
    {
      Boolean Result = true;
      Result &= initdata.CreateSection(SECTION_UCIMAGEPROCESSING);
      //
      Result &= initdata.WriteString(NAME_DEFAULTFILEMASK, tbxFileName512Convert.Text);
      //Result &= initdata.WriteBoolean(NAME_SHORTSIDE, rbtShortSide.Checked);
      //Result &= initdata.WriteBoolean(NAME_LONGSIDE, rbtLongSide.Checked);
      Result &= initdata.WriteBoolean(NAME_CONVERTGREYSCALE, cbxConvertGreyScale.Checked);
      Result &= initdata.WriteBoolean(NAME_SHOWCUTFRAME, cbxShowCutFrame.Checked);
      Result &= initdata.WriteBoolean(NAME_SHOWFILEINDEX, cbxShowFileIndex.Checked);
      Result &= initdata.WriteBoolean(NAME_GAMMALINEAR, rbtGammaLinear.Checked);
      Result &= initdata.WriteBoolean(NAME_GAMMAINVERSE, rbtGammaInverse.Checked);
      Result &= initdata.WriteBoolean(NAME_GAMMAHISTOGRAM, rbtGammaHistogram.Checked);
      //
      Result &= initdata.WriteFloat32(NAME_TRANSLATEX, (float)nudTranslateX.Value);
      Result &= initdata.WriteFloat32(NAME_TRANSLATEY, (float)nudTranslateY.Value);
      Result &= initdata.WriteFloat32(NAME_ROTATEANGLE, (float)nudRotateAngle.Value);
      Result &= initdata.WriteFloat32(NAME_SCALEX, (float)nudScaleX.Value);
      Result &= initdata.WriteFloat32(NAME_SCALEY, (float)nudScaleY.Value);
      //
      Result &= initdata.WriteString(NAME_SELECTFILEDIRECTORY, DialogSelectFileDirectory.SelectedPath);
      Result &= initdata.WriteString(NAME_DEFAULTFILEMASK, tbxFileName512Convert.Text);
      Result &= initdata.WriteInt32(NAME_FILEINDEXSTART, (Int32)nudFile512IndexStart.Value);
      Result &= initdata.WriteInt32(NAME_FILEINDEXEND, (Int32)nudFile512IndexEnd.Value);
      Result &= initdata.WriteInt32(NAME_FILEINDEXSTEP, (Int32)nudFile512IndexStep.Value);
      //
      Result &= initdata.CloseSection();
      return Result;
    }
    //
    //---------------------------------------------------------------
    //  Helper
    //---------------------------------------------------------------
    public Boolean LoadSourceFromFile(String filename)
    {
      if (FImage24b.LoadSourceFromFile(filename))
      {
        tbxLoadFile.Text = Path.GetFileName(filename);
        SetFrameBackColorActive();
        if (FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor))
        {
          TransformProjection();
          return true;
        }
      }
      return false;
    }
    public Boolean NormalizeSource()
    {
      Boolean Result = FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);//, IsShortSide());
      if (Result)
      {
        TransformProjection();
      }
      return Result;
    }
    public Boolean TransformProjection(float scalex, float scaley, float angledeg, float dx, float dy)
    {
      Boolean Result = FImage24b.TransformProjection(scalex, scaley, angledeg, dx, dy, cbxShowCutFrame.Checked);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    public Boolean TransformProjection()
    {
      Boolean Result = FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle,
                                                     TranslateX, TranslateY, cbxShowCutFrame.Checked);
      if (Result)
      {
        pbxImage.Image = FImage24b.GetBitmapProjection();
      }
      return Result;
    }
    //
    //---------------------------------------------------------------
    //  Event - LoadFromFile
    //---------------------------------------------------------------
    private void btnLoadFromFile_Click(object sender, EventArgs e)
    {
      if (DialogResult.OK == DialogLoadImage.ShowDialog())
      {
        LoadSourceFromFile(DialogLoadImage.FileName);
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - CutFrame
    //---------------------------------------------------------------
    private void cbxShowCutFrame_CheckedChanged(object sender, EventArgs e)
    {
      TransformProjection();
    }
    private void btnCutFrame_Click(object sender, EventArgs e)
    {
      if (FImage24b.CutFrame(ScaleX, ScaleY, RotateAngle,
                             TranslateX, TranslateY))
      {
        ScaleX = 1.0f;
        ScaleY = 1.0f;
        RotateAngle = 0.0f;
        TranslateX = 0.0f;
        TranslateY = 0.0f;
        if (FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle,
                                          TranslateX, TranslateY, 
                                          cbxShowCutFrame.Checked))
        {
          pbxImage.Image = FImage24b.GetBitmapProjection();
        }
      }
      SetFrameBackColorPassive();
    }
    //
    //---------------------------------------------------------------
    //  Event - RotateAngle
    //---------------------------------------------------------------
    private void SetRotateAngle(float value)
    {
      if (value != (float)(scbRotateAngle.Value / 10.0))
      {
        scbRotateAngle.Value = (int)(10.0 * value);
      }
      if (value != (float)nudRotateAngle.Value)
      {
        nudRotateAngle.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle,
                                    TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
      SetFrameBackColorActive();
    }
    private void scbRotateAngle_ValueChanged(object sender, EventArgs e)
    {
      SetRotateAngle((float)(scbRotateAngle.Value / 10.0));
    }
    private void nudRotateAngle_ValueChanged(object sender, EventArgs e)
    {
      SetRotateAngle((float)(nudRotateAngle.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - TranslateX
    //---------------------------------------------------------------
    private void SetTranslateX(float value)
    {
      if (value != (float)(scbTranslateX.Value / 10.0))
      {
        scbTranslateX.Value = (int)(10.0 * value);
      }
      if (value != (float)nudTranslateX.Value)
      {
        nudTranslateX.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, 
                                    TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
      SetFrameBackColorActive();
    }
    private void scbTranslateX_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateX((float)(scbTranslateX.Value / 10.0));
    }
    private void nudTranslateX_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateX((float)(nudTranslateX.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - TranslateY
    //---------------------------------------------------------------
    private void SetTranslateY(float value)
    {
      if (value != (float)(scbTranslateY.Value / 10.0))
      {
        scbTranslateY.Value = (int)(10.0 * value);
      }
      if (value != (float)nudTranslateY.Value)
      {
        nudTranslateY.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, 
                                    TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
      SetFrameBackColorActive();
    }
    private void scbTranslateY_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateY((float)(scbTranslateY.Value / 10.0));
    }
    private void nudTranslateY_ValueChanged(object sender, EventArgs e)
    {
      SetTranslateY((float)(nudTranslateY.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - ScaleX
    //---------------------------------------------------------------
    private void SetScaleX(float value)
    {
      float SBF = 1.0f;
      if (0 < scbScaleX.Value)
      {
        SBF = (float)(1.0 + scbScaleX.Value / 100.0);
      }
      else
      if (scbScaleX.Value < 0)
      {
        SBF = (float)(1.0 + scbScaleX.Value / 100.0);
      } // else 1.0
      if (value != SBF)
      {
        if (1.0f < value)
        {
          scbScaleX.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        if (1.0f < value)
        {
          scbScaleX.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        { // 1.0
          scbScaleX.Value = 0;
        }
      }
      if (value != (float)nudScaleX.Value)
      {
        nudScaleX.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, 
                                    TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
      SetFrameBackColorActive();
    }
    private void scbScaleX_ValueChanged(object sender, EventArgs e)
    {
      if (0 < scbScaleX.Value)
      {
        SetScaleX((float)(1.0 + scbScaleX.Value / 100.0));
      }
      else
      if (scbScaleX.Value < 0)
      {
        SetScaleX((float)(1.0 + scbScaleX.Value / 100.0));
      }
      else
      {
        SetScaleX(1.0f);
      }
    }
    private void nudScaleX_ValueChanged(object sender, EventArgs e)
    {
      SetScaleX((float)(nudScaleX.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - ScaleY
    //---------------------------------------------------------------
    private void SetScaleY(float value)
    {
      float SBF = 1.0f;
      if (0 < scbScaleY.Value)
      {
        SBF = (float)(1.0 - scbScaleY.Value / 100.0);
      }
      else
      if (scbScaleY.Value < 0)
      {
        SBF = (float)(1.0 - scbScaleY.Value / 100.0);
      } // else 1.0
      if (value != SBF)
      {
        if (1.0f < value)
        {
          scbScaleY.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        if (1.0f < value)
        {
          scbScaleY.Value = (int)(100.0 * (SBF - 1.0));
        }
        else
        { // 1.0
          scbScaleY.Value = 0;
        }
      }
      if (value != (float)nudScaleY.Value)
      {
        nudScaleY.Value = (decimal)value;
      }
      FImage24b.TransformProjection(ScaleX, ScaleY, RotateAngle, 
                                    TranslateX, TranslateY, cbxShowCutFrame.Checked);
      pbxImage.Image = FImage24b.GetBitmapProjection();
      SetFrameBackColorActive();
    }
    private void scbScaleY_ValueChanged(object sender, EventArgs e)
    {
      if (0 < scbScaleY.Value)
      {
        SetScaleY((float)(1.0 - scbScaleY.Value / 100.0));
      }
      else
      if (scbScaleY.Value < 0)
      {
        SetScaleY((float)(1.0 - scbScaleY.Value / 100.0));
      }
      else
      {
        SetScaleY(1.0f);
      }
    }
    private void nudScaleY_ValueChanged(object sender, EventArgs e)
    {
      SetScaleY((float)(nudScaleY.Value));
    }
    //
    //---------------------------------------------------------------
    //  Event - GreyScale
    //---------------------------------------------------------------
    private void cbxConvertGreyScale_CheckedChanged(object sender, EventArgs e)
    {
      FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);//, IsShortSide());
      TransformProjection();
    }
    //
    //---------------------------------------------------------------
    //  Event - BackColor
    //---------------------------------------------------------------
    private void rbtBackColorWhite_CheckedChanged(object sender, EventArgs e)
    {
      FBackColor = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
      FImage24b.SetFrameColor(FBackColor);
      TransformProjection();
    }
    private void rbtBackColorBlack_CheckedChanged(object sender, EventArgs e)
    {
      FBackColor = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
      FImage24b.SetFrameColor(FBackColor);
      TransformProjection();
    }
    private void rbtBackColorUser_CheckedChanged(object sender, EventArgs e)
    {
      FBackColor = lblBackColorUser.BackColor;
      FImage24b.SetFrameColor(FBackColor);
      TransformProjection();
    }
    private void lblBackColorUser_Click(object sender, EventArgs e)
    {
      DialogColor.Color = lblBackColorUser.BackColor;
      if (DialogResult.OK == DialogColor.ShowDialog())
      {
        lblBackColorUser.BackColor = DialogColor.Color;
        if (rbtBackColorUser.Checked)
        {
          FBackColor = lblBackColorUser.BackColor;
          FImage24b.SetFrameColor(FBackColor);
          TransformProjection();
        }
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - GammaFactor
    //---------------------------------------------------------------
    private void rbtGammaLinear_CheckedChanged(object sender, EventArgs e)
    { // Set HistogramLinear
      if (rbtGammaLinear.Checked)
      {
        FImage24b.SetGammaHistogramLinear();
        FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);//, IsShortSide());
        TransformProjection();
      }
    }
    private void rbtGammaInverse_CheckedChanged(object sender, EventArgs e)
    { // Set HistogramInverse
      if (rbtGammaInverse.Checked)
      {
        FImage24b.SetGammaHistogramInverse();
        FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);//, IsShortSide());
        TransformProjection();
      }
    }
    private void rbtGammaHistogram_CheckedChanged(object sender, EventArgs e)
    { // Load Histogram
      if (rbtGammaHistogram.Checked)
      {
        if (!File.Exists(DialogLoadHistogram.FileName))
        {
          DialogLoadHistogram.FileName = ".\\source\\GammaFactor.hst.xml";
        }
        FImage24b.LoadGammaHistogramFromFile(DialogLoadHistogram.FileName);
        FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);//, IsShortSide());
        TransformProjection();
      }
    }
    private void btnSelectGammaHistogram_Click(object sender, EventArgs e)
    {
      DialogLoadHistogram.FileName = ".\\source\\GammaFactor.hst.xml";
      if (DialogResult.OK == DialogLoadHistogram.ShowDialog())
      {
        rbtGammaHistogram.CheckedChanged -= rbtGammaHistogram_CheckedChanged;
        rbtGammaHistogram.Checked = true;
        rbtGammaHistogram.CheckedChanged += rbtGammaHistogram_CheckedChanged;
        FImage24b.LoadGammaHistogramFromFile(DialogLoadHistogram.FileName);
        FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);//, IsShortSide());
        TransformProjection();
      }
    }
    //
    // Special Function: Save default HistogramLinear
    private void rbtGammaLinear_KeyDown(object sender, KeyEventArgs e)
    {
      String FN = ".\\source\\GammaLinear.hst.xml";
      if (!File.Exists(FN))
      {
        FImage24b.SetGammaHistogramLinear();
        FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);//, IsShortSide());
        TransformProjection();
        FImage24b.SaveGammaHistogramToFile(FN);
      }
    }
    //
    // Special Function: Save default HistogramInverse
    private void rbtGammaInverse_KeyDown(object sender, KeyEventArgs e)
    {
      String FN = ".\\source\\GammaInverse.hst.xml";
      if (!File.Exists(FN))
      {
        FImage24b.SetGammaHistogramInverse();
        FImage24b.NormalizeSource(cbxConvertGreyScale.Checked, FBackColor);//, IsShortSide());
        TransformProjection();
        FImage24b.SaveGammaHistogramToFile(FN);
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - Show FileIndex
    //---------------------------------------------------------------
    private void SetFileIndex(int value)
    {
      if (value != scbShowIndex.Value)
      {
        scbShowIndex.Value = value;
        // debug Console.WriteLine(String.Format("scb {0} {1}", value, (int)nudShowIndex.Value));
      }
      if (value != nudShowIndex.Value)
      {
        nudShowIndex.Value = value;
        // debug Console.WriteLine(String.Format("nud {0} {1}", value, (int)nudShowIndex.Value));
      }
      FImage24b.ConvertShow256x256(scbShowIndex.Value, cbxShowCutFrame.Checked, FBackColor);
      pbxImage.Image = FImage24b.GetBitmapProjection();
    }
    private void nudShowIndex_ValueChanged(object sender, EventArgs e)
    {
      SetFileIndex((int)nudShowIndex.Value);
    }
    private void scbShowIndex_ValueChanged(object sender, EventArgs e)
    {
      SetFileIndex(scbShowIndex.Value);
    }

    private void cbxShowFileIndex_CheckedChanged(object sender, EventArgs e)
    {
      if (cbxShowFileIndex.Checked)
      { // Mode: Show FileIndex Enabled
        pnlFrame.Enabled = false;
        pnlFile.Enabled = false;
        nudShowIndex.Enabled = true;
        scbShowIndex.Enabled = true;
        SetFileIndex(scbShowIndex.Value);
      }
      else
      { // Mode: Show FileIndex Disabled
        pnlFrame.Enabled = true;
        pnlFile.Enabled = true;
        nudShowIndex.Enabled = false;
        scbShowIndex.Enabled = false;
        TransformProjection();
      }
    }
    //
    //---------------------------------------------------------------
    //  Event - Convert to File
    //---------------------------------------------------------------
    private void btnDefaultFileMask_Click(object sender, EventArgs e)
    {
      tbxFileName512Convert.Text = INIT_DEFAULTFILEMASK;
    }
    private void btnConvert512_Click(object sender, EventArgs e)
    {
      // NC !!! DialogSelectFileDirectory.SelectedPath = Directory.GetCurrentDirectory() + "\\result";
      if (DialogResult.OK == DialogSelectFileDirectory.ShowDialog())
      { // Console.WriteLine(DialogSelectFileDirectory.SelectedPath);
        String FileMask = DialogSelectFileDirectory.SelectedPath + "\\" + tbxFileName512Convert.Text;
        FImage24b.ConvertToFile2048x512(FileMask, (int)nudFile512IndexStart.Value,
                                        (int)nudFile512IndexEnd.Value, (int)nudFile512IndexStep.Value);
      }
    }

  }
}
