﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
using System.Threading;
//
using Initdata;
using UCNotifier;
using Task;
using XmlFile;
using Image24b;
using Uart;
using CommandProtocol;
using CPLaserAreaScanner;
//
namespace Glamma
{ //
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    // Global
    private const String NAME_AUTOSTART = "AutoStart";
    private const Boolean INIT_AUTOSTART = true;
    private const String NAME_AUTOINTERVAL = "AutoInterval";
    private const Int32 INIT_AUTOINTERVAL = 1000;
    private const String NAME_DEBUG = "Debug";
    private const Boolean INIT_DEBUG = true;
    private const String NAME_SHOWPAGE = "ShowPage";
    private const Int32 INIT_SHOWPAGE = 1;
    //------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------
    private CUart FUart;
    private CCommandList FCommandList;
    private String FRxdLine = "";
    private Int32 FRxdCount = 0;
    private Int32 FRxdIndex = 0;
    private Boolean FResult = false;
    // 
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //	
    //------------------------------------------------------
    //  Section - Addon Constructor
    //------------------------------------------------------
    //	 
    private void InstantiateProgrammerControls()
    {
      CInitdata.Init();
      mitSendDiagnosticEmail.Visible = false;
      //
      FUCSerialNumber.SetNotifier(FUCNotifier);
      //this.Controls.Remove(tbpSerialNumberProductKey);
      //
      //####################################################################################
      // Init - Instance - Client
      //####################################################################################
      InstantiateUart();
      //
      InstantiateUartTerminal();
      //
      FCommandList = new CCommandList();
      // -> Main! FCommandList.SetUdpTextDeviceClient(FUdpTextDeviceClient);
      FCommandList.SetNotifier(FUCNotifier);
      FCommandList.SetOnExecutionStart(CommandListOnExecutionStart);
      FCommandList.SetOnExecutionBusy(CommandListOnExecutionBusy);
      FCommandList.SetOnExecutionEnd(CommandListOnExecutionEnd);
      FCommandList.SetOnExecutionAbort(CommandListOnExecutionAbort);
      //
      tmrStartup.Interval = INIT_AUTOINTERVAL;
      cbxAutomate.Checked = INIT_AUTOSTART;
      tbcGlamma.SelectedIndex = INIT_SHOWPAGE;
      //
      InstantiateUCLaserAreaCommand();
    }
    private void FreeProgrammerControls()
    {
      tmrStartup.Stop();
      FUart.Close();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Init
    //------------------------------------------------------------------------
    private Boolean LoadInitdataProgrammerControls(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.LoadInitdata(initdata);
      //
      Int32 IValue = INIT_AUTOINTERVAL;
      Result &= initdata.ReadInt32(NAME_AUTOINTERVAL, out IValue, IValue);
      nudAutoInterval.Value = IValue;
      //
      Boolean BValue = INIT_AUTOSTART;
      Result &= initdata.ReadBoolean(NAME_AUTOSTART, out BValue, BValue);
      cbxAutomate.Checked = BValue;
      tmrStartup.Enabled = BValue;
      //
      BValue = INIT_DEBUG;
      Result &= initdata.ReadBoolean(NAME_DEBUG, out BValue, BValue);
      cbxDebug.Checked = BValue;
      //
      IValue = INIT_SHOWPAGE;
      Result &= initdata.ReadInt32(NAME_SHOWPAGE, out IValue, IValue);
      tbcGlamma.SelectedIndex = IValue;
      //
      Result &= LoadInitdataUart(initdata);
      Result &= LoadInitdataUartTerminal(initdata);
      //
      Result &= LoadInitdataUCLaserAreaCommand(initdata);
      //
      FUCImageProcessing.LoadInitdata(initdata);
      //
      FCommandList.Execute();
      //
      return Result;
    }
    private Boolean SaveInitdataProgrammerControls(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      Result &= FUCSerialNumber.SaveInitdata(initdata);
      Result &= initdata.WriteBoolean(NAME_DEBUG, cbxDebug.Checked);
      Result &= initdata.WriteInt32(NAME_SHOWPAGE, tbcGlamma.SelectedIndex);
      Result &= initdata.WriteInt32(NAME_AUTOINTERVAL, (Int32)nudAutoInterval.Value);
      Result &= initdata.WriteBoolean(NAME_AUTOSTART, cbxAutomate.Checked);
      //
      Result &= SaveInitdataUart(initdata);
      Result &= SaveInitdataUartTerminal(initdata);
      //
      Result &= SaveInitdataUCLaserAreaCommand(initdata);
      //
      FUCImageProcessing.SaveInitdata(initdata);
      //
      return Result;
    }
    //
    //#########################################################################
    //  Segment - Callback - CommandList
    //#########################################################################
    //
    private delegate void CBCommandListOnExecutionStart(RTaskData data);
    private void CommandListOnExecutionStart(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionStart CB = new CBCommandListOnExecutionStart(CommandListOnExecutionStart);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          //!!! FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionStart", data.Name));
        }
        if (0 < FCommandList.Count)
        {
          CCommand Command = FCommandList.Peek();
          if (Command is CCommand)
          {
            String CommandText = Command.GetCommandText();
            FUart.WriteLine(CommandText);
          }
        }
      }
    }
    private delegate Boolean CBCommandListOnExecutionBusy(RTaskData data);
    private Boolean CommandListOnExecutionBusy(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionBusy CB = new CBCommandListOnExecutionBusy(CommandListOnExecutionBusy);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          //!!! FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionBusy", data.Name));
        }
      }
      return false;
    }
    private delegate void CBCommandListOnExecutionEnd(RTaskData data);
    private void CommandListOnExecutionEnd(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionEnd CB = new CBCommandListOnExecutionEnd(CommandListOnExecutionEnd);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          //!!! FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionEnd", data.Name));
        }
      }
    }
    private delegate void CBCommandListOnExecutionAbort(RTaskData data);
    private void CommandListOnExecutionAbort(RTaskData data)
    {
      if (this.InvokeRequired)
      {
        CBCommandListOnExecutionAbort CB = new CBCommandListOnExecutionAbort(CommandListOnExecutionAbort);
        Invoke(CB, new object[] { data });
      }
      else
      {
        if (cbxDebug.Checked)
        {
          //!!! FUCNotifier.Write(String.Format("Main[{0}] - OnExecutionAbort", data.Name));
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - Common
    //###########################################################################################
    //
    private delegate void CBAnalyseDateTimePrompt();
    private void AnalyseDateTimePrompt()
    {
      if (this.InvokeRequired)
      {
        CBAnalyseDateTimePrompt CB = new CBAnalyseDateTimePrompt(AnalyseDateTimePrompt);
        Invoke(CB, new object[] { });
      }
      else
      {
        if (14 <= FRxdLine.Length)
        { // test ":hh:mm:ss.mmm>"
          if ((CDefine.SEPARATOR_COLON == FRxdLine[0]) && (CDefine.SEPARATOR_GREATER == FRxdLine[13]))
          {
            Char[] Separators = new Char[] { CDefine.SEPARATOR_MARK,
                                             CDefine.SEPARATOR_COLON,
                                             CDefine.SEPARATOR_DOT,
                                             CDefine.SEPARATOR_GREATER };
            String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
            if (3 < Tokens.Length)
            {
              UInt16 Hours, Minutes, Seconds, Millis;
              Hours = UInt16.Parse(Tokens[0]);
              Minutes = UInt16.Parse(Tokens[1]);
              Seconds = UInt16.Parse(Tokens[2]);
              Millis = UInt16.Parse(Tokens[3]);
              FUCLaserAreaScannerCommand.SetProcessTime(Hours, Minutes, Seconds, Millis);
              if (cbxDebug.Checked)
              {
                //FUCNotifier.Write(String.Format("DATETIME[{0}:{1}:{2}.{3}]",
                //                                Tokens[0], Tokens[1], Tokens[2], Tokens[3]));                
              }
              // debug Console.WriteLine("VVV" + rxdline + "VVV");
              FRxdLine = FRxdLine.Substring(14);
              // debug Console.WriteLine("NNN" + rxdline + "NNN");
              // Event -> NO command.SignalTextReceived();

            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseStateLaserAreaScanner();
    private void AnalyseStateLaserAreaScanner()
    {
      if (this.InvokeRequired)
      {
        CBAnalyseStateLaserAreaScanner CB =
          new CBAnalyseStateLaserAreaScanner(AnalyseStateLaserAreaScanner);
        Invoke(CB, new object[] { });
      }
      else
      {
        if (7 <= FRxdLine.Length)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          Int32 TL = Tokens.Length;
          if (4 <= TL)
          {
            if (CDefine.PROMPT_RESPONSE == Tokens[0])
            {
              Int32 SMTC;
              Int32.TryParse(Tokens[2], out SMTC);
              Int32 SubState;
              Int32.TryParse(Tokens[3], out SubState);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("STATELASERAREASCANNER[{0}|{1}]", SMTC, SubState));
              }
              FUCLaserAreaScannerCommand.SetState(Tokens);
              // deleting STP-Information from RxdLine
              Int32 S = 3 + Tokens[0].Length + Tokens[1].Length + Tokens[2].Length + Tokens[3].Length;
              FRxdLine = FRxdLine.Substring(S);
              //
              if ((0 == SMTC) && (0 == SubState))
              {
                if (0 < FCommandList.Count)
                {
                  CCommand Command = (CCommand)FCommandList.Peek();
                  if (Command is CCommand)
                  {
                    Command.SignalTextReceived();
                  }
                }
              }
            }
          }
        }
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    private void cbxAutomate_CheckedChanged(object sender, EventArgs e)
    {
      tmrStartup.Enabled = cbxAutomate.Checked;
      if (tmrStartup.Enabled)
      {
        FStartupState = 0;
      }
    }
    //
    //------------------------------------------------------------------------
    //  Section - Startup
    //------------------------------------------------------------------------
    private Boolean OnStartupTick(Int32 startupstate)
    {
      switch (FStartupState)
      {
        case 0:
          //tbcMain.SelectedIndex = 1;
          //FUCEditorLaserStepFile.LoadFromFile("Matrix18002200100.lsf.txt");
          return true;
        case 1:
          return true;
        case 2:
          return true;
        case 3:
          return true;
        case 4:
          return true;
        case 5:
          return true;
        case 6:
          //FUdpTextDeviceClient.Preset("GSV");
          return true;
        case 7:
          return true;
        case 8:
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! First (Client -> Server)");
          return true;
        case 9:
          //FUdpTextDeviceClient.Preset("GHV");
          //// debug 
          //FUCNotifier.Write("*** UdpTextDeviceClient - Transmit Message:");
          //FUdpTextDeviceClient.Preset("Hello World! Second (Client -> Server)");
          return true;
        case 10:
          return true;
        case 11:
          return true;
        case 12:
          //Application.Exit();
          //return false;
          return true;
        case 13:
          return true;
        case 14:
          return true;
        case 15:
          return true;
        case 16:
          return true;
        case 17:
          return true;
        case 18:
          return true;
        case 19:
          return true;
        case 20:
          return true;
        default:
          // NC !!!! cbxAutomate.Checked = false;
          return false;
      }
    }  


  }
}

