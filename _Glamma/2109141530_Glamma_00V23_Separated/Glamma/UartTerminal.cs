﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
using System.Threading;
//
using Initdata;
using UCNotifier;
using Task;
using XmlFile;
using Image24b;
using Uart;
using CommandProtocol;
using CPLaserAreaScanner;
//
namespace Glamma
{
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  {
    void InstantiateUartTerminal()
    {
      // already exist FUCUartTerminal = new CUCUartTerminal....
      FUCUartTerminal.SetNotifier(FUCNotifier);
      FUCUartTerminal.SetOnTerminalOpen(UCUartTerminalOnTerminalOpen);
      FUCUartTerminal.SetOnTerminalClose(UCUartTerminalOnTerminalClose);
      FUCUartTerminal.SetOnTransmitText(UCUartTerminalOnTransmitText);
    }

    private Boolean LoadInitdataUartTerminal(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      return Result;
    }
    //
    private Boolean SaveInitdataUartTerminal(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCUartTerminal
    //------------------------------------------------------------------------
    private void UCUartTerminalOnTerminalOpen(String portname, EComPort comport, EBaudrate baudrate, EParity parity,
                                              EDatabits databits, EStopbits stopbits, EHandshake handshake)
    {
      String Line;
      if (FUart.Open(portname, comport, baudrate, parity, databits, stopbits, handshake))
      {
        if (cbxDebug.Checked)
        {
          Line = String.Format("ComPort[{0}]:({1}/{2}/{3}/{4}/{5}/{6}) correctly opened!",
                               portname, CUart.ComPortEnumeratorName(comport),
                               CUart.BaudrateEnumeratorName(baudrate), CUart.ParityEnumeratorName(parity),
                               CUart.DatabitsEnumeratorName(databits), CUart.StopbitsEnumeratorName(stopbits),
                               CUart.HandshakeEnumeratorName(handshake));
          FUCNotifier.Write(Line);
        }
        FUCUartTerminal.SetUartOpened(true);
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Error(HEADER_LIBRARY, 1, String.Format("Cannot open ComPort[{0}]",
                                                             CUart.ComPortEnumeratorName(comport)));
        }
        FUCUartTerminal.SetUartOpened(false);
      }
    }
    //
    private void UCUartTerminalOnTerminalClose(EComPort comport)
    {
      if (FUart.Close())
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Write(String.Format("ComPort[{0}] correctly closed!", CUart.ComPortEnumeratorName(comport)));
        }
      }
      else
      {
        if (cbxDebug.Checked)
        {
          FUCNotifier.Error(HEADER_LIBRARY, 1, String.Format("Cannot close ComPort[{0}]",
                                                            CUart.ComPortEnumeratorName(comport)));
        }
      }
    }
    //
    private void UCUartTerminalOnTransmitText(string text, int delaycharacter, int delaycr, int delaylf)
    {
      if (cbxDebug.Checked)
      {
        FUart.WriteText(text);
      }
    }
    //
  }
}