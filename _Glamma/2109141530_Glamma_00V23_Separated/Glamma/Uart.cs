﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Xml;
using System.Diagnostics;
using System.Threading;
//
using Initdata;
using UCNotifier;
using Task;
using XmlFile;
using Image24b;
using Uart;
using CommandProtocol;
using CPLaserAreaScanner;
//
namespace Glamma
{
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------------------------
    // Global
    private const String NAME_COMPORT = "ComPort";
    private const EComPort INIT_COMPORT = EComPort.cp1;

    private void InstantiateUart()
    {
      FUart = new CUart();
      FUart.SetOnErrorDetected(UartOnErrorDetected);
      FUart.SetOnLineReceived(UartOnLineReceived);
      FUart.SetOnLineTransmitted(UartOnLineTransmitted);
      FUart.SetOnPinChanged(UartOnPinChanged);
      FUart.SetOnTextReceived(UartOnTextReceived);
      FUart.SetOnTextTransmitted(UartOnTextTransmitted);
    }
    //
    private Boolean LoadInitdataUart(CInitdataReader initdata)
    {
      Boolean Result = true;
      //
      String SCP = CUart.COMPORTS[(Int32)INIT_COMPORT];
      Result &= initdata.ReadEnumeration(NAME_COMPORT, out SCP, SCP);
      EComPort ECP = CUart.ComPortTextEnumerator(SCP);
      FUCUartTerminal.SetComPort(ECP);
      //
      return Result;
    }
    //
    private Boolean SaveInitdataUart(CInitdataWriter initdata)
    {
      Boolean Result = true;
      //
      String SCP = CUart.ComPortEnumeratorName(FUCUartTerminal.GetComPort());
      Result &= initdata.WriteEnumeration(NAME_COMPORT, SCP);
      //
      return Result;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Callback - Uart
    //------------------------------------------------------------------------
    private void UartOnErrorDetected(EComPort comport, Int32 errorcode, String errortext)
    {
    }
    //
    private void UartOnLineReceived(EComPort comport, String line)
    {
      if (cbxDebug.Checked)
      {
        FUCNotifier.Write(String.Format("ComPort[{0}] Rxd<{1}>", CUart.ComPortEnumeratorName(comport), line));
      }
      FRxdLine = line;
      AnalyseDateTimePrompt();
      AnalyseStateLaserAreaScanner();
      //  change with ...  - AnalyseTemperatureValues();
      if (0 < FCommandList.Count)
      {
        CCommand Command = FCommandList.Peek();
        if (Command is CCommand)
        { // Analyse Command:
          if (1 <= FRxdLine.Length)
          {
            FResult = false;
            // Common
            AnalyseGetHelp(Command);
            if (FResult) return;
            AnalyseGetProgramHeader(Command);
            if (FResult) return;
            AnalyseGetSoftwareVersion(Command);
            if (FResult) return;
            AnalyseGetHardwareVersion(Command);
            if (FResult) return;
            AnalyseGetProcessCount(Command);
            if (FResult) return;
            AnalyseSetProcessCount(Command);
            if (FResult) return;
            AnalyseGetProcessPeriodms(Command);
            if (FResult) return;
            AnalyseSetProcessPeriodms(Command);
            if (FResult) return;
            AnalyseStopProcessExecution(Command);
            if (FResult) return;
            // LedSystem
            AnalyseGetLedSystem(Command);
            if (FResult) return;
            AnalyseSetLedSystemHigh(Command);
            if (FResult) return;
            AnalyseSetLedSystemLow(Command);
            if (FResult) return;
            AnalyseBlinkLedSystem(Command);
            if (FResult) return;
            // LaserRange
            AnalyseGetPositionX(Command);
            if (FResult) return;
            AnalyseSetPositionX(Command);
            if (FResult) return;
            AnalyseGetPositionY(Command);
            if (FResult) return;
            AnalyseSetPositionY(Command);
            if (FResult) return;
            AnalyseGetRangeX(Command);
            if (FResult) return;
            AnalyseSetRangeX(Command);
            if (FResult) return;
            AnalyseGetRangeY(Command);
            if (FResult) return;
            AnalyseSetRangeY(Command);
            if (FResult) return;
            AnalyseGetDelayMotionms(Command);
            if (FResult) return;
            AnalyseSetDelayMotionms(Command);
            if (FResult) return;
            AnalyseGetPulseWidthus(Command);
            if (FResult) return;
            AnalyseSetPulseWidthus(Command);
            if (FResult) return;
            // LaserPosition
            //AnalysePulseLaserPosition(Command);
            //if (FResult) return;
            //AnalyseAbortLaserPosition(Command);
            //if (FResult) return;
            //// LaserLine
            //AnalysePulseLaserRow(Command);
            //if (FResult) return;
            //AnalyseAbortLaserRow(Command);
            //if (FResult) return;
            //AnalysePulseLaserCol(Command);
            //if (FResult) return;
            //AnalyseAbortLaserCol(Command);
            //if (FResult) return;
            //// LaserMatrix
            //AnalysePulseLaserMatrix(Command);
            //if (FResult) return;
            //AnalyseAbortLaserMatrix(Command);
            //if (FResult) return;
            //// LaserImage
            //// no Command LoadImage!!!
            //AnalysePulseLaserImage(Command);
            //if (FResult) return;
            //AnalyseAbortLaserImage(Command);
            //if (FResult) return;
          }
        }
      }
    }
    //
    private void UartOnLineTransmitted(EComPort comport, String line)
    {
      if (cbxDebug.Checked)
      {
        FUCNotifier.Write(String.Format("ComPort[{0}] Txd<{1}>", CUart.ComPortEnumeratorName(comport), line));
      }
    }
    //
    private void UartOnPinChanged(EComPort comport, Boolean pincts, Boolean pinrts,
                                  Boolean pindsr, Boolean pindtr, Boolean pindcd)
    {
    }
    //
    private void UartOnTextReceived(EComPort comport, String text)
    {
    }
    //
    private void UartOnTextTransmitted(EComPort comport, String text)
    {
      if (cbxDebug.Checked)
      {
        FUCNotifier.Write(String.Format("ComPort[{0}] Txd<{1}>", CUart.ComPortEnumeratorName(comport), text));
      }
    }
    //
  }
}