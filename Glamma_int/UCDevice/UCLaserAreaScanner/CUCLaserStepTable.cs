﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using LaserScanner;
//
namespace UCLaserAreaScanner
{
  public partial class CUCLaserStepTable : UserControl
  {
    //public const int XLOCATION = 0;
    //public const int YLOCATION = 1;
    //public const int PULSEPERIODMS = 2;
    //public const int PULSECOUNT = 3;

    public CUCLaserStepTable()
    {
      InitializeComponent();
      dgvSteptable.Columns.Clear();
      dgvSteptable.Columns.Add("Index", "Index");
      dgvSteptable.Columns.Add("PositionX", "PositionX");
      dgvSteptable.Columns.Add("PositionY", "PositionY");
      dgvSteptable.Columns.Add("PulsePeriod", "PulsePeriod[ms]");
      dgvSteptable.Columns.Add("PulseCount", "PulseCount");
      dgvSteptable.Columns.Add("DelayMotion", "DelayMotion[ms]");
    }

    public void ClearAllSteps()
    {
      dgvSteptable.Rows.Clear();
    }

    public void PassivateRefresh()
    {
      dgvSteptable.SuspendLayout();
    }
    public void ActivateRefresh()
    {
      dgvSteptable.ResumeLayout();
    }

    public void SetLaserStepList(CLaserStepList lasersteplist)
    {
      try
      {
        PassivateRefresh();
        dgvSteptable.Rows.Clear();
        Int32 LSC = lasersteplist.Count;
        for (Int32 LSI = 0; LSI < LSC; LSI++)
        {
          CLaserStep LS = lasersteplist[LSI];    
          UInt32 PX = LS.PositionX;
          UInt32 PY = LS.PositionY;
          UInt32 PP = LS.PulsePeriodms;
          UInt32 PC = LS.PulseCount;
          UInt32 DM = LS.DelayMotionms;
          dgvSteptable.Rows.Add(String.Format("{0}", LSI),
                                String.Format("{0}", PX),
                                String.Format("{0}", PY),
                                String.Format("{0}", PP),
                                String.Format("{0}", PC),
                                String.Format("{0}", DM));
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
      }
      finally
      {
        ActivateRefresh();
      }
    }


    public void AddStep(UInt16 xposition, UInt16 yposition,
                        UInt16 pulseperiodms, UInt16 pulsecount,
                        UInt16 delaymotionms)
    {
      dgvSteptable.Rows.Add(String.Format("{0}", dgvSteptable.RowCount),
                            String.Format("{0}", xposition),
                            String.Format("{0}", yposition),
                            String.Format("{0}", pulseperiodms),
                            String.Format("{0}", pulsecount),
                            String.Format("{0}", pulseperiodms));
    }

    //public void AddStepRange(DataGridViewRow steptable)
    //{
    //  dgvSteptable.Rows.AddRange(steptable);
    //}

    private delegate void CBSelectLaserStep(UInt32 index);
    public void SelectLaserStep(UInt32 index)
    {
      if (this.InvokeRequired)
      {
        CBSelectLaserStep CB = new CBSelectLaserStep(SelectLaserStep);
        Invoke(CB, new object[] { index });
      }
      else
      {
        if (index < dgvSteptable.RowCount)
        {
          dgvSteptable.Rows[(int)index].Selected = true;
          dgvSteptable.FirstDisplayedScrollingRowIndex = (int)index;
        }
      }        
    }


  }
}