﻿namespace UCMultiColorText
{
  partial class CUCMultiColorText
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.cmsMultiColorText = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitClearProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSaveProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.mitLoadProtocol = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
      this.mitEnableEditing = new System.Windows.Forms.ToolStripMenuItem();
      this.DialogLoadProtocol = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveProtocol = new System.Windows.Forms.SaveFileDialog();
      this.mitAddSpaceLine = new System.Windows.Forms.ToolStripMenuItem();
      this.cmsMultiColorText.SuspendLayout();
      this.SuspendLayout();
      // 
      // cmsMultiColorText
      // 
      this.cmsMultiColorText.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitClearProtocol,
            this.mitSaveProtocol,
            this.mitLoadProtocol,
            this.toolStripMenuItem1,
            this.mitEnableEditing,
            this.mitAddSpaceLine});
      this.cmsMultiColorText.Name = "cmsMultiColorText";
      this.cmsMultiColorText.Size = new System.Drawing.Size(153, 142);
      // 
      // mitClearProtocol
      // 
      this.mitClearProtocol.Name = "mitClearProtocol";
      this.mitClearProtocol.Size = new System.Drawing.Size(152, 22);
      this.mitClearProtocol.Text = "Clear Protocol";
      this.mitClearProtocol.Click += new System.EventHandler(this.mitClearAllLines_Click);
      // 
      // mitSaveProtocol
      // 
      this.mitSaveProtocol.Name = "mitSaveProtocol";
      this.mitSaveProtocol.Size = new System.Drawing.Size(152, 22);
      this.mitSaveProtocol.Text = "Save Protocol";
      this.mitSaveProtocol.Click += new System.EventHandler(this.mitSaveProtocol_Click);
      // 
      // mitLoadProtocol
      // 
      this.mitLoadProtocol.Name = "mitLoadProtocol";
      this.mitLoadProtocol.Size = new System.Drawing.Size(152, 22);
      this.mitLoadProtocol.Text = "Load Protocol";
      this.mitLoadProtocol.Click += new System.EventHandler(this.mitLoadProtocol_Click);
      // 
      // toolStripMenuItem1
      // 
      this.toolStripMenuItem1.Name = "toolStripMenuItem1";
      this.toolStripMenuItem1.Size = new System.Drawing.Size(149, 6);
      // 
      // mitEnableEditing
      // 
      this.mitEnableEditing.CheckOnClick = true;
      this.mitEnableEditing.Name = "mitEnableEditing";
      this.mitEnableEditing.Size = new System.Drawing.Size(152, 22);
      this.mitEnableEditing.Text = "Enable Editing";
      this.mitEnableEditing.Click += new System.EventHandler(this.mitEnableEditing_Click);
      // 
      // DialogLoadProtocol
      // 
      this.DialogLoadProtocol.DefaultExt = "txt";
      this.DialogLoadProtocol.FileName = "Protocol";
      this.DialogLoadProtocol.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadProtocol.Title = "Load Protocol";
      // 
      // DialogSaveProtocol
      // 
      this.DialogSaveProtocol.DefaultExt = "txt";
      this.DialogSaveProtocol.FileName = "Protocol";
      this.DialogSaveProtocol.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogSaveProtocol.Title = "Save Protocol";
      // 
      // mitAddSpaceLine
      // 
      this.mitAddSpaceLine.Name = "mitAddSpaceLine";
      this.mitAddSpaceLine.Size = new System.Drawing.Size(152, 22);
      this.mitAddSpaceLine.Text = "Add SpaceLine";
      this.mitAddSpaceLine.Click += new System.EventHandler(this.mitAddSpaceline_Click);
      // 
      // CUCMultiColorText
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ContextMenuStrip = this.cmsMultiColorText;
      this.Name = "CUCMultiColorText";
      this.Size = new System.Drawing.Size(232, 232);
      this.cmsMultiColorText.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ContextMenuStrip cmsMultiColorText;
    private System.Windows.Forms.ToolStripMenuItem mitClearProtocol;
    private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
    private System.Windows.Forms.ToolStripMenuItem mitSaveProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitLoadProtocol;
    private System.Windows.Forms.OpenFileDialog DialogLoadProtocol;
    private System.Windows.Forms.SaveFileDialog DialogSaveProtocol;
    private System.Windows.Forms.ToolStripMenuItem mitEnableEditing;
    private System.Windows.Forms.ToolStripMenuItem mitAddSpaceLine;

    // ??? private System.Windows.Forms.ContextMenuStrip cmsMultiColorText;
    // ??? private System.Windows.Forms.ToolStripMenuItem mitClearAllLines;
    // ??? private System.Windows.Forms.ToolStripMenuItem addSpacelineToolStripMenuItem;
  }
}
