﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using Uart;
//
namespace UCUartOpenClose
{
  public delegate void DOnPortSelectedChanged(EComPort value);

  public partial class CUCPortsSelectable : UserControl
  {
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnPortSelectedChanged FOnPortSelectedChanged;

    public CUCPortsSelectable()
    {
      InitializeComponent();
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnPortSelectedChanged(DOnPortSelectedChanged value)
    {
      FOnPortSelectedChanged = value;
    }

    public EComPort GetPortSelected()
    {
      EComPort Result = CUart.ComPortTextEnumerator(cbxPortsSelectable.Text);
      return Result;
    }
    public void SetPortSelected(EComPort value)
    {
      for (Int32 SI = 0; SI < cbxPortsSelectable.Items.Count; SI++)      
      {
        String SValue = (String)cbxPortsSelectable.Items[SI];
        EComPort CP = CUart.ComPortTextEnumerator(SValue);
        if (value == CP)
        {
          cbxPortsSelectable.SelectedIndex = SI;
          return;
        }
      }
    }


    private void cbxPortsSelectable_IndexChanged(object sender, EventArgs e)
    {
      if (FOnPortSelectedChanged is DOnPortSelectedChanged)
      {
        FOnPortSelectedChanged(GetPortSelected());
      }
    }

    public void RefreshPortsSelectable()
    {
      cbxPortsSelectable.Items.Clear();
      String[] SelectablePorts = CUart.GetSelectablePorts();
      if (null != SelectablePorts)
      {
        cbxPortsSelectable.Items.AddRange(SelectablePorts);
        int SI = cbxPortsSelectable.Items.Count - 1;
        cbxPortsSelectable.SelectedIndex = SI;
      }
    }


    public Boolean IsAvailable()
    {
      return (0 <= cbxPortsSelectable.SelectedIndex);
    }

  }
}





