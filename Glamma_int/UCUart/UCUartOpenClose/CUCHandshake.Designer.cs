﻿namespace UCUartOpenClose
{
  partial class CUCHandshake
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxHandshake = new System.Windows.Forms.ComboBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // cbxHandshake
      // 
      this.cbxHandshake.Dock = System.Windows.Forms.DockStyle.Right;
      this.cbxHandshake.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxHandshake.FormattingEnabled = true;
      this.cbxHandshake.Location = new System.Drawing.Point(63, 0);
      this.cbxHandshake.Name = "cbxHandshake";
      this.cbxHandshake.Size = new System.Drawing.Size(72, 21);
      this.cbxHandshake.TabIndex = 4;
      // 
      // panel1
      // 
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(63, 4);
      this.panel1.TabIndex = 72;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Location = new System.Drawing.Point(0, 4);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(63, 17);
      this.label1.TabIndex = 73;
      this.label1.Text = "Handshake";
      // 
      // CUCHandshake
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.cbxHandshake);
      this.Name = "CUCHandshake";
      this.Size = new System.Drawing.Size(135, 21);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ComboBox cbxHandshake;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label1;
  }
}
