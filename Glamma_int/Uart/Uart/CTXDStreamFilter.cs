using System;
using System.Collections.Generic;
using System.Text;

namespace Uart
{	//
	// prinzipiell konzipiert als 1:1-Filter
	//
	public class CTXDStreamFilter : CStreamFilter
	{
		//
		//------------------------
		//	Constructor
		//------------------------
		//
		public CTXDStreamFilter():
			base()
		{ 
		}
		//
		//------------------------
		//	Task
		//------------------------
		//
		public override String Execute(Char newcharacter)
		{
			return base.Execute(newcharacter);
		}
		public override String Execute(String newtext)
		{
			return base.Execute(newtext);
		}
		public override Byte[] Execute(Byte[] buffer)
		{
			return base.Execute(buffer);
		}

	}
}
