﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Uart")]
[assembly: AssemblyDescription("1711031602")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("FM")]
[assembly: AssemblyProduct("Uart")]
[assembly: AssemblyCopyright("Copyright © LLG 2017")]
[assembly: AssemblyTrademark("FM")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("d2adfab4-875d-4b68-b0a4-a842ab723fbd")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("2.6.*")]
[assembly: AssemblyFileVersion("2.6")]
