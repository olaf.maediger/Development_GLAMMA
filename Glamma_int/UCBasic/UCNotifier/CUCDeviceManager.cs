﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
namespace UCNotifier
{
  public partial class CUCDeviceManager : UserControl
  {
    //
    //-----------------------------------------------------
    //  Segment - Const
    //-----------------------------------------------------
    //
    private const Int32 SPACEX = 4;
    private const Int32 SPACEX2 = SPACEX / 2;
    private const Int32 WIDTH_BUTTON = 76;
    private const Int32 WIDTH_BUTTON2 = WIDTH_BUTTON / 2;
    //
    private const String STRING_EMPTY = "";
    //
    //-----------------------------------------------------
    //  Segment - Field
    //-----------------------------------------------------
    //
    private CDevicelist FDevicelist;
    //
    //-----------------------------------------------------
    //  Segment - Constructor
    //-----------------------------------------------------
    //
    public CUCDeviceManager()
    {
      InitializeComponent();
      //
      FDevicelist = new CDevicelist();
      //
      FUCAddDevice.SetDevicelist(FDevicelist);
      FUCAddDevice.SetOnDeviceCancel(OnAddDeviceCancel);
      FUCAddDevice.SetOnDeviceConfirm(OnAddDeviceConfirm);
      
      FUCEditDevice.SetDevicelist(FDevicelist);
      FUCEditDevice.SetOnDeviceCancel(OnEditDeviceCancel);
      FUCEditDevice.SetOnDeviceConfirm(OnEditDeviceConfirm);
      
      FUCDeleteDevice.SetDevicelist(FDevicelist);
      FUCDeleteDevice.SetOnDeviceCancel(OnDeleteDeviceCancel);
      FUCDeleteDevice.SetOnDeviceConfirm(OnDeleteDeviceConfirm);
      //
      Init();
    }

    public Int32 GetTransformDeviceCount()
    {
      return FDevicelist.Count;
    }

    public CTransformDevice GetTransformDevice(Int32 index)
    {
      if ((0 <= index) && (index < FDevicelist.Count))
      {
        CTransformBase TransformBase = FDevicelist[index];
        if (TransformBase is CTransformDevice)
        {
          return (CTransformDevice)TransformBase;
        }
      }
      return null;
    }
    //
    //-----------------------------------------------------
    //  Segment - Event
    //-----------------------------------------------------
    //
    private void pnlKeys_Resize(object sender, EventArgs e)
    {
      Int32 CX = pnlKeys.Width / 2;
      if (btnClose.Visible)
      {
        btnCancel.Visible = true;
        btnDeleteDevice.Left = CX - WIDTH_BUTTON2;
        btnEditDevice.Left = CX - WIDTH_BUTTON2 - 1  * WIDTH_BUTTON - 1 * SPACEX;
        btnAddDevice.Left = CX - WIDTH_BUTTON2 - 2 * WIDTH_BUTTON - 2 * SPACEX;
        btnClose.Left = CX + WIDTH_BUTTON2 + SPACEX;
        btnCancel.Left = CX + WIDTH_BUTTON2 + WIDTH_BUTTON + 2 * SPACEX;
      } else
      {
        btnCancel.Visible = false;
        btnEditDevice.Left = CX - btnEditDevice.Width / 2;
        btnAddDevice.Left = btnEditDevice.Left - btnAddDevice.Width - SPACEX;
        btnDeleteDevice.Left = CX + btnEditDevice.Width / 2 + SPACEX;
      }
    }

    private void FUCListBoxDevices_SelectedIndexChanged(object sender, EventArgs e)
    {
      Int32 SI = FUCListBoxDevices.SelectedIndex;
      CTransformDevice TD = (CTransformDevice)FDevicelist[SI];
      if (TD is CTransformDevice)
      {
        RDeviceData DeviceData;
        TD.GetData(out DeviceData);
        /* DeviceData.TransformData.Name = ExtractName();
        DeviceData.TransformData.Version = ExtractVersion();
        DeviceData.TransformData.Date = ExtractDate();
        DeviceData.TransformData.DiscountUser = ExtractDiscountUser();
        DeviceData.TransformData.User = ExtractUser();
        DeviceData.SerialNumber = ExtractSerialNumber();
        DeviceData.TransformData.ProductNumber = ExtractProductNumber();
        DeviceData.TransformData.ProductKey = ExtractProductKey();*/
        //
        FUCAddDevice.SetData(DeviceData);
        FUCEditDevice.SetData(DeviceData);
        FUCDeleteDevice.SetData(DeviceData);
      }
    }
    //
    //-----------------------------------------------------
    //  Segment - Initialisation
    //-----------------------------------------------------
    //
    private void Init()
    {
      FUCAddDevice.Visible = false;
      FUCEditDevice.Visible = false;
      FUCDeleteDevice.Visible = false;
      btnClose.Visible = true;
      //
      btnAddDevice.Width = WIDTH_BUTTON;
      btnEditDevice.Width = WIDTH_BUTTON;
      btnDeleteDevice.Width = WIDTH_BUTTON;
      btnClose.Left = WIDTH_BUTTON;
      btnCancel.Left = WIDTH_BUTTON;
    }
    //
    //---------------------------------------------------------------
    //  Segment - Helper
    //---------------------------------------------------------------
    //
    private Boolean RefreshUCDeviceManager()
    {
      ClearLines();
      foreach (CTransformDevice TD in FDevicelist)
      {
        AddLine(TD);
      }
      return true;
    }

    public void ForceResize()
    {
      pnlKeys_Resize(this, null);
    }

    private CTransformDevice ExtractTransformDevice()
    {
      if ((0 <= FUCListBoxDevices.SelectedIndex) &&
          (FUCListBoxDevices.SelectedIndex < FDevicelist.Count))
      {
        CTransformBase TransformBase = FDevicelist[FUCListBoxDevices.SelectedIndex];
        if (TransformBase is CTransformDevice)
        {
          return (CTransformDevice)TransformBase;
        }
      }
      return null;
    }

    private String ExtractName()
    {
      CTransformDevice TransformDevice = ExtractTransformDevice();
      if (TransformDevice is CTransformDevice)
      {
        return TransformDevice.Name;
      }
      return STRING_EMPTY;
    }
    /* NCprivate String ExtractVersion()
    {
      CTransformDevice TransformDevice = ExtractTransformDevice();
      if (TransformDevice is CTransformDevice)
      {
        return TransformDevice.Version;
      }
      return STRING_EMPTY;
    }
    private String ExtractDate()
    {
      CTransformDevice TransformDevice = ExtractTransformDevice();
      if (TransformDevice is CTransformDevice)
      {
        return TransformDevice.Date;
      }
      return STRING_EMPTY;
    }*/
    private Boolean ExtractDiscountUser()
    {
      CTransformDevice TransformDevice = ExtractTransformDevice();
      if (TransformDevice is CTransformDevice)
      {
        return TransformDevice.DiscountUser;
      }
      return false;
    }
    private String ExtractUser()
    {
      CTransformDevice TransformDevice = ExtractTransformDevice();
      if (TransformDevice is CTransformDevice)
      {
        return TransformDevice.User;
      }
      return STRING_EMPTY;
    }
    private Int32 ExtractSerialNumber()
    {
      CTransformDevice TransformDevice = ExtractTransformDevice();
      if (TransformDevice is CTransformDevice)
      {
        return TransformDevice.SerialNumber;
      }
      return 0;
    }
    /* NC private Int32 ExtractProductNumber()
    {
      CTransformDevice TransformDevice = ExtractTransformDevice();
      if (TransformDevice is CTransformDevice)
      {
        return TransformDevice.ProductNumber;
      }
      return 0;
    }*/
    private String ExtractProductKey()
    {
      CTransformDevice TransformDevice = ExtractTransformDevice();
      if (TransformDevice is CTransformDevice)
      {
        return TransformDevice.ProductKey;
      }
      return STRING_EMPTY;
    }
    //
    //---------------------------------------------------------------
    //  Segment - Management - Device
    //---------------------------------------------------------------
    //
    public Int32 GetDeviceCount()
    {
      return FDevicelist.Count;
    }

    private Boolean CheckIndex(Int32 index)
    {
      return ((0 <= index) && (index < FDevicelist.Count));
    }

    public Int32 GetDeviceSerialNumber(Int32 index)
    {
      if (CheckIndex(index))
      {
        CTransformDevice TD = (CTransformDevice)FDevicelist[index];
        if (TD is CTransformDevice)
        {
          return TD.SerialNumber;
        }
      }
      return 0;
    }

    public String GetDeviceProductKey(Int32 index)
    {
      if (CheckIndex(index))
      {
        CTransformDevice TD = (CTransformDevice)FDevicelist[index];
        if (TD is CTransformDevice)
        {
          return TD.ProductKey;
        }
      }
      return STRING_EMPTY;
    }

    public Boolean AddDevice(String applicationkey,
                             String name,
                             String version,
                             String date,
                             Boolean discountuser,
                             String user,
                             Int32 serialnumber,
                             String productkey)
    {
      CTransform Transform = new CTransform();
      Int32 ProductNumber = Transform.BuildDevice(applicationkey,
                                                  name, version, date,
                                                  discountuser, user,
                                                  serialnumber);
      String ProductKey = Transform.BuildProductKey(ProductNumber);
      if (productkey == ProductKey)
      {
        if (FDevicelist.Add(name, version, date,
                            discountuser, user, serialnumber,
                            ProductNumber, ProductKey))
        {
          return RefreshUCDeviceManager();
        }
      }
      return false;
    }

    public Boolean AddDevice(String applicationkey,
                             String name,
                             String version,
                             String date,
                             Boolean discountuser,
                             String user,
                             Int32 serialnumber)
    {
      CTransform Transform = new CTransform();
      Int32 ProductNumber = Transform.BuildDevice(applicationkey,
                                                  name, version, date,
                                                  discountuser, user, serialnumber);
      String ProductKey = Transform.BuildProductKey(ProductNumber);
      if (FDevicelist.Add(name, version, date, 
                          discountuser, user, serialnumber,
                          ProductNumber, ProductKey))
      {
        return RefreshUCDeviceManager();
      }
      return false;
    }

    public Boolean AddDevice(String name,
                             String version,
                             String date,
                             Boolean discountuser,
                             String user,
                             Int32 serialnumber,
                             Int32 productnumber,
                             String productkey)
    {
      if (FDevicelist.Add(name, version, date, 
                          discountuser, user, serialnumber,
                          productnumber, productkey))
      {
        return RefreshUCDeviceManager();
      }
      return false;
    }

    public Boolean AddDevice(RDeviceData data)
    {
      if (FDevicelist.Add(data))
      {
        return RefreshUCDeviceManager();
      }
      return false;
    }

    public Boolean DeleteDevice(String name,
                                Int32 productnumber,
                                String productkey)
    {
      if (FDevicelist.Delete(name, productnumber, productkey))
      {
        return RefreshUCDeviceManager();
      }
      return false;
    }

    public Boolean ClearDevicelist()
    {
      FDevicelist.Clear();
      return (0 == FDevicelist.Count);
    }

    public Boolean CheckAllDevices(String applicationkey, 
                                   Int32 count)
    {
      Int32 DeviceCount = 0;
      Boolean Result = (0 < count);
      CTransform Transform = new CTransform();
      foreach (CTransformDevice TD in FDevicelist)
      {
        DeviceCount++;
        Int32 ProductNumber = Transform.BuildDevice(applicationkey,
                                                    TD.Name, TD.Version, TD.Date,
                                                    TD.DiscountUser, TD.User,
                                                    TD.SerialNumber);
        String ProductKey = Transform.BuildProductKey(ProductNumber);
        Result &= (ProductKey == TD.ProductKey);
      }
      Result &= (DeviceCount == count);
      return Result;
    }

    public Boolean CheckDevice(String applicationkey,
                               Int32 serialnumber)
    {
      Boolean Result = false;
      CTransform Transform = new CTransform();
      foreach (CTransformDevice TD in FDevicelist)
      {
        Int32 ProductNumber = Transform.BuildDevice(applicationkey,
                                                    TD.Name, TD.Version, TD.Date,
                                                    TD.DiscountUser, TD.User,
                                                    serialnumber);//TD.SerialNumber);
        String ProductKey = Transform.BuildProductKey(ProductNumber);
        Result |= (ProductKey == TD.ProductKey);
      }
      return Result;
    }
    //
    //-----------------------------------------------------
    //  Segment - Management - Line
    //-----------------------------------------------------
    //
    public Boolean ClearLines()
    {
      FUCListBoxDevices.Items.Clear();
      return true;
    }

    public Boolean AddLine(CTransformDevice device)
    {
      String Line = String.Format(" {0,-20} {1,-10} {2} ", 
                                  device.Name, device.SerialNumber, 
                                  device.ProductKey);
      FUCListBoxDevices.Items.Add(Line);
      FUCListBoxDevices.SelectedIndex = FUCListBoxDevices.Items.Count - 1;
      return true;
    }
    //
    //-----------------------------------------------------
    //  Segment - Menu - Add
    //-----------------------------------------------------
    //
    private void btnAddDevice_Click(object sender, EventArgs e)
    {
      pnlKeys.Visible = false;
      FUCAddDevice.Visible = true;
      FUCEditDevice.Visible = false;
      FUCDeleteDevice.Visible = false;
      FUCAddDevice.RefreshControls();
    }

    private void OnAddDeviceCancel(RDeviceData data)
    {
      FUCAddDevice.Visible = false;
      pnlKeys.Visible = true;
    }

    private void OnAddDeviceConfirm(RDeviceData data)
    {
      if ((0 < data.TransformData.Name.Length) &&
          // NC (0 < data.TransformData.ProductNumber) &&
          (0 < data.TransformData.ProductKey.Length))
      {
        FDevicelist.Add(data);
        //
        FUCAddDevice.Visible = false;
        pnlKeys.Visible = true;
        // 
        RefreshUCDeviceManager();
        FUCListBoxDevices.SelectedIndex = FUCListBoxDevices.Items.Count - 1;
      }
    }
    //
    //-----------------------------------------------------
    //  Segment - Menu - Edit
    //-----------------------------------------------------
    //
    private void btnEditDevice_Click(object sender, EventArgs e)
    {
      pnlKeys.Visible = false;
      FUCAddDevice.Visible = false;
      FUCEditDevice.Visible = true;
      FUCDeleteDevice.Visible = false;
      FUCEditDevice.RefreshControls();
    }

    private void OnEditDeviceCancel(RDeviceData data)
    {
      FUCEditDevice.Visible = false;
      pnlKeys.Visible = true;
    }

    private void OnEditDeviceConfirm(RDeviceData data)
    {
      if ((0 < data.TransformData.Name.Length) &&
          (0 < data.TransformData.ProductNumber) &&
          (0 < data.TransformData.ProductKey.Length))
      {
        FDevicelist.Change(FUCListBoxDevices.SelectedIndex, data);
        //
        FUCEditDevice.Visible = false;
        pnlKeys.Visible = true;
        // 
        RefreshUCDeviceManager();
        FUCListBoxDevices.SelectedIndex = FUCListBoxDevices.Items.Count - 1;
      }
    }
    //
    //-----------------------------------------------------
    //  Segment - Menu - Delete
    //-----------------------------------------------------
    //
    private void btnDeleteDevice_Click(object sender, EventArgs e)
    {
      pnlKeys.Visible = false;
      FUCAddDevice.Visible = false;
      FUCEditDevice.Visible = false;
      FUCDeleteDevice.Visible = true;
      FUCDeleteDevice.RefreshControls();
    }

    private void OnDeleteDeviceCancel(RDeviceData data)
    {
      FUCDeleteDevice.Visible = false;
      pnlKeys.Visible = true;
    }

    private void OnDeleteDeviceConfirm(RDeviceData data)
    {
      FDevicelist.Delete(data.TransformData.Name,
                              data.TransformData.ProductNumber,
                              data.TransformData.ProductKey);
      //
      FUCDeleteDevice.Visible = false;
      pnlKeys.Visible = true;
      // 
      RefreshUCDeviceManager();
      FUCListBoxDevices.SelectedIndex = FUCListBoxDevices.Items.Count - 1;
    }
    //
    //-----------------------------------------------------
    //  Segment - Button-Event - Common
    //-----------------------------------------------------
    //
    private void btnClose_Click(object sender, EventArgs e)
    {
      //...
    }

    private void btnCancel_Click(object sender, EventArgs e)
    {
      //...
    }
    //
    //-----------------------------------------------------
    //  Segment - 
    //-----------------------------------------------------
    //




  }
}
