﻿using System;
using System.Windows.Forms;
//
using Initdata;
using CommandProtocol;
using CPLaserAreaScanner;
//
namespace Glamma
{//
  //#########################################################
  //	Section - Main - Extended
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    // LaserAreaScannerCommand
    private const String NAME_PROCESSCOUNT = "ProcessCount";
    private const Int32 INIT_PROCESSCOUNT = 1;
    private const String NAME_PROCESSPERIODMS = "ProcessPeriodms";
    private const Int32 INIT_PROCESSPERIODMS = 1000;
    private const String NAME_POSITIONXACTUAL = "PositionXActual";
    private const Int32 INIT_POSITIONXACTUAL = 2000;
    private const String NAME_POSITIONXMINIMUM = "PositionXMinimum";
    private const Int32 INIT_POSITIONXMINIMUM = 1800;
    private const String NAME_POSITIONXMAXIMUM = "PositionXMaximum";
    private const Int32 INIT_POSITIONXMAXIMUM = 2200;
    private const String NAME_POSITIONXDELTA = "PositionXDelta";
    private const Int32 INIT_POSITIONXDELTA = 100;
    private const String NAME_POSITIONYACTUAL = "PositionYActual";
    private const Int32 INIT_POSITIONYACTUAL = 2000;
    private const String NAME_POSITIONYMINIMUM = "PositionYMinimum";
    private const Int32 INIT_POSITIONYMINIMUM = 1800;
    private const String NAME_POSITIONYMAXIMUM = "PositionYMaximum";
    private const Int32 INIT_POSITIONYMAXIMUM = 2200;
    private const String NAME_POSITIONYDELTA = "PositionYDelta";
    private const Int32 INIT_POSITIONYDELTA = 100;
    private const String NAME_DELAYMOTIONMS = "DelayMotionms";
    private const Int32 INIT_DELAYMOTIONMS = 10;
    private const String NAME_PULSEWIDTHUS = "PulseWidthus";
    private const Int32 INIT_PULSEWIDTHUS = 500000;    //
    //
    private void InstantiateUCLaserAreaCommand()
    {
      // already exist FUCLaserAreaScannerCommand = new CUCLaserAreaScannerCommand....
      // UCLaserAreaScannerCommand - Common
      FUCLaserAreaScannerCommand.SetOnGetHelp(UCLaserAreaScannerCommandOnGetHelp);
      FUCLaserAreaScannerCommand.SetOnGetProgramHeader(UCLaserAreaScannerCommandOnGetProgramHeader);
      FUCLaserAreaScannerCommand.SetOnGetHardwareVersion(UCLaserAreaScannerCommandOnGetHardwareVersion);
      FUCLaserAreaScannerCommand.SetOnGetSoftwareVersion(UCLaserAreaScannerCommandOnGetSoftwareVersion);
      FUCLaserAreaScannerCommand.SetOnGetProcessCount(UCLaserAreaScannerCommandOnGetProcessCount);
      FUCLaserAreaScannerCommand.SetOnSetProcessCount(UCLaserAreaScannerCommandOnSetProcessCount);
      FUCLaserAreaScannerCommand.SetOnGetProcessPeriodms(UCLaserAreaScannerCommandOnGetProcessPeriodms);
      FUCLaserAreaScannerCommand.SetOnSetProcessPeriodms(UCLaserAreaScannerCommandOnSetProcessPeriodms);
      FUCLaserAreaScannerCommand.SetOnAbortProcessExecution(UCLaserAreaScannerCommandOnAbortProcessExecution);
      // UCLaserAreaScannerCommand - LedSystem
      FUCLaserAreaScannerCommand.SetOnGetLedSystem(UCLaserAreaScannerCommandOnGetLedSystem);
      FUCLaserAreaScannerCommand.SetOnSetLedSystemLow(UCLaserAreaScannerCommandOnSetLedSystemLow);
      FUCLaserAreaScannerCommand.SetOnSetLedSystemHigh(UCLaserAreaScannerCommandOnSetLedSystemHigh);
      FUCLaserAreaScannerCommand.SetOnBlinkLedSystem(UCLaserAreaScannerCommandOnBlinkLedSystem);
      // UCLaserAreaScannerCommand - LaserRange
      FUCLaserAreaScannerCommand.SetOnGetPositionX(UCLaserAreaScannerCommandOnGetPositionX);
      FUCLaserAreaScannerCommand.SetOnSetPositionX(UCLaserAreaScannerCommandOnSetPositionX);
      FUCLaserAreaScannerCommand.SetOnGetPositionY(UCLaserAreaScannerCommandOnGetPositionY);
      FUCLaserAreaScannerCommand.SetOnSetPositionY(UCLaserAreaScannerCommandOnSetPositionY);
      FUCLaserAreaScannerCommand.SetOnGetRangeX(UCLaserAreaScannerCommandOnGetRangeX);
      FUCLaserAreaScannerCommand.SetOnSetRangeX(UCLaserAreaScannerCommandOnSetRangeX);
      FUCLaserAreaScannerCommand.SetOnGetRangeY(UCLaserAreaScannerCommandOnGetRangeY);
      FUCLaserAreaScannerCommand.SetOnSetRangeY(UCLaserAreaScannerCommandOnSetRangeY);
      FUCLaserAreaScannerCommand.SetOnGetDelayMotionms(UCLaserAreaScannerCommandOnGetDelayMotionms);
      FUCLaserAreaScannerCommand.SetOnSetDelayMotionms(UCLaserAreaScannerCommandOnSetDelayMotionms);
      FUCLaserAreaScannerCommand.SetOnGetPulseWidthus(UCLaserAreaScannerCommandOnGetPulseWidthus);
      FUCLaserAreaScannerCommand.SetOnSetPulseWidthus(UCLaserAreaScannerCommandOnSetPulseWidthus);
    }
    //
    private Boolean LoadInitdataUCLaserAreaCommand(CInitdataReader initdata)
    { //  LaserAreaScanner
      Int32 IValue = INIT_PROCESSCOUNT;
      Boolean Result = initdata.ReadInt32(NAME_PROCESSCOUNT, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetProcessCount((UInt32)IValue);
      FCommandList.Enqueue(new CSetProcessCount((UInt32)IValue));
      //
      IValue = INIT_PROCESSPERIODMS;
      Result &= initdata.ReadInt32(NAME_PROCESSPERIODMS, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetProcessPeriodms((UInt32)IValue);
      FCommandList.Enqueue(new CSetProcessPeriod((UInt32)IValue));
      //
      IValue = INIT_POSITIONXACTUAL;
      Result &= initdata.ReadInt32(NAME_POSITIONXACTUAL, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionXActual((UInt16)IValue);
      //
      IValue = INIT_POSITIONXMINIMUM;
      Result &= initdata.ReadInt32(NAME_POSITIONXMINIMUM, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionXMinimum((UInt16)IValue);
      //
      IValue = INIT_POSITIONXMAXIMUM;
      Result &= initdata.ReadInt32(NAME_POSITIONXMAXIMUM, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionXMaximum((UInt16)IValue);
      //
      IValue = INIT_POSITIONXDELTA;
      Result &= initdata.ReadInt32(NAME_POSITIONXDELTA, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionXDelta((UInt16)IValue);
      //
      IValue = INIT_POSITIONYACTUAL;
      Result &= initdata.ReadInt32(NAME_POSITIONYACTUAL, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionYActual((UInt16)IValue);
      //
      IValue = INIT_POSITIONYMINIMUM;
      Result &= initdata.ReadInt32(NAME_POSITIONYMINIMUM, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionYMinimum((UInt16)IValue);
      //
      IValue = INIT_POSITIONYMAXIMUM;
      Result &= initdata.ReadInt32(NAME_POSITIONYMAXIMUM, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionYMaximum((UInt16)IValue);
      //
      IValue = INIT_POSITIONYDELTA;
      Result &= initdata.ReadInt32(NAME_POSITIONYDELTA, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPositionYDelta((UInt16)IValue);
      //
      IValue = INIT_DELAYMOTIONMS;
      Result &= initdata.ReadInt32(NAME_DELAYMOTIONMS, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetDelayMotionms((UInt32)IValue);
      FCommandList.Enqueue(new CSetDelayMotion((UInt32)IValue));
      //
      IValue = INIT_PULSEWIDTHUS;
      Result &= initdata.ReadInt32(NAME_PULSEWIDTHUS, out IValue, IValue);
      FUCLaserAreaScannerCommand.SetPulseWidthus((UInt32)IValue);
      FCommandList.Enqueue(new CSetPulseWidthus((UInt32)IValue));
      //
      return Result;
    }
    //
    private Boolean SaveInitdataUCLaserAreaCommand(CInitdataWriter initdata)
    {
      //  LaserAreaScanner
      Boolean Result = initdata.WriteInt32(NAME_PROCESSCOUNT,
                                           (Int32)FUCLaserAreaScannerCommand.GetProcessCount());
      //
      Result &= initdata.WriteInt32(NAME_PROCESSPERIODMS,
                                    (Int32)FUCLaserAreaScannerCommand.GetProcessPeriodms());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONXACTUAL,
                                    FUCLaserAreaScannerCommand.GetPositionXActual());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONXMINIMUM,
                                    FUCLaserAreaScannerCommand.GetPositionXMinimum());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONXMAXIMUM,
                                    FUCLaserAreaScannerCommand.GetPositionXMaximum());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONXDELTA,
                                    FUCLaserAreaScannerCommand.GetPositionXDelta());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONYACTUAL,
                                    FUCLaserAreaScannerCommand.GetPositionYActual());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONYMINIMUM,
                                    FUCLaserAreaScannerCommand.GetPositionYMinimum());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONYMAXIMUM,
                                    FUCLaserAreaScannerCommand.GetPositionYMaximum());
      //
      Result &= initdata.WriteInt32(NAME_POSITIONYDELTA,
                                    FUCLaserAreaScannerCommand.GetPositionYDelta());
      //
      Result &= initdata.WriteInt32(NAME_DELAYMOTIONMS,
                                    (Int32)FUCLaserAreaScannerCommand.GetDelayMotionms());
      //
      Result &= initdata.WriteInt32(NAME_PULSEWIDTHUS,
                                    (Int32)FUCLaserAreaScannerCommand.GetPulseWidthus());
      //
      return Result;
    }

    //
    //###########################################################################################
    //  Segment - Analyse - Common
    //###########################################################################################
    private delegate void CBAnalyseGetHelp(CCommand command);//, String rxdline);
    private void AnalyseGetHelp(CCommand command)//, String rxdline)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetHelp CB = new CBAnalyseGetHelp(AnalyseGetHelp);
        Invoke(CB, new object[] { command });//, rxdline });
      }
      else
      {
        FResult = false;
        if (command is CGetHelp)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetHelp.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetHelp.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FUCLaserAreaScannerCommand.ClearMessages();
              FUCLaserAreaScannerCommand.AddMessageLine("");
              FRxdIndex = 0;
              FResult = true;
            }
          }
          if (1 <= Tokens.Length)
          {
            if (CDefine.SEPARATOR_NUMBER == Tokens[0][0])
            {
              FRxdIndex++;
              if (cbxDebug.Checked)
              {
                String Line = String.Format("HELP[{0}|{1}]<{2}>", FRxdIndex, FRxdCount, FRxdLine);
                FUCNotifier.Write(Line);
              }
              if (FRxdCount <= FRxdIndex)
              {
                command.SignalTextReceived();
                FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine);
              }
              else
              {
                FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine + "\r\n");
              }
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetProgramHeader(CCommand command);
    private void AnalyseGetProgramHeader(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProgramHeader CB = new CBAnalyseGetProgramHeader(AnalyseGetProgramHeader);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProgramHeader)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetProgramHeader.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProgramHeader.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FUCLaserAreaScannerCommand.ClearMessages();
              FUCLaserAreaScannerCommand.AddMessageLine("");
              FRxdIndex = 0;
              FResult = true;
            }
          }
          if (1 <= Tokens.Length)
          {
            if (CDefine.SEPARATOR_NUMBER == Tokens[0][0])
            {
              FRxdIndex++;
              if (cbxDebug.Checked)
              {
                String Line = String.Format("PROGRAMHEADER[{0}|{1}]<{2}>", FRxdIndex, FRxdCount, FRxdLine);
                FUCNotifier.Write(Line);
              }
              if (FRxdCount <= FRxdIndex)
              {
                command.SignalTextReceived();
                FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine);
              }
              else
              {
                FUCLaserAreaScannerCommand.AddMessageLine(FRxdLine + "\r\n");
              }
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetHardwareVersion(CCommand command);
    private void AnalyseGetHardwareVersion(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetHardwareVersion CB = new CBAnalyseGetHardwareVersion(AnalyseGetHardwareVersion);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetHardwareVersion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetHardwareVersion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetHardwareVersion.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FRxdIndex = 0;
              FResult = true;
            }
            else
              if (CDefine.TOKEN_NUMBER == Tokens[0])
            {
              FRxdIndex++;
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("HARDWAREVERSION[{0}]", Tokens[2]));
              }
              FUCLaserAreaScannerCommand.SetHardwareVersion(Tokens[2]);
              if (FRxdCount <= FRxdIndex) command.SignalTextReceived();
              FResult = true;
            }

          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetSoftwareVersion(CCommand command);
    private void AnalyseGetSoftwareVersion(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetSoftwareVersion CB = new CBAnalyseGetSoftwareVersion(AnalyseGetSoftwareVersion);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetSoftwareVersion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetSoftwareVersion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetSoftwareVersion.HEADER + " expected");
              }
              Int32.TryParse(Tokens[2], out FRxdCount);
              FRxdIndex = 0;
              FResult = true;
            }
            else
              if (CDefine.TOKEN_NUMBER == Tokens[0])
            {
              FRxdIndex++;
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SOFTWAREVERSION[{0}]", Tokens[2]));
              }
              FUCLaserAreaScannerCommand.SetSoftwareVersion(Tokens[2]);
              if (FRxdCount <= FRxdIndex) command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetProcessCount(CCommand command);
    private void AnalyseGetProcessCount(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProcessCount CB = new CBAnalyseGetProcessCount(AnalyseGetProcessCount);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProcessCount)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetProcessCount.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProcessCount.HEADER + " expected");
              }
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[2], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPROCESSCOUNT[{0}]", ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetProcessCount(CCommand command);
    private void AnalyseSetProcessCount(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetProcessCount CB = new CBAnalyseSetProcessCount(AnalyseSetProcessCount);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetProcessCount)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetProcessCount.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetProcessCount.HEADER + " expected");
              }
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[2], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPROCESSCOUNT[{0}]", ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetProcessPeriodms(CCommand command);
    private void AnalyseGetProcessPeriodms(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetProcessPeriodms CB = new CBAnalyseGetProcessPeriodms(AnalyseGetProcessPeriodms);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetProcessPeriod)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetProcessPeriod.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetProcessPeriod.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPROCESSPERIOD[{0}]", ProcessPeriod));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetProcessPeriodms(CCommand command);
    private void AnalyseSetProcessPeriodms(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetProcessPeriodms CB = new CBAnalyseSetProcessPeriodms(AnalyseSetProcessPeriodms);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetProcessPeriod)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetProcessPeriod.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetProcessPeriod.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPROCESSPERIOD[{0}]", ProcessPeriod));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseStopProcessExecution(CCommand command);
    private void AnalyseStopProcessExecution(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseStopProcessExecution CB = new CBAnalyseStopProcessExecution(AnalyseStopProcessExecution);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CStopProcessExecution)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CStopProcessExecution.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CStopProcessExecution.HEADER + " expected");
              }
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("STOPPROCESSEXECUTION");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LedSystem
    //###########################################################################################
    private delegate void CBAnalyseGetLedSystem(CCommand command);
    private void AnalyseGetLedSystem(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetLedSystem CB = new CBAnalyseGetLedSystem(AnalyseGetLedSystem);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetLedSystem)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetLedSystem.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetLedSystem.HEADER + " expected");
              }
              Int32 State;
              Int32.TryParse(Tokens[2], out State);
              FUCLaserAreaScannerCommand.SetStateLedSystem(State);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETSTATELEDSYSTEM[{0}]", State));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetLedSystemHigh(CCommand command);
    private void AnalyseSetLedSystemHigh(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetLedSystemHigh CB = new CBAnalyseSetLedSystemHigh(AnalyseSetLedSystemHigh);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetLedSystemHigh)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetLedSystemHigh.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetLedSystemHigh.HEADER + " expected");
              }
              FUCLaserAreaScannerCommand.SetStateLedSystem((int)EStateLed.On);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("SETLEDSYSTEMHIGH");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetLedSystemLow(CCommand command);
    private void AnalyseSetLedSystemLow(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetLedSystemLow CB = new CBAnalyseSetLedSystemLow(AnalyseSetLedSystemLow);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetLedSystemLow)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (2 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetLedSystemLow.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetLedSystemLow.HEADER + " expected");
              }
              FUCLaserAreaScannerCommand.SetStateLedSystem((int)EStateLed.Off);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write("SETLEDSYSTEMLOW");
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseBlinkLedSystem(CCommand command);
    private void AnalyseBlinkLedSystem(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseBlinkLedSystem CB = new CBAnalyseBlinkLedSystem(AnalyseBlinkLedSystem);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CBlinkLedSystem)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (4 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CBlinkLedSystem.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CBlinkLedSystem.HEADER + " expected");
              }
              UInt32 ProcessPeriod;
              UInt32.TryParse(Tokens[2], out ProcessPeriod);
              FUCLaserAreaScannerCommand.SetProcessPeriodms(ProcessPeriod);
              UInt32 ProcessCount;
              UInt32.TryParse(Tokens[3], out ProcessCount);
              FUCLaserAreaScannerCommand.SetProcessCount(ProcessCount);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("BLINKLEDSYSTEM[{0}|{1}]", ProcessPeriod, ProcessCount));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    //###########################################################################################
    //  Segment - Analyse - LaserRange
    //###########################################################################################
    private delegate void CBAnalyseGetPositionX(CCommand command);
    private void AnalyseGetPositionX(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetPositionX CB = new CBAnalyseGetPositionX(AnalyseGetPositionX);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetPositionX)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetPositionX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetPositionX.HEADER + " expected");
              }
              UInt16 Position;
              UInt16.TryParse(Tokens[2], out Position);
              FUCLaserAreaScannerCommand.SetPositionXActual(Position);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPOSITIONX[{0}]", Position));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetPositionX(CCommand command);
    private void AnalyseSetPositionX(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetPositionX CB = new CBAnalyseSetPositionX(AnalyseSetPositionX);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetPositionX)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetPositionX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetPositionX.HEADER + " expected");
              }
              UInt16 Position;
              UInt16.TryParse(Tokens[2], out Position);
              FUCLaserAreaScannerCommand.SetPositionXActual(Position);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPOSITIONX[{0}]", Position));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetPositionY(CCommand command);
    private void AnalyseGetPositionY(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetPositionY CB = new CBAnalyseGetPositionY(AnalyseGetPositionY);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetPositionY)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetPositionY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetPositionY.HEADER + " expected");
              }
              UInt16 Position;
              UInt16.TryParse(Tokens[2], out Position);
              FUCLaserAreaScannerCommand.SetPositionYActual(Position);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPOSITIONY[{0}]", Position));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetPositionY(CCommand command);
    private void AnalyseSetPositionY(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetPositionY CB = new CBAnalyseSetPositionY(AnalyseSetPositionY);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetPositionY)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetPositionY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetPositionY.HEADER + " expected");
              }
              UInt16 Position;
              UInt16.TryParse(Tokens[2], out Position);
              FUCLaserAreaScannerCommand.SetPositionYActual(Position);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPOSITIONY[{0}]", Position));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetRangeX(CCommand command);
    private void AnalyseGetRangeX(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetRangeX CB = new CBAnalyseGetRangeX(AnalyseGetRangeX);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetRangeX)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (5 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetRangeX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetRangeX.HEADER + " expected");
              }
              UInt16 Minimum, Maximum, Delta;
              UInt16.TryParse(Tokens[2], out Minimum);
              UInt16.TryParse(Tokens[3], out Maximum);
              UInt16.TryParse(Tokens[4], out Delta);
              FUCLaserAreaScannerCommand.SetPositionXMinimum(Minimum);
              FUCLaserAreaScannerCommand.SetPositionXMaximum(Maximum);
              FUCLaserAreaScannerCommand.SetPositionXDelta(Delta);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETRANGEX[{0}, {1}, {2}]",
                                                Minimum, Maximum, Delta));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetRangeX(CCommand command);
    private void AnalyseSetRangeX(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetRangeX CB = new CBAnalyseSetRangeX(AnalyseSetRangeX);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetRangeX)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (5 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetRangeX.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetRangeX.HEADER + " expected");
              }
              UInt16 Minimum, Maximum, Delta;
              UInt16.TryParse(Tokens[2], out Minimum);
              UInt16.TryParse(Tokens[3], out Maximum);
              UInt16.TryParse(Tokens[4], out Delta);
              FUCLaserAreaScannerCommand.SetPositionXMinimum(Minimum);
              FUCLaserAreaScannerCommand.SetPositionXMaximum(Maximum);
              FUCLaserAreaScannerCommand.SetPositionXDelta(Delta);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETRANGEX[{0}, {1}, {2}]",
                                                Minimum, Maximum, Delta));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetRangeY(CCommand command);
    private void AnalyseGetRangeY(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetRangeY CB = new CBAnalyseGetRangeY(AnalyseGetRangeY);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetRangeY)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (5 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetRangeY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetRangeY.HEADER + " expected");
              }
              UInt16 Minimum, Maximum, Delta;
              UInt16.TryParse(Tokens[2], out Minimum);
              UInt16.TryParse(Tokens[3], out Maximum);
              UInt16.TryParse(Tokens[4], out Delta);
              FUCLaserAreaScannerCommand.SetPositionYMinimum(Minimum);
              FUCLaserAreaScannerCommand.SetPositionYMaximum(Maximum);
              FUCLaserAreaScannerCommand.SetPositionYDelta(Delta);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETRANGEY[{0}, {1}, {2}]",
                                                Minimum, Maximum, Delta));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetRangeY(CCommand command);
    private void AnalyseSetRangeY(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetRangeY CB = new CBAnalyseSetRangeY(AnalyseSetRangeY);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetRangeY)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (5 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetRangeY.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetRangeY.HEADER + " expected");
              }
              UInt16 Minimum, Maximum, Delta;
              UInt16.TryParse(Tokens[2], out Minimum);
              UInt16.TryParse(Tokens[3], out Maximum);
              UInt16.TryParse(Tokens[4], out Delta);
              FUCLaserAreaScannerCommand.SetPositionYMinimum(Minimum);
              FUCLaserAreaScannerCommand.SetPositionYMaximum(Maximum);
              FUCLaserAreaScannerCommand.SetPositionYDelta(Delta);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETRANGEY[{0}, {1}, {2}]",
                                                Minimum, Maximum, Delta));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetDelayMotionms(CCommand command);
    private void AnalyseGetDelayMotionms(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetDelayMotionms CB = new CBAnalyseGetDelayMotionms(AnalyseGetDelayMotionms);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetDelayMotion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetDelayMotion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetDelayMotion.HEADER + " expected");
              }
              UInt32 Delay;
              UInt32.TryParse(Tokens[2], out Delay);
              FUCLaserAreaScannerCommand.SetDelayMotionms(Delay);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETDELAYMOTION[{0}]", Delay));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetDelayMotionms(CCommand command);
    private void AnalyseSetDelayMotionms(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetDelayMotionms CB = new CBAnalyseSetDelayMotionms(AnalyseSetDelayMotionms);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetDelayMotion)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetDelayMotion.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetDelayMotion.HEADER + " expected");
              }
              UInt32 Delay;
              UInt32.TryParse(Tokens[2], out Delay);
              FUCLaserAreaScannerCommand.SetDelayMotionms(Delay);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETDELAYMOTION[{0}]", Delay));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseGetPulseWidthus(CCommand command);
    private void AnalyseGetPulseWidthus(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseGetPulseWidthus CB = new CBAnalyseGetPulseWidthus(AnalyseGetPulseWidthus);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CGetPulseWidthus)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CGetPulseWidthus.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CGetPulseWidthus.HEADER + " expected");
              }
              UInt32 PW;
              UInt32.TryParse(Tokens[2], out PW);
              FUCLaserAreaScannerCommand.SetPulseWidthus(PW);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("GETPULSEWIDTHUS[{0}]", PW));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }
    //
    private delegate void CBAnalyseSetPulseWidthus(CCommand command);
    private void AnalyseSetPulseWidthus(CCommand command)
    {
      if (this.InvokeRequired)
      {
        CBAnalyseSetPulseWidthus CB = new CBAnalyseSetPulseWidthus(AnalyseSetPulseWidthus);
        Invoke(CB, new object[] { command });
      }
      else
      {
        FResult = false;
        if (command is CSetPulseWidthus)
        {
          Char[] Separators = new Char[] { CDefine.SEPARATOR_SPACE };
          String[] Tokens = FRxdLine.Split(Separators, StringSplitOptions.RemoveEmptyEntries);
          if (3 <= Tokens.Length)
          {
            if (CDefine.TOKEN_COLON == Tokens[0])
            {
              if (CSetPulseWidthus.HEADER != Tokens[1])
              {
                FUCNotifier.Error("Main", 1, CSetDelayMotion.HEADER + " expected");
              }
              UInt32 PW;
              UInt32.TryParse(Tokens[2], out PW);
              FUCLaserAreaScannerCommand.SetPulseWidthus(PW);
              if (cbxDebug.Checked)
              {
                FUCNotifier.Write(String.Format("SETPULSEWIDTHUS[{0}]", PW));
              }
              command.SignalTextReceived();
              FResult = true;
            }
          }
        }
      }
    }








    //
    //------------------------------------------------------------------------
    //  Section - Callback - UCLaserAreaScannerCommand
    //------------------------------------------------------------------------
    private delegate void CBUCLaserAreaScannerCommandOnGetHelp();
    private void UCLaserAreaScannerCommandOnGetHelp()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetHelp CB =
          new CBUCLaserAreaScannerCommandOnGetHelp(UCLaserAreaScannerCommandOnGetHelp);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetHelp());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetProgramHeader();
    private void UCLaserAreaScannerCommandOnGetProgramHeader()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetProgramHeader CB =
          new CBUCLaserAreaScannerCommandOnGetProgramHeader(UCLaserAreaScannerCommandOnGetProgramHeader);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProgramHeader());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetHardwareVersion();
    private void UCLaserAreaScannerCommandOnGetHardwareVersion()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetHardwareVersion CB =
          new CBUCLaserAreaScannerCommandOnGetHardwareVersion(UCLaserAreaScannerCommandOnGetHardwareVersion);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetHardwareVersion());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetSoftwareVersion();
    private void UCLaserAreaScannerCommandOnGetSoftwareVersion()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetSoftwareVersion CB =
          new CBUCLaserAreaScannerCommandOnGetSoftwareVersion(UCLaserAreaScannerCommandOnGetSoftwareVersion);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetSoftwareVersion());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetProcessCount();
    private void UCLaserAreaScannerCommandOnGetProcessCount()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetProcessCount CB =
          new CBUCLaserAreaScannerCommandOnGetProcessCount(UCLaserAreaScannerCommandOnGetProcessCount);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProcessCount());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetProcessCount(UInt32 count);
    private void UCLaserAreaScannerCommandOnSetProcessCount(UInt32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetProcessCount CB =
          new CBUCLaserAreaScannerCommandOnSetProcessCount(UCLaserAreaScannerCommandOnSetProcessCount);
        Invoke(CB, new object[] { count });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetProcessCount(count));
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetProcessPeriodms();
    private void UCLaserAreaScannerCommandOnGetProcessPeriodms()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetProcessPeriodms CB =
          new CBUCLaserAreaScannerCommandOnGetProcessPeriodms(UCLaserAreaScannerCommandOnGetProcessPeriodms);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetProcessPeriod());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetProcessPeriodms(UInt32 period);
    private void UCLaserAreaScannerCommandOnSetProcessPeriodms(UInt32 period)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetProcessPeriodms CB =
          new CBUCLaserAreaScannerCommandOnSetProcessPeriodms(UCLaserAreaScannerCommandOnSetProcessPeriodms);
        Invoke(CB, new object[] { period });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetProcessPeriod(period));
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnAbortProcessExecution();
    private void UCLaserAreaScannerCommandOnAbortProcessExecution()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnAbortProcessExecution CB =
          new CBUCLaserAreaScannerCommandOnAbortProcessExecution(UCLaserAreaScannerCommandOnAbortProcessExecution);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        //if (FProcessPulseStepLine is CProcessPulseStepList)
        //{
        //  FProcessPulseStepLine.Abort();
        //}
        //if (FProcessPulseStepTable is CProcessPulseStepList)
        //{
        //  FProcessPulseStepTable.Abort();
        //}
        FCommandList.Clear(); //!!!!!!!!!!!!! Only Clear with Abort!!!
        FCommandList.Enqueue(new CAbortProcessExecution());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetLedSystem();
    private void UCLaserAreaScannerCommandOnGetLedSystem()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetLedSystem CB =
          new CBUCLaserAreaScannerCommandOnGetLedSystem(UCLaserAreaScannerCommandOnGetLedSystem);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetLedSystem());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetLedSystemLow();
    private void UCLaserAreaScannerCommandOnSetLedSystemLow()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetLedSystemLow CB =
          new CBUCLaserAreaScannerCommandOnSetLedSystemLow(UCLaserAreaScannerCommandOnSetLedSystemLow);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetLedSystemLow());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetLedSystemHigh();
    private void UCLaserAreaScannerCommandOnSetLedSystemHigh()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetLedSystemHigh CB =
          new CBUCLaserAreaScannerCommandOnSetLedSystemHigh(UCLaserAreaScannerCommandOnSetLedSystemHigh);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetLedSystemHigh());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnBlinkLedSystem(UInt32 period, UInt32 count);
    private void UCLaserAreaScannerCommandOnBlinkLedSystem(UInt32 period, UInt32 count)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnBlinkLedSystem CB =
          new CBUCLaserAreaScannerCommandOnBlinkLedSystem(UCLaserAreaScannerCommandOnBlinkLedSystem);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CBlinkLedSystem(period, count));
        FCommandList.Execute();
      }
    }
    //
    //----------------------------------------------------------------------
    //  Section - Callback - LaserRange
    //----------------------------------------------------------------------
    private delegate void CBUCLaserAreaScannerCommandOnGetPositionX();
    private void UCLaserAreaScannerCommandOnGetPositionX()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetPositionX CB =
          new CBUCLaserAreaScannerCommandOnGetPositionX(UCLaserAreaScannerCommandOnGetPositionX);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPositionX());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetPositionX(UInt16 position);
    private void UCLaserAreaScannerCommandOnSetPositionX(UInt16 position)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetPositionX CB =
          new CBUCLaserAreaScannerCommandOnSetPositionX(UCLaserAreaScannerCommandOnSetPositionX);
        Invoke(CB, new object[] { position });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPositionX(position));
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetPositionY();
    private void UCLaserAreaScannerCommandOnGetPositionY()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetPositionY CB =
          new CBUCLaserAreaScannerCommandOnGetPositionY(UCLaserAreaScannerCommandOnGetPositionY);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPositionY());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetPositionY(UInt16 position);
    private void UCLaserAreaScannerCommandOnSetPositionY(UInt16 position)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetPositionY CB =
          new CBUCLaserAreaScannerCommandOnSetPositionY(UCLaserAreaScannerCommandOnSetPositionY);
        Invoke(CB, new object[] { position });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPositionY(position));
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetRangeX();
    private void UCLaserAreaScannerCommandOnGetRangeX()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetRangeX CB =
          new CBUCLaserAreaScannerCommandOnGetRangeX(UCLaserAreaScannerCommandOnGetRangeX);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetRangeX());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetRangeX(UInt16 minimum, UInt16 maximum, UInt16 delta);
    private void UCLaserAreaScannerCommandOnSetRangeX(UInt16 minimum, UInt16 maximum, UInt16 delta)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetRangeX CB =
          new CBUCLaserAreaScannerCommandOnSetRangeX(UCLaserAreaScannerCommandOnSetRangeX);
        Invoke(CB, new object[] { minimum, maximum, delta });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetRangeX(minimum, maximum, delta));
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetRangeY();
    private void UCLaserAreaScannerCommandOnGetRangeY()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetRangeY CB =
          new CBUCLaserAreaScannerCommandOnGetRangeY(UCLaserAreaScannerCommandOnGetRangeY);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetRangeY());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetRangeY(UInt16 minimum, UInt16 maximum, UInt16 delta);
    private void UCLaserAreaScannerCommandOnSetRangeY(UInt16 minimum, UInt16 maximum, UInt16 delta)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetRangeY CB =
          new CBUCLaserAreaScannerCommandOnSetRangeY(UCLaserAreaScannerCommandOnSetRangeY);
        Invoke(CB, new object[] { minimum, maximum, delta });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetRangeY(minimum, maximum, delta));
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetDelayMotionms();
    private void UCLaserAreaScannerCommandOnGetDelayMotionms()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetDelayMotionms CB =
          new CBUCLaserAreaScannerCommandOnGetDelayMotionms(UCLaserAreaScannerCommandOnGetDelayMotionms);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetDelayMotion());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetDelayMotionms(UInt32 delay);
    private void UCLaserAreaScannerCommandOnSetDelayMotionms(UInt32 delay)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetDelayMotionms CB =
          new CBUCLaserAreaScannerCommandOnSetDelayMotionms(UCLaserAreaScannerCommandOnSetDelayMotionms);
        Invoke(CB, new object[] { delay });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetDelayMotion(delay));
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnGetPulseWidthus();
    private void UCLaserAreaScannerCommandOnGetPulseWidthus()
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnGetPulseWidthus CB =
          new CBUCLaserAreaScannerCommandOnGetPulseWidthus(UCLaserAreaScannerCommandOnGetPulseWidthus);
        Invoke(CB, new object[] { });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CGetPulseWidthus());
        FCommandList.Execute();
      }
    }
    //
    private delegate void CBUCLaserAreaScannerCommandOnSetPulseWidthus(UInt32 pulsewidth);
    private void UCLaserAreaScannerCommandOnSetPulseWidthus(UInt32 pulsewidth)
    {
      if (this.InvokeRequired)
      {
        CBUCLaserAreaScannerCommandOnSetPulseWidthus CB =
          new CBUCLaserAreaScannerCommandOnSetPulseWidthus(UCLaserAreaScannerCommandOnSetPulseWidthus);
        Invoke(CB, new object[] { pulsewidth });
      }
      else
      { // Create / Execute Command
        FCommandList.Enqueue(new CSetPulseWidthus(pulsewidth));
        FCommandList.Execute();
      }
    }






  }
}