﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
//
namespace UCLaserAreaScanner
{ //
  //--------------------------------------------------------------------------
  //  Segment - Definition - UCLaserStepLine
  //--------------------------------------------------------------------------
  //
  public partial class CUCLaserStepLine : UserControl
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    // LaserRange
    private DOnGetPositionX FOnGetPositionX;
    private DOnSetPositionX FOnSetPositionX;
    private DOnGetPositionY FOnGetPositionY;
    private DOnSetPositionY FOnSetPositionY;
    private DOnGetRangeX FOnGetRangeX;
    private DOnSetRangeX FOnSetRangeX;
    private DOnGetRangeY FOnGetRangeY;
    private DOnSetRangeY FOnSetRangeY;
    private DOnGetDelayMotionms FOnGetDelayMotionms;
    private DOnSetDelayMotionms FOnSetDelayMotionms;
    private DOnGetProcessCount FOnGetProcessCount;
    private DOnSetProcessCount FOnSetProcessCount;
    private DOnGetProcessPeriodms FOnGetProcessPeriodms;
    private DOnSetProcessPeriodms FOnSetProcessPeriodms;
    //private DOnGetPulseCount FOnGetPulseCount;
    //private DOnSetPulseCount FOnSetPulseCount;
    //private DOnGetPulsePeriodms FOnGetPulsePeriodms;
    //private DOnSetPulsePeriodms FOnSetPulsePeriodms;
    private DOnGetPulseWidthus FOnGetPulseWidthus;
    private DOnSetPulseWidthus FOnSetPulseWidthus;
    // LaserLine
    private DOnPulseLaserRow FOnPulseLaserRow;
    private DOnAbortLaserRow FOnAbortLaserRow;
    private DOnPulseLaserCol FOnPulseLaserCol;
    private DOnAbortLaserCol FOnAbortLaserCol;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCLaserStepLine()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    // LaserRange
    public void SetOnGetProcessCount(DOnGetProcessCount value)
    {
      FOnGetProcessCount = value;
    }
    public void SetOnSetProcessCount(DOnSetProcessCount value)
    {
      FOnSetProcessCount = value;
    }
    public void SetOnGetProcessPeriodms(DOnGetProcessPeriodms value)
    {
      FOnGetProcessPeriodms = value;
    }
    public void SetOnSetProcessPeriodms(DOnSetProcessPeriodms value)
    {
      FOnSetProcessPeriodms = value;
    }
    public void SetOnGetPositionX(DOnGetPositionX value)
    {
      FOnGetPositionX = value;
    }
    public void SetOnSetPositionX(DOnSetPositionX value)
    {
      FOnSetPositionX = value;
    }
    public void SetOnGetRangeX(DOnGetRangeX value)
    {
      FOnGetRangeX = value;
    }
    public void SetOnSetRangeX(DOnSetRangeX value)
    {
      FOnSetRangeX = value;
    }
    public void SetOnGetPositionY(DOnGetPositionY value)
    {
      FOnGetPositionY = value;
    }
    public void SetOnSetPositionY(DOnSetPositionY value)
    {
      FOnSetPositionY = value;
    }
    public void SetOnGetRangeY(DOnGetRangeY value)
    {
      FOnGetRangeY = value;
    }
    public void SetOnSetRangeY(DOnSetRangeY value)
    {
      FOnSetRangeY = value;
    }
    public void SetOnGetDelayMotionms(DOnGetDelayMotionms value)
    {
      FOnGetDelayMotionms = value;
    }
    public void SetOnSetDelayMotionms(DOnSetDelayMotionms value)
    {
      FOnSetDelayMotionms = value;
    }
    public void SetOnGetPulseWidthus(DOnGetPulseWidthus value)
    {
      FOnGetPulseWidthus = value;
    }
    public void SetOnSetPulseWidthus(DOnSetPulseWidthus value)
    {
      FOnSetPulseWidthus = value;
    }
    // LaserLine
    public void SetOnPulseLaserRow(DOnPulseLaserRow value)
    {
      FOnPulseLaserRow = value;
    }
    public void SetOnAbortLaserRow(DOnAbortLaserRow value)
    {
      FOnAbortLaserRow = value;
    }
    public void SetOnPulseLaserCol(DOnPulseLaserCol value)
    {
      FOnPulseLaserCol = value;
    }
    public void SetOnAbortLaserCol(DOnAbortLaserCol value)
    {
      FOnAbortLaserCol = value;
    }


    public UInt16 GetPositionXActual()
    {
      return (UInt16)nudPositionXActual.Value;
    }
    public void SetPositionXActual(UInt16 positionx)
    {
      nudPositionXActual.Value = positionx;
    }
    public UInt16 GetPositionYActual()
    {
      return (UInt16)nudPositionYActual.Value;
    }
    public void SetPositionYActual(UInt16 positiony)
    {
      nudPositionYActual.Value = positiony;
    }
    public UInt16 GetPositionXMinimum()
    {
      return (UInt16)nudPositionXMinimum.Value;
    }
    public void SetPositionXMinimum(UInt16 positionx)
    {
      nudPositionXMinimum.Value = positionx;
    }
    public UInt16 GetPositionYMinimum()
    {
      return (UInt16)nudPositionYMinimum.Value;
    }
    public void SetPositionYMinimum(UInt16 positiony)
    {
      nudPositionYMinimum.Value = positiony;
    }
    public UInt16 GetPositionXMaximum()
    {
      return (UInt16)nudPositionXMaximum.Value;
    }
    public void SetPositionXMaximum(UInt16 positionx)
    {
      nudPositionXMaximum.Value = positionx;
    }
    public UInt16 GetPositionYMaximum()
    {
      return (UInt16)nudPositionYMaximum.Value;
    }
    public void SetPositionYMaximum(UInt16 positiony)
    {
      nudPositionYMaximum.Value = positiony;
    }
    public UInt16 GetPositionXDelta()
    {
      return (UInt16)nudPositionXDelta.Value;
    }
    public void SetPositionXDelta(UInt16 positionx)
    {
      nudPositionXDelta.Value = positionx;
    }
    public UInt16 GetPositionYDelta()
    {
      return (UInt16)nudPositionYDelta.Value;
    }
    public void SetPositionYDelta(UInt16 positiony)
    {
      nudPositionYDelta.Value = positiony;
    }

    public void SetProcessCount(UInt32 value)
    {
      nudPulseCount.Value = value;
    }

    public void SetProcessPeriodms(UInt32 value)
    {
      nudPulsePeriodms.Value = value;
    }

    public UInt32 GetDelayMotionms()
    {
      return (UInt32)nudDelayMotionms.Value;
    }
    public void SetDelayMotionms(UInt32 value)
    {
      nudDelayMotionms.Value = value;
    }

    public UInt32 GetPulseWidthus()
    {
      return (UInt32)nudPulseWidthus.Value;
    }
    public void SetPulseWidthus(UInt32 value)
    {
      nudPulseWidthus.Value = value;
    }


    public Boolean SkipZeroPulsesLine
    {
      get { return cbxSkipZeroPulsesLine.Checked; }
      set { cbxSkipZeroPulsesLine.Checked = value; }
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    private void btnGetPositionX_Click(object sender, EventArgs e)
    {
      if (FOnGetPositionX is DOnGetPositionX)
      {
        FOnGetPositionX();
      }
    }

    private void btnSetPositionX_Click(object sender, EventArgs e)
    {
      if (FOnSetPositionX is DOnSetPositionX)
      {
        UInt16 Position = (UInt16)nudPositionXActual.Value;
        FOnSetPositionX(Position);
      }
    }

    private void btnGetRangeX_Click(object sender, EventArgs e)
    {
      if (FOnGetRangeX is DOnGetRangeX)
      {
        FOnGetRangeX();
      }
    }

    private void btnSetRangeX_Click(object sender, EventArgs e)
    {
      if (FOnSetRangeX is DOnSetRangeX)
      {
        UInt16 Minimum = (UInt16)nudPositionXMinimum.Value;
        UInt16 Maximum = (UInt16)nudPositionXMaximum.Value;
        UInt16 Delta = (UInt16)nudPositionXDelta.Value;
        FOnSetRangeX(Minimum, Maximum, Delta);
      }
    }

    private void btnGetPositionY_Click(object sender, EventArgs e)
    {
      if (FOnGetPositionY is DOnGetPositionY)
      {
        FOnGetPositionY();
      }
    }

    private void btnSetPositionY_Click(object sender, EventArgs e)
    {
      if (FOnSetPositionY is DOnSetPositionY)
      {
        UInt16 Position = (UInt16)nudPositionYActual.Value;
        FOnSetPositionY(Position);
      }
    }

    private void btnGetRangeY_Click(object sender, EventArgs e)
    {
      if (FOnGetRangeY is DOnGetRangeY)
      {
        FOnGetRangeY();
      }
    }

    private void btnSetRangeY_Click(object sender, EventArgs e)
    {
      if (FOnSetRangeY is DOnSetRangeY)
      {
        UInt16 Minimum = (UInt16)nudPositionYMinimum.Value;
        UInt16 Maximum = (UInt16)nudPositionYMaximum.Value;
        UInt16 Delta = (UInt16)nudPositionYDelta.Value;
        FOnSetRangeY(Minimum, Maximum, Delta);
      }
    }

    private void btnGetDelayMotionms_Click(object sender, EventArgs e)
    {
      if (FOnGetDelayMotionms is DOnGetDelayMotionms)
      {
        FOnGetDelayMotionms();
      }
    }

    private void btnSetDelayMotionms_Click(object sender, EventArgs e)
    {
      if (FOnSetDelayMotionms is DOnSetDelayMotionms)
      {
        UInt32 Value = (UInt32)nudDelayMotionms.Value;
        FOnSetDelayMotionms(Value);
      }
    }

    private void btnGetPulseWidthus_Click(object sender, EventArgs e)
    {
      if (FOnGetPulseWidthus is DOnGetPulseWidthus)
      {
        FOnGetPulseWidthus();
      }
    }

    private void btnSetPulseWidthus_Click(object sender, EventArgs e)
    {
      if (FOnSetPulseWidthus is DOnSetPulseWidthus)
      {
        UInt32 Value = (UInt32)nudPulseWidthus.Value;
        FOnSetPulseWidthus(Value);
      }
    }

    private void btnGetPulseCount_Click(object sender, EventArgs e)
    {
      if (FOnGetProcessCount is DOnGetProcessCount)
      {
        FOnGetProcessCount();
      }
    }

    private void btnSetPulseCount_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessCount is DOnSetProcessCount)
      {
        UInt32 Value = (UInt32)nudPulseCount.Value;
        FOnSetProcessCount(Value);
      }
    }

    private void btnGetPulsePeriodms_Click(object sender, EventArgs e)
    {
      if (FOnGetProcessPeriodms is DOnGetProcessPeriodms)
      {
        FOnGetProcessPeriodms();
      }
    }

    private void btnSetPulsePeriodms_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessPeriodms is DOnSetProcessPeriodms)
      {
        UInt32 Value = (UInt32)nudPulsePeriodms.Value;
        FOnSetProcessPeriodms(Value);
      }
    }

    private void btnPulseLaserRow_Click(object sender, EventArgs e)
    {
      if (FOnPulseLaserRow is DOnPulseLaserRow)
      {
        UInt32 Period = (UInt32)nudPulsePeriodms.Value;
        UInt32 Count = (UInt32)nudPulseCount.Value;
        if (0 < Count)
        {
          FOnPulseLaserRow(Period, Count);
        }
      }
    }

    private void btnAbortLaserRow_Click(object sender, EventArgs e)
    {
      if (FOnAbortLaserRow is DOnAbortLaserRow)
      {
        FOnAbortLaserRow();
      }
    }

    private void btnPulseLaserCol_Click(object sender, EventArgs e)
    {
      if (FOnPulseLaserCol is DOnPulseLaserCol)
      {
        UInt32 Period = (UInt32)nudPulsePeriodms.Value;
        UInt32 Count = (UInt32)nudPulseCount.Value;
        if (0 < Count)
        {
          FOnPulseLaserCol(Period, Count);
        }
      }
    }

    private void btnAbortLaserCol_Click(object sender, EventArgs e)
    {
      if (FOnAbortLaserCol is DOnAbortLaserCol)
      {
        FOnAbortLaserCol();
      }
    }

  }
}
