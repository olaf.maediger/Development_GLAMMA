﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPLaserAreaScanner;
//
namespace UCLaserAreaScanner
{
  public partial class CUCLaserAreaScannerMatrix : UserControl
  { //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    // 
    private DOnLoadLaserSteptable FOnLoadLaserSteptable;
    private DOnPulseSteptable FOnPulseSteptable;
    private DOnAbortSteptable FOnAbortSteptable;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //

    public CUCLaserAreaScannerMatrix()
    {
      InitializeComponent();
    }



    // LaserImage
    public void SetOnLoadLaserSteptable(DOnLoadLaserSteptable value)
    {
      FOnLoadLaserSteptable = value;
    }
    public void SetOnPulseSteptable(DOnPulseSteptable value)
    {
      FOnPulseSteptable = value;
    }
    public void SetOnAbortSteptable(DOnAbortSteptable value)
    {
      FOnAbortSteptable = value;
    }

    
    public void SetLaserPoints(UInt32 rowcount, UInt32 colcount, UInt16[,] laserpoints,
                               UInt16 pulsecountminimum, UInt16 pulsecountmaximum)
    {
      if (laserpoints is UInt16[,])
      {
        Int32 Size = laserpoints.Length;
        if (0 < Size)
        {
          //////////////////////// !!!!!!!!!!!!!!!!!!!!!!! FUCLaserStepMatrix.SetLaserPoints(rowcount, colcount, laserpoints, 
          ////////////////////////                                  pulsecountminimum, pulsecountmaximum);
          FUCLaserStepMatrix.SetScaleFactor((UInt16)nudScaleFactor.Value);
        }
      }
    }


    public void SelectLaserStep(UInt32 index)
    {
      FUCLaserStepMatrix.SelectLaserStep(index);
    }






    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void cbxZoom_CheckedChanged(object sender, EventArgs e)
    {
      FUCLaserStepMatrix.SetZoom(cbxZoom.Checked);
    }

    Int32 GetScaleFactor()
    {
      return (Int32)nudScaleFactor.Value;
    }
    void SetScaleFactor(Int32 value)
    {
      FUCLaserStepMatrix.SetScaleFactor((UInt16)value);
    }















    private void btnLoadLaserSteptable_Click(object sender, EventArgs e)
    {
      if (FOnLoadLaserSteptable is DOnLoadLaserSteptable)
      {
        if (DialogResult.OK == DialogLoadLaserSteptable.ShowDialog())
        {
          FOnLoadLaserSteptable(DialogLoadLaserSteptable.FileName);
        }
      }
    }

    private void btnPulseSteptable_Click(object sender, EventArgs e)
    {
      if (FOnPulseSteptable is DOnPulseSteptable)
      {
        FOnPulseSteptable();
      }
    }

    private void btnAbortSteptable_Click(object sender, EventArgs e)
    {
      if (FOnAbortSteptable is DOnAbortSteptable)
      {
        FOnAbortSteptable();
      }
    }

    private void nudScaleFactor_ValueChanged(object sender, EventArgs e)
    {
      FUCLaserStepMatrix.SetScaleFactor((UInt16)nudScaleFactor.Value);
    }
  }
}
