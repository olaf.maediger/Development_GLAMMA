﻿namespace UCTemperature
{
  partial class CUCTemperatureDisplay
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxMatrix = new System.Windows.Forms.PictureBox();
      this.lblHeader = new System.Windows.Forms.Label();
      this.FUCTemperatureSegment7 = new UCTemperature.CUCTemperatureSegment();
      this.FUCTemperatureSegment6 = new UCTemperature.CUCTemperatureSegment();
      this.FUCTemperatureSegment5 = new UCTemperature.CUCTemperatureSegment();
      this.FUCTemperatureSegment4 = new UCTemperature.CUCTemperatureSegment();
      this.FUCTemperatureSegment3 = new UCTemperature.CUCTemperatureSegment();
      this.FUCTemperatureSegment2 = new UCTemperature.CUCTemperatureSegment();
      this.FUCTemperatureSegment1 = new UCTemperature.CUCTemperatureSegment();
      this.FUCTemperatureSegment0 = new UCTemperature.CUCTemperatureSegment();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix)).BeginInit();
      this.SuspendLayout();
      // 
      // pbxMatrix
      // 
      this.pbxMatrix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxMatrix.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxMatrix.Location = new System.Drawing.Point(0, 0);
      this.pbxMatrix.Name = "pbxMatrix";
      this.pbxMatrix.Size = new System.Drawing.Size(742, 384);
      this.pbxMatrix.TabIndex = 0;
      this.pbxMatrix.TabStop = false;
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(742, 23);
      this.lblHeader.TabIndex = 1;
      this.lblHeader.Text = "Temperature Display";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // FUCTemperatureSegment7
      // 
      this.FUCTemperatureSegment7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCTemperatureSegment7.Header = "T7";
      this.FUCTemperatureSegment7.Location = new System.Drawing.Point(376, 296);
      this.FUCTemperatureSegment7.Name = "FUCTemperatureSegment7";
      this.FUCTemperatureSegment7.Size = new System.Drawing.Size(354, 76);
      this.FUCTemperatureSegment7.TabIndex = 111;
      this.FUCTemperatureSegment7.Unit = "°C";
      // 
      // FUCTemperatureSegment6
      // 
      this.FUCTemperatureSegment6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCTemperatureSegment6.Header = "T6";
      this.FUCTemperatureSegment6.Location = new System.Drawing.Point(12, 296);
      this.FUCTemperatureSegment6.Name = "FUCTemperatureSegment6";
      this.FUCTemperatureSegment6.Size = new System.Drawing.Size(354, 76);
      this.FUCTemperatureSegment6.TabIndex = 110;
      this.FUCTemperatureSegment6.Unit = "°C";
      // 
      // FUCTemperatureSegment5
      // 
      this.FUCTemperatureSegment5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCTemperatureSegment5.Header = "T5";
      this.FUCTemperatureSegment5.Location = new System.Drawing.Point(376, 209);
      this.FUCTemperatureSegment5.Name = "FUCTemperatureSegment5";
      this.FUCTemperatureSegment5.Size = new System.Drawing.Size(354, 76);
      this.FUCTemperatureSegment5.TabIndex = 109;
      this.FUCTemperatureSegment5.Unit = "°C";
      // 
      // FUCTemperatureSegment4
      // 
      this.FUCTemperatureSegment4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCTemperatureSegment4.Header = "T4";
      this.FUCTemperatureSegment4.Location = new System.Drawing.Point(12, 209);
      this.FUCTemperatureSegment4.Name = "FUCTemperatureSegment4";
      this.FUCTemperatureSegment4.Size = new System.Drawing.Size(354, 76);
      this.FUCTemperatureSegment4.TabIndex = 108;
      this.FUCTemperatureSegment4.Unit = "°C";
      // 
      // FUCTemperatureSegment3
      // 
      this.FUCTemperatureSegment3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCTemperatureSegment3.Header = "T3";
      this.FUCTemperatureSegment3.Location = new System.Drawing.Point(376, 122);
      this.FUCTemperatureSegment3.Name = "FUCTemperatureSegment3";
      this.FUCTemperatureSegment3.Size = new System.Drawing.Size(354, 76);
      this.FUCTemperatureSegment3.TabIndex = 107;
      this.FUCTemperatureSegment3.Unit = "°C";
      // 
      // FUCTemperatureSegment2
      // 
      this.FUCTemperatureSegment2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCTemperatureSegment2.Header = "T2";
      this.FUCTemperatureSegment2.Location = new System.Drawing.Point(12, 122);
      this.FUCTemperatureSegment2.Name = "FUCTemperatureSegment2";
      this.FUCTemperatureSegment2.Size = new System.Drawing.Size(354, 76);
      this.FUCTemperatureSegment2.TabIndex = 106;
      this.FUCTemperatureSegment2.Unit = "°C";
      // 
      // FUCTemperatureSegment1
      // 
      this.FUCTemperatureSegment1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCTemperatureSegment1.Header = "T1";
      this.FUCTemperatureSegment1.Location = new System.Drawing.Point(376, 34);
      this.FUCTemperatureSegment1.Name = "FUCTemperatureSegment1";
      this.FUCTemperatureSegment1.Size = new System.Drawing.Size(354, 76);
      this.FUCTemperatureSegment1.TabIndex = 105;
      this.FUCTemperatureSegment1.Unit = "°C";
      // 
      // FUCTemperatureSegment0
      // 
      this.FUCTemperatureSegment0.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.FUCTemperatureSegment0.Header = "T0";
      this.FUCTemperatureSegment0.Location = new System.Drawing.Point(12, 34);
      this.FUCTemperatureSegment0.Name = "FUCTemperatureSegment0";
      this.FUCTemperatureSegment0.Size = new System.Drawing.Size(354, 76);
      this.FUCTemperatureSegment0.TabIndex = 104;
      this.FUCTemperatureSegment0.Unit = "°C";
      // 
      // CUCTemperatureDisplay
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.FUCTemperatureSegment7);
      this.Controls.Add(this.FUCTemperatureSegment6);
      this.Controls.Add(this.FUCTemperatureSegment5);
      this.Controls.Add(this.FUCTemperatureSegment4);
      this.Controls.Add(this.FUCTemperatureSegment3);
      this.Controls.Add(this.FUCTemperatureSegment2);
      this.Controls.Add(this.FUCTemperatureSegment1);
      this.Controls.Add(this.FUCTemperatureSegment0);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pbxMatrix);
      this.Name = "CUCTemperatureDisplay";
      this.Size = new System.Drawing.Size(742, 384);
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxMatrix;
    private System.Windows.Forms.Label lblHeader;
    private CUCTemperatureSegment FUCTemperatureSegment0;
    private CUCTemperatureSegment FUCTemperatureSegment1;
    private CUCTemperatureSegment FUCTemperatureSegment3;
    private CUCTemperatureSegment FUCTemperatureSegment2;
    private CUCTemperatureSegment FUCTemperatureSegment5;
    private CUCTemperatureSegment FUCTemperatureSegment4;
    private CUCTemperatureSegment FUCTemperatureSegment7;
    private CUCTemperatureSegment FUCTemperatureSegment6;
  }
}
