﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPMultiTemperature;
//
namespace UCTemperature
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - UCTemperature
  //--------------------------------------------------------------------------
  //
  public partial class CUCTemperatureCommand : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private DOnGetHelp FOnGetHelp;
    private DOnGetProgramHeader FOnGetProgramHeader;
    private DOnGetHardwareVersion FOnGetHardwareVersion;
    private DOnGetSoftwareVersion FOnGetSoftwareVersion;
    private DOnGetLedSystem FOnGetLedSystem;
    private DOnSwitchLedSystemOn FOnSwitchLedSystemOn;
    private DOnSwitchLedSystemOff FOnSwitchLedSystemOff;
    private DOnBlinkLedSystem FOnBlinkLedSystem;
    private DOnSetProcessCount FOnSetProcessCount;
    private DOnSetProcessPeriod FOnSetProcessPeriod;
    private DOnStopProcessExecution FOnStopProcessExecution;
    private DOnGetTemperatureChannel FOnGetTemperatureChannel;
    private DOnGetTemperatureInterval FOnGetTemperatureInterval;
    private DOnGetAllTemperatures FOnGetAllTemperatures;
    private DOnRepeatTemperatureChannel FOnRepeatTemperatureChannel;
    private DOnRepeatTemperatureInterval FOnRepeatTemperatureInterval;
    private DOnRepeatAllTemperatures FOnRepeatAllTemperatures;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCTemperatureCommand()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetOnGetHelp(DOnGetHelp value)
    {
      FOnGetHelp = value;
    }
    public void SetOnGetProgramHeader(DOnGetProgramHeader value)
    {
      FOnGetProgramHeader = value;
    }
    public void SetOnGetHardwareVersion(DOnGetHardwareVersion value)
    {
      FOnGetHardwareVersion = value;
    }
    public void SetHardwareVersion(String value)
    {
      tbxHardwareVersion.Text = value;
    }
    public void SetOnGetSoftwareVersion(DOnGetSoftwareVersion value)
    {
      FOnGetSoftwareVersion = value;
    }
    public void SetSoftwareVersion(String value)
    {
      tbxSoftwareVersion.Text = value;
    }
    public void SetOnGetLedSystem(DOnGetLedSystem value)
    {
      FOnGetLedSystem = value;
    }
    public void SetOnSwitchLedSystemOn(DOnSwitchLedSystemOn value)
    {
      FOnSwitchLedSystemOn = value;
    }
    public void SetOnSwitchLedSystemOff(DOnSwitchLedSystemOff value)
    {
      FOnSwitchLedSystemOff = value;
    }
    public void SetOnBlinkLedSystem(DOnBlinkLedSystem value)
    {
      FOnBlinkLedSystem = value;
    }
    public void SetOnSetProcessCount(DOnSetProcessCount value)
    {
      FOnSetProcessCount = value;
    }
    public void SetOnSetProcessPeriod(DOnSetProcessPeriod value)
    {
      FOnSetProcessPeriod = value;
    }
    public void SetOnStopProcessExecution(DOnStopProcessExecution value)
    {
      FOnStopProcessExecution = value;
    }
    public void SetOnGetTemperatureChannel(DOnGetTemperatureChannel value)
    {
      FOnGetTemperatureChannel = value;
    }
    public void SetOnGetTemperatureInterval(DOnGetTemperatureInterval value)
    {
      FOnGetTemperatureInterval = value;
    }
    public void SetOnGetAllTemperatures(DOnGetAllTemperatures value)
    {
      FOnGetAllTemperatures = value;
    }
    public void SetOnRepeatTemperatureChannel(DOnRepeatTemperatureChannel value)
    {
      FOnRepeatTemperatureChannel = value;
    }
    public void SetOnRepeatTemperatureInterval(DOnRepeatTemperatureInterval value)
    {
      FOnRepeatTemperatureInterval = value;
    }
    public void SetOnRepeatAllTemperatures(DOnRepeatAllTemperatures value)
    {
      FOnRepeatAllTemperatures = value;
    }

    public void SetState(String[] tokens)
    {
      Int32 TL = tokens.Length;
      if (4 <= TL)
      {
        if ((":" == tokens[0]) && (CGetStateMultiTemperatureController.HEADER == tokens[1]))
        {
          Int32 SMTC;
          Int32.TryParse(tokens[2], out SMTC);
          Int32 SubState;
          Int32.TryParse(tokens[3], out SubState);
          lblStateMTCIndex.Text = String.Format("{0} | {1}", SMTC, SubState);
          switch ((EStateProcess)SMTC)
          { // Common
            case EStateProcess.Idle:
              lblStateMTControllerText.Text = "Idle";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            case EStateProcess.Welcome:
              lblStateMTControllerText.Text = "Welcome";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            case EStateProcess.GetHelp:
              lblStateMTControllerText.Text = "GetHelp";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetProgramHeader:
              lblStateMTControllerText.Text = "GetProgramHeader";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetSoftwareVersion:
              lblStateMTControllerText.Text = "GetSoftwareVersion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetHardwareVersion:
              lblStateMTControllerText.Text = "GetHardwareVersion";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetProcessCount:
              lblStateMTControllerText.Text = "SetProcessCount";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.SetProcessPeriod:
              lblStateMTControllerText.Text = "SetProcessPeriod";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.StopProcessExecution:
              lblStateMTControllerText.Text = "StopProcessExecution";
              lblStateMTControllerText.BackColor = Color.PeachPuff;
              break;
            // Measurement - LedSystem
            case EStateProcess.GetLedSystem:
              lblStateMTControllerText.Text = "GetLedSystem";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.LedSystemOn:
              lblStateMTControllerText.Text = "LedSystemOn";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.LedSystemOff:
              lblStateMTControllerText.Text = "LedSystemOff";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.BlinkLedSystem:
              lblStateMTControllerText.Text = "BlinkLedSystem";
              lblStateMTControllerText.BackColor = Color.LightPink;
              SetStateLedSystem(SubState);
              break;
            // Measurement - Temperature
            case EStateProcess.GetTemperatureChannel:
              lblStateMTControllerText.Text = "GetTemperatureChannel";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetTemperatureInterval:
              lblStateMTControllerText.Text = "GetTemperatureInterval";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.GetAllTemperatures:
              lblStateMTControllerText.Text = "GetAllTemperatures";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.RepeatTemperatureChannel:
              lblStateMTControllerText.Text = "RepeatTemperatureChannel";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.RepeatTemperatureInterval:
              lblStateMTControllerText.Text = "RepeatTemperatureInterval";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            case EStateProcess.RepeatAllTemperatures:
              lblStateMTControllerText.Text = "RepeatAllTemperatures";
              lblStateMTControllerText.BackColor = Color.LightPink;
              break;
            default: // saUndefined
              lblStateMTControllerText.Text = "???Undefined???";
              lblStateMTControllerText.BackColor = Color.Red;
              break;
          }
        }
      }
      else
      {
        lblStateMTCIndex.Text = "???";
      }
    }

    public void SetProcessCount(Int32 value)
    {
      nudProcessCount.Value = value;
    }

    public void SetProcessPeriod(Int32 value)
    {
      nudProcessPeriod.Value = value;
    }

    public void SetStateLedSystem(Int32 state)
    {
      switch (state)
      {
        case 0: // EStateLed.Off:
          pnlStateLedSystem.BackColor = Color.MediumBlue;
          break;
        case 1: //EStateLed.On:
          pnlStateLedSystem.BackColor = Color.GreenYellow;
          break;
        default: // EStateLed.Undefined:
          pnlStateLedSystem.BackColor = Color.Fuchsia;
          break;
      }
    }

    public void SetChannelLow(Int32 value)
    {
      nudChannelLow.Value = value;
    }

    public void SetChannelHigh(Int32 value)
    {
      nudChannelHigh.Value = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
    public void ClearMessages()
    {
      tbxMessages.Text = "";
    }

    public void AddMessageLine(String line)
    {
      tbxMessages.Text += line;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //
    private void btnGetHelp_Click(object sender, EventArgs e)
    {
      if (FOnGetHelp is DOnGetHelp)
      {
        FOnGetHelp();
      }
    }

    private void btnGetProgramHeader_Click(object sender, EventArgs e)
    {
      if (FOnGetProgramHeader is DOnGetProgramHeader)
      {
        FOnGetProgramHeader();
      }
    }

    private void btnGetHardwareVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetHardwareVersion is DOnGetHardwareVersion)
      {
        FOnGetHardwareVersion();
      }
    }

    private void btnGetSoftwareVersion_Click(object sender, EventArgs e)
    {
      if (FOnGetSoftwareVersion is DOnGetSoftwareVersion)
      {
        FOnGetSoftwareVersion();
      }
    }

    private void btnGetLedSystem_Click(object sender, EventArgs e)
    {
      if (FOnGetLedSystem is DOnGetLedSystem)
      {
        FOnGetLedSystem();
      }
    }

    private void btnSwitchLedSystemOn_Click(object sender, EventArgs e)
    {
      if (FOnSwitchLedSystemOn is DOnSwitchLedSystemOn)
      {
        FOnSwitchLedSystemOn();
      }

    }

    private void btnSwitchLedSystemOff_Click(object sender, EventArgs e)
    {
      if (FOnSwitchLedSystemOff is DOnSwitchLedSystemOff)
      {
        FOnSwitchLedSystemOff();
      }

    }

    private void btnBlinkLedSystem_Click(object sender, EventArgs e)
    {
      if (FOnBlinkLedSystem is DOnBlinkLedSystem)
      {
        Int32 Period = (Int32)nudProcessPeriod.Value;
        Int32 Count = (Int32)nudProcessCount.Value;
        FOnBlinkLedSystem(Period, Count);
      }

    }

    private void btnSetProcessCount_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessCount is DOnSetProcessCount)
      {
        Int32 Count = (Int32)nudProcessCount.Value;
        FOnSetProcessCount(Count);
      }

    }

    private void btnSetProcessPeriod_Click(object sender, EventArgs e)
    {
      if (FOnSetProcessPeriod is DOnSetProcessPeriod)
      {
        Int32 Period = (Int32)nudProcessPeriod.Value;
        FOnSetProcessPeriod(Period);
      }

    }

    private void btnStopProcessExecution_Click(object sender, EventArgs e)
    {
      if (FOnStopProcessExecution is DOnStopProcessExecution)
      {
        FOnStopProcessExecution();
      }

    }

    private void btnGetTemperatureChannel_Click(object sender, EventArgs e)
    {
      if (FOnGetTemperatureChannel is DOnGetTemperatureChannel)
      {
        Int32 Channel = (Int32)nudChannelLow.Value;
        FOnGetTemperatureChannel(Channel);
      }

    }

    private void btnGetTemperatureInterval_Click(object sender, EventArgs e)
    {
      if (FOnGetTemperatureInterval is DOnGetTemperatureInterval)
      {
        Int32 ChannelLow = (Int32)nudChannelLow.Value;
        Int32 ChannelHigh = (Int32)nudChannelHigh.Value;
        FOnGetTemperatureInterval(ChannelLow, ChannelHigh);
      }

    }

    private void btnGetAllTemperatures_Click(object sender, EventArgs e)
    {
      if (FOnGetAllTemperatures is DOnGetAllTemperatures)
      {
        FOnGetAllTemperatures();
      }

    }

    private void btnRepeatTemperatureChannel_Click(object sender, EventArgs e)
    {
      if (FOnRepeatTemperatureChannel is DOnRepeatTemperatureChannel)
      {
        Int32 Channel = (Int32)nudChannelLow.Value;
        FOnRepeatTemperatureChannel(Channel);
      }

    }

    private void btnRepeatTemperatureInterval_Click(object sender, EventArgs e)
    {
      if (FOnRepeatTemperatureInterval is DOnRepeatTemperatureInterval)
      {
        Int32 ChannelLow = (Int32)nudChannelLow.Value;
        Int32 ChannelHigh = (Int32)nudChannelHigh.Value;
        FOnRepeatTemperatureInterval(ChannelLow, ChannelHigh);
      }

    }

    private void btnRepeatAllTemperatures_Click(object sender, EventArgs e)
    {
      if (FOnRepeatAllTemperatures is DOnRepeatAllTemperatures)
      {
        FOnRepeatAllTemperatures();
      }

    }


  }
}
