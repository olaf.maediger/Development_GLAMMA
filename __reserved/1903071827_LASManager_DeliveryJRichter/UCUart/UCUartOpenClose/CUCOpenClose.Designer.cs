﻿namespace UCUartOpenClose
{
  partial class CUCOpenClose
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.cbxOpenClose = new System.Windows.Forms.CheckBox();
      this.tmrAutoOpen = new System.Windows.Forms.Timer(this.components);
      this.SuspendLayout();
      // 
      // cbxOpenClose
      // 
      this.cbxOpenClose.Appearance = System.Windows.Forms.Appearance.Button;
      this.cbxOpenClose.Dock = System.Windows.Forms.DockStyle.Fill;
      this.cbxOpenClose.Location = new System.Drawing.Point(0, 0);
      this.cbxOpenClose.Name = "cbxOpenClose";
      this.cbxOpenClose.Size = new System.Drawing.Size(88, 35);
      this.cbxOpenClose.TabIndex = 0;
      this.cbxOpenClose.Text = "Open/Close";
      this.cbxOpenClose.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      this.cbxOpenClose.UseVisualStyleBackColor = true;
      this.cbxOpenClose.CheckedChanged += new System.EventHandler(this.cbxOpenClose_CheckedChanged);
      // 
      // tmrAutoOpen
      // 
      this.tmrAutoOpen.Interval = 1000;
      this.tmrAutoOpen.Tick += new System.EventHandler(this.tmrAutoOpen_Tick);
      // 
      // CUCOpenClose
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.cbxOpenClose);
      this.Name = "CUCOpenClose";
      this.Size = new System.Drawing.Size(88, 35);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.CheckBox cbxOpenClose;
    private System.Windows.Forms.Timer tmrAutoOpen;

  }
}
