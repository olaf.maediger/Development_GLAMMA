﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using UCNotifier;
using Uart;
//
namespace UCUartOpenClose
{
  public delegate void DOnUCOpenCloseChanged(Boolean opened);
  //
  public partial class CUCOpenClose : UserControl
  {
    private const String TEXT_OPEN = "Open";
    private const String TEXT_CLOSE = "Close";
    //
    //------------------------------------------------------------------------
    //  Section - Field
    //------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    private DOnUCOpenCloseChanged FOnOpenCloseChanged;

    public CUCOpenClose()
    {
      InitializeComponent();
      cbxOpenClose.Checked = false;
      cbxOpenClose.Text = TEXT_OPEN;
      //
      tmrAutoOpen.Interval = 1000;
      tmrAutoOpen.Enabled = true;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnUCOpenCloseChanged(DOnUCOpenCloseChanged value)
    {
      FOnOpenCloseChanged = value;
    }
    //
    //------------------------------------------------------------------------
    //  Section - Event
    //------------------------------------------------------------------------
    //
    private void tmrAutoOpen_Tick(object sender, EventArgs e)
    {
      tmrAutoOpen.Enabled = false;
      Close();
      Open();
    }

    private void cbxOpenClose_CheckedChanged(object sender, EventArgs e)
    {
      if (FOnOpenCloseChanged is DOnUCOpenCloseChanged)
      {
        FOnOpenCloseChanged(cbxOpenClose.Checked);
      }
      if (cbxOpenClose.Checked)
      { // opened -> "CLOSE"
        cbxOpenClose.Text = TEXT_CLOSE;
      }
      else
      { // closed -> "OPEN"
        cbxOpenClose.Text = TEXT_OPEN;
      }
    }

    public Boolean Open()
    {
      if (!cbxOpenClose.Checked)
      {
        cbxOpenClose.Checked = true;
        return true;
      }
      return false;
    }

    public Boolean Close()
    {
      if (cbxOpenClose.Checked)
      {
        cbxOpenClose.Checked = false;
        return true;
      }
      return false;
    }

    public void SetStateOpened(Boolean value)
    {
      cbxOpenClose.CheckedChanged -= cbxOpenClose_CheckedChanged;
      cbxOpenClose.Checked = value;
      if (cbxOpenClose.Checked)
      { // opened -> "CLOSE"
        cbxOpenClose.Text = TEXT_CLOSE;
      }
      else
      { // closed -> "OPEN"
        cbxOpenClose.Text = TEXT_OPEN;
      }
      cbxOpenClose.CheckedChanged += cbxOpenClose_CheckedChanged;
    }

  }
}
