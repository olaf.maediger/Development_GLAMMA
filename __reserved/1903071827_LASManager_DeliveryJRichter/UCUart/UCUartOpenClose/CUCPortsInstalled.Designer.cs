﻿namespace UCUartOpenClose
{
  partial class CUCPortsInstalled
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxPortsInstalled = new System.Windows.Forms.ComboBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label5 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // cbxPortsInstalled
      // 
      this.cbxPortsInstalled.Dock = System.Windows.Forms.DockStyle.Right;
      this.cbxPortsInstalled.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxPortsInstalled.FormattingEnabled = true;
      this.cbxPortsInstalled.Location = new System.Drawing.Point(73, 0);
      this.cbxPortsInstalled.Name = "cbxPortsInstalled";
      this.cbxPortsInstalled.Size = new System.Drawing.Size(61, 21);
      this.cbxPortsInstalled.TabIndex = 58;
      // 
      // panel1
      // 
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(73, 4);
      this.panel1.TabIndex = 59;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label5.Location = new System.Drawing.Point(0, 4);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(73, 21);
      this.label5.TabIndex = 62;
      this.label5.Text = "Ports Installed";
      // 
      // CUCPortsInstalled
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label5);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.cbxPortsInstalled);
      this.Name = "CUCPortsInstalled";
      this.Size = new System.Drawing.Size(134, 25);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ComboBox cbxPortsInstalled;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label5;

  }
}
