﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;

namespace CPLaserAreaScanner
{
  public enum EStateProcess
  { // Common
    Undefined = -1,
    Idle = 0,
    Welcome = 1,
    GetHelp = 2,
    GetProgramHeader = 3,
    GetSoftwareVersion = 4,
    GetHardwareVersion = 5,
    GetProcessCount = 6,
    SetProcessCount = 7,
    GetProcessPeriod = 8,
    SetProcessPeriod = 9,
    StopProcessExecution = 10,
    // LedSystem
    GetLedSystem = 11,
    LedSystemOn = 12,
    LedSystemOff = 13,
    BlinkLedSystem = 14,
    // LaserRange
    GetPositionX = 15,
    SetPositionX = 16,
    GetPositionY = 17,
    SetPositionY = 18,
    GetRangeX = 19,
    SetRangeX = 20,
    GetRangeY = 21,
    SetRangeY = 22,
    GetDelayMotion = 23,
    SetDelayMotion = 24,
    GetPulseWidth = 25,
    SetPulseWidth = 26,
    // LaserPosition
    PulseLaserPosition = 27,
    AbortLaserPosition = 28,
    // LaserLine
    PulseLaserRow = 29,
    AbortLaserRow = 30,
    PulseLaserCol = 31,
    AbortLaserCol = 32,
    // LaserMatrix
    PulseLaserMatrix = 33,
    AbortLaserMatrix = 34,
    // LaserImage
    LoadImage = 35,
    PulseSteptable = 36,
    AbortSteptable = 37
  };

  public enum EStateLed
  {
    Undefined = -1,
    Off = 0,
    On = 1
  };

  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - Callback
  //--------------------------------------------------------------------------
  // Common
  public delegate void DOnGetHelp();
  public delegate void DOnGetProgramHeader();
  public delegate void DOnGetHardwareVersion();
  public delegate void DOnGetSoftwareVersion();
  public delegate void DOnGetProcessCount();
  public delegate void DOnSetProcessCount(UInt32 count);
  public delegate void DOnGetProcessPeriodms();
  public delegate void DOnSetProcessPeriodms(UInt32 value);
  public delegate void DOnAbortProcessExecution();
  // LedSystem
  public delegate void DOnGetLedSystem();
  public delegate void DOnSetLedSystemLow();
  public delegate void DOnSetLedSystemHigh();
  public delegate void DOnBlinkLedSystem(UInt32 period, UInt32 count);
  // LaserRange
  public delegate void DOnGetPositionX();
  public delegate void DOnSetPositionX(UInt16 position);
  public delegate void DOnGetRangeX();
  public delegate void DOnSetRangeX(UInt16 minimum, UInt16 maximum, UInt16 delta);
  public delegate void DOnGetPositionY();
  public delegate void DOnSetPositionY(UInt16 position);
  public delegate void DOnGetRangeY();
  public delegate void DOnSetRangeY(UInt16 minimum, UInt16 maximum, UInt16 delta);
  public delegate void DOnGetDelayMotionms();
  public delegate void DOnSetDelayMotionms(UInt32 value);
  public delegate void DOnGetPulseWidthus();
  public delegate void DOnSetPulseWidthus(UInt32 value);
  public delegate void DOnGetPulseCount();
  public delegate void DOnSetPulseCount(UInt32 value);
  public delegate void DOnGetPulsePeriodms();
  public delegate void DOnSetPulsePeriodms(UInt32 value);
  // LaserPosition
  public delegate void DOnPulseLaserPosition(UInt16 positionx, UInt16 positiony,
                                             UInt32 period, UInt32 count);
  public delegate void DOnAbortLaserPosition();
  // LaserLine
  public delegate void DOnPulseLaserRow(UInt32 periodms, UInt32 count);
  public delegate void DOnAbortLaserRow();
  public delegate void DOnPulseLaserCol(UInt32 periodms, UInt32 count);
  public delegate void DOnAbortLaserCol();
  // LaserMatrix
  public delegate void DOnPulseLaserMatrix(UInt32 period, UInt32 count);
  public delegate void DOnAbortLaserMatrix();
  // LaserImage
  public delegate void DOnLoadLaserSteptable(String filename);
  public delegate void DOnLoadLaserImage(Bitmap bitmap);
  public delegate void DOnPulseSteptable();
  public delegate void DOnAbortSteptable();
  //
  public class CDefine
  {
    public const String PROMPT_NEWLINE = "\r\n";
    public const String PROMPT_INPUT = ">";
    public const String PROMPT_ANSWER = "#";
    public const String PROMPT_RESPONSE = "!";
    public const String PROMPT_EVENT = ":";
    //
    public const Char SEPARATOR_SPACE = ' ';
    public const Char SEPARATOR_MARK = '!';
    public const Char SEPARATOR_COLON = ':';
    public const Char SEPARATOR_DOT = '.';
    public const Char SEPARATOR_GREATER = '>';
    public const Char SEPARATOR_NUMBER = '#';
    //
    public const String TOKEN_SPACE = " ";
    public const String TOKEN_MARK = "!";
    public const String TOKEN_COLON = ":";
    public const String TOKEN_DOT = ".";
    public const String TOKEN_GREATER = ">";
    public const String TOKEN_NUMBER = "#";
    //
    // Common
    public const String SHORT_H = "H";
    public const String SHORT_GPH = "GPH";
    public const String SHORT_GSV = "GSV";
    public const String SHORT_GHV = "GHV";
    public const String SHORT_GPC = "GPC";
    public const String SHORT_SPC = "SPC";
    public const String SHORT_GPP = "GPP";
    public const String SHORT_SPP = "SPP";
    public const String SHORT_SPE = "SPE";
    public const String SHORT_A   = "A";
    // LedSystem                    
    public const String SHORT_GLS = "GLS";
    public const String SHORT_LSH = "LSH";
    public const String SHORT_LSL = "LSL";
    public const String SHORT_BLS = "BLS";
    // LaserRange
    public const String SHORT_GPX = "GPX";
    public const String SHORT_SPX = "SPX";
    public const String SHORT_GPY = "GPY";
    public const String SHORT_SPY = "SPY";
    public const String SHORT_GRX = "GRX";
    public const String SHORT_SRX = "SRX";
    public const String SHORT_GRY = "GRY";
    public const String SHORT_SRY = "SRY";
    public const String SHORT_GDM = "GDM";
    public const String SHORT_SDM = "SDM";
    public const String SHORT_GPW = "GPW";
    public const String SHORT_SPW = "SPW";
    //
    // LaserPosition
    public const String SHORT_PLP = "PLP";
    public const String SHORT_ALP = "ALP";
    //
    // LaserLine
    public const String SHORT_PLR = "PLR";
    public const String SHORT_ALR = "ALR";
    public const String SHORT_PLC = "PLC";
    public const String SHORT_ALC = "ALC";
    //
    // LaserMatrix
    public const String SHORT_PLM = "PLM";
    public const String SHORT_ALM = "ALM";
    //
    // LaserImage
    public const String SHORT_PLI = "PLI";
    public const String SHORT_ALI = "ALI";
    //
  }
}
