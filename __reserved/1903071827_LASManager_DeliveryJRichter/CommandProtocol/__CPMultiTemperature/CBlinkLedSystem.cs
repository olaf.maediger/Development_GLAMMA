﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using CommandProtocol;
//
namespace CPMultiTemperature
{
  public class CBlinkLedSystem : CCommand
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String HEADER = CDefine.SHORT_BLS;
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private Int32 FPeriod;
    private Int32 FCount;
    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CBlinkLedSystem(Int32 period, Int32 count)
      : base(HEADER)
    {
      FPeriod = period;
      FCount = count;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public override string GetCommandHeader()
    {
      return FHeader;
    }

    public override string GetCommandText()
    {
      return String.Format("{0} {1} {2}", FHeader, FPeriod, FCount);
    }


  }
}
