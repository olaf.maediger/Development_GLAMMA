﻿using System;
//
using Task;
//
namespace LASManager
{
  public delegate void DOnPulseStepListStart();
  public delegate Boolean DOnPulseStepListBusy();
  public delegate void DOnPulseStepListEnd();
  public delegate void DOnPulseStepListAbort();

  public class CProcessPulseStepList
  {
    private CProcess FProcess;
    private DOnPulseStepListStart FOnPulseStepListStart;
    private DOnPulseStepListBusy FOnPulseStepListBusy;
    private DOnPulseStepListEnd FOnPulseStepListEnd;
    private DOnPulseStepListAbort FOnPulseStepListAbort;
    //
    public CProcessPulseStepList(DOnPulseStepListStart onpulsestepliststart,
                              DOnPulseStepListBusy onpulsesteplistbusy,
                              DOnPulseStepListEnd onpulsesteplistend,
                              DOnPulseStepListAbort onpulsesteplistabort)
    {
      FOnPulseStepListStart = onpulsestepliststart;
      FOnPulseStepListBusy = onpulsesteplistbusy;
      FOnPulseStepListEnd = onpulsesteplistend;
      FOnPulseStepListAbort = onpulsesteplistabort;
      FProcess = new CProcess("PulseStepList",
                              SelfOnExecutionStart,
                              SelfOnExecutionBusy,
                              SelfOnExecutionEnd,
                              SelfOnExecutionAbort);
    }

    public Boolean Start()
    {
      return FProcess.Start();
    }

    public Boolean Abort()
    {
      if (FProcess.IsActive())
      {
        return FProcess.Abort();
      }
      return false;
    }

    protected void SelfOnExecutionStart(RTaskData data)
    {
      if (FOnPulseStepListStart is DOnPulseStepListStart)
      {
        FOnPulseStepListStart();
      }
    }
    protected Boolean SelfOnExecutionBusy(RTaskData data)
    {
      if (FOnPulseStepListBusy is DOnPulseStepListBusy)
      {
        return FOnPulseStepListBusy();
      }
      return false;
    }
    protected void SelfOnExecutionEnd(RTaskData data) 
    {
      if (FOnPulseStepListEnd is DOnPulseStepListEnd)
      {
        FOnPulseStepListEnd();
      }
    }
    protected void SelfOnExecutionAbort(RTaskData data)
    {
      if (FOnPulseStepListAbort is DOnPulseStepListAbort)
      {
        FOnPulseStepListAbort();
      }
    }


  }
}
