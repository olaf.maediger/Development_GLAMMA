﻿namespace UCUartOpenClose
{
  partial class CUCParity
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.cbxParity = new System.Windows.Forms.ComboBox();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label1 = new System.Windows.Forms.Label();
      this.SuspendLayout();
      // 
      // cbxParity
      // 
      this.cbxParity.Dock = System.Windows.Forms.DockStyle.Right;
      this.cbxParity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
      this.cbxParity.FormattingEnabled = true;
      this.cbxParity.Location = new System.Drawing.Point(34, 0);
      this.cbxParity.Name = "cbxParity";
      this.cbxParity.Size = new System.Drawing.Size(56, 21);
      this.cbxParity.TabIndex = 2;
      // 
      // panel1
      // 
      this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
      this.panel1.Location = new System.Drawing.Point(0, 0);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(34, 4);
      this.panel1.TabIndex = 70;
      // 
      // label1
      // 
      this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.label1.Location = new System.Drawing.Point(0, 4);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(34, 17);
      this.label1.TabIndex = 71;
      this.label1.Text = "Parity";
      // 
      // CUCParity
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.label1);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.cbxParity);
      this.Name = "CUCParity";
      this.Size = new System.Drawing.Size(90, 21);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.ComboBox cbxParity;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label1;
  }
}
