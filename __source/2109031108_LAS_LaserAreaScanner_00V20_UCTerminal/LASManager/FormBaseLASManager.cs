﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
//
using Initdata;
using UCNotifier;
using UCTextEditor;
using Programdata;
//
namespace LASManager
{
  //
  //------------------------------------------------------
  //  Section - Error
  //------------------------------------------------------
  //	
  public enum EErrorCode 
  {
    None = 0,
    InvalidLibraryCall = 1,
    InvalidComPort,
    Invalid
  };
  //
  //#########################################################
  //	Section - Main - Basic
  //#########################################################
  //
  public partial class FormClient : Form
  { //
    //------------------------------------------------------
    //  Section - Constant
    //------------------------------------------------------
    //		
    private const String HEADER_LIBRARY = "Main";
    private const String INIT_SEGMENTBASE = "Application";
    //
    private const String APPLICATION_TITLE = "LASManager";
    private const Int32 APPLICATION_COUNT = 1;
    private const String APPLICATION_NAME = APPLICATION_TITLE;
    private const String APPLICATION_VERSION = "06V25";
    private const String APPLICATION_DATE = "30. August 2018";
    private const Boolean APPLICATION_DISCOUNTUSER = false;
    private const String APPLICATION_USER = "OM";
    //
    private const String FORMAT_MODULE_NAME = APPLICATION_TITLE + "{0}";
    private const String MODULE_VERSION = APPLICATION_VERSION;
    private const String MODULE_DATE = APPLICATION_DATE;
    private const Boolean MODULE_DISCOUNTUSER = false;
    private const String MODULE_USER = APPLICATION_USER;
    //
    private const Int32 DEVICE_COUNT_MINIMUM = 1;
    private const String DEVICE_NAME = "LaserAreaScanner";
    private const String DEVICE_VERSION = "01V03";
    private const String DEVICE_DATE = "30. December 2017";
    private const Boolean DEVICE_DISCOUNTUSER = false;
    private const String DEVICE_USER = "AD";
    //
    private const String MAIN_HELPFILE = APPLICATION_TITLE + "Help.chm";
    //// MTS - MultipleTemperatureScanner
    //public const String DESCRIPTION = "Measuring Temperature";
    //public const String POINT1 = " - with PT100 and T1000";
    //public const String POINT2 = " - selectable 1 .. 8 Channel";
    //public const String POINT3 = " - 2, 3 and 4 wire PT-Sensors";
    // LAS - LaserAreaScanner
    public const String DESCRIPTION = "Laser Servo Scanner";
    public const String POINT1 = " - Preset X- Y- Mirror Position";
    public const String POINT2 = " - LaserPulse Frequency Pulsecount";
    public const String POINT3 = " - Executing Images";
    //
    public const String COMPANYTARGETTOP = "Laser-Laboratorium";
    public const String COMPANYTARGETBOTTOM = "Göttingen e.V.";
    public const String STREETTARGET = "Hans-Adolf-Krebs-Weg";
    public const String STREETNUMBERTARGET = "1";
    public const String POSTALCODETARGET = "D - 37077";
    public const String CITYTARGET = "Göttingen";
    //
    //public const String COMPANYTARGETTOP = "Firma Maediger";
    //public const String COMPANYTARGETBOTTOM = "Programmierung & Entwicklung";
    //public const String STREETTARGET = "Bundesstrasse";
    //public const String STREETNUMBERTARGET = "37";
    //public const String POSTALCODETARGET = "D - 37191";
    //public const String CITYTARGET = "Katlenburg-Lindau";
    //
    public const String COMPANYSOURCETOP = "Laser-Laboratorium";
    public const String COMPANYSOURCEBOTTOM = "Göttingen e.V.";
    public const String STREETSOURCE = "Hans-Adolf-Krebs-Weg";
    public const String STREETNUMBERSOURCE = "1";
    public const String POSTALCODESOURCE = "D - 37077";
    public const String CITYSOURCE = "Göttingen";
    //
    //public const String COMPANYSOURCETOP = "Firma Maediger";
    //public const String COMPANYSOURCEBOTTOM = "Programmierung & Entwicklung";
    //public const String STREETSOURCE = "Bundesstrasse";
    //public const String STREETNUMBERSOURCE = "37";
    //public const String POSTALCODESOURCE = "D - 37191";
    //public const String CITYSOURCE = "Katlenburg-Lindau";
    //
    private const String SUBDIRECTORY_TARGET =
      "LLG\\" + APPLICATION_TITLE + APPLICATION_VERSION;
    //
    // !!!!! private const String SUBDIRECTORY_TARGET =
    // !!!!!  "MWare\\" + APPLICATION_TITLE + APPLICATION_VERSION;
    //
    public const String INIT_FILENAME = APPLICATION_TITLE;
    public const String INIT_FILEEXTENSION = ".ini.xml";
    public const String INIT_HEADER = APPLICATION_TITLE;
    //
    private const String NAME_HEIGHTPROTOCOL = "HeightProtocol";
    private const String NAME_SAVEAUTOMATICATPROGRAMEND = "SaveAutomaticAtProgramEnd";
    //
    private const FormWindowState INIT_WINDOWSTATE = FormWindowState.Normal;
    public const Int32 INIT_LEFT = 10;
    public const Int32 INIT_TOP = 10;
    public const Int32 INIT_WIDTH = 1024;
    public const Int32 INIT_HEIGHT = 800;
    //
    private const Int32 INIT_HEIGHTPROTOCOL = 300;
    private const Boolean INIT_SAVEAUTOMATICATPROGRAMEND = false;
    //
    //
    public static readonly String[] ERRORS = 
    { 
      "None",
      "Invalid Library Call",
  	  "Invalid"
    };
    //
    //------------------------------------------------------
    //  Section - Member
    //------------------------------------------------------
    //		
    private String[] FArguments;
    private String FInitdataFilename = INIT_FILENAME + INIT_FILEEXTENSION;
    private String FInitdataDirectory;
    private CUCNotifier FUCNotifier;
    private Int32 FStartupState;
    private DOnStartupTick FOnStartupTick;
    //
    //------------------------------------------------------
    //  Section - Constructor
    //------------------------------------------------------
    //		
    public FormClient()
    {	// DecimalPoint 
      CInitdata.Init();
      // DecimalPoint
      InitializeComponent();
      StartPosition = FormStartPosition.Manual;
      //
      this.Load += FormMain_Load;
      this.FormClosing += FormMain_FormClosing;
      this.FOnStartupTick = OnStartupTick;
      //
      FArguments = Environment.GetCommandLineArgs();
      //
      // NC !!!FUCSerialNumber = new CUCSerialNumber();
      // NC !!!this.Controls.Add(FUCSerialNumber);
      // NC !!!FUCSerialNumber.Dock = DockStyle.Top;
      FUCSerialNumber.SetDeviceName(DEVICE_NAME);
      //
      //----------------------------
      // Instantiation UCNotifier
      //----------------------------
      FUCNotifier = new CUCNotifier();
      FUCNotifier.SetProtocolParameter(SUBDIRECTORY_TARGET,
                                       APPLICATION_NAME, APPLICATION_VERSION,
                                       COMPANYSOURCETOP,
                                       COMPANYSOURCEBOTTOM,
                                       APPLICATION_USER);
      pnlProtocol.Controls.Add(FUCNotifier);
      FUCNotifier.Dock = DockStyle.Fill;
      FUCNotifier.Protocol(BuildHeader(), "Creating all Controls");
      //
      //----------------------------
      // Instantiation Help
      //----------------------------
      FHelpProvider.HelpNamespace = Path.Combine(Directory.GetCurrentDirectory(), MAIN_HELPFILE);
      String Line = String.Format("WorkingDirectory: {0}", Directory.GetCurrentDirectory());
      FUCNotifier.Write(Line);
      Line = String.Format("HelpFile: {0}", FHelpProvider.HelpNamespace);
      FUCNotifier.Write(Line);
      //
      //
      // Startup-Timer
      FStartupState = 0;
      tmrStartup.Interval = 1000;
      // NC tmrStartup.Enabled = true;
      // NC 
      tmrStartup.Enabled = false;
      tmrStartup.Tick += new System.EventHandler(tmrStartup_Tick);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // Instantiate Programmer-Controls:
      InstantiateProgrammerControls();
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      //
      InitControls();
      //
      //----------------------------
      // Instantiation Initdata
      //----------------------------
      if (!CInitdata.BuildInitdataDirectory(SUBDIRECTORY_TARGET,
                                            INIT_FILENAME,
                                            INIT_FILEEXTENSION,
                                            out FInitdataDirectory,
                                            out FInitdataFilename))
      { // No initfile present -> build and save project.ini.xml with INIT_-values:
        LoadProgramdata(FInitdataDirectory, FInitdataFilename);
        SaveProgramdata(FInitdataDirectory, FInitdataFilename);
      }
      // NC !!! Int32 EI = (int)EErrorCode.None;
      // NC !!! FUCNotifier.Error(BuildHeader(), EI, ERRORS[EI]);
      FUCNotifier.Protocol(BuildHeader(), "Construction successful.");
    }
    //
    //------------------------------------------------------
    //  Section - Property
    //------------------------------------------------------
    //	

    //
    //------------------------------------------------------
    //  Section - Startup-Arguments
    //------------------------------------------------------
    //
    public void SetArguments(String[] arguments)
    {
      FArguments = arguments;
    }
    //
    //------------------------------------------------------
    //  Section - Form-Events
    //------------------------------------------------------
    //
    private void FormMain_Load(object sender, EventArgs e)
    {
      LoadProgramdata(FInitdataDirectory, FInitdataFilename);
    }

    private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
    {
      FUCNotifier.SaveOnClosing();
      if (mitCSaveAutomaticAtProgramEnd.Checked)
      {
        SaveProgramdata(FInitdataDirectory, FInitdataFilename);
      }
      // Save all Subdevices
      FreeProgrammerControls();
    }

    private void InitdataOnNotify(String module, String line)
    {
      FUCNotifier.Write(module, line);
    }
    //
    //----------------------------
    //  Section - Init
    //----------------------------
    //
    private void InitApplicationTitle()
    {
      Text = APPLICATION_TITLE + "  Version " + APPLICATION_VERSION + " | " + COMPANYSOURCETOP;
    }

    private void InitControls()
    {
      FUCNotifier.Protocol(BuildHeader(), "Initialisation of Controls.");
      InitApplicationTitle();
      //
      //  Event - System
      mitSQuit.Click += new EventHandler(mitSQuit_Click);
      //
      //  Event - Configuration
      mitCResetToDefaultConfiguration.Click += new EventHandler(mitCResetToDefaultConfiguration_Click);
      mitCSaveAutomaticAtProgramEnd.Click += new EventHandler(mitCSaveAutomaticAtProgramEnd_Click);
      mitCLoadStartupConfiguration.Click += new EventHandler(mitCLoadStartupConfiguration_Click);
      mitCSaveStartupConfiguration.Click += new EventHandler(mitCSaveStartupConfiguration_Click);
      mitCShowEditStartupConfiguration.Click += new EventHandler(mitCShowEditStartupConfiguration_Click);
      mitCLoadSelectableConfiguration.Click += new EventHandler(mitCLoadSelectableConfiguration_Click);
      mitCSaveSelectableConfiguration.Click += new EventHandler(mitCSaveSelectableConfiguration_Click);
      mitCShowEditSelectableConfiguration.Click += new EventHandler(mitCShowEditSelectableConfiguration_Click);
      //
      //  Event - 
      mitPEditParameter.Click += new EventHandler(mitPEditParameter_Click);
      mitPClearProtocol.Click += new EventHandler(mitPClearProtocol_Click);
      mitPCopyToClipboard.Click += new EventHandler(mitPCopyToClipboard_Click);
      mitPLoadDiagnosticFile.Click += new EventHandler(mitPLoadDiagnosticFile_Click);
      mitPSaveDiagnosticFile.Click += new EventHandler(mitPSaveDiagnosticFile_Click);
      mitSendDiagnosticEmail.Click += new EventHandler(mitSendDiagnosticEmail_Click);
      //
      //  Event - Help
      mitHTopic.Click += new EventHandler(mitHTopic_Click);
      mitHContents.Click += new EventHandler(mitHContents_Click);
      mitHSearch.Click += new EventHandler(mitHSearch_Click);
      mitHEnterApplicationProductKey.Click += new EventHandler(mitHEnterApplicationProductKey_Click);
      mitHEnterDeviceProductKey.Click += new EventHandler(mitHEnterDeviceProductKey_Click);
      mitHAbout.Click += new EventHandler(mitHAbout_Click);
    }

    private Boolean LoadProgramdata(String directory, String filename)
    {
      Boolean Result = true;
      // Open     
      FUCNotifier.Protocol(BuildHeader(), String.Format("Directory..: '{0}'", directory));
      FUCNotifier.Protocol(BuildHeader(), String.Format("FileName...: '{0}'", filename));
      String FileEntry = Path.Combine(directory, filename);
      //
      CInitdataReader IDR = new CInitdataReader();
      // with Protocol: IDR.Open(FileEntry, INIT_SEGMENTBASE, InitdataOnNotify);
      // without Protocol: 
      Result &= IDR.Open(FileEntry, INIT_SEGMENTBASE, null);
      //
      // MainWindow
      Result &= IDR.OpenSection(CInitdata.NAME_MAINWINDOW);
      //
      Int32 IValue;
      String ApplicationTitle = APPLICATION_TITLE;
      // Main - WindowText
      Result &= IDR.ReadString(CInitdata.NAME_TITLE, out ApplicationTitle, ApplicationTitle);
      this.Text = ApplicationTitle;
      //
      // Main - WindowState / Left, Top, Width, Height
      Result &= CInitdata.LoadWindowStateBorder(IDR, this,
                                                INIT_WINDOWSTATE,
                                                INIT_LEFT, INIT_TOP,
                                                INIT_WIDTH, INIT_HEIGHT);
      //
      // MainWindow - Autosave
      Boolean BValue = INIT_SAVEAUTOMATICATPROGRAMEND;
      Result &= IDR.ReadBoolean(NAME_SAVEAUTOMATICATPROGRAMEND, out BValue, BValue);
      mitCSaveAutomaticAtProgramEnd.Checked = BValue;
      //
      //-----------------------------------
      // Load/Create Notifier-Applications
      //-----------------------------------
      Int32 ApplicationCount = APPLICATION_COUNT;
      Result &= IDR.ReadInt32(CUCNotifier.NAME_APPLICATIONCOUNT, out ApplicationCount, ApplicationCount);
      ApplicationCount = Math.Max(APPLICATION_COUNT, ApplicationCount);
      String ApplicationProductKey = CUCNotifier.INIT_APPLICATIONPRODUCTKEY;
      for (Int32 AI = 0; AI < ApplicationCount; AI++)
      {
        String Name = String.Format(CUCNotifier.FORMAT_APPLICATIONPRODUCTKEY, AI);
        Result &= IDR.ReadString(Name, out ApplicationProductKey, ApplicationProductKey);
        if (0 == AI)
        {
          Result &= FUCNotifier.AddApplication(APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_DATE,
                                               COMPANYTARGETTOP, STREETTARGET,
                                               STREETNUMBERTARGET,
                                               POSTALCODETARGET,
                                               CITYTARGET,
                                               APPLICATION_DISCOUNTUSER, APPLICATION_USER, ApplicationProductKey);
        }
        else
        {
          String ModuleName = String.Format(FORMAT_MODULE_NAME, AI.ToString());
          Result &= FUCNotifier.AddApplication(ModuleName, MODULE_VERSION, MODULE_DATE,
                                               COMPANYTARGETTOP, STREETTARGET,
                                               STREETNUMBERTARGET,
                                               POSTALCODETARGET,
                                               CITYTARGET,
                                               MODULE_DISCOUNTUSER, MODULE_USER, ApplicationProductKey);
        }
      }
      Result &= (APPLICATION_COUNT == FUCNotifier.GetApplicationCount());
      //
      //-----------------------------------
      // Load/Create Notifier-Devices
      //-----------------------------------
      FUCSerialNumber.ClearAllDevices();
      Int32 DeviceCount = DEVICE_COUNT_MINIMUM;
      Result &= IDR.ReadInt32(CUCNotifier.NAME_DEVICECOUNT, out DeviceCount, DeviceCount);
      DeviceCount = Math.Max(DEVICE_COUNT_MINIMUM, DeviceCount);
      String DeviceProductKey = CUCNotifier.INIT_DEVICEPRODUCTKEY;
      for (Int32 DI = 0; DI < DeviceCount; DI++)
      {
        String Name = String.Format(CUCNotifier.FORMAT_DEVICESERIALNUMBER, DI);
        IValue = CUCNotifier.INIT_DEVICESERIALNUMBER;
        Result &= IDR.ReadInt32(Name, out IValue, IValue);
        //
        Name = String.Format(CUCNotifier.FORMAT_DEVICEPRODUCTKEY, DI);
        Result &= IDR.ReadString(Name, out DeviceProductKey, DeviceProductKey);
        //
        Result &= FUCNotifier.AddDevice(ApplicationProductKey,
                                        DEVICE_NAME, 
                                        DEVICE_VERSION, 
                                        DEVICE_DATE,
                                        DEVICE_DISCOUNTUSER, 
                                        DEVICE_USER,
                                        IValue,
                                        DeviceProductKey);
        // Refresh DeviceID-Selection
        Result &= Result &= FUCSerialNumber.AddDevice(DEVICE_NAME, IValue, DeviceProductKey);
      }
      //
      // MainWindow
      Result &= IDR.CloseSection();
      //
      //
      // Notifier
      Result &= FUCNotifier.LoadInitdata(IDR);
      //
      //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      // LoadInitdata Programmer-Controls:
      Result &= LoadInitdataProgrammerControls(IDR);
      //
      // Close
      Result &= IDR.Close();
      IDR = null;
      //
      return Result;
    }

    private Boolean SaveProgramdata(String directory, String filename)
    { // Open
      FUCNotifier.Protocol(BuildHeader(), String.Format("Directory..: '{0}'", directory));
      FUCNotifier.Protocol(BuildHeader(), String.Format("FileName...: '{0}'", filename));
      String FileEntry = Path.Combine(directory, filename);
      //
      Boolean Result = true;
      CInitdataWriter IDW = new CInitdataWriter();
      // with Protocol: IDW.Open(FileEntry, INIT_SEGMENTBASE, InitdataOnNotify);
      // without Protocol: 
      Result &= IDW.Open(FileEntry, INIT_SEGMENTBASE, null);
      //
      // MainWindow
      Result &= IDW.CreateSection(CInitdata.NAME_MAINWINDOW);
      //
      // Main - WindowText
      InitApplicationTitle();
      Result &= IDW.WriteString(CInitdata.NAME_TITLE, this.Text);
      //
      // Main - WindowState / Left, Top, Width, Height
      Result &= CInitdata.SaveWindowStateBorder(IDW, this);
      //
      // MainWindow - Autosave
      Result &= IDW.WriteBoolean(NAME_SAVEAUTOMATICATPROGRAMEND, mitCSaveAutomaticAtProgramEnd.Checked);
      //
      //-----------------------------------
      // Save Notifier-Applications
      //-----------------------------------
      Int32 ApplicationCount = FUCNotifier.GetApplicationCount();
      ApplicationCount = Math.Max(APPLICATION_COUNT, ApplicationCount);
      Result &= IDW.WriteInt32(CUCNotifier.NAME_APPLICATIONCOUNT, ApplicationCount);
      for (Int32 AI = 0; AI < ApplicationCount; AI++)
      {
        String Name = String.Format(CUCNotifier.FORMAT_APPLICATIONPRODUCTKEY, AI);
        Result &= IDW.WriteString(Name, FUCNotifier.GetApplicationProductKey(AI));
      }
      //
      //-----------------------------------
      // Save Notifier-Devices
      //-----------------------------------
      Int32 DeviceCount = FUCNotifier.GetDeviceCount();
      DeviceCount = Math.Max(DEVICE_COUNT_MINIMUM, DeviceCount);
      Result &= IDW.WriteInt32(CUCNotifier.NAME_DEVICECOUNT, DeviceCount);
      for (Int32 DI = 0; DI < DeviceCount; DI++)
      {
        String Name = String.Format(CUCNotifier.FORMAT_DEVICESERIALNUMBER, DI);
        Result &= IDW.WriteInt32(Name, FUCNotifier.GetDeviceSerialNumber(DI));
        //
        Name = String.Format(CUCNotifier.FORMAT_DEVICEPRODUCTKEY, DI);
        Result &= IDW.WriteString(Name, FUCNotifier.GetDeviceProductKey(DI));
      }
      //
      // MainWindow
      Result &= IDW.CloseSection();
      //
      //
      // Notifier
      Result &= FUCNotifier.SaveInitdata(IDW);
      //
      // SaveInitdata Programmer-Controls:
      Result &= SaveInitdataProgrammerControls(IDW);
      //
      // Close
      Result &= IDW.Close();
      IDW = null;
      return Result;
    }
    //
    //----------------------------------------
    //  Section - Private Helper
    //----------------------------------------
    //

    //
    //------------------------------------------------------
    //  Section - Helper
    //------------------------------------------------------
    //
    private String BuildHeader()
    {
      return String.Format("{0}", HEADER_LIBRARY);
    }
    //
    //----------------------------------------
    //  Section - Startup
    //----------------------------------------
    //
    private void tmrStartup_Tick(object sender, EventArgs e)
    {
      if (FOnStartupTick is DOnStartupTick)
      {
        Boolean TE = FOnStartupTick(FStartupState);
        tmrStartup.Enabled = TE;
      }
      FStartupState++;
    }
    //
    //----------------------------------------
    //  Menu-Event-Methods : Help
    //----------------------------------------
    //
    private void mitHContents_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.TableOfContents);
    }

    private void mitHTopic_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.Topic, "./Main/MIndex.html");
    }

    private void mitHSearch_Click(object sender, EventArgs e)
    {
      Help.ShowHelp(this, MAIN_HELPFILE, HelpNavigator.KeywordIndex);
    }

    private void mitHEnterApplicationProductKey_Click(object sender, EventArgs e)
    {
      if (FUCNotifier.ShowApplicationProductKeyModal())
      {
        if (FUCNotifier.ShowSaveQuestionModal())
        {
          SaveProgramdata(FInitdataDirectory, FInitdataFilename);
          Close();
        }
      }
    }

    private void mitHEnterDeviceProductKey_Click(object sender, EventArgs e)
    {
      if (FUCNotifier.ShowDeviceProductKeyModal())
      {
        if (FUCNotifier.ShowSaveQuestionModal())
        {
          SaveProgramdata(FInitdataDirectory, FInitdataFilename);
          Close();
        }
      }
    }

    private void mitHAbout_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = ":";
      FUCNotifier.Protocol(BuildHeader(), Line);
      FUCNotifier.Protocol(BuildHeader(), "Call About-Dialog");
      CInitdata ID = new CInitdata();
      ID.SetApplicationData(APPLICATION_NAME, APPLICATION_VERSION, APPLICATION_DATE);
      ID.SetDescriptionData(DESCRIPTION, POINT1, POINT2, POINT3);
      ID.SetCompanySourceData(COMPANYSOURCETOP, COMPANYSOURCEBOTTOM,
                              STREETSOURCE, CITYSOURCE);
      ID.SetCompanyTargetData(COMPANYTARGETTOP, COMPANYTARGETBOTTOM, STREETTARGET, CITYTARGET);
      RNotifierData NotifierData;
      FUCNotifier.GetData(out NotifierData);
      ID.SetProductKey(FUCNotifier.GetApplicationProductKey(0));
      ID.SetIcon(this.Icon);
      ID.ShowModalDialogAbout();
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }
    //
    //----------------------------------------
    //  Menu-Event-Methods : System
    //----------------------------------------
    //
    private void mitSQuit_Click(object sender, EventArgs e)
    {
      FUCNotifier.Protocol(BuildHeader(), "Exit Application");
      Close();
    }
    //
    //--------------------------------------------------------------
    //  Section - Menu-Event-Methods: Protocol
    //--------------------------------------------------------------
    //
    private void mitPEditParameter_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Edit Protocol-Parameter:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      Result &= FUCNotifier.EditParameterModal();
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitPClearProtocol_Click(object sender, EventArgs e)
    {
      String Line = "Clear Protocol.";
      FUCNotifier.Protocol(BuildHeader(), Line);
      FUCNotifier.Clear();
    }

    private void mitPCopyToClipboard_Click(object sender, EventArgs e)
    {
      String Line = "Copy Protocol to Clipboard.";
      FUCNotifier.Protocol(BuildHeader(), Line);
      FUCNotifier.CopyToClipboard();
    }

    private void mitPLoadDiagnosticFile_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Diagnostic-File with Dialog:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      Result &= FUCNotifier.LoadDiagnosticFileModal();
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitPSaveDiagnosticFile_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Diagnostic-File with Dialog:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      Result &= FUCNotifier.SaveDiagnosticFileModal();
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitSendDiagnosticEmail_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Send Diagnostic-Email:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      Result &= FUCNotifier.SendDiagnosticEmail();
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }
    //
    //--------------------------------------------------------------
    //  Section - Menu-Event-Methods : Configuration
    //--------------------------------------------------------------
    //
    private void mitCResetToDefaultConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Reset to Default-Configuration:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      Result &= CInitdata.DeleteInitdataFile(SUBDIRECTORY_TARGET,
                                             INIT_FILENAME,
                                             INIT_FILEEXTENSION);
      Result &= LoadProgramdata(FInitdataDirectory, FInitdataFilename);
      Result &= SaveProgramdata(FInitdataDirectory, FInitdataFilename);
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitCSaveAutomaticAtProgramEnd_Click(object sender, EventArgs e)
    {
      mitCSaveAutomaticAtProgramEnd.Checked = !mitCSaveAutomaticAtProgramEnd.Checked;
      String Line = String.Format("Save automatic at program end: -> {0}",
                                  mitCSaveAutomaticAtProgramEnd.Checked);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitCLoadStartupConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Startup-Configuration:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      Result &= LoadProgramdata(FInitdataDirectory, FInitdataFilename);
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitCSaveStartupConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Startup-Configuration:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      Result &= SaveProgramdata(FInitdataDirectory, FInitdataFilename);
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitCShowEditStartupConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Show / Edit Startup-Configuration:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      String FileEntry = Path.Combine(FInitdataDirectory, FInitdataFilename);
      CDialogTextEditor DialogTextEditor = new CDialogTextEditor();
      DialogTextEditor.Text = Line; // Title!
      DialogTextEditor.Width = 800;
      DialogTextEditor.Height = 600;
      DialogTextEditor.IsTextReadOnly = true;
      DialogTextEditor.LoadFileExtension = "ini.xml";
      DialogTextEditor.LoadFileName = APPLICATION_NAME;
      DialogTextEditor.LoadFileFilter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      DialogTextEditor.SaveFileExtension = "ini.xml";
      DialogTextEditor.SaveFileName = APPLICATION_NAME;
      DialogTextEditor.SaveFileFilter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
      Result &= DialogTextEditor.LoadFromFile(FileEntry);
      if (Result)
      {
        if (DialogResult.OK == DialogTextEditor.ShowDialog())
        {
          if (DialogTextEditor.TextIsChanged)
          {
            Result &= DialogTextEditor.SaveToFile(FileEntry);
          }
        }
      }
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitCLoadSelectableConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Load Selectable-Configuration:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      DialogSaveInitdata.InitialDirectory = FInitdataDirectory;
      DialogSaveInitdata.FileName = FInitdataFilename;
      Result &= (DialogResult.OK == DialogLoadInitdata.ShowDialog());
      if (Result)
      {
        String Directory = Path.GetDirectoryName(DialogLoadInitdata.FileName);
        String FileName = Path.GetFileName(DialogLoadInitdata.FileName);
        Result &= LoadProgramdata(Directory, FileName);
      }
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitCSaveSelectableConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = true;
      String Line = "Save Selectable-Configuration:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      DialogSaveInitdata.InitialDirectory = FInitdataDirectory;
      DialogSaveInitdata.FileName = FInitdataFilename;
      Result &= (DialogResult.OK == DialogSaveInitdata.ShowDialog());
      if (Result)
      {
        String Directory = Path.GetDirectoryName(DialogSaveInitdata.FileName);
        String FileName = Path.GetFileName(DialogSaveInitdata.FileName);
        Result &= SaveProgramdata(Directory, FileName);
      }
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }

    private void mitCShowEditSelectableConfiguration_Click(object sender, EventArgs e)
    {
      Boolean Result = false;
      String Line = "Show / Edit Selectable-Configuration:";
      FUCNotifier.Protocol(BuildHeader(), Line);
      //
      DialogSaveInitdata.InitialDirectory = FInitdataDirectory;
      DialogSaveInitdata.FileName = FInitdataFilename;
      if (DialogResult.OK == DialogLoadInitdata.ShowDialog())
      {
        CDialogTextEditor DialogTextEditor = new CDialogTextEditor();
        DialogTextEditor.Text = Line; // Title!
        DialogTextEditor.Width = 800;
        DialogTextEditor.Height = 600;
        DialogTextEditor.IsTextReadOnly = true;
        DialogTextEditor.LoadFileExtension = "ini.xml";
        DialogTextEditor.LoadFileName = Path.GetFileNameWithoutExtension(DialogLoadInitdata.FileName);
        DialogTextEditor.LoadFileFilter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
        DialogTextEditor.SaveFileExtension = "ini.xml";
        DialogTextEditor.SaveFileName = Path.GetFileNameWithoutExtension(DialogLoadInitdata.FileName);
        DialogTextEditor.SaveFileFilter = "Initdata XML files (*.ini.xml)|*.ini.xml|All files (*.*)|*.*";
        Result = DialogTextEditor.LoadFromFile(DialogLoadInitdata.FileName);
        if (Result)
        {
          if (DialogResult.OK == DialogTextEditor.ShowDialog())
          {
            if (DialogTextEditor.TextIsChanged)
            {
              Result &= DialogTextEditor.SaveToFile(DialogLoadInitdata.FileName);
            }
          }
        }
      }
      Line = String.Format("-> {0}", Result);
      FUCNotifier.Protocol(BuildHeader(), Line);
    }
  }
}


