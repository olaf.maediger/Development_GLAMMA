﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using UCNotifier;
using Task;
// - Main! using UdpTextDevice;
//
namespace CommandProtocol
{
  public class CCommandList : Queue<CCommand>
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //
    private CNotifier FNotifier;
    // -> Main private CUdpTextDeviceClient FUdpTextDeviceClient;
    private DOnExecutionStart FOnExecutionStart;
    private DOnExecutionBusy FOnExecutionBusy;
    private DOnExecutionEnd FOnExecutionEnd;
    private DOnExecutionAbort FOnExecutionAbort;
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetNotifier(CNotifier value)
    {
      FNotifier = value;
    }

    public void SetOnExecutionStart(DOnExecutionStart value)
    {
      FOnExecutionStart = value;
    }
    public void SetOnExecutionBusy(DOnExecutionBusy value)
    {
      FOnExecutionBusy = value;
    }
    public void SetOnExecutionEnd(DOnExecutionEnd value)
    {
      FOnExecutionEnd = value;
    }
    public void SetOnExecutionAbort(DOnExecutionAbort value)
    {
      FOnExecutionAbort = value;
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Callback
    //--------------------------------------------------------------------------
    //
    private void OnExecutionStart(RTaskData data)
    {
      // debug FNotifier.Write(String.Format("CommandList[{0}] - OnExecutionStart", data.Name));
      if (FOnExecutionStart is DOnExecutionStart)
      {
        FOnExecutionStart(data);
      }
    }

    private Boolean OnExecutionBusy(RTaskData data)
    {
      Boolean Result = false;
      // debug FNotifier.Write(String.Format("CommandList[{0}] - OnExecutionBusy - Start", data.Name));
      if (FOnExecutionBusy is DOnExecutionBusy)
      {
        Result = FOnExecutionBusy(data);
      }
      // debug FNotifier.Write(String.Format("CommandList[{0}] - OnExecutionBusy - End", data.Name));
      return Result;
    }

    private void OnExecutionEnd(RTaskData data)
    {
      // debug FNotifier.Write(String.Format("CommandList[{0}] - OnExecutionEnd", data.Name));
      if (FOnExecutionEnd is DOnExecutionEnd)
      {
        FOnExecutionEnd(data);
      }
      //// Finished Command terminates -> dequeue!
      //Dequeue();
      //Execute();
    }

    private void OnExecutionAbort(RTaskData data)
    {
      // debug FNotifier.Write(String.Format("CommandList[{0}] - OnExecutionAbort", data.Name));
      if (FOnExecutionAbort is DOnExecutionAbort)
      {
        FOnExecutionAbort(data);
      }
      // Finished Command terminates -> dequeue!
      Dequeue();
      // ??????????????? 
      Execute();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Public
    //--------------------------------------------------------------------------
    //
    public new Boolean Enqueue(CCommand command)
    {
      try
      {
        base.Enqueue(command);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return false;
      }
    }

    public new CCommand Dequeue()
    {
      try
      {
        return base.Dequeue();
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }

    public Boolean Execute()
    {
      if (0 < base.Count)
      {
        CProtocolBase PB = Peek();
        PB.SetNotifier(FNotifier);
        PB.SetOnExecutionStart(OnExecutionStart);
        PB.SetOnExecutionBusy(OnExecutionBusy);
        PB.SetOnExecutionEnd(OnExecutionEnd);
        PB.SetOnExecutionAbort(OnExecutionAbort);
        if (PB is CCommand)
        {
          return PB.Execute();
        }
      }
      return false;
    }


  }
}
