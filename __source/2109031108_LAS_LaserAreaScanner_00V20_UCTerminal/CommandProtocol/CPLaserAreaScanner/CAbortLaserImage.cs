﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//
using CommandProtocol;
//
namespace CPLaserAreaScanner
{
  public class CAbortLaserImage : CCommand
  { //
    //--------------------------------------------------------------------------
    //  Segment - Constant
    //--------------------------------------------------------------------------
    //
    public const String HEADER = CDefine.SHORT_ALI;

    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CAbortLaserImage()
      : base(HEADER)
    {
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public override string GetCommandHeader()
    {
      return FHeader;
    }

    public override string GetCommandText()
    {
      return String.Format("{0}", FHeader);
    }

  }
}
