﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CPMultiTemperature
{
  public enum EStateProcess
  { // Common
    Undefined = -1,
    Idle = 0,
    Welcome = 1,
    GetHelp = 2,
    GetProgramHeader = 3,
    GetSoftwareVersion = 4,
    GetHardwareVersion = 5,
    SetProcessCount = 6,
    SetProcessPeriod = 7,
    StopProcessExecution = 8,
    // LedSystem
    GetLedSystem = 9,
    LedSystemOn = 10,
    LedSystemOff = 11,
    BlinkLedSystem = 12,
    // Measurement
    GetTemperatureChannel = 13,
    GetTemperatureInterval = 14,
    GetAllTemperatures = 15,
    RepeatTemperatureChannel = 16,
    RepeatTemperatureInterval = 17,
    RepeatAllTemperatures = 18
  };

  public enum EStateLed
  {
    Undefined = -1,
    Off = 0,
    On = 1
  };

  public class CDefine
  {
    public const String PROMPT_NEWLINE = "\r\n";
    public const String PROMPT_INPUT = ">";
    public const String PROMPT_ANSWER = "#";
    public const String PROMPT_RESPONSE = "!";
    public const String PROMPT_EVENT = ":";
    //
    public const Char SEPARATOR_SPACE = ' ';
    public const Char SEPARATOR_MARK = '!';
    public const Char SEPARATOR_COLON = ':';
    public const Char SEPARATOR_DOT = '.';
    public const Char SEPARATOR_GREATER = '>';
    public const Char SEPARATOR_NUMBER = '#';
    //
    public const String TOKEN_SPACE = " ";
    public const String TOKEN_MARK = "!";
    public const String TOKEN_COLON = ":";
    public const String TOKEN_DOT = ".";
    public const String TOKEN_GREATER = ">";
    public const String TOKEN_NUMBER = "#";
    //
    // Common
    public const String SHORT_H = "H";
    public const String SHORT_GPH = "GPH";
    public const String SHORT_GSV = "GSV";
    public const String SHORT_GHV = "GHV";
    public const String SHORT_SPC = "SPC";
    public const String SHORT_SPP = "SPP";
    public const String SHORT_SPE = "SPE";
    public const String SHORT_STATEMTC = "STP";
    // LedSystem                    
    public const String SHORT_GLS = "GLS";
    public const String SHORT_LSH = "LSH";
    public const String SHORT_LSL = "LSL";
    public const String SHORT_BLS = "BLS";
    // Temperature
    public const String SHORT_GTC = "GTC";
    public const String SHORT_GTI = "GTI";
    public const String SHORT_GAT = "GAT";
    public const String SHORT_RTC = "RTC";
    public const String SHORT_RTI = "RTI";
    public const String SHORT_RAT = "RAT";
    //
  }
}
