﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//
using CPMultiTemperature;
//
namespace UCTemperature
{
  //
  //--------------------------------------------------------------------------
  //  Segment - Definition - UCTemperatureDisplay
  //--------------------------------------------------------------------------
  //
  public partial class CUCTemperatureDisplay : UserControl
  {
    //
    //--------------------------------------------------------------------------
    //  Segment - Field
    //--------------------------------------------------------------------------
    //

    //
    //--------------------------------------------------------------------------
    //  Segment - Constructor
    //--------------------------------------------------------------------------
    //
    public CUCTemperatureDisplay()
    {
      InitializeComponent();
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Property
    //--------------------------------------------------------------------------
    //
    public void SetTemperatureValues(Int32 channellow, Int32 channelhigh, Double[] temperatures)
    {
      FUCTemperatureSegment0.SetValue(temperatures[0]);
      FUCTemperatureSegment1.SetValue(temperatures[1]);
      FUCTemperatureSegment2.SetValue(temperatures[2]);
      FUCTemperatureSegment3.SetValue(temperatures[3]);
      FUCTemperatureSegment4.SetValue(temperatures[4]);
      FUCTemperatureSegment5.SetValue(temperatures[5]);
      FUCTemperatureSegment6.SetValue(temperatures[6]);
      FUCTemperatureSegment7.SetValue(temperatures[7]);
    }
    //
    //--------------------------------------------------------------------------
    //  Segment - Management
    //--------------------------------------------------------------------------
    //
 
    //
    //--------------------------------------------------------------------------
    //  Segment - Event
    //--------------------------------------------------------------------------
    //

  }
}
