﻿namespace UCTemperature
{
  partial class CUCTemperatureCommand
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pbxMatrix = new System.Windows.Forms.PictureBox();
      this.lblHeader = new System.Windows.Forms.Label();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label9 = new System.Windows.Forms.Label();
      this.lblStateMTCIndex = new System.Windows.Forms.Label();
      this.lblStateMTControllerText = new System.Windows.Forms.Label();
      this.label19 = new System.Windows.Forms.Label();
      this.btnGetLedSystem = new System.Windows.Forms.Button();
      this.tbxMessages = new System.Windows.Forms.TextBox();
      this.tbxHardwareVersion = new System.Windows.Forms.TextBox();
      this.tbxSoftwareVersion = new System.Windows.Forms.TextBox();
      this.btnGetHelp = new System.Windows.Forms.Button();
      this.btnSwitchLedSystemOn = new System.Windows.Forms.Button();
      this.btnGetHardwareVersion = new System.Windows.Forms.Button();
      this.btnGetSoftwareVersion = new System.Windows.Forms.Button();
      this.btnGetProgramHeader = new System.Windows.Forms.Button();
      this.btnStopProcessExecution = new System.Windows.Forms.Button();
      this.btnSwitchLedSystemOff = new System.Windows.Forms.Button();
      this.btnBlinkLedSystem = new System.Windows.Forms.Button();
      this.btnGetTemperatureChannel = new System.Windows.Forms.Button();
      this.btnGetTemperatureInterval = new System.Windows.Forms.Button();
      this.btnGetAllTemperatures = new System.Windows.Forms.Button();
      this.label6 = new System.Windows.Forms.Label();
      this.nudChannelHigh = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.nudChannelLow = new System.Windows.Forms.NumericUpDown();
      this.btnRepeatAllTemperatures = new System.Windows.Forms.Button();
      this.btnRepeatTemperatureInterval = new System.Windows.Forms.Button();
      this.btnRepeatTemperatureChannel = new System.Windows.Forms.Button();
      this.nudProcessCount = new System.Windows.Forms.NumericUpDown();
      this.nudProcessPeriod = new System.Windows.Forms.NumericUpDown();
      this.btnSetProcessPeriod = new System.Windows.Forms.Button();
      this.btnSetProcessCount = new System.Windows.Forms.Button();
      this.panel1 = new System.Windows.Forms.Panel();
      this.pnlStateLedSystem = new System.Windows.Forms.Panel();
      this.label18 = new System.Windows.Forms.Label();
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix)).BeginInit();
      this.panel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudChannelHigh)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudChannelLow)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessCount)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessPeriod)).BeginInit();
      this.panel1.SuspendLayout();
      this.SuspendLayout();
      // 
      // pbxMatrix
      // 
      this.pbxMatrix.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.pbxMatrix.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pbxMatrix.Location = new System.Drawing.Point(0, 0);
      this.pbxMatrix.Name = "pbxMatrix";
      this.pbxMatrix.Size = new System.Drawing.Size(565, 569);
      this.pbxMatrix.TabIndex = 0;
      this.pbxMatrix.TabStop = false;
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(565, 23);
      this.lblHeader.TabIndex = 1;
      this.lblHeader.Text = "Temperature Command";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // panel2
      // 
      this.panel2.BackColor = System.Drawing.SystemColors.Info;
      this.panel2.Controls.Add(this.label9);
      this.panel2.Controls.Add(this.lblStateMTCIndex);
      this.panel2.Controls.Add(this.lblStateMTControllerText);
      this.panel2.Controls.Add(this.label19);
      this.panel2.Location = new System.Drawing.Point(249, 53);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(311, 27);
      this.panel2.TabIndex = 86;
      // 
      // label9
      // 
      this.label9.Location = new System.Drawing.Point(111, 7);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(8, 13);
      this.label9.TabIndex = 68;
      this.label9.Text = "-";
      this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStateMTCIndex
      // 
      this.lblStateMTCIndex.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStateMTCIndex.Location = new System.Drawing.Point(61, 4);
      this.lblStateMTCIndex.Name = "lblStateMTCIndex";
      this.lblStateMTCIndex.Size = new System.Drawing.Size(47, 19);
      this.lblStateMTCIndex.TabIndex = 67;
      this.lblStateMTCIndex.Text = "- - -";
      this.lblStateMTCIndex.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // lblStateMTControllerText
      // 
      this.lblStateMTControllerText.BackColor = System.Drawing.Color.PeachPuff;
      this.lblStateMTControllerText.Location = new System.Drawing.Point(121, 4);
      this.lblStateMTControllerText.Name = "lblStateMTControllerText";
      this.lblStateMTControllerText.Size = new System.Drawing.Size(184, 19);
      this.lblStateMTControllerText.TabIndex = 66;
      this.lblStateMTControllerText.Text = "- - -";
      this.lblStateMTControllerText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label19
      // 
      this.label19.AutoSize = true;
      this.label19.Location = new System.Drawing.Point(3, 7);
      this.label19.Name = "label19";
      this.label19.Size = new System.Drawing.Size(58, 13);
      this.label19.TabIndex = 65;
      this.label19.Text = "State MTC";
      // 
      // btnGetLedSystem
      // 
      this.btnGetLedSystem.Location = new System.Drawing.Point(4, 412);
      this.btnGetLedSystem.Name = "btnGetLedSystem";
      this.btnGetLedSystem.Size = new System.Drawing.Size(89, 24);
      this.btnGetLedSystem.TabIndex = 82;
      this.btnGetLedSystem.Text = "GetLedSystem";
      this.btnGetLedSystem.UseVisualStyleBackColor = true;
      this.btnGetLedSystem.Click += new System.EventHandler(this.btnGetLedSystem_Click);
      // 
      // tbxMessages
      // 
      this.tbxMessages.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.tbxMessages.Location = new System.Drawing.Point(4, 84);
      this.tbxMessages.Multiline = true;
      this.tbxMessages.Name = "tbxMessages";
      this.tbxMessages.ScrollBars = System.Windows.Forms.ScrollBars.Both;
      this.tbxMessages.Size = new System.Drawing.Size(556, 323);
      this.tbxMessages.TabIndex = 78;
      // 
      // tbxHardwareVersion
      // 
      this.tbxHardwareVersion.Enabled = false;
      this.tbxHardwareVersion.Location = new System.Drawing.Point(125, 28);
      this.tbxHardwareVersion.Name = "tbxHardwareVersion";
      this.tbxHardwareVersion.Size = new System.Drawing.Size(117, 20);
      this.tbxHardwareVersion.TabIndex = 77;
      this.tbxHardwareVersion.Text = "-";
      this.tbxHardwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // tbxSoftwareVersion
      // 
      this.tbxSoftwareVersion.Enabled = false;
      this.tbxSoftwareVersion.Location = new System.Drawing.Point(370, 29);
      this.tbxSoftwareVersion.Name = "tbxSoftwareVersion";
      this.tbxSoftwareVersion.Size = new System.Drawing.Size(85, 20);
      this.tbxSoftwareVersion.TabIndex = 76;
      this.tbxSoftwareVersion.Text = "-";
      this.tbxSoftwareVersion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnGetHelp
      // 
      this.btnGetHelp.Location = new System.Drawing.Point(3, 55);
      this.btnGetHelp.Name = "btnGetHelp";
      this.btnGetHelp.Size = new System.Drawing.Size(117, 23);
      this.btnGetHelp.TabIndex = 71;
      this.btnGetHelp.Text = "GetHelp";
      this.btnGetHelp.UseVisualStyleBackColor = true;
      this.btnGetHelp.Click += new System.EventHandler(this.btnGetHelp_Click);
      // 
      // btnSwitchLedSystemOn
      // 
      this.btnSwitchLedSystemOn.Location = new System.Drawing.Point(96, 412);
      this.btnSwitchLedSystemOn.Name = "btnSwitchLedSystemOn";
      this.btnSwitchLedSystemOn.Size = new System.Drawing.Size(118, 24);
      this.btnSwitchLedSystemOn.TabIndex = 70;
      this.btnSwitchLedSystemOn.Text = "SwitchLedSystemOn";
      this.btnSwitchLedSystemOn.UseVisualStyleBackColor = true;
      this.btnSwitchLedSystemOn.Click += new System.EventHandler(this.btnSwitchLedSystemOn_Click);
      // 
      // btnGetHardwareVersion
      // 
      this.btnGetHardwareVersion.Location = new System.Drawing.Point(3, 26);
      this.btnGetHardwareVersion.Name = "btnGetHardwareVersion";
      this.btnGetHardwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetHardwareVersion.TabIndex = 68;
      this.btnGetHardwareVersion.Text = "GetHardwareVersion";
      this.btnGetHardwareVersion.UseVisualStyleBackColor = true;
      this.btnGetHardwareVersion.Click += new System.EventHandler(this.btnGetHardwareVersion_Click);
      // 
      // btnGetSoftwareVersion
      // 
      this.btnGetSoftwareVersion.Location = new System.Drawing.Point(248, 27);
      this.btnGetSoftwareVersion.Name = "btnGetSoftwareVersion";
      this.btnGetSoftwareVersion.Size = new System.Drawing.Size(117, 23);
      this.btnGetSoftwareVersion.TabIndex = 67;
      this.btnGetSoftwareVersion.Text = "GetSoftwareVersion";
      this.btnGetSoftwareVersion.UseVisualStyleBackColor = true;
      this.btnGetSoftwareVersion.Click += new System.EventHandler(this.btnGetSoftwareVersion_Click);
      // 
      // btnGetProgramHeader
      // 
      this.btnGetProgramHeader.Location = new System.Drawing.Point(125, 55);
      this.btnGetProgramHeader.Name = "btnGetProgramHeader";
      this.btnGetProgramHeader.Size = new System.Drawing.Size(118, 23);
      this.btnGetProgramHeader.TabIndex = 66;
      this.btnGetProgramHeader.Text = "GetProgramHeader";
      this.btnGetProgramHeader.UseVisualStyleBackColor = true;
      this.btnGetProgramHeader.Click += new System.EventHandler(this.btnGetProgramHeader_Click);
      // 
      // btnStopProcessExecution
      // 
      this.btnStopProcessExecution.Location = new System.Drawing.Point(400, 442);
      this.btnStopProcessExecution.Name = "btnStopProcessExecution";
      this.btnStopProcessExecution.Size = new System.Drawing.Size(131, 24);
      this.btnStopProcessExecution.TabIndex = 88;
      this.btnStopProcessExecution.Text = "StopProcessExecution";
      this.btnStopProcessExecution.UseVisualStyleBackColor = true;
      this.btnStopProcessExecution.Click += new System.EventHandler(this.btnStopProcessExecution_Click);
      // 
      // btnSwitchLedSystemOff
      // 
      this.btnSwitchLedSystemOff.Location = new System.Drawing.Point(218, 412);
      this.btnSwitchLedSystemOff.Name = "btnSwitchLedSystemOff";
      this.btnSwitchLedSystemOff.Size = new System.Drawing.Size(117, 24);
      this.btnSwitchLedSystemOff.TabIndex = 92;
      this.btnSwitchLedSystemOff.Text = "SwitchLedSystemOff";
      this.btnSwitchLedSystemOff.UseVisualStyleBackColor = true;
      this.btnSwitchLedSystemOff.Click += new System.EventHandler(this.btnSwitchLedSystemOff_Click);
      // 
      // btnBlinkLedSystem
      // 
      this.btnBlinkLedSystem.Location = new System.Drawing.Point(339, 412);
      this.btnBlinkLedSystem.Name = "btnBlinkLedSystem";
      this.btnBlinkLedSystem.Size = new System.Drawing.Size(97, 24);
      this.btnBlinkLedSystem.TabIndex = 93;
      this.btnBlinkLedSystem.Text = "BlinkLedSystem";
      this.btnBlinkLedSystem.UseVisualStyleBackColor = true;
      this.btnBlinkLedSystem.Click += new System.EventHandler(this.btnBlinkLedSystem_Click);
      // 
      // btnGetTemperatureChannel
      // 
      this.btnGetTemperatureChannel.Location = new System.Drawing.Point(4, 506);
      this.btnGetTemperatureChannel.Name = "btnGetTemperatureChannel";
      this.btnGetTemperatureChannel.Size = new System.Drawing.Size(172, 24);
      this.btnGetTemperatureChannel.TabIndex = 94;
      this.btnGetTemperatureChannel.Text = "GetTemperatureChannel";
      this.btnGetTemperatureChannel.UseVisualStyleBackColor = true;
      this.btnGetTemperatureChannel.Click += new System.EventHandler(this.btnGetTemperatureChannel_Click);
      // 
      // btnGetTemperatureInterval
      // 
      this.btnGetTemperatureInterval.Location = new System.Drawing.Point(181, 506);
      this.btnGetTemperatureInterval.Name = "btnGetTemperatureInterval";
      this.btnGetTemperatureInterval.Size = new System.Drawing.Size(172, 24);
      this.btnGetTemperatureInterval.TabIndex = 95;
      this.btnGetTemperatureInterval.Text = "GetTemperatureInterval";
      this.btnGetTemperatureInterval.UseVisualStyleBackColor = true;
      this.btnGetTemperatureInterval.Click += new System.EventHandler(this.btnGetTemperatureInterval_Click);
      // 
      // btnGetAllTemperatures
      // 
      this.btnGetAllTemperatures.Location = new System.Drawing.Point(358, 506);
      this.btnGetAllTemperatures.Name = "btnGetAllTemperatures";
      this.btnGetAllTemperatures.Size = new System.Drawing.Size(172, 24);
      this.btnGetAllTemperatures.TabIndex = 96;
      this.btnGetAllTemperatures.Text = "GetAllTemperatures";
      this.btnGetAllTemperatures.UseVisualStyleBackColor = true;
      this.btnGetAllTemperatures.Click += new System.EventHandler(this.btnGetAllTemperatures_Click);
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(146, 481);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(97, 13);
      this.label6.TabIndex = 100;
      this.label6.Text = "ChannelIndex High";
      // 
      // nudChannelHigh
      // 
      this.nudChannelHigh.Location = new System.Drawing.Point(244, 479);
      this.nudChannelHigh.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
      this.nudChannelHigh.Name = "nudChannelHigh";
      this.nudChannelHigh.Size = new System.Drawing.Size(34, 20);
      this.nudChannelHigh.TabIndex = 99;
      this.nudChannelHigh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudChannelHigh.Value = new decimal(new int[] {
            7,
            0,
            0,
            0});
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(9, 481);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(95, 13);
      this.label7.TabIndex = 98;
      this.label7.Text = "ChannelIndex Low";
      // 
      // nudChannelLow
      // 
      this.nudChannelLow.Location = new System.Drawing.Point(105, 479);
      this.nudChannelLow.Maximum = new decimal(new int[] {
            7,
            0,
            0,
            0});
      this.nudChannelLow.Name = "nudChannelLow";
      this.nudChannelLow.Size = new System.Drawing.Size(34, 20);
      this.nudChannelLow.TabIndex = 97;
      this.nudChannelLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // btnRepeatAllTemperatures
      // 
      this.btnRepeatAllTemperatures.Location = new System.Drawing.Point(358, 536);
      this.btnRepeatAllTemperatures.Name = "btnRepeatAllTemperatures";
      this.btnRepeatAllTemperatures.Size = new System.Drawing.Size(172, 24);
      this.btnRepeatAllTemperatures.TabIndex = 103;
      this.btnRepeatAllTemperatures.Text = "RepeatAllTemperatures";
      this.btnRepeatAllTemperatures.UseVisualStyleBackColor = true;
      this.btnRepeatAllTemperatures.Click += new System.EventHandler(this.btnRepeatAllTemperatures_Click);
      // 
      // btnRepeatTemperatureInterval
      // 
      this.btnRepeatTemperatureInterval.Location = new System.Drawing.Point(181, 536);
      this.btnRepeatTemperatureInterval.Name = "btnRepeatTemperatureInterval";
      this.btnRepeatTemperatureInterval.Size = new System.Drawing.Size(172, 24);
      this.btnRepeatTemperatureInterval.TabIndex = 102;
      this.btnRepeatTemperatureInterval.Text = "RepeatTemperatureInterval";
      this.btnRepeatTemperatureInterval.UseVisualStyleBackColor = true;
      this.btnRepeatTemperatureInterval.Click += new System.EventHandler(this.btnRepeatTemperatureInterval_Click);
      // 
      // btnRepeatTemperatureChannel
      // 
      this.btnRepeatTemperatureChannel.Location = new System.Drawing.Point(4, 536);
      this.btnRepeatTemperatureChannel.Name = "btnRepeatTemperatureChannel";
      this.btnRepeatTemperatureChannel.Size = new System.Drawing.Size(172, 24);
      this.btnRepeatTemperatureChannel.TabIndex = 101;
      this.btnRepeatTemperatureChannel.Text = "RepeatTemperatureChannel";
      this.btnRepeatTemperatureChannel.UseVisualStyleBackColor = true;
      this.btnRepeatTemperatureChannel.Click += new System.EventHandler(this.btnRepeatTemperatureChannel_Click);
      // 
      // nudProcessCount
      // 
      this.nudProcessCount.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudProcessCount.Location = new System.Drawing.Point(121, 445);
      this.nudProcessCount.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
      this.nudProcessCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudProcessCount.Name = "nudProcessCount";
      this.nudProcessCount.Size = new System.Drawing.Size(73, 20);
      this.nudProcessCount.TabIndex = 74;
      this.nudProcessCount.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudProcessCount.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // nudProcessPeriod
      // 
      this.nudProcessPeriod.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudProcessPeriod.Location = new System.Drawing.Point(326, 445);
      this.nudProcessPeriod.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
      this.nudProcessPeriod.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudProcessPeriod.Name = "nudProcessPeriod";
      this.nudProcessPeriod.Size = new System.Drawing.Size(70, 20);
      this.nudProcessPeriod.TabIndex = 72;
      this.nudProcessPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudProcessPeriod.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      // 
      // btnSetProcessPeriod
      // 
      this.btnSetProcessPeriod.Location = new System.Drawing.Point(199, 442);
      this.btnSetProcessPeriod.Name = "btnSetProcessPeriod";
      this.btnSetProcessPeriod.Size = new System.Drawing.Size(123, 24);
      this.btnSetProcessPeriod.TabIndex = 87;
      this.btnSetProcessPeriod.Text = "SetProcessPeriod [ms]";
      this.btnSetProcessPeriod.UseVisualStyleBackColor = true;
      this.btnSetProcessPeriod.Click += new System.EventHandler(this.btnSetProcessPeriod_Click);
      // 
      // btnSetProcessCount
      // 
      this.btnSetProcessCount.Location = new System.Drawing.Point(4, 442);
      this.btnSetProcessCount.Name = "btnSetProcessCount";
      this.btnSetProcessCount.Size = new System.Drawing.Size(113, 24);
      this.btnSetProcessCount.TabIndex = 69;
      this.btnSetProcessCount.Text = "SetProcessCount [1]";
      this.btnSetProcessCount.UseVisualStyleBackColor = true;
      this.btnSetProcessCount.Click += new System.EventHandler(this.btnSetProcessCount_Click);
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.SystemColors.Info;
      this.panel1.Controls.Add(this.pnlStateLedSystem);
      this.panel1.Controls.Add(this.label18);
      this.panel1.Location = new System.Drawing.Point(440, 411);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(90, 27);
      this.panel1.TabIndex = 104;
      // 
      // pnlStateLedSystem
      // 
      this.pnlStateLedSystem.BackColor = System.Drawing.Color.DarkRed;
      this.pnlStateLedSystem.Location = new System.Drawing.Point(64, 7);
      this.pnlStateLedSystem.Name = "pnlStateLedSystem";
      this.pnlStateLedSystem.Size = new System.Drawing.Size(17, 15);
      this.pnlStateLedSystem.TabIndex = 0;
      // 
      // label18
      // 
      this.label18.AutoSize = true;
      this.label18.Location = new System.Drawing.Point(3, 8);
      this.label18.Name = "label18";
      this.label18.Size = new System.Drawing.Size(59, 13);
      this.label18.TabIndex = 65;
      this.label18.Text = "LedSystem";
      // 
      // CUCTemperatureCommand
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.btnRepeatAllTemperatures);
      this.Controls.Add(this.btnRepeatTemperatureInterval);
      this.Controls.Add(this.btnRepeatTemperatureChannel);
      this.Controls.Add(this.label6);
      this.Controls.Add(this.nudChannelHigh);
      this.Controls.Add(this.label7);
      this.Controls.Add(this.nudChannelLow);
      this.Controls.Add(this.btnGetAllTemperatures);
      this.Controls.Add(this.btnGetTemperatureInterval);
      this.Controls.Add(this.btnGetTemperatureChannel);
      this.Controls.Add(this.btnBlinkLedSystem);
      this.Controls.Add(this.btnSwitchLedSystemOff);
      this.Controls.Add(this.btnStopProcessExecution);
      this.Controls.Add(this.btnSetProcessPeriod);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.btnGetLedSystem);
      this.Controls.Add(this.tbxMessages);
      this.Controls.Add(this.tbxHardwareVersion);
      this.Controls.Add(this.tbxSoftwareVersion);
      this.Controls.Add(this.nudProcessCount);
      this.Controls.Add(this.nudProcessPeriod);
      this.Controls.Add(this.btnGetHelp);
      this.Controls.Add(this.btnSwitchLedSystemOn);
      this.Controls.Add(this.btnSetProcessCount);
      this.Controls.Add(this.btnGetHardwareVersion);
      this.Controls.Add(this.btnGetSoftwareVersion);
      this.Controls.Add(this.btnGetProgramHeader);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pbxMatrix);
      this.Name = "CUCTemperatureCommand";
      this.Size = new System.Drawing.Size(565, 569);
      ((System.ComponentModel.ISupportInitialize)(this.pbxMatrix)).EndInit();
      this.panel2.ResumeLayout(false);
      this.panel2.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudChannelHigh)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudChannelLow)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessCount)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudProcessPeriod)).EndInit();
      this.panel1.ResumeLayout(false);
      this.panel1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.PictureBox pbxMatrix;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label lblStateMTCIndex;
    private System.Windows.Forms.Label lblStateMTControllerText;
    private System.Windows.Forms.Label label19;
    private System.Windows.Forms.Button btnGetLedSystem;
    private System.Windows.Forms.TextBox tbxMessages;
    private System.Windows.Forms.TextBox tbxHardwareVersion;
    private System.Windows.Forms.TextBox tbxSoftwareVersion;
    private System.Windows.Forms.Button btnGetHelp;
    private System.Windows.Forms.Button btnSwitchLedSystemOn;
    private System.Windows.Forms.Button btnGetHardwareVersion;
    private System.Windows.Forms.Button btnGetSoftwareVersion;
    private System.Windows.Forms.Button btnGetProgramHeader;
    private System.Windows.Forms.Button btnStopProcessExecution;
    private System.Windows.Forms.Button btnSwitchLedSystemOff;
    private System.Windows.Forms.Button btnBlinkLedSystem;
    private System.Windows.Forms.Button btnGetTemperatureChannel;
    private System.Windows.Forms.Button btnGetTemperatureInterval;
    private System.Windows.Forms.Button btnGetAllTemperatures;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.NumericUpDown nudChannelHigh;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown nudChannelLow;
    private System.Windows.Forms.Button btnRepeatAllTemperatures;
    private System.Windows.Forms.Button btnRepeatTemperatureInterval;
    private System.Windows.Forms.Button btnRepeatTemperatureChannel;
    private System.Windows.Forms.NumericUpDown nudProcessCount;
    private System.Windows.Forms.NumericUpDown nudProcessPeriod;
    private System.Windows.Forms.Button btnSetProcessPeriod;
    private System.Windows.Forms.Button btnSetProcessCount;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Panel pnlStateLedSystem;
    private System.Windows.Forms.Label label18;
  }
}
