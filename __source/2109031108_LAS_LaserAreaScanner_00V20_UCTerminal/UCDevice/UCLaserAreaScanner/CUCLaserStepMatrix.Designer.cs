﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserStepMatrix
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlImage = new System.Windows.Forms.Panel();
      this.pbxImage = new System.Windows.Forms.PictureBox();
      this.pnlImage.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlImage
      // 
      this.pnlImage.AutoScroll = true;
      this.pnlImage.BackColor = System.Drawing.Color.DarkRed;
      this.pnlImage.Controls.Add(this.pbxImage);
      this.pnlImage.Dock = System.Windows.Forms.DockStyle.Fill;
      this.pnlImage.Location = new System.Drawing.Point(0, 0);
      this.pnlImage.Name = "pnlImage";
      this.pnlImage.Size = new System.Drawing.Size(604, 341);
      this.pnlImage.TabIndex = 3;
      // 
      // pbxImage
      // 
      this.pbxImage.BackColor = System.Drawing.Color.Silver;
      this.pbxImage.Location = new System.Drawing.Point(0, 0);
      this.pbxImage.Name = "pbxImage";
      this.pbxImage.Size = new System.Drawing.Size(554, 284);
      this.pbxImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pbxImage.TabIndex = 5;
      this.pbxImage.TabStop = false;
      // 
      // CUCLaserStepMatrix
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.pnlImage);
      this.Name = "CUCLaserStepMatrix";
      this.Size = new System.Drawing.Size(604, 341);
      this.pnlImage.ResumeLayout(false);
      this.pnlImage.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pbxImage)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlImage;
    private System.Windows.Forms.PictureBox pbxImage;
  }
}
