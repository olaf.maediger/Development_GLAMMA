﻿namespace UCLaserAreaScanner
{
  partial class CUCLaserAreaScannerMatrix
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.pnlBottom = new System.Windows.Forms.Panel();
      this.btnLoadLaserSteptable = new System.Windows.Forms.Button();
      this.btnAbortLaserImage = new System.Windows.Forms.Button();
      this.btnPulseLaserImage = new System.Windows.Forms.Button();
      this.DialogLoadLaserSteptable = new System.Windows.Forms.OpenFileDialog();
      this.lblHeader = new System.Windows.Forms.Label();
      this.pnlLeft = new System.Windows.Forms.Panel();
      this.label10 = new System.Windows.Forms.Label();
      this.nudYMaximum = new System.Windows.Forms.NumericUpDown();
      this.label11 = new System.Windows.Forms.Label();
      this.nudYMinimum = new System.Windows.Forms.NumericUpDown();
      this.label8 = new System.Windows.Forms.Label();
      this.nudXMaximum = new System.Windows.Forms.NumericUpDown();
      this.label9 = new System.Windows.Forms.Label();
      this.nudXMinimum = new System.Windows.Forms.NumericUpDown();
      this.label7 = new System.Windows.Forms.Label();
      this.nudPulsePeriodms = new System.Windows.Forms.NumericUpDown();
      this.nudPulsesMaximum = new System.Windows.Forms.NumericUpDown();
      this.nudPulsesMinimum = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.nudScaleFactor = new System.Windows.Forms.NumericUpDown();
      this.cbxZoom = new System.Windows.Forms.CheckBox();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.FUCLaserStepMatrix = new UCLaserAreaScanner.CUCLaserStepMatrix();
      this.pnlBottom.SuspendLayout();
      this.pnlLeft.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriodms)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsesMaximum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsesMinimum)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleFactor)).BeginInit();
      this.SuspendLayout();
      // 
      // pnlBottom
      // 
      this.pnlBottom.Controls.Add(this.btnLoadLaserSteptable);
      this.pnlBottom.Controls.Add(this.btnAbortLaserImage);
      this.pnlBottom.Controls.Add(this.btnPulseLaserImage);
      this.pnlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
      this.pnlBottom.Location = new System.Drawing.Point(0, 466);
      this.pnlBottom.Name = "pnlBottom";
      this.pnlBottom.Size = new System.Drawing.Size(715, 37);
      this.pnlBottom.TabIndex = 6;
      // 
      // btnLoadLaserSteptable
      // 
      this.btnLoadLaserSteptable.Location = new System.Drawing.Point(7, 6);
      this.btnLoadLaserSteptable.Name = "btnLoadLaserSteptable";
      this.btnLoadLaserSteptable.Size = new System.Drawing.Size(106, 25);
      this.btnLoadLaserSteptable.TabIndex = 146;
      this.btnLoadLaserSteptable.Text = "Load Steptable";
      this.btnLoadLaserSteptable.UseVisualStyleBackColor = true;
      this.btnLoadLaserSteptable.Click += new System.EventHandler(this.btnLoadLaserSteptable_Click);
      // 
      // btnAbortLaserImage
      // 
      this.btnAbortLaserImage.Location = new System.Drawing.Point(231, 6);
      this.btnAbortLaserImage.Name = "btnAbortLaserImage";
      this.btnAbortLaserImage.Size = new System.Drawing.Size(106, 25);
      this.btnAbortLaserImage.TabIndex = 145;
      this.btnAbortLaserImage.Text = "Abort LaserImage";
      this.btnAbortLaserImage.UseVisualStyleBackColor = true;
      this.btnAbortLaserImage.Click += new System.EventHandler(this.btnAbortLaserImage_Click);
      // 
      // btnPulseLaserImage
      // 
      this.btnPulseLaserImage.Location = new System.Drawing.Point(119, 6);
      this.btnPulseLaserImage.Name = "btnPulseLaserImage";
      this.btnPulseLaserImage.Size = new System.Drawing.Size(106, 25);
      this.btnPulseLaserImage.TabIndex = 144;
      this.btnPulseLaserImage.Text = "Pulse LaserImage";
      this.btnPulseLaserImage.UseVisualStyleBackColor = true;
      this.btnPulseLaserImage.Click += new System.EventHandler(this.btnPulseLaserImage_Click);
      // 
      // DialogLoadLaserSteptable
      // 
      this.DialogLoadLaserSteptable.Filter = "Steptable (*.stp)|*.stp|All Files (*.*)|*.*";
      this.DialogLoadLaserSteptable.Title = "Load Steptable";
      // 
      // lblHeader
      // 
      this.lblHeader.BackColor = System.Drawing.SystemColors.Info;
      this.lblHeader.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblHeader.Location = new System.Drawing.Point(0, 0);
      this.lblHeader.Name = "lblHeader";
      this.lblHeader.Size = new System.Drawing.Size(715, 23);
      this.lblHeader.TabIndex = 107;
      this.lblHeader.Text = "LaserAreaScanner - Matrix";
      this.lblHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pnlLeft
      // 
      this.pnlLeft.BackColor = System.Drawing.Color.Gainsboro;
      this.pnlLeft.Controls.Add(this.label10);
      this.pnlLeft.Controls.Add(this.nudYMaximum);
      this.pnlLeft.Controls.Add(this.label11);
      this.pnlLeft.Controls.Add(this.nudYMinimum);
      this.pnlLeft.Controls.Add(this.label8);
      this.pnlLeft.Controls.Add(this.nudXMaximum);
      this.pnlLeft.Controls.Add(this.label9);
      this.pnlLeft.Controls.Add(this.nudXMinimum);
      this.pnlLeft.Controls.Add(this.label7);
      this.pnlLeft.Controls.Add(this.nudPulsePeriodms);
      this.pnlLeft.Controls.Add(this.nudPulsesMaximum);
      this.pnlLeft.Controls.Add(this.nudPulsesMinimum);
      this.pnlLeft.Controls.Add(this.label1);
      this.pnlLeft.Controls.Add(this.nudScaleFactor);
      this.pnlLeft.Controls.Add(this.cbxZoom);
      this.pnlLeft.Controls.Add(this.label5);
      this.pnlLeft.Controls.Add(this.label4);
      this.pnlLeft.Dock = System.Windows.Forms.DockStyle.Left;
      this.pnlLeft.Location = new System.Drawing.Point(0, 23);
      this.pnlLeft.Name = "pnlLeft";
      this.pnlLeft.Size = new System.Drawing.Size(147, 443);
      this.pnlLeft.TabIndex = 108;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(11, 136);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(58, 13);
      this.label10.TabIndex = 33;
      this.label10.Text = "YMaximum";
      // 
      // nudYMaximum
      // 
      this.nudYMaximum.Location = new System.Drawing.Point(90, 133);
      this.nudYMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudYMaximum.Name = "nudYMaximum";
      this.nudYMaximum.ReadOnly = true;
      this.nudYMaximum.Size = new System.Drawing.Size(49, 20);
      this.nudYMaximum.TabIndex = 32;
      this.nudYMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudYMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(11, 114);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(55, 13);
      this.label11.TabIndex = 31;
      this.label11.Text = "YMinimum";
      // 
      // nudYMinimum
      // 
      this.nudYMinimum.Location = new System.Drawing.Point(90, 111);
      this.nudYMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudYMinimum.Name = "nudYMinimum";
      this.nudYMinimum.ReadOnly = true;
      this.nudYMinimum.Size = new System.Drawing.Size(49, 20);
      this.nudYMinimum.TabIndex = 30;
      this.nudYMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudYMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(11, 89);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(58, 13);
      this.label8.TabIndex = 29;
      this.label8.Text = "XMaximum";
      // 
      // nudXMaximum
      // 
      this.nudXMaximum.Location = new System.Drawing.Point(90, 86);
      this.nudXMaximum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudXMaximum.Name = "nudXMaximum";
      this.nudXMaximum.ReadOnly = true;
      this.nudXMaximum.Size = new System.Drawing.Size(49, 20);
      this.nudXMaximum.TabIndex = 28;
      this.nudXMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudXMaximum.Value = new decimal(new int[] {
            2200,
            0,
            0,
            0});
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(11, 67);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(55, 13);
      this.label9.TabIndex = 27;
      this.label9.Text = "XMinimum";
      // 
      // nudXMinimum
      // 
      this.nudXMinimum.Location = new System.Drawing.Point(90, 64);
      this.nudXMinimum.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudXMinimum.Name = "nudXMinimum";
      this.nudXMinimum.ReadOnly = true;
      this.nudXMinimum.Size = new System.Drawing.Size(49, 20);
      this.nudXMinimum.TabIndex = 26;
      this.nudXMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudXMinimum.Value = new decimal(new int[] {
            1800,
            0,
            0,
            0});
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(11, 161);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(59, 13);
      this.label7.TabIndex = 25;
      this.label7.Text = "Period [ms]";
      // 
      // nudPulsePeriodms
      // 
      this.nudPulsePeriodms.Location = new System.Drawing.Point(90, 158);
      this.nudPulsePeriodms.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPulsePeriodms.Name = "nudPulsePeriodms";
      this.nudPulsePeriodms.ReadOnly = true;
      this.nudPulsePeriodms.Size = new System.Drawing.Size(49, 20);
      this.nudPulsePeriodms.TabIndex = 24;
      this.nudPulsePeriodms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulsePeriodms.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      // 
      // nudPulsesMaximum
      // 
      this.nudPulsesMaximum.Location = new System.Drawing.Point(90, 205);
      this.nudPulsesMaximum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPulsesMaximum.Name = "nudPulsesMaximum";
      this.nudPulsesMaximum.ReadOnly = true;
      this.nudPulsesMaximum.Size = new System.Drawing.Size(49, 20);
      this.nudPulsesMaximum.TabIndex = 22;
      this.nudPulsesMaximum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPulsesMaximum.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      // 
      // nudPulsesMinimum
      // 
      this.nudPulsesMinimum.Location = new System.Drawing.Point(90, 183);
      this.nudPulsesMinimum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudPulsesMinimum.Name = "nudPulsesMinimum";
      this.nudPulsesMinimum.ReadOnly = true;
      this.nudPulsesMinimum.Size = new System.Drawing.Size(49, 20);
      this.nudPulsesMinimum.TabIndex = 20;
      this.nudPulsesMinimum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(9, 35);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(64, 13);
      this.label1.TabIndex = 5;
      this.label1.Text = "ScaleFactor";
      // 
      // nudScaleFactor
      // 
      this.nudScaleFactor.Location = new System.Drawing.Point(76, 32);
      this.nudScaleFactor.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.nudScaleFactor.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudScaleFactor.Name = "nudScaleFactor";
      this.nudScaleFactor.Size = new System.Drawing.Size(49, 20);
      this.nudScaleFactor.TabIndex = 1;
      this.nudScaleFactor.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudScaleFactor.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.nudScaleFactor.ValueChanged += new System.EventHandler(this.nudScaleFactor_ValueChanged);
      // 
      // cbxZoom
      // 
      this.cbxZoom.AutoSize = true;
      this.cbxZoom.Location = new System.Drawing.Point(12, 10);
      this.cbxZoom.Name = "cbxZoom";
      this.cbxZoom.Size = new System.Drawing.Size(110, 17);
      this.cbxZoom.TabIndex = 2;
      this.cbxZoom.Text = "ZoomFit / Original";
      this.cbxZoom.UseVisualStyleBackColor = true;
      this.cbxZoom.Click += new System.EventHandler(this.cbxZoom_CheckedChanged);
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(11, 208);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(82, 13);
      this.label5.TabIndex = 23;
      this.label5.Text = "PulsesMaximum";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(11, 186);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(79, 13);
      this.label4.TabIndex = 21;
      this.label4.Text = "PulsesMinimum";
      // 
      // FUCLaserStepMatrix
      // 
      this.FUCLaserStepMatrix.Dock = System.Windows.Forms.DockStyle.Fill;
      this.FUCLaserStepMatrix.Location = new System.Drawing.Point(147, 23);
      this.FUCLaserStepMatrix.Name = "FUCLaserStepMatrix";
      this.FUCLaserStepMatrix.Size = new System.Drawing.Size(568, 443);
      this.FUCLaserStepMatrix.TabIndex = 109;
      // 
      // CUCLaserAreaScannerMatrix
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.Color.Transparent;
      this.Controls.Add(this.FUCLaserStepMatrix);
      this.Controls.Add(this.pnlLeft);
      this.Controls.Add(this.lblHeader);
      this.Controls.Add(this.pnlBottom);
      this.Name = "CUCLaserAreaScannerMatrix";
      this.Size = new System.Drawing.Size(715, 503);
      this.pnlBottom.ResumeLayout(false);
      this.pnlLeft.ResumeLayout(false);
      this.pnlLeft.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudYMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudXMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsePeriodms)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsesMaximum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudPulsesMinimum)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.nudScaleFactor)).EndInit();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.Panel pnlBottom;
    private System.Windows.Forms.Button btnLoadLaserSteptable;
    private System.Windows.Forms.Button btnAbortLaserImage;
    private System.Windows.Forms.Button btnPulseLaserImage;
    private System.Windows.Forms.OpenFileDialog DialogLoadLaserSteptable;
    private System.Windows.Forms.Label lblHeader;
    private System.Windows.Forms.Panel pnlLeft;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.NumericUpDown nudYMaximum;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.NumericUpDown nudYMinimum;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.NumericUpDown nudXMaximum;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.NumericUpDown nudXMinimum;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown nudPulsePeriodms;
    private System.Windows.Forms.NumericUpDown nudPulsesMaximum;
    private System.Windows.Forms.NumericUpDown nudPulsesMinimum;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.NumericUpDown nudScaleFactor;
    private System.Windows.Forms.CheckBox cbxZoom;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private CUCLaserStepMatrix FUCLaserStepMatrix;
  }
}
