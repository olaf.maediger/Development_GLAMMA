﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UCNotifier
{
  public partial class CDialogDeviceProductManager : Form
  {
    private CUCDeviceManager FUCDeviceManager;

    public CDialogDeviceProductManager()
    {
      InitializeComponent();
    }

    public void SetUCDeviceManager(CUCDeviceManager ucmanager)
    {
      FUCDeviceManager = ucmanager;
      this.Controls.Add(FUCDeviceManager); // ??? is this correct ???
      FUCDeviceManager.Dock = DockStyle.Fill;
    }

    private void CDialogDeviceManager_FormClosing(object sender, FormClosingEventArgs e)
    {
      this.Controls.Remove(FUCDeviceManager);
    }

    private void CDialogDeviceManager_Load(object sender, EventArgs e)
    {
      FUCDeviceManager.ForceResize();
    }

  }
}
