﻿namespace UCNotifier
{
	partial class CUCNotifierBase
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
      this.components = new System.ComponentModel.Container();
      this.DialogLoadFromFile = new System.Windows.Forms.OpenFileDialog();
      this.DialogSaveToFile = new System.Windows.Forms.SaveFileDialog();
      this.mitDeleteAll = new System.Windows.Forms.ToolStripMenuItem();
      this.cmsNotifier = new System.Windows.Forms.ContextMenuStrip(this.components);
      this.mitLoadFromFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitSaveToFile = new System.Windows.Forms.ToolStripMenuItem();
      this.mitCopyToClipboard = new System.Windows.Forms.ToolStripMenuItem();
      this.lbxNotifier = new System.Windows.Forms.ListBox();
      this.cmsNotifier.SuspendLayout();
      this.SuspendLayout();
      // 
      // DialogLoadFromFile
      // 
      this.DialogLoadFromFile.DefaultExt = "txt";
      this.DialogLoadFromFile.FileName = "WriteStream";
      this.DialogLoadFromFile.Filter = "Text Files (*.txt)|*.txt|All files (*.*)|*.*";
      this.DialogLoadFromFile.Title = "Load StreamData from File";
      // 
      // DialogSaveToFile
      // 
      this.DialogSaveToFile.DefaultExt = "txt";
      this.DialogSaveToFile.FileName = "StreamData";
      this.DialogSaveToFile.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
      this.DialogSaveToFile.Title = "Save StreamData to File";
      // 
      // mitDeleteAll
      // 
      this.mitDeleteAll.Name = "mitDeleteAll";
      this.mitDeleteAll.Size = new System.Drawing.Size(171, 22);
      this.mitDeleteAll.Text = "Delete All";
      this.mitDeleteAll.Click += new System.EventHandler(this.mitDeleteAll_Click);
      // 
      // cmsNotifier
      // 
      this.cmsNotifier.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mitLoadFromFile,
            this.mitSaveToFile,
            this.mitCopyToClipboard,
            this.mitDeleteAll});
      this.cmsNotifier.Name = "cmsWriteStream";
      this.cmsNotifier.Size = new System.Drawing.Size(172, 92);
      // 
      // mitLoadFromFile
      // 
      this.mitLoadFromFile.Name = "mitLoadFromFile";
      this.mitLoadFromFile.Size = new System.Drawing.Size(171, 22);
      // 
      // mitSaveToFile
      // 
      this.mitSaveToFile.Name = "mitSaveToFile";
      this.mitSaveToFile.Size = new System.Drawing.Size(171, 22);
      // 
      // mitCopyToClipboard
      // 
      this.mitCopyToClipboard.Name = "mitCopyToClipboard";
      this.mitCopyToClipboard.Size = new System.Drawing.Size(171, 22);
      this.mitCopyToClipboard.Text = "Copy to Clipboard";
      this.mitCopyToClipboard.Click += new System.EventHandler(this.mitCopyToClipboard_Click);
      // 
      // lbxNotifier
      // 
      this.lbxNotifier.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.lbxNotifier.FormattingEnabled = true;
      this.lbxNotifier.HorizontalScrollbar = true;
      this.lbxNotifier.IntegralHeight = false;
      this.lbxNotifier.ItemHeight = 16;
      this.lbxNotifier.Location = new System.Drawing.Point(38, 69);
      this.lbxNotifier.Name = "lbxNotifier";
      this.lbxNotifier.Size = new System.Drawing.Size(280, 294);
      this.lbxNotifier.TabIndex = 1;
      // 
      // CUCNotifierBase
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.lbxNotifier);
      this.Name = "CUCNotifierBase";
      this.Size = new System.Drawing.Size(354, 416);
      this.cmsNotifier.ResumeLayout(false);
      this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.OpenFileDialog DialogLoadFromFile;
		private System.Windows.Forms.SaveFileDialog DialogSaveToFile;
		private System.Windows.Forms.ToolStripMenuItem mitDeleteAll;
		private System.Windows.Forms.ContextMenuStrip cmsNotifier;
		private System.Windows.Forms.ToolStripMenuItem mitLoadFromFile;
		private System.Windows.Forms.ToolStripMenuItem mitSaveToFile;
		private System.Windows.Forms.ToolStripMenuItem mitCopyToClipboard;
		private System.Windows.Forms.ListBox lbxNotifier;
	}
}
