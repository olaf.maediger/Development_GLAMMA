﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
//
using TextFile;
//
namespace BitmapImage
{
  public class CBitmap
  {
    const Int32 BITS_PER_PIXEL = 32;
    const PixelFormat PIXEL_FORMAT = PixelFormat.Format32bppArgb;
    const Int32 INIT_WIDTH = 5;
    const Int32 INIT_HEIGHT = 5;
    //
    public const Int32 BITMAP_MAXIMUM_WIDTH = 2000;
    public const Int32 BITMAP_MAXIMUM_HEIGHT = 2000;
    //
    protected System.Drawing.Bitmap FBitmap;
    //
    public CBitmap(Int32 width, Int32 height)
    {
      FBitmap = new System.Drawing.Bitmap(width, height, PIXEL_FORMAT);
    }
    public CBitmap(Bitmap bitmap)
    {
      FBitmap = new System.Drawing.Bitmap(bitmap.Width, bitmap.Height, PIXEL_FORMAT);
      Graphics G = Graphics.FromImage(FBitmap);
      Rectangle DR = new Rectangle(0, 0, bitmap.Width, bitmap.Height);
      //G.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.Low;
      ////////G.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.NearestNeighbor;
      //G.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
      ////////G.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
      ////////G.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.None;
      //G.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.Half;
      //G.PixelOffsetMode = System.Drawing.Drawing2D.PixelOffsetMode.HighQuality;
      //G.CompositingMode = System.Drawing.Drawing2D.CompositingMode.SourceCopy;
      //G.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighSpeed;
      G.DrawImage(bitmap, DR);
    }

    public CBitmap(String filename)
    {
      if (File.Exists(filename))
      {
        FBitmap = new System.Drawing.Bitmap(filename);
        switch (FBitmap.PixelFormat)
        {
          case PixelFormat.Format24bppRgb:
            FBitmap = CBitmap.Convert24RgbTo32Argb(FBitmap);
            break;
        }
      }
      else
      {
        FBitmap = new System.Drawing.Bitmap(INIT_WIDTH, INIT_HEIGHT, PIXEL_FORMAT);
      }
    }

    public unsafe CBitmap(Bitmap bitmapsource, ref UInt16 scalefactor)
    {
      try
      {
        Int32 BSW = bitmapsource.Width;
        Int32 BSH = bitmapsource.Height;
        //
        Int32 BDW = Math.Min(BITMAP_MAXIMUM_WIDTH, scalefactor * BSW);
        Int32 BDH = Math.Min(BITMAP_MAXIMUM_HEIGHT, scalefactor * BSH);
        scalefactor = (UInt16)(BDW / BSW);
        scalefactor = (UInt16)(BDH / BSH);
        Int32 SFX = scalefactor;
        Int32 SFY = scalefactor;
        //
        PixelFormat PFS = bitmapsource.PixelFormat; // Argb32
        Rectangle RS = new Rectangle(0, 0, BSW, BSH);
        BitmapData BDS = bitmapsource.LockBits(RS, ImageLockMode.ReadOnly, PFS);
        //
        FBitmap = new Bitmap(SFX * BSW, SFY * BSH, PIXEL_FORMAT);
        BDW = FBitmap.Width;
        BDH = FBitmap.Height;
        PixelFormat PFD = FBitmap.PixelFormat; // Argb32
        Rectangle RD = new Rectangle(0, 0, BDW, BDH);
        BitmapData BDD = FBitmap.LockBits(RD, ImageLockMode.WriteOnly, PFD);
        //
        Int32 YSI = 0;
        while (YSI < BSH)
        {
          for (Int32 YDI = 0; YDI < SFY; YDI++)
          {
            Byte* PBS = (Byte*)(BDS.Scan0 + YSI * BDS.Stride);
            Byte* PBD = (Byte*)(BDD.Scan0 + (YSI * SFY + YDI) * BDD.Stride);
            Int32 XSI = 0;
            while (XSI < BSW)
            {
              for (Int32 XDI = 0; XDI < SFX; XDI++)
              { // B
                *PBD = *PBS;
                PBS++;
                PBD++;
                // G
                *PBD = *PBS;
                PBS++;
                PBD++;
                // R
                *PBD = *PBS;
                PBS++;
                PBD++;
                // A
                *PBD = 0xFF;
                PBS++;
                PBD++;
                //
                PBS -= 4;
              }
              PBS += 4;
              XSI++;
            }
          }
          YSI++;
        }
        bitmapsource.UnlockBits(BDS);
        FBitmap.UnlockBits(BDD);
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
      }
    }

    public Bitmap GetBitmap()
    {
      return FBitmap;
    }

    public Int32 GetWidth()
    {
      return FBitmap.Width;
    }
    public Int32 GetHeight()
    {
      return FBitmap.Height;
    }

    protected unsafe Boolean SetBitmapColor(Byte red, Byte green, Byte blue)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadWrite, PF);
        Int32 VectorSize = BD.Stride * BD.Height;
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { // B
            *PBD = blue;
            PBD++;
            // G
            *PBD = green;
            PBD++;
            // R
            *PBD = red;
            PBD++;
            // A
            *PBD = 0xFF;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    protected unsafe Boolean SetBitmapGrey(Byte grey)
    {
      return SetBitmapColor(grey, grey, grey);
    }

    public unsafe Boolean ConvertGreyScale(Boolean inverted)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          {
            //Byte CG = (Byte)((*(0 + PBD)) * 0.114f + (*(1 + PBD)) * 0.587f + (*(2 + PBD)) * 0.299f);
            Byte CG = (Byte)((*(0 + PBD)) * 0.333f + (*(1 + PBD)) * 0.333f + (*(2 + PBD)) * 0.333f);
            if (inverted)
            {
              CG = (Byte)((Byte)0xFF - CG);
            }
            // B
            *PBD = CG;
            PBD++;
            // G
            *PBD = CG;
            PBD++;
            // R
            *PBD = CG;
            PBD++;
            // A
            *PBD = 0xFF;// CG;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    public unsafe Boolean ConvertMonoChrome(Boolean inverted,
                                            Int32 threshold,
                                            Color colorlow,
                                            Color colorhigh)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        Byte CLR = colorlow.R;
        Byte CLG = colorlow.G;
        Byte CLB = colorlow.B;
        Byte CHR = colorhigh.R;
        Byte CHG = colorhigh.G;
        Byte CHB = colorhigh.B;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadWrite, PF);
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          {
            //            Byte CG = (Byte)((*(0 + PBD)) * 0.114f + (*(1 + PBD)) * 0.587f + (*(2 + PBD)) * 0.299f);
            Byte CG = (Byte)((*(0 + PBD)) * 0.333f + (*(1 + PBD)) * 0.333f + (*(2 + PBD)) * 0.333f);
            if (inverted)
            {
              CG = (Byte)((Byte)0xFF - CG);
            }
            if (threshold <= CG)
            { // B
              *PBD = CHB;
              PBD++;
              // G
              *PBD = CHG;
              PBD++;
              // R
              *PBD = CHR;
              PBD++;
            }
            else
            { // B
              *PBD = CLB;
              PBD++;
              // G
              *PBD = CLG;
              PBD++;
              // R
              *PBD = CLR;
              PBD++;
            }
            // A
            *PBD = 0xFF;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    public unsafe Byte[] GetVectorDataGrey()
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 VectorSize = 1 * BW * BH;
        Byte[] VectorData = new Byte[VectorSize];
        Byte CG = 0;
        Int32 VI = 0;
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { // B
            CG = *PBD;
            PBD++;
            // G
            CG += *PBD;
            PBD++;
            // R
            CG += *PBD;
            PBD++;
            // A
            PBD++;
            //
            VectorData[VI] = (Byte)(CG / 3);
            VI++;
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return VectorData;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return null;
      }
    }

    public unsafe Boolean SetVectorDataGrey(Byte[] vectordata)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 VectorSize = vectordata.Length;
        Int32 VI = 0;
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { // B
            *PBD = vectordata[VI];
            PBD++;
            // G
            *PBD = vectordata[VI];
            PBD++;
            // R
            *PBD = vectordata[VI];
            PBD++;
            // A
            *PBD = 0xFF;
            PBD++;
            //
            XI++;
            VI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    public unsafe Boolean SetVectorDataRgb24(Byte[] vectordata)
    {
      try
      {
        Int32 BW = FBitmap.Width;
        Int32 BH = FBitmap.Height;
        PixelFormat PF = FBitmap.PixelFormat;
        Rectangle R = new Rectangle(0, 0, BW, BH);
        BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
        Int32 VectorSize = vectordata.Length;
        Int32 VI = 0;
        Int32 YI = 0;
        while (YI < BH)
        {
          Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
          //
          Int32 XI = 0;
          while (XI < BW)
          { // B
            *PBD = vectordata[VI];
            PBD++;
            VI++;
            // G
            *PBD = vectordata[VI];
            PBD++;
            VI++;
            // R
            *PBD = vectordata[VI];
            PBD++;
            VI++;
            // A
            *PBD = 0xFF;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        FBitmap.UnlockBits(BD);
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    public static unsafe Bitmap Convert24RgbTo32Argb(Bitmap bitmapsource)
    {
      try
      {
        Int32 BSW = bitmapsource.Width;
        Int32 BSH = bitmapsource.Height;
        PixelFormat PFS = bitmapsource.PixelFormat; // Rgb24
        Rectangle RS = new Rectangle(0, 0, BSW, BSH);
        BitmapData BDS = bitmapsource.LockBits(RS, ImageLockMode.ReadOnly, PFS);
        //
        Bitmap BitmapTarget = new Bitmap(BSW, BSH, PIXEL_FORMAT);
        Int32 BDW = BitmapTarget.Width;
        Int32 BDH = BitmapTarget.Height;
        PixelFormat PFD = BitmapTarget.PixelFormat; // Argb32
        Rectangle RD = new Rectangle(0, 0, BDW, BDH);
        BitmapData BDD = BitmapTarget.LockBits(RD, ImageLockMode.WriteOnly, PFD);
        //
        Int32 YI = 0;
        while (YI < BSH)
        {
          Byte* PBS = (Byte*)(BDS.Scan0 + YI * BDS.Stride);
          Byte* PBD = (Byte*)(BDD.Scan0 + YI * BDD.Stride);
          //
          Int32 XI = 0;
          while (XI < BSW)
          { // B
            *PBD = *PBS;
            PBS++;
            PBD++;
            // G
            *PBD = *PBS;
            PBS++;
            PBD++;
            // R
            *PBD = *PBS;
            PBS++;
            PBD++;
            // A
            *PBD = 0xFF;
            // NC PBS++;
            PBD++;
            //
            XI++;
          }
          YI++;
        }
        bitmapsource.UnlockBits(BDS);
        BitmapTarget.UnlockBits(BDD);
        //
        return BitmapTarget;
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
        return null;
      }
    }


    public unsafe Boolean CompressFourToOne()
    {
      try
      { // Source
        Int32 BWS = FBitmap.Width;
        Int32 BHS = FBitmap.Height;
        PixelFormat PFS = FBitmap.PixelFormat;
        Rectangle RS = new Rectangle(0, 0, BWS, BHS);
        BitmapData BDS = FBitmap.LockBits(RS, ImageLockMode.ReadOnly, PFS);
        // Target
        Int32 BWT = BWS / 2;
        Int32 BHT = BHS / 2;
        PixelFormat PFT = FBitmap.PixelFormat;
        Bitmap BT = new Bitmap(BWT, BHT, PFT);
        Rectangle RT = new Rectangle(0, 0, BWT, BHT);
        BitmapData BDT = BT.LockBits(RT, ImageLockMode.WriteOnly, PFT);
        //
        int YIS = 0;
        Int32 YIT = 0;
        while (YIT < BHT)
        { // Source
          Byte* PBS0 = (Byte*)(BDS.Scan0 + YIS * BDS.Stride);
          Byte* PBS1 = (Byte*)(BDS.Scan0 + (1 + YIS) * BDS.Stride);
          // Target
          Byte* PBT = (Byte*)(BDT.Scan0 + YIT * BDT.Stride);
          //
          Int32 XIT = 0;
          while (XIT < BWT)
          { // Source
            // B
            UInt16 P00B = *(PBS0 + 0);
            UInt16 P10B = *(PBS1 + 0);
            UInt16 P01B = *(PBS0 + 4);
            UInt16 P11B = *(PBS1 + 4);
            // G
            UInt16 P00G = *(PBS0 + 1);
            UInt16 P10G = *(PBS1 + 1);
            UInt16 P01G = *(PBS0 + 5);
            UInt16 P11G = *(PBS1 + 5);
            // R
            UInt16 P00R = *(PBS0 + 2);
            UInt16 P10R = *(PBS1 + 2);
            UInt16 P01R = *(PBS0 + 6);
            UInt16 P11R = *(PBS1 + 6);
            // A
            UInt16 P00A = *(PBS0 + 3);
            UInt16 P10A = *(PBS1 + 3);
            UInt16 P01A = *(PBS0 + 7);
            UInt16 P11A = *(PBS1 + 7);
            //
            PBS0 += 8;
            PBS1 += 8;
            //
            // Target
            *PBT = (Byte)((P00B + P01B + P10B + P11B) / 4);
            PBT++;
            *PBT = (Byte)((P00G + P01G + P10G + P11G) / 4);
            PBT++;
            *PBT = (Byte)((P00R + P01R + P10R + P11R) / 4);
            PBT++;
            *PBT = (Byte)0xFF;
            PBT++;
            //
            XIT++;
          }
          YIT++;
          YIS += 2;
        }
        FBitmap.UnlockBits(BDS);
        BT.UnlockBits(BDT);
        FBitmap = BT;
        return true;
      }
      catch (Exception e)
      {
        Console.WriteLine(e);
        return false;
      }
    }

    public unsafe Boolean SaveSteptableShortLong(String filename, UInt32 pulseperiodms,
                                                 UInt32 delaymotionshortms, UInt32 delaymotionlongms,
                                                 UInt16 pulsesminimum, UInt16 pulsesmaximum,
                                                 UInt16 xminimum, UInt16 xmaximum,
                                                 UInt16 yminimum, UInt16 ymaximum)
    {
      {
        try
        { // Source: Bitmap
          Int32 BW = FBitmap.Width;
          Int32 BH = FBitmap.Height;
          PixelFormat PF = FBitmap.PixelFormat;
          Rectangle R = new Rectangle(0, 0, BW, BH);
          BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
          // Target: Steptable
          Double PulseFactor = (Double)(pulsesmaximum - pulsesminimum) / 255.0;
          CTextFile Steptable = new CTextFile();
          Steptable.OpenWrite(filename);
          Steptable.Write(String.Format("{0,5}{1,5}{2,5}{3,5} LeftRight\r\n", 
                                        BW, BH, delaymotionshortms, delaymotionlongms));
          UInt16 PX = xminimum;
          Double DX = (Double)(xmaximum - xminimum) / (Double)(BW - 1);
          UInt16 PY = yminimum;
          Double DY = (Double)(ymaximum - yminimum) / (Double)(BH - 1);
          // Bitmap -> Steptable
          Int32 YI = 0;
          while (YI < BH)
          {
            Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
            //
            Int32 XI = 0;
            while (XI < BW)
            { // B
              Byte VB = (Byte)(*PBD);
              PBD++;
              // G
              Byte VG = (Byte)(*PBD);
              PBD++;
              // R
              Byte VR = (Byte)(*PBD);
              PBD++;
              // A
              // NC *PBD;
              PBD++;
              // 
              // PositionX
              PX = (UInt16)(xminimum + DX * XI);
              Steptable.Write(String.Format("{0,5}", PX));
              // PositionY
              PY = (UInt16)(yminimum + DY * YI);
              Steptable.Write(String.Format("{0,5}", PY));
              // PulsePeriod
              Steptable.Write(String.Format("{0,5}", pulseperiodms));
              // PulseCount
              Byte VT = (Byte)(((UInt16)VB + (UInt16)VG + (UInt16)VR) / 3);
              UInt16 PC = (UInt16)(0.5 + pulsesminimum + PulseFactor * VT);
              // debug Console.WriteLine(String.Format(" {0,6}", PC));
              Steptable.Write(String.Format("{0,5}", PC));
              if (0 == XI)
              { // DelayMotionms Long - First Point long distance
                Steptable.Write(String.Format("{0,5}\r\n", delaymotionlongms));
              }
              else
              { // DelayMotionms Short - Neighbour-Point
                Steptable.Write(String.Format("{0,5}\r\n", delaymotionshortms));
              }
              XI++;
            }
            YI++;
          }
          // Finish
          FBitmap.UnlockBits(BD);
          Steptable.Write(String.Format("\r\n"));
          Steptable.Close();
          return true;
        }
        catch (Exception e)
        {
          Console.WriteLine(e);
          return false;
        }
      }
    }

    public unsafe Boolean SaveSteptableMeander(String filename, UInt32 pulseperiodms,
                                               UInt32 delaymotionshortms, UInt32 delaymotionlongms,
                                               UInt16 pulsesminimum, UInt16 pulsesmaximum,
                                               UInt16 xminimum, UInt16 xmaximum,
                                               UInt16 yminimum, UInt16 ymaximum)
    {
      {
        try
        { // Source: Bitmap
          Int32 BW = FBitmap.Width;
          Int32 BH = FBitmap.Height;
          PixelFormat PF = FBitmap.PixelFormat;
          Rectangle R = new Rectangle(0, 0, BW, BH);
          BitmapData BD = FBitmap.LockBits(R, ImageLockMode.ReadOnly, PF);
          // Target: Steptable
          Double PulseFactor = (Double)(pulsesmaximum - pulsesminimum) / 255.0;
          CTextFile Steptable = new CTextFile();
          Steptable.OpenWrite(filename);
          Steptable.Write(String.Format("{0,5}{1,5}{2,5}{3,5} Meander\r\n",
                                        BW, BH, delaymotionshortms, delaymotionlongms));
          UInt16 PX = xminimum;
          Double DX = (Double)(xmaximum - xminimum) / (Double)(BW - 1);
          UInt16 PY = yminimum;
          Double DY = (Double)(ymaximum - yminimum) / (Double)(BH - 1);
          // Bitmap -> Steptable
          Int32 YI = 0;
          while (YI < BH)
          {
            if (0 == (YI % 2))
            { // 0, 2, .... even - left -> right
              Byte* PBD = (Byte*)(BD.Scan0 + YI * BD.Stride);
              Int32 XI = 0;
              while (XI < BW)
              { // B
                Byte VB = (Byte)(*PBD);
                PBD++;
                // G
                Byte VG = (Byte)(*PBD);
                PBD++;
                // R
                Byte VR = (Byte)(*PBD);
                PBD++;
                // A
                // NC *PBD;
                PBD++;
                // 
                // PositionX
                PX = (UInt16)(xminimum + DX * XI);
                Steptable.Write(String.Format("{0,5}", PX));
                // PositionY
                PY = (UInt16)(yminimum + DY * YI);
                Steptable.Write(String.Format("{0,5}", PY));
                // PulsePeriod
                Steptable.Write(String.Format("{0,5}", pulseperiodms));
                // PulseCount
                Byte VT = (Byte)(((UInt16)VB + (UInt16)VG + (UInt16)VR) / 3);
                UInt16 PC = (UInt16)(0.5 + pulsesminimum + PulseFactor * VT);
                Steptable.Write(String.Format("{0,5}", PC));
                // DelayMotionms Short - Only Neighbour-Points!!!
                Steptable.Write(String.Format("{0,5}\r\n", delaymotionshortms));
                XI++;
              }
            }
            else
            { // 1, 3, .... odd - left <- right
              Byte* PBD = (Byte*)(BD.Scan0 + 3 + 4 * (BW - 1) + YI * BD.Stride);
              Int32 XI = BW - 1;
              while (0 <= XI)
              { // A
                // NC *PBD;
                PBD--;
                // R
                Byte VR = (Byte)(*PBD);
                PBD--;
                // G
                Byte VG = (Byte)(*PBD);
                PBD--;
                // B
                Byte VB = (Byte)(*PBD);
                PBD--;
                // 
                // PositionX
                PX = (UInt16)(xminimum + DX * XI);
                Steptable.Write(String.Format("{0,5}", PX));
                // PositionY
                PY = (UInt16)(yminimum + DY * YI);
                Steptable.Write(String.Format("{0,5}", PY));
                // PulsePeriod
                Steptable.Write(String.Format("{0,5}", pulseperiodms));
                // PulseCount
                Byte VT = (Byte)(((UInt16)VB + (UInt16)VG + (UInt16)VR) / 3);
                UInt16 PC = (UInt16)(0.5 + pulsesminimum + PulseFactor * VT);
                Steptable.Write(String.Format("{0,5}", PC));
                // DelayMotionms Short - Only Neighbour-Points!!!
                Steptable.Write(String.Format("{0,5}\r\n", delaymotionshortms));
                XI--;
              }
            }
            YI++;
          }
          // Finish
          FBitmap.UnlockBits(BD);
          Steptable.Write(String.Format("\r\n"));
          Steptable.Close();
          return true;
        }
        catch (Exception e)
        {
          Console.WriteLine(e);
          return false;
        }
      }
    }




  }
}