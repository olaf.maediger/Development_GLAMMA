﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BitmapImage
{
  public class CBitmapRgb32 : CBitmap
  {
    public CBitmapRgb32(String filename)
      : base(1, 1)
    {
      FBitmap = new System.Drawing.Bitmap(filename);
    }

  }
}
