﻿namespace LaserAreaScanner
{
  partial class CUCStepperMotor
  {
    /// <summary> 
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary> 
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Component Designer generated code

    /// <summary> 
    /// Required method for Designer support - do not modify 
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.label3 = new System.Windows.Forms.Label();
      this.nudPositionX = new System.Windows.Forms.NumericUpDown();
      this.lblTitle = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.panel1 = new System.Windows.Forms.Panel();
      this.label2 = new System.Windows.Forms.Label();
      this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
      this.label4 = new System.Windows.Forms.Label();
      this.panel2 = new System.Windows.Forms.Panel();
      this.label5 = new System.Windows.Forms.Label();
      this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
      this.label6 = new System.Windows.Forms.Label();
      this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionX)).BeginInit();
      this.panel1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
      this.panel2.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
      this.SuspendLayout();
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(224, 375);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(88, 13);
      this.label3.TabIndex = 32;
      this.label3.Text = "PositionX Target ";
      // 
      // nudPositionX
      // 
      this.nudPositionX.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.nudPositionX.Location = new System.Drawing.Point(327, 373);
      this.nudPositionX.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.nudPositionX.Name = "nudPositionX";
      this.nudPositionX.Size = new System.Drawing.Size(48, 20);
      this.nudPositionX.TabIndex = 31;
      this.nudPositionX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.nudPositionX.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // lblTitle
      // 
      this.lblTitle.BackColor = System.Drawing.Color.NavajoWhite;
      this.lblTitle.Dock = System.Windows.Forms.DockStyle.Top;
      this.lblTitle.Location = new System.Drawing.Point(0, 0);
      this.lblTitle.Name = "lblTitle";
      this.lblTitle.Size = new System.Drawing.Size(562, 13);
      this.lblTitle.TabIndex = 33;
      this.lblTitle.Text = "<title>";
      this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(151, 454);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(35, 13);
      this.label1.TabIndex = 34;
      this.label1.Text = "label1";
      // 
      // panel1
      // 
      this.panel1.BackColor = System.Drawing.SystemColors.HighlightText;
      this.panel1.Controls.Add(this.label4);
      this.panel1.Controls.Add(this.numericUpDown1);
      this.panel1.Controls.Add(this.label2);
      this.panel1.Location = new System.Drawing.Point(147, 505);
      this.panel1.Name = "panel1";
      this.panel1.Size = new System.Drawing.Size(200, 24);
      this.panel1.TabIndex = 35;
      // 
      // label2
      // 
      this.label2.Dock = System.Windows.Forms.DockStyle.Left;
      this.label2.Location = new System.Drawing.Point(0, 0);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(88, 24);
      this.label2.TabIndex = 33;
      this.label2.Text = "PositionX Target ";
      this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // numericUpDown1
      // 
      this.numericUpDown1.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.numericUpDown1.Location = new System.Drawing.Point(94, 3);
      this.numericUpDown1.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.numericUpDown1.Name = "numericUpDown1";
      this.numericUpDown1.Size = new System.Drawing.Size(48, 20);
      this.numericUpDown1.TabIndex = 34;
      this.numericUpDown1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown1.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // label4
      // 
      this.label4.Dock = System.Windows.Forms.DockStyle.Right;
      this.label4.Location = new System.Drawing.Point(165, 0);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(35, 24);
      this.label4.TabIndex = 35;
      this.label4.Text = "steps";
      this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // panel2
      // 
      this.panel2.BackColor = System.Drawing.SystemColors.HighlightText;
      this.panel2.Controls.Add(this.label5);
      this.panel2.Controls.Add(this.numericUpDown2);
      this.panel2.Controls.Add(this.label6);
      this.panel2.Location = new System.Drawing.Point(147, 532);
      this.panel2.Name = "panel2";
      this.panel2.Size = new System.Drawing.Size(200, 24);
      this.panel2.TabIndex = 36;
      // 
      // label5
      // 
      this.label5.Dock = System.Windows.Forms.DockStyle.Right;
      this.label5.Location = new System.Drawing.Point(165, 0);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(35, 24);
      this.label5.TabIndex = 35;
      this.label5.Text = "steps";
      this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // numericUpDown2
      // 
      this.numericUpDown2.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.numericUpDown2.Location = new System.Drawing.Point(94, 3);
      this.numericUpDown2.Maximum = new decimal(new int[] {
            4095,
            0,
            0,
            0});
      this.numericUpDown2.Name = "numericUpDown2";
      this.numericUpDown2.Size = new System.Drawing.Size(48, 20);
      this.numericUpDown2.TabIndex = 34;
      this.numericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      this.numericUpDown2.Value = new decimal(new int[] {
            2000,
            0,
            0,
            0});
      // 
      // label6
      // 
      this.label6.Dock = System.Windows.Forms.DockStyle.Left;
      this.label6.Location = new System.Drawing.Point(0, 0);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(88, 24);
      this.label6.TabIndex = 33;
      this.label6.Text = "PositionX Target ";
      this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // tableLayoutPanel1
      // 
      this.tableLayoutPanel1.ColumnCount = 2;
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Location = new System.Drawing.Point(97, 70);
      this.tableLayoutPanel1.Name = "tableLayoutPanel1";
      this.tableLayoutPanel1.RowCount = 2;
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel1.Size = new System.Drawing.Size(330, 216);
      this.tableLayoutPanel1.TabIndex = 37;
      // 
      // CUCStepperMotor
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackColor = System.Drawing.SystemColors.Info;
      this.Controls.Add(this.tableLayoutPanel1);
      this.Controls.Add(this.panel2);
      this.Controls.Add(this.panel1);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.lblTitle);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.nudPositionX);
      this.Name = "CUCStepperMotor";
      this.Size = new System.Drawing.Size(562, 767);
      ((System.ComponentModel.ISupportInitialize)(this.nudPositionX)).EndInit();
      this.panel1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
      this.panel2.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.NumericUpDown nudPositionX;
    private System.Windows.Forms.Label lblTitle;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Panel panel1;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.NumericUpDown numericUpDown1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Panel panel2;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.NumericUpDown numericUpDown2;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
  }
}
