#include "Utilities.h"
//
UInt8 CharacterHexadecimalToByte(Character character)
{
  switch (character)
  {
    case '0':
      return 0x00;
    case '1':
      return 0x01;
    case '2':
      return 0x02;
    case '3':
      return 0x03;
    case '4':
      return 0x04;
    case '5':
      return 0x05;
    case '6':
      return 0x06;
    case '7':
      return 0x07;
    case '8':
      return 0x08;
    case '9':
      return 0x09;
    case 'A':
      return 0x0A;
    case 'B':
      return 0x0B;
    case 'C':
      return 0x0C;
    case 'D':
      return 0x0D;
    case 'E':
      return 0x0E;
    case 'F':
      return 0x0F;
    }
  return 0x00;
}

Character DigitHexadecimalToCharacter(UInt8 digit)
{
  switch (digit)
  {
    case 0x00:	return '0';
    case 0x01:	return '1';
    case 0x02:	return '2';
    case 0x03:	return '3';
    case 0x04:	return '4';
    case 0x05:	return '5';
    case 0x06:	return '6';
    case 0x07:	return '7';
    case 0x08:	return '8';
    case 0x09:	return '9';
    case 0x0A:	return 'A';
    case 0x0B:	return 'B';
    case 0x0C:	return 'C';
    case 0x0D:	return 'D';
    case 0x0E:	return 'E';
    case 0x0F:	return 'F';
  }
  return '0';
}

PCharacter UInt8ToAsciiHexadecimal(UInt8 value, PCharacter buffer)
{	
  UInt8 BValue;
  BValue = (UInt8)(value >> 4);
  buffer[0] = DigitHexadecimalToCharacter(BValue);
  BValue = (UInt8)(0x0F & value);
  buffer[1] = DigitHexadecimalToCharacter(BValue);
  buffer[2] = 0x00;
  return buffer;
}

PCharacter UInt16ToAsciiHexadecimal(UInt16 value, PCharacter buffer)
{	
  UInt8 BValue;
  BValue = (UInt8)((0xF000 & value) >> 12);
  buffer[0] = DigitHexadecimalToCharacter(BValue);
  BValue = (UInt8)((0x0F00 & value) >>  8);
  buffer[1] = DigitHexadecimalToCharacter(BValue);
  BValue = (UInt8)((0x00F0 & value) >>  4);
  buffer[2] = DigitHexadecimalToCharacter(BValue);
  BValue = (UInt8)((0x000F & value) >>  0);
  buffer[3] = DigitHexadecimalToCharacter(BValue);
  buffer[4] = 0x00;
  return buffer;
}

UInt8 AsciiHexadecimalToByte(PCharacter text)
{
  UInt8 Result = 0x00;
  if (2 <= strlen(text))
  {
    strupr(text);
    Result = CharacterHexadecimalToByte(text[0]) << 4;
    Result |= CharacterHexadecimalToByte(text[1]) << 0;
  }		
  return Result;
}

PCharacter Int16ToAsciiHexadecimal(Int16 value, PCharacter buffer)
{
  // ??? buffer = itoa(value, buffer, 16);	
  // strupr(buffer);
  sprintf(buffer, "%X", value);
  return buffer;
}

PCharacter Int16ToAsciiDecimal(Int16 value, PCharacter buffer)
{
  sprintf(buffer, "%i", value);
  return buffer;
}

PCharacter UInt16ToAsciiDecimal(Int16 value, PCharacter buffer)
{
  // ??? buffer = utoa(value, buffer, 10);
  sprintf(buffer, "%u", value);
  return buffer;
}

PCharacter FloatToAscii(Float value, PCharacter buffer)
{
  sprintf(buffer, "%f", value);
  return buffer;
}

PCharacter BooleanToAscii(Boolean value, PCharacter buffer)
{
  if (value)
  {
    strcpy(buffer, "true");
    return buffer;
  } 
  strcpy(buffer, "false");
  return buffer;
}

String TemperatureDoubleString(Double value)
{
  String SValue = "";
  if (value < 0)
  {
    SValue = String(abs(value), 1);
    if (value <= -100.0) 
    { // below -099.9 : -100.0
      SValue = "-" + SValue;
    }
    else
      if (value <= -10.0) 
      { // below -009.9 : -010.0
        SValue = "-0" + SValue;
      }
      else
        if (value <= -1.0) 
        { // below -000.9 : -001.0
          SValue = "-00" + SValue;
        }
        else
        { // -0.9 .. 0.0
          SValue = "-000" + SValue;
        }        
  }
  else
    if (0.0 == value) 
    { // == 0.0
      SValue = "   0.0";
    }
    else
    { // 0 < value
      SValue = String(value, 1);
      if (value < +1.0) 
      { // 0.0 .. 0.9
        SValue = "+000" + SValue;
      }
      else
        if (value < +10.0) 
        { // 1.0 .. 9.9
          SValue = "+00" + SValue;
        }
        else
          if (value < +100.0)
          { // 10.0 .. 99.9
            SValue = "+0" + SValue;
          }      
  }
  return SValue;
}











