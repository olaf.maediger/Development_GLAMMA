#include "Defines.h"
#if defined(PROCESSOR_ARDUINODUE)
//
#include "Arduino.h"

#if defined(_SAM3XA_)

#ifndef IrqTimerDue_h
#define IrqTimerDue_h

#include <inttypes.h>

/*
	This fixes compatibility for Arduono Servo Library.
	Uncomment to make it compatible.

	Note that:
		+ Timers: 0,2,3,4,5 WILL NOT WORK, and will
				  neither be accessible by Timer0,...
*/
// #define USING_SERVO_LIB	true

#ifdef USING_SERVO_LIB
	#warning "HEY! You have set flag USING_SERVO_LIB. Timer0, 2,3,4 and 5 are not available"
#endif


#if defined TC2
#define NUM_TIMERS  9
#else
#define NUM_TIMERS  6
#endif

class CHardwareTimerDue
{
  protected:
	// Represents the timer id (index for the array of Timer structs)
	const unsigned short FTimerID;
	// Stores the object timer frequency
	// (allows to access current timer period and frequency):
	static double FFrequency[NUM_TIMERS];
	// Picks the best clock to lower the error
	static uint8_t GetBestClock(double frequency, uint32_t& retRC);
  // Make Interrupt handlers friends, so they can use callbacks
  friend void TC0_Handler(void);
  friend void TC1_Handler(void);
  friend void TC2_Handler(void);
  friend void TC3_Handler(void);
  friend void TC4_Handler(void);
  friend void TC5_Handler(void);
#if NUM_TIMERS > 6
  friend void TC6_Handler(void);
  friend void TC7_Handler(void);
  friend void TC8_Handler(void);
#endif
	static void (*FCallbacks[NUM_TIMERS])();
  //
	struct Timer
	{
		Tc *tc;
		uint32_t channel;
		IRQn_Type irq;
	};
	// Store timer configuration (static, as it's fixed for every object)
	static const Timer Timers[NUM_TIMERS];
  //
  public:
//	static CHardwareTimerDue GetAvailable(void);
	CHardwareTimerDue(unsigned short timerid);
	CHardwareTimerDue& SetInterruptHandler(void (*isr)());
	CHardwareTimerDue& ResetInterruptHandler(void);
	CHardwareTimerDue& Start(double microseconds = -1);
	CHardwareTimerDue& Stop(void);
	CHardwareTimerDue& SetFrequencyHz(double frequency);
	CHardwareTimerDue& SetPeriodus(double microseconds);

	double GetFrequencyHz(void) const;
	double GetPeriodus(void) const;

  inline __attribute__((always_inline)) bool operator== (const CHardwareTimerDue& rhs) const
    {return FTimerID == rhs.FTimerID; };
  inline __attribute__((always_inline)) bool operator!= (const CHardwareTimerDue& rhs) const
    {return FTimerID != rhs.FTimerID; };
};

#endif

#else
	#error Oops! Trying to include DueTimer on another device?
#endif
//
#endif // defined(PROCESSOR_ARDUINODUE)

