//
//--------------------------------
//  Library Dac
//--------------------------------
//
#ifndef DacMcp4725_h
#define DacMcp4725_h
//
#include <Wire.h>
#include "Arduino.h"
#include "Defines.h"
//
//--------------------------------
//  Section - Dac
//--------------------------------
//
class CDacMcp4725
{
  public:
  const static Byte MCP4725_I2CADDRESS_DEFAULT   = 0x62;
  const static Byte MCP4725_I2CADDRESS_HIGH      = 0x63;
  const static Byte MCP4725A0_I2CADDRESS_DEFAULT = 0x60;
  const static Byte MCP4725A0_I2CADDRESS_HIGH    = 0x61;
  const static Byte MCP4725A2_I2CADDRESS_DEFAULT = 0x64;
  const static Byte MCP4725A2_I2CADDRESS_HIGH    = 0x65;
  private:
  const static Byte MCP4725_WRITEDAC             = 0x40;
  const static Byte MCP4725_WRITEDACEEPROM       = 0x20;

  private:
  Byte FI2CAddress;
  UInt32 FValue;
  
  public:
  CDacMcp4725(Byte i2caddress = MCP4725_I2CADDRESS_DEFAULT);
  Boolean Open();
  Boolean Close();
  UInt32 GetValue();
  void SetValue(UInt32 value);
};
//
#endif // DacMcp4725_h
