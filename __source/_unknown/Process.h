#ifndef Process_h
#define Process_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Serial.h"
#include "Pulse.h"
//
#define MASK_STATEPROCESS "%s STP %i %i"
//
extern CPulse PulseTrigger;
//
enum EStateProcess
{ // Common
  spUndefined = -1,
  spIdle = 0,
  spWelcome = 1,
  spGetHelp = 2,
  spGetProgramHeader = 3,
  spGetSoftwareVersion = 4,
  spGetHardwareVersion = 5,
  spGetProcessCount = 6,
  spSetProcessCount = 7,
  spGetProcessPeriod = 8,
  spSetProcessPeriod = 9,
  spStopProcessExecution = 10,
  // LedSystem
  spGetLedSystem = 11,
  spLedSystemOn = 12,  
  spLedSystemOff = 13,  
  spBlinkLedSystem = 14,  
  // LaserRange
  spGetPositionX = 15,
  spSetPositionX = 16,
  spGetPositionY = 17,
  spSetPositionY = 18,
  spGetRangeX = 19,
  spSetRangeX = 20,
  spGetRangeY = 21,
  spSetRangeY = 22,
  spGetDelayMotion = 23,
  spSetDelayMotion = 24,
  spGetPulseWidth = 25,
  spSetPulseWidth = 26,
  // LaserPosition
  spPulseLaserPosition = 27,
  spAbortLaserPosition = 28,
  // LaserLine
  spPulseLaserRow = 29,
  spAbortLaserRow = 30,
  spPulseLaserCol = 31,
  spAbortLaserCol = 32,
  // LaserMatrix
  spPulseLaserMatrix = 33,
  spAbortLaserMatrix = 34,
  // LaserImage
  spPulseLaserImage = 35,
  spAbortLaserImage = 36
  //
};
//
class CProcess
{ //
  //  Segment - Field
  //
  private:
  // Common
  EStateProcess FState;
  UInt8 FSubstate;
  UInt32 FStateTick;                  // [1]
  UInt32 FTimeStamp;                  // [ms]
  UInt32 FTimeMark;                   // [ms]
  UInt64 FTimeStampus;                // [us]
  UInt64 FTimeMarkus;                 // [us]
  UInt32 FProcessCount;               // [1]
  UInt32 FProcessStep;                // [1]
  UInt32 FProcessPeriod;              // [ms] ???????????????????????????????????????????????????
  //  
  // Range
  UInt16 FXPositionActual;            // [stp]
  UInt16 FXPositionDelta;             // [stp]
  UInt16 FXPositionMinimum;           // [stp]
  UInt16 FXPositionMaximum;           // [stp]
  UInt16 FYPositionActual;            // [stp]
  UInt16 FYPositionDelta;             // [stp]
  UInt16 FYPositionMinimum;           // [stp]
  UInt16 FYPositionMaximum;           // [stp]
  UInt32 FDelayMotion;              // [ms]
  // -> Pulse !!! UInt32 FPulseWidthus;               // [us]
  //  
  void LedSystemState();
  //
  public:
  CProcess();
  // Property
  EStateProcess GetState();
  void SetState(EStateProcess stateprocess);
  void SetState(EStateProcess stateprocess, UInt8 substate);
  // Measurement - Time
  // First: TimeStamp!!!
  inline void SetTimeStamp()
  {
    FTimeStamp = millis();
    FTimeMark = FTimeStamp;
    FTimeStampus = micros();
    FTimeMarkus = FTimeStamp;
  }
  // Later: TimeMarker!!!
  inline void SetTimeMark()
  {
    FTimeMark = millis();
    FTimeMarkus = micros();
  }
  inline UInt32 GetTimeSpan()
  {
    return FTimeMark - FTimeStamp;
  }
  inline UInt32 GetTimeSpanus()
  {
    return FTimeMarkus - FTimeStampus;
  }

  inline void SetProcessCount(UInt32 count)
  {
    FProcessStep = 0;
    FProcessCount = count;
  }
  inline UInt32 GetProcessCount(void)
  {
    return FProcessCount;
  }

  inline void SetProcessStep(UInt32 stepindex)
  {
    FProcessStep = stepindex;
  }
  inline UInt32 GetProcessStep(void)
  {
    return FProcessStep;
  }

  inline Boolean IncrementProcessStep()
  {
    FProcessStep++;
    return (FProcessCount <= FProcessStep);
  }

  inline void SetProcessPeriod(UInt32 period)
  {
    FProcessPeriod = period;
  }
  inline UInt32 GetProcessPeriod(void)
  {
    return FProcessPeriod;
  }
  //
  //  Range - Delay
  //
  inline UInt32 GetDelayMotion()
  {
    return FDelayMotion;
  }
  inline void SetDelayMotion(UInt32 value)
  {
    FDelayMotion = value;
  }
  //
  //  Range - PulseWidthus
  //
  inline UInt32 GetPulseWidthus()
  {
    return PulseTrigger.GetPulseWidthus();
  }
  inline void SetPulseWidthus(UInt32 value)
  {
    PulseTrigger.SetPulseWidthus(value);
  }
  //
  //  X-Position
  //
  inline UInt32 GetXPositionActual()
  {
    return FXPositionActual;
  }
  inline void SetXPositionActual(UInt32 value)
  {
    FXPositionActual = value;
  }
    
  inline UInt32 GetXPositionDelta()
  {
    return FXPositionDelta;
  }
  inline void SetXPositionDelta(UInt32 value)
  {
    FXPositionDelta = value;
  }
  
  inline UInt32 GetXPositionMinimum()
  {
    return FXPositionMinimum;
  }
  inline void SetXPositionMinimum(UInt32 value)
  {
    FXPositionMinimum = value;
  }
  
  inline UInt32 GetXPositionMaximum()
  {
    return FXPositionMaximum;
  }
  inline void SetXPositionMaximum(UInt32 value)
  {
    FXPositionMaximum = value;
  }
  //
  //  Y-Position
  //
  inline UInt32 GetYPositionActual()
  {
    return FYPositionActual;
  }
  inline void SetYPositionActual(UInt32 value)
  {
    FYPositionActual = value;
  }
    
  inline UInt32 GetYPositionDelta()
  {
    return FYPositionDelta;
  }
  inline void SetYPositionDelta(UInt32 value)
  {
    FYPositionDelta = value;
  }
  
  inline UInt32 GetYPositionMinimum()
  {
    return FYPositionMinimum;
  }
  inline void SetYPositionMinimum(UInt32 value)
  {
    FYPositionMinimum = value;
  }
  
  inline UInt32 GetYPositionMaximum()
  {
    return FYPositionMaximum;
  }
  inline void SetYPositionMaximum(UInt32 value)
  {
    FYPositionMaximum = value;
  }

  inline UInt32 IncrementXPosition()
  {
    FXPositionActual += FXPositionDelta;
    return FXPositionActual;
  }
  inline UInt32 DecrementXPosition()
  {
    FXPositionActual -= FXPositionDelta;
    return FXPositionActual;
  }
  
  inline UInt32 IncrementYPosition()
  {
    FYPositionActual += FYPositionDelta;
    return FYPositionActual;
  }
  inline UInt32 DecrementYPosition()
  {
    FYPositionActual -= FYPositionDelta;
    return FYPositionActual;
  }
  // Management
  Boolean Open();
  Boolean Close();
  private:
  // System
  void HandleUndefined(CSerial &serial);
  void HandleIdle(CSerial &serial);
  void HandleWelcome(CSerial &serial);
  void HandleGetHelp(CSerial &serial);
  void HandleGetProgramHeader(CSerial &serial);
  void HandleGetSoftwareVersion(CSerial &serial);
  void HandleGetHardwareVersion(CSerial &serial);
  void HandleGetProcessCount(CSerial &serial);
  void HandleSetProcessCount(CSerial &serial);
  void HandleGetProcessPeriod(CSerial &serial);
  void HandleSetProcessPeriod(CSerial &serial);
  void HandleStopProcessExecution(CSerial &serial);
  // LedSystem
  void HandleGetLedSystem(CSerial &serial);
  void HandleLedSystemOn(CSerial &serial);
  void HandleLedSystemOff(CSerial &serial);
  void HandleBlinkLedSystem(CSerial &serial);
  // Measurement - LaserRange
  void HandleGetPositionX(CSerial &serial);      
  void HandleSetPositionX(CSerial &serial);      
  void HandleGetPositionY(CSerial &serial);      
  void HandleSetPositionY(CSerial &serial);       
  void HandleGetRangeX(CSerial &serial);      
  void HandleSetRangeX(CSerial &serial);      
  void HandleGetRangeY(CSerial &serial);      
  void HandleSetRangeY(CSerial &serial);      
  void HandleGetDelayMotion(CSerial &serial); 
  void HandleSetDelayMotion(CSerial &serial); 
  void HandleGetPulseWidthus(CSerial &serial); 
  void HandleSetPulseWidthus(CSerial &serial);   
  // Measurement - LaserPosition
  void HandlePulseLaserPosition(CSerial &serial);
  void HandleAbortLaserPosition(CSerial &serial);
  // Measurement - LaserLine
  void HandlePulseLaserRow(CSerial &serial);
  void HandleAbortLaserRow(CSerial &serial);
  void HandlePulseLaserCol(CSerial &serial);
  void HandleAbortLaserCol(CSerial &serial);
  // Measurement - LaserMatrix
  void HandlePulseLaserMatrix(CSerial &serial);
  void HandleAbortLaserMatrix(CSerial &serial);
  // Measurement - LaserImage
  //void HandleInitLaserImage(CSerial &serial);
  void HandlePulseLaserImage(CSerial &serial);
  void HandleAbortLaserImage(CSerial &serial);
  //
  public:
  // Collector
  void Handle(CSerial &serial);
};
//
#endif // Process_h
