#include "Serial.h"
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
void CSerialBase::SetRxdEcho(bool rxdecho)
{
  FRxdEcho = rxdecho;
}
 
String CSerialBase::BuildTime(long unsigned milliseconds)
{
  long unsigned SystemTime = milliseconds;
  unsigned Millis = SystemTime % 1000;
  long unsigned RTS = SystemTime / 1000;
  unsigned Hours = RTS / 3600;
  RTS = RTS - 3600 * Hours;
  unsigned Minutes = RTS / 60;
  unsigned Seconds = RTS - 60 * Minutes;
  char Buffer[16];
  sprintf(Buffer, "%s%2.2d:%2.2d:%2.2d.%3.3d", PROMPT_RESPONSE, Hours, Minutes, Seconds, Millis); 
  return Buffer;
}

void CSerialBase::WriteNewLine()
{
  Write(PROMPT_NEWLINE);  
}
  
void CSerialBase::WritePrompt()
{  
  Write(BuildTime(millis()));
  Write(PROMPT_INPUT);
}
  
void CSerialBase::WriteAnswer()
{
  Write(PROMPT_ANSWER);
}
//
//----------------------------------------------------
//  Segment - CSerialH 
//----------------------------------------------------
//
#if ((defined PROCESSOR_ARDUINOUNO)||(defined PROCESSOR_ARDUINOMEGA)||(defined PROCESSOR_STM32F103C8)||(defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36)||(defined PROCESSOR_ARDUINODUE))
bool CSerialH::Open(UInt32 baudrate)
{
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialH::Close()
{
  FRxdEcho = false;
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}

int CSerialH::GetRxdByteCount()
{
  return FPSerial->available();
}

char CSerialH::ReadCharacter()
{
  char C = (char)FPSerial->read();
  if (FRxdEcho && (isalnum(C) || (' ' == C)))
  {
    FPSerial->write(C);
  }
  return C;
}

void CSerialH::Write(const char* text)
{  
  FPSerial->write(text); 
}

void CSerialH::Write(String text)
{
  FPSerial->write(text.c_str());
} 
#endif // ....

 //
//----------------------------------------------------
//  Segment - CSerialU
//----------------------------------------------------
//
#if ((defined PROCESSOR_TEENSY32) || (defined PROCESSOR_TEENSY36))
bool CSerialU::Open(UInt32 baudrate)
{
  FRxdEcho = INIT_RXDECHO;
  FPSerial->begin(baudrate);
  FIsOpen = true;
  return FIsOpen;
}

bool CSerialU::Close()
{
  FRxdEcho = RXDECHO_OFF;
  FPSerial->end();
  FIsOpen = false;
  return !FIsOpen;
}
 
int CSerialU::GetRxdByteCount()
{
  return FPSerial->available();
}

char CSerialU::ReadCharacter()
{
  char C = (char)FPSerial->read();
  if (FRxdEcho && (isalnum(C) || (' ' == C)))
  {
    FPSerial->write(C);
  }
  return C;
}

void CSerialU::Write(char* text)
{  
  FPSerial->write(text); 
}

void CSerialU::Write(String text)
{
  FPSerial->write(text.c_str());
}  
#endif  
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
#ifdef PROCESSOR_ARDUINOUNO
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_ARDUINOUNO

#ifdef PROCESSOR_ARDUINOMEGA
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_ARDUINOMEGA

#ifdef PROCESSOR_STM32F103C8
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_STM32F103C8

#if ((defined PROCESSOR_TEENSY32) || (defined PROCESSOR_TEENSY36))
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(usb_serial_class *serial)
{
  CSerialU *PSerialU = (CSerialU*)new CSerialU(serial);
  FPSerial = PSerialU;
}

CSerial::CSerial(usb_serial_class &serial)
{
  CSerialU *PSerialU = (CSerialU*)new CSerialU(&serial);
  FPSerial = PSerialU; 
}
#endif // PROCESSOR_TEENSY32 PROCESSOR_TEENSY36

#ifdef PROCESSOR_ARDUINODUE
CSerial::CSerial(HardwareSerial *serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(serial);
  FPSerial = (CSerialBase*)PSerialH;
}

CSerial::CSerial(HardwareSerial &serial)
{
  CSerialH *PSerialH = (CSerialH*)new CSerialH(&serial);
  FPSerial = (CSerialBase*)PSerialH;
}
#endif // PROCESSOR_ARDUINODUE
//
//----------------------------------------------
//
bool CSerial::Open(UInt32 baudrate)
{
  return FPSerial->Open(baudrate);
}

bool CSerial::Close()
{
  return FPSerial->Close();
}

void CSerial::SetRxdEcho(bool rxdecho)
{
  FPSerial->SetRxdEcho(rxdecho);
}
//
//----------------------------------------------
//
void CSerial::WriteNewLine()
{
  FPSerial->WriteNewLine();
}

void CSerial::WriteAnswer()
{
  FPSerial->WriteAnswer();
}

void CSerial::WritePrompt()
{
  FPSerial->WritePrompt();
}

void CSerial::WriteTime()
{
  long unsigned Time = millis();
  String STime = FPSerial->BuildTime(Time);
  FPSerial->Write(STime);
}
//
//----------------------------------------------
//
char CSerial::ReadCharacter()
{
  char C = (char)FPSerial->ReadCharacter();
  return C;
}

int CSerial::GetRxdByteCount()
{
  return FPSerial->GetRxdByteCount();
}
//
//----------------------------------------------
//
void CSerial::Write(char character)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%c", character);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char *text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s", text);
  FPSerial->Write(Buffer);
}

void CSerial::Write(String text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s", text.c_str());
  FPSerial->Write(Buffer);
}

void CSerial::Write(UInt8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%u", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(UInt16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%u", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(UInt32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%lu", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Int8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%i", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Int16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%i", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Int32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%li", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Float number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%f", number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(Double number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%lf", number);
  FPSerial->Write(Buffer);
}
//
//----------------------------------------------
//
void CSerial::Write(const char* mask, char character)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, character);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, const char *text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, text);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, String text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, text.c_str());
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, UInt8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, UInt16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, UInt32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Int8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Int16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Int32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Float number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}

void CSerial::Write(const char* mask, Double number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, mask, number);
  FPSerial->Write(Buffer);
}
//
//----------------------------------------------
//
void CSerial::WriteLine(const char *text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", text);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(String text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", text.c_str());
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(UInt8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%u\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(UInt16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%u\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(UInt32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%lu\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Int8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%i\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Int16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%i\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Int32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%li\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Float number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%f\r\n", number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(Double number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%lf\r\n", number);
  FPSerial->Write(Buffer);
}
//
//----------------------------------------------
//
void CSerial::WriteLine(const char* mask, const char *text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, text);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, String text)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, text.c_str());
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, UInt8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, UInt16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, UInt32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Int8 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Int16 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Int32 number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Float number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}

void CSerial::WriteLine(const char* mask, Double number)
{
  char Buffer[SIZE_FORMATBUFFER];
  sprintf(Buffer, "%s\r\n", mask);
  sprintf(Buffer, Buffer, number);
  FPSerial->Write(Buffer);
}
//
//----------------------------------------------
//

















