#ifndef IrqTimer_h
#define IrqTimer_h
//
#include "Defines.h"
//
#if defined(PROCESSOR_STM32F103C8)
#include <HardwareTimer.h>
#elif defined(PROCESSOR_ARDUINODUE)
#include "HardwareTimerDue.h"
#endif
//
class CIrqTimer
{
  protected:
  int FChannel;
#if defined(PROCESSOR_STM32F103C8)
  HardwareTimer* FPHardwareTimer;
#elif defined(PROCESSOR_ARDUINODUE)
  CHardwareTimerDue* FPHardwareTimer;
#endif
  void (*FPIrqHandler)(void);
  long unsigned FPulseCount;
  //
  public:
  CIrqTimer(int channel);
  void SetInterruptHandler(void (*pirqhandler)(void));
  void SetPeriodus(long unsigned periodus);
  void Start();
  void Stop();
  void SetPulseCount(long unsigned pulsecount);
  void DecrementStopPulseCount();
};
//
#endif  // IrqTimer_h
