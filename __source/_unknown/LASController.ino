#include "Defines.h"
#include "Pulse.h"
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Serial.h"
#include "Led.h"
#include "LedLine.h"
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  #include "DacInternal.h"
#endif
#if (defined(DAC_MCP4725_X) || defined(DAC_MCP4725_Y)) 
  #include "DacMcp4725.h"
#endif
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Instance - System 
//---------------------------------------------------
//
/*
GPW
SPW 1000000
BLS 2000 2

BLS 1000 2
SPW 1000000
PLP 2000 2000 2000 3

SPW 500000
PLP 2000 2000 1000 3
SPW 50000
PLP 2000 2000 100 3
PLI 2000 2000 1000000 3
*/
//
CError LASError;
CProcess LASProcess;
CCommand LASCommand;
//
//---------------------------------------------------
// Segment - Global Instance - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
CLedLine LedLine(61, 60, PIN_LEDMOTIONY, PIN_LEDMOTIONX,  // 7..4
                 57, PIN_LEDBUSY, 55, PIN_LEDERROR);      // 3..0
CLed LedLaser(PIN_LEDLASER, LEDLASER_INVERTED);
//
//---------------------------------------------------
// Segment - Global Instance - Dac
//---------------------------------------------------
//
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  CDacInternal DacInternal_XY(true, true);
#endif
#ifdef DAC_MCP4725_X
CDacMcp4725 DacMcp4725_X(CDacMcp4725::MCP4725_I2CADDRESS_DEFAULT);
#endif
#ifdef DAC_MCP4725_Y
CDacMcp4725 DacMcp4725_Y(CDacMcp4725::MCP4725_I2CADDRESS_HIGH);
#endif
//
//---------------------------------------------------
// Segment - Global Instance - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort und CommandPort!
// NC - CSerial SerialDebug(Serial);
// normal CSerial SerialCommand(Serial1);
CSerial SerialCommand(Serial);
// NC - CSerial SerialPrinter(Serial2);
// NC - Serial3
//
//---------------------------------------------------
// Segment - Global Instance - Pulse
//---------------------------------------------------
//
CPulse PulseTrigger(TIMERID_PULSEPERIOD, TIMERID_PULSEWIDTH);
//
//---------------------------------------------------
// Segment - Forward-Declaration
//---------------------------------------------------
//

//
//---------------------------------------------------
// Segment - Setup
//---------------------------------------------------
//
void setup() 
{ //-------------------------------------------
  // Device - Led - LedLaser
  //-------------------------------------------
  LedLaser.Open();
  LedLaser.Off();
  //-------------------------------------------
  // Device - Led - LedSystem
  //-------------------------------------------
  LedSystem.Open();
  LedLine.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    LedLine.On();
    delay(40);
    LedSystem.On();
    LedLine.Off();
    delay(40);
  }
  LedSystem.Off();
  //-------------------------------------------
  // Device - Dac
  //-------------------------------------------
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  DacInternal_XY.Open();
#endif  
#ifdef DAC_MCP4725_X
  DacMcp4725_X.Open();
#endif  
#ifdef DAC_MCP4725_Y
  DacMcp4725_Y.Open();
#endif  
  //-------------------------------------------
  // Device - Serial
  //-------------------------------------------
  // 
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  //-------------------------------------------
  // Device - Command
  //-------------------------------------------
  LASCommand.WriteProgramHeader(SerialCommand);
  LASCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //-------------------------------------------
  // Device - Process 
  //-------------------------------------------
  LASProcess.Open();
  LASProcess.SetState(spWelcome);
}
//
//---------------------------------------------------
// Segment - Loop
//---------------------------------------------------
//
void loop() 
{ 
  LASError.Handle(SerialCommand);    
  LASCommand.Handle(SerialCommand);
  LASProcess.Handle(SerialCommand);  
}




