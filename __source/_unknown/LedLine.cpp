//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "LedLine.h"
//
CLedLine::CLedLine(int pin7, int pin6, int pin5, int pin4, 
                   int pin3, int pin2, int pin1, int pin0)
{
  FPins[0] = pin0;
  FPins[1] = pin1;
  FPins[2] = pin2;
  FPins[3] = pin3;
  FPins[4] = pin4;
  FPins[5] = pin5;
  FPins[6] = pin6;
  FPins[7] = pin7;
  for (int IP = 0; IP < 8; IP++)
  {
    FStates[IP] = slUndefined;
  }  
}



EStateLed CLedLine::GetState(int ledindex)
{
  return FStates[ledindex];
}



Boolean CLedLine::Open()
{
  for (int IP = 0; IP < 8; IP++)
  {
    if (PIN_NC != FPins[IP])
    {
      pinMode(FPins[IP], OUTPUT);
      digitalWrite(FPins[IP], LOW);
      FStates[IP] = slOff;
    }
  }
  return true;
}

Boolean CLedLine::Close()
{
  for (int IP = 0; IP < 8; IP++)
  {
    if (PIN_NC != FPins[IP])
    {
      digitalWrite(FPins[IP], LOW);
      pinMode(FPins[IP], INPUT);
      FStates[IP] = slUndefined;
    }
  }
  return true;
}



void CLedLine::On()
{
  for (int IP = 0; IP < 8; IP++)
  {
    if (PIN_NC != FPins[IP])
    {
      digitalWrite(FPins[IP], HIGH);
      FStates[IP] = slOn;
    }
  }   
}

void CLedLine::On(byte mask)
{
  byte Mask = 0x01;
  for (int IP = 0; IP < 8; IP++)
  {
    if (0 < (mask & Mask))
    {
      if (PIN_NC != FPins[IP])
      {
        digitalWrite(FPins[IP], HIGH);
        FStates[IP] = slOn;
      }
    }
    Mask <<= 1;
  }   
}

void CLedLine::Off()
{
  for (int IP = 0; IP < 8; IP++)
  {
    if (PIN_NC != FPins[IP])
    {
      digitalWrite(FPins[IP], LOW);
      FStates[IP] = slOff;
    }
  }   
}

void CLedLine::Off(byte mask)
{
  byte Mask = 0x01;
  for (int IP = 0; IP < 8; IP++)
  {
    if (0 < (mask & Mask))
    {
      if (PIN_NC != FPins[IP])
      {
        digitalWrite(FPins[IP], LOW);
        FStates[IP] = slOff;
      }
    }
    Mask <<= 1;    
  }   
}


void CLedLine::Toggle()
{
  for (int IP = 0; IP < 8; IP++)
  {
    if (slOff == FStates[IP])
    {
      if (PIN_NC != FPins[IP])
      {
        digitalWrite(FPins[IP], HIGH);
        FStates[IP] = slOn;
      }
    } 
    else
      if (slOn == FStates[IP])
      {
        if (PIN_NC != FPins[IP])
        {
          digitalWrite(FPins[IP], LOW);
          FStates[IP] = slOff;
        }
      } 
  }   
}


void CLedLine::Toggle(byte mask)
{
  byte Mask = 0x01;
  for (int IP = 0; IP < 8; IP++)
  {
    if (0 <(mask & Mask))
    {
      if (PIN_NC != FPins[IP])
      {
        if (slOff == FStates[IP])
        {
          digitalWrite(FPins[IP], HIGH);
          FStates[IP] = slOn;
        } 
        else
          if (slOn == FStates[IP])
          {
            digitalWrite(FPins[IP], LOW);
            FStates[IP] = slOff;
          }
      }
    }
    Mask <<= 1;    
  }   
}

void CLedLine::Pulse(UInt32 delaymillis, UInt32 count)
{
  UInt32 DMS = delaymillis / 2;
  for (UInt32 CI = 0; CI < count; CI++)
  {
    for (int IP = 0; IP < 8; IP++)
    {
      if (PIN_NC != FPins[IP])
      {
        digitalWrite(FPins[IP], HIGH);
        FStates[IP] = slOn;
      }
    }   
    delay(DMS);
    for (int IP = 0; IP < 8; IP++)
    {
      if (PIN_NC != FPins[IP])
      {
        digitalWrite(FPins[IP], LOW);
        FStates[IP] = slOff;
      }
    }   
    delay(DMS);
  }
}

void CLedLine::Pulse(byte mask, UInt32 delaymillis, UInt32 count)
{
  UInt32 DMS = delaymillis / 2;
  for (UInt32 CI = 0; CI < count; CI++)
  {
    byte Mask = 0x01;
    for (int IP = 0; IP < 8; IP++)
    {
      if (0 < (Mask & mask))
      {
        if (PIN_NC != FPins[IP])
        {
          digitalWrite(FPins[IP], HIGH);
          FStates[IP] = slOn;
        }
      }
      Mask <<= 1;
    }   
    delay(DMS);
    Mask = 0x01;
    for (int IP = 0; IP < 8; IP++)
    {
      if (0 < (Mask & mask))
      {
        if (PIN_NC != FPins[IP])
        {
          digitalWrite(FPins[IP], LOW);
          FStates[IP] = slOff;
        }
      }
      Mask <<= 1;
    }
    delay(DMS);  
  }   
}



