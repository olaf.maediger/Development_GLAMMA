//
#include "Command.h"
#include "Led.h"
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  #include "DacInternal.h"
#endif
#if (defined(DAC_MCP4725_X) || defined(DAC_MCP4725_Y)) 
  #include "DacMcp4725.h"
#endif
#include "Error.h"
#include "Process.h"
#include "Pulse.h"
//
//-------------------------------------------
//  External Global Variables 
//-------------------------------------------
//  
extern CLed LedSystem;
extern CLed LedLaser;
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  extern CDacInternal DacInternal_XY;
#endif  
#ifdef DAC_MCP4725_X
extern CDacMcp4725 DacMcp4725_X;
#endif  
#ifdef DAC_MCP4725_Y 
extern CDacMcp4725 DacMcp4725_Y;
#endif  
extern CSerial SerialCommand;
extern CError LASError;
extern CProcess LASProcess;
extern CCommand LASCommand;
extern CPulse PulseTrigger;
//
//
//#########################################################
//  Segment - Constructor
//#########################################################
//
CCommand::CCommand()
{
  Init();
}

CCommand::~CCommand()
{
  Init();
}
//
//#########################################################
//  Segment - Helper
//#########################################################
//
void CCommand::ZeroRxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdBuffer[CI] = 0x00;
  }
  FRxdBufferIndex = 0;
}

void CCommand::ZeroTxdBuffer()
{
  int CI;
  for (CI = 0; CI < SIZE_TXDBUFFER; CI++)
  {
    FTxdBuffer[CI] = 0x00;
  }
}

void CCommand::ZeroRxdCommandLine()
{
  int CI;
  for (CI = 0; CI < SIZE_RXDBUFFER; CI++)
  {
    FRxdCommandLine[CI] = 0x00;
  }
}
//
//#########################################################
//  Segment - Management
//#########################################################
//
void CCommand::Init()
{
  ZeroRxdBuffer();
  ZeroTxdBuffer();
  ZeroRxdCommandLine();
}

Boolean CCommand::DetectRxdLine(CSerial &serial)
{
  while (0 < serial.GetRxdByteCount())
  {
    Character C = serial.ReadCharacter();
    switch (C)
    {
      case CR:
        FRxdBuffer[FRxdBufferIndex] = ZERO;
        FRxdBufferIndex = 0; // restart
        strupr(FRxdBuffer);
        strcpy(FRxdCommandLine, FRxdBuffer);
        return true;
      case LF: // ignore
        break;
      default: 
        FRxdBuffer[FRxdBufferIndex] = C;
        FRxdBufferIndex++;
        break;
    }
  }
  return false;
}

Boolean CCommand::Analyse(CSerial &serial)
{
  if (DetectRxdLine(serial))
  {
    char *PTerminal = (char*)" \t\r\n";
    char *PRxdCommandLine = FRxdCommandLine;
    FPRxdCommand = strtok(FRxdCommandLine, PTerminal);
    if (FPRxdCommand)
    {
      FRxdParameterCount = 0;
      char *PRxdParameter;
      while (PRxdParameter = strtok(0, PTerminal))
      {
        FPRxdParameters[FRxdParameterCount] = PRxdParameter;
        FRxdParameterCount++;
        if (COUNT_RXDPARAMETERS < FRxdParameterCount)
        {
          LASError.SetCode(ecToManyParameters);
          ZeroRxdBuffer();
          // debug serialtarget.WriteLine("error[ecToManyParameters]");
          serial.WriteNewLine();
          serial.WritePrompt();
          return false;
        }
      }  
//      debug sprintf(TxdBuffer, "\n\r# RxdCommand<%s>", PRxdCommand);
//      debug serial.Write(TxdBuffer);
//      if (0 < RxdParameterCount)
//      {
//        int II;
//        for (II = 0; II < RxdParameterCount; II++)
//        {
//          sprintf(TxdBuffer, " RxdParameter[%i]<%s>", II, PRxdParameters[II]);
//          serial.Write(TxdBuffer);
//        }
//      }
      //
      ZeroRxdBuffer();
      serial.WriteNewLine();
      return true;
    }
    ZeroRxdBuffer();
    serial.WriteNewLine();
    serial.WritePrompt();
  }
  return false;
}
//
//#########################################################
//  Segment - Basic Output
//#########################################################
//
void CCommand::WriteProgramHeader(CSerial &serial)
{  
  serial.WriteAnswer();
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PROJECT, ARGUMENT_PROJECT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWARE, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_HARDWARE, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_DATE, ARGUMENT_DATE);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_TIME, ARGUMENT_TIME);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_AUTHOR, ARGUMENT_AUTHOR);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PORT, ARGUMENT_PORT);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(MASK_PARAMETER, ARGUMENT_PARAMETER);
  serial.WriteNewLine();
  serial.WriteAnswer();
  serial.Write(TITLE_LINE);
  serial.WriteNewLine();  
  serial.WriteAnswer();
  serial.WriteNewLine();
}

void CCommand::WriteSoftwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_SOFTWAREVERSION, ARGUMENT_SOFTWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHardwareVersion(CSerial &serial)
{
  serial.WriteAnswer();
  serial.Write(MASK_HARDWAREVERSION, ARGUMENT_HARDWARE);
  serial.WriteNewLine();
}

void CCommand::WriteHelp(CSerial &serial)
{
  serial.WriteAnswer(); serial.WriteLine(HELP_COMMON);
  serial.WriteAnswer(); serial.WriteLine(MASK_H, SHORT_H);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPH, SHORT_GPH);
  serial.WriteAnswer(); serial.WriteLine(MASK_GSV, SHORT_GSV);
  serial.WriteAnswer(); serial.WriteLine(MASK_GHV, SHORT_GHV);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPC, SHORT_GPC);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPC, SHORT_SPC);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPP, SHORT_GPP);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPP, SHORT_SPP);
  serial.WriteAnswer(); serial.WriteLine(MASK_A, SHORT_A);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LEDSYSTEM);
  serial.WriteAnswer(); serial.WriteLine(MASK_GLS, SHORT_GLS);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSH, SHORT_LSH);
  serial.WriteAnswer(); serial.WriteLine(MASK_LSL, SHORT_LSL);
  serial.WriteAnswer(); serial.WriteLine(MASK_BLS, SHORT_BLS);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERRANGE);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPX, SHORT_GPX);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPX, SHORT_SPX);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPY, SHORT_GPY);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPY, SHORT_SPY);
  serial.WriteAnswer(); serial.WriteLine(MASK_GRX, SHORT_GRX);
  serial.WriteAnswer(); serial.WriteLine(MASK_SRX, SHORT_SRX);
  serial.WriteAnswer(); serial.WriteLine(MASK_GRY, SHORT_GRY);
  serial.WriteAnswer(); serial.WriteLine(MASK_SRY, SHORT_SRY);
  serial.WriteAnswer(); serial.WriteLine(MASK_GDM, SHORT_GDM);
  serial.WriteAnswer(); serial.WriteLine(MASK_SDM, SHORT_SDM);
  serial.WriteAnswer(); serial.WriteLine(MASK_GPW, SHORT_GPW);
  serial.WriteAnswer(); serial.WriteLine(MASK_SPW, SHORT_SPW);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERPOSITION);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLP, SHORT_PLP);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALP, SHORT_ALP);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERLINE);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLR, SHORT_PLR);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALR, SHORT_ALR);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLC, SHORT_PLC);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALC, SHORT_ALC);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERMATRIX);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLM, SHORT_PLM);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALM, SHORT_ALM);
  //
  serial.WriteAnswer(); serial.WriteLine(HELP_LASERIMAGE);
  serial.WriteAnswer(); serial.WriteLine(MASK_PLI, SHORT_PLI);
  serial.WriteAnswer(); serial.WriteLine(MASK_ALI, SHORT_ALI);
}
//
//#########################################################
//  Segment - Command - Execution - Common
//#########################################################
//
void CCommand::ExecuteGetHelp(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetHelp);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HELP);
    serial.WriteLine(GetTxdBuffer());         
    WriteHelp(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetProgramHeader(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetProgramHeader);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HEADER);
    serial.WriteLine(GetTxdBuffer());   
    WriteProgramHeader(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetSoftwareVersion(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetSoftwareVersion);
    // Analyse parameters: -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_SOFTWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteSoftwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetHardwareVersion(CSerial &serial)
{
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetHardwareVersion);
    // Analyse parameters: -
    // Response
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), COUNT_HARDWAREVERSION);
    serial.WriteLine(GetTxdBuffer());   
    WriteHardwareVersion(serial);
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetProcessCount(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetProcessCount);
    // Analyse parameters: -
    // Execute:
    UInt32 PC = LASProcess.GetProcessCount();    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetProcessCount(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetProcessCount);
    // Analyse parameters:
    UInt32 PC = atol(GetPRxdParameters(0));
    // Execute:
    LASProcess.SetProcessCount(PC);
    PC = LASProcess.GetProcessCount();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetProcessPeriod);
    // Analyse parameters: -
    // Execute:
    UInt32 PP = LASProcess.GetProcessPeriod();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetProcessPeriod(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetProcessPeriod);
    // Analyse parameters:
    UInt32 PP = atol(GetPRxdParameters(0));
    // Execute:
    LASProcess.SetProcessPeriod(PP);
    PP = LASProcess.GetProcessPeriod();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PP);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteAbortAll(CSerial &serial)
{ //?????????????????????????????????????????????????????????????????????????????????????????????????????
  // LASProcess.SetProcessStep(0);
  // LASProcess.SetProcessCount(0);
  LASProcess.SetState(spIdle);
  // Analyse parameters:
  // Execute:
  // Response:
  sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LedSystem
//#########################################################
//
void CCommand::ExecuteGetLedSystem(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetLedSystem);
    // Analyse parameters:
    // Execute:
    int State = LedSystem.GetState();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %i", 
            PROMPT_RESPONSE, GetPRxdCommand(), State);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteLedSystemOn(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spLedSystemOn);
    // Analyse parameters:
    // Execute:
    LedSystem.On();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteLedSystemOff(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spLedSystemOff);
    // Analyse parameters:
    // Execute:
    LedSystem.Off();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteBlinkLedSystem(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spBlinkLedSystem);
    // Analyse parameters:
    UInt32 Period = atol(GetPRxdParameters(0)); // [us]!!!!!!!!
    UInt32 Count = atol(GetPRxdParameters(1));  // [1] 
    // Execute:
    LASProcess.SetProcessPeriod(Period);
    LASProcess.SetProcessCount(Count);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), Period, Count);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LaserRange
//#########################################################
//
void CCommand::ExecuteGetPositionX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetPositionX);
    // Analyse parameters: -
    // Execute:
    UInt32 PX = LASProcess.GetXPositionActual();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", PROMPT_RESPONSE, 
            GetPRxdCommand(), PX);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPositionX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetPositionX);
    // Analyse parameters ( SPX <x> ):
    UInt32 PX = atol(GetPRxdParameters(0));
    // Execute:
    LASProcess.SetXPositionActual(PX);    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", PROMPT_RESPONSE, 
            GetPRxdCommand(), PX);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetPositionY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetPositionY);
    // Analyse parameters: -
    // Execute:
    UInt32 PY = LASProcess.GetYPositionActual();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PY);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPositionY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetPositionY);
    // Analyse parameters ( SPY <y> ):
    UInt32 PY = atol(GetPRxdParameters(0));
    // Execute:
    LASProcess.SetYPositionActual(PY);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", PROMPT_RESPONSE, 
            GetPRxdCommand(), PY);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetRangeX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetRangeX);
    // Analyse parameters: -
    // Execute:
    UInt32 PL = LASProcess.GetXPositionMinimum();
    UInt32 PH = LASProcess.GetXPositionMaximum();
    UInt32 PD = LASProcess.GetXPositionDelta();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", PROMPT_RESPONSE, 
            GetPRxdCommand(), PL, PH, PD);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetRangeX(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetRangeX);
    // Analyse parameters ( SRX <xmin> <xmax> <dx> ):
    UInt32 PL = atol(GetPRxdParameters(0));
    UInt32 PH = atol(GetPRxdParameters(1));
    UInt32 PD = atol(GetPRxdParameters(2));
    // Execute:
    LASProcess.SetXPositionMinimum(PL);
    LASProcess.SetXPositionMaximum(PH);
    LASProcess.SetXPositionDelta(PD);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", PROMPT_RESPONSE, 
            GetPRxdCommand(), PL, PH, PD);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetRangeY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetRangeY);
    // Analyse parameters: -
    // Execute:
    UInt32 PL = LASProcess.GetYPositionMinimum();
    UInt32 PH = LASProcess.GetYPositionMaximum();
    UInt32 PD = LASProcess.GetYPositionDelta();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PL, PH, PD);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetRangeY(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetRangeY);
    // Analyse parameters ( SRY <ymin> <ymax> <dy> ):
    UInt32 PL = atol(GetPRxdParameters(0));
    UInt32 PH = atol(GetPRxdParameters(1));
    UInt32 PD = atol(GetPRxdParameters(2));
    // Execute:
    LASProcess.SetYPositionMinimum(PL);
    LASProcess.SetYPositionMaximum(PH);
    LASProcess.SetYPositionDelta(PD);   
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PL, PH, PD);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetDelayMotion(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetDelayMotion);
    // Analyse parameters: -
    // Execute:
    UInt32 D = LASProcess.GetDelayMotion();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), D);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetDelayMotion(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetDelayMotion);
    // Analyse parameters ( SDM <delay> ):
    UInt32 D = atol(GetPRxdParameters(0));    
    // Execute:
    LASProcess.SetDelayMotion(D);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), D);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteGetPulseWidthus(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spGetPulseWidth);
    // Analyse parameters: - ( GPW )
    // Execute:
    UInt32 PW = LASProcess.GetPulseWidthus();
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}

void CCommand::ExecuteSetPulseWidthus(CSerial &serial)
{ 
  if (spIdle == LASProcess.GetState())
  {
    LASProcess.SetState(spSetPulseWidth);
    // Analyse parameters ( SPW <width> ):
    UInt32 PW = atol(GetPRxdParameters(0));    
    // Execute:
    LASProcess.SetPulseWidthus(PW);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PW);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
  }
  else
  {
    // Error - Command Timing
  }
}
//
//#########################################################
//  Segment - Execution - LaserPosition
//#########################################################
//
// PulseLaserPosition PLP <x> <y> <p> <c> : Pulse Laser <x><y> <p>eriod <c>ount"
// Definition of XPositionActual, YPositionActual, ProcessPeriod, ProcessCount
void CCommand::ExecutePulseLaserPosition(CSerial &serial)
{ 
//  if (spIdle == LASProcess.GetState())
//  {
    LASProcess.SetState(spPulseLaserPosition);
    // Analyse parameters ( PLP <x> <y> <p> <c> ):
    UInt32 PositionX = atol(GetPRxdParameters(0));
    UInt32 PositionY = atol(GetPRxdParameters(1));
    UInt32 PulsePeriod = atol(GetPRxdParameters(2));
    UInt32 PulseCount = atol(GetPRxdParameters(3));
    // Execute:
    LASProcess.SetXPositionActual(PositionX);
    LASProcess.SetYPositionActual(PositionY);
    LASProcess.SetProcessPeriod(PulsePeriod);
    LASProcess.SetProcessCount(PulseCount);
    PulseTrigger.SetPulsePeriodus(1000 * PulsePeriod);
    PulseTrigger.SetPulseCount(PulseCount);        
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu %lu", PROMPT_RESPONSE, 
            GetPRxdCommand(), PositionX, PositionY, PulsePeriod, PulseCount);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
//  }
//  else
//  {
//    // Error - Command Timing
//  }
}

void CCommand::ExecuteAbortLaserPosition(CSerial &serial)
{ 
    LASProcess.SetState(spAbortLaserPosition);
//    LASProcess.SetProcessStep(0);
//    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -
    // Execute : -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LaserLine
//#########################################################
//
void CCommand::ExecutePulseLaserRow(CSerial &serial)
{
//  if (spIdle == LASProcess.GetState())
//  {
    LASProcess.SetState(spPulseLaserRow);
    // Analyse parameters ( PLR <p> <n> ):
    UInt32 PP = atol(GetPRxdParameters(0));
    UInt32 PC = atol(GetPRxdParameters(1));
    UInt32 PW = LASProcess.GetPulseWidthus();
    PulseTrigger.SetPulsePeriodus(1000 * PP);
    PulseTrigger.SetPulseCount(PC);
    PulseTrigger.SetPulseWidthus(PW);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PP, PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
//  }
//  else
//  {
//    // Error - Command Timing
//  }
}

void CCommand::ExecuteAbortLaserRow(CSerial &serial)
{
    LASProcess.SetState(spAbortLaserRow);
    PulseTrigger.Stop();
//    LASProcess.SetProcessStep(0);
//    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -    
    // Execute : -    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}

void CCommand::ExecutePulseLaserCol(CSerial &serial)
{
//  if (spIdle == LASProcess.GetState())
//  {
    LASProcess.SetState(spPulseLaserCol);
    // Analyse parameters ( PLC <p> <n> ):
    UInt32 PP = atol(GetPRxdParameters(0));
    UInt32 PC = atol(GetPRxdParameters(1));
    LASProcess.SetProcessPeriod(PP);
    LASProcess.SetProcessCount(PC);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PP, PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
//  }
//  else
//  {
//    // Error - Command Timing
//  }
}

void CCommand::ExecuteAbortLaserCol(CSerial &serial)
{
    LASProcess.SetState(spAbortLaserCol);
    LASProcess.SetProcessStep(0);
    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -    
    // Execute : -    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LaserMatrix
//#########################################################
//
void CCommand::ExecutePulseLaserMatrix(CSerial &serial)
{
//  if (spIdle == LASProcess.GetState())
//  {
    LASProcess.SetState(spPulseLaserMatrix);
    // Analyse parameters ( PLM <p> <n> ):
    UInt32 PP = atol(GetPRxdParameters(0));
    UInt32 PC = atol(GetPRxdParameters(1));
    LASProcess.SetProcessPeriod(PP);
    LASProcess.SetProcessCount(PC);
    // Response:
    sprintf(GetTxdBuffer(), "%s %s %lu %lu", 
            PROMPT_RESPONSE, GetPRxdCommand(), PP, PC);
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
//  }
//  else
//  {
//    // Error - Command Timing
//  }
}

void CCommand::ExecuteAbortLaserMatrix(CSerial &serial)
{
    LASProcess.SetState(spAbortLaserMatrix);
    LASProcess.SetProcessStep(0);
    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -    
    // Execute : -    
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution - LaserImage
//#########################################################
//
void CCommand::ExecutePulseLaserImage(CSerial &serial)
{
  LASProcess.SetState(spPulseLaserImage);
  // Analyse parameters ( PLI <x> <y> <p> <c> <d> ):
  UInt32 XP = atol(GetPRxdParameters(0));
  UInt32 YP = atol(GetPRxdParameters(1));
  UInt32 PP = atol(GetPRxdParameters(2));
  UInt32 PC = atol(GetPRxdParameters(3));
  UInt32 DM = atol(GetPRxdParameters(4));
  LASProcess.SetXPositionActual(XP);
  LASProcess.SetYPositionActual(YP);
  LASProcess.SetProcessPeriod(PP);
  LASProcess.SetProcessCount(PC);
  LASProcess.SetDelayMotion(DM);
  // Response:
  sprintf(GetTxdBuffer(), "%s %s %lu %lu %lu %lu %lu", 
          PROMPT_RESPONSE, GetPRxdCommand(), XP, YP, PP, PC, DM);
  serial.WriteLine(GetTxdBuffer());
  serial.WritePrompt();
}

void CCommand::ExecuteAbortLaserImage(CSerial &serial)
{
    LASProcess.SetState(spAbortLaserImage);
    LASProcess.SetProcessStep(0);
    LASProcess.SetProcessCount(0);
    LASProcess.SetState(spIdle);
    // Analyse parameters : -
    // Response:
    sprintf(GetTxdBuffer(), "%s %s", 
            PROMPT_RESPONSE, GetPRxdCommand());
    serial.WriteLine(GetTxdBuffer());
    serial.WritePrompt();
}
//
//#########################################################
//  Segment - Execution (All)
//#########################################################
//
void CCommand::CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate)
{
//  switch (stateprocess)
//  { // Base
//    case spUndefined:
//      break; 
//    case spIdle:
//      break; 
//    default:
//      break;
//  }  
//  serial.WriteNewLine();
//  serial.WritePrompt();
  sprintf(LASCommand.GetTxdBuffer(), MASK_STATEPROCESS, PROMPT_EVENT, stateprocess, substate);
  serial.WriteLine(LASCommand.GetTxdBuffer());
  serial.WritePrompt();
}

Boolean CCommand::Execute(CSerial &serial)
{ 
// debug sprintf(TxdBuffer, "ExecuteRxdCommand: %s", PRxdCommand);
// debug Serial.WriteLine(TxdBuffer);
  if (!strcmp(SHORT_H, GetPRxdCommand()))
  { 
    ExecuteGetHelp(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPH, GetPRxdCommand()))
  {
    ExecuteGetProgramHeader(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GSV, GetPRxdCommand()))
  {
    ExecuteGetSoftwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GHV, GetPRxdCommand()))
  {
    ExecuteGetHardwareVersion(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPC, GetPRxdCommand()))
  {
    ExecuteGetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPC, GetPRxdCommand()))
  {
    ExecuteSetProcessCount(serial);
    return true;
  } else 
  if (!strcmp(SHORT_GPP, GetPRxdCommand()))
  {
    ExecuteGetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_SPP, GetPRxdCommand()))
  {
    ExecuteSetProcessPeriod(serial);
    return true;
  } else 
  if (!strcmp(SHORT_A, GetPRxdCommand()))
  {
    ExecuteAbortAll(serial);
    return true;
  } else 
  // ----------------------------------
  // LedSystem
  // ---------------------------------- 
  if (!strcmp(SHORT_GLS, GetPRxdCommand()))
  {
    ExecuteGetLedSystem(serial);
    return true;
  } else 
  if (!strcmp(SHORT_LSH, GetPRxdCommand()))
  {
    ExecuteLedSystemOn(serial);
    return true;
  } else   
  if (!strcmp(SHORT_LSL, GetPRxdCommand()))
  {
    ExecuteLedSystemOff(serial);
    return true;
  } else   
  if (!strcmp(SHORT_BLS, GetPRxdCommand()))
  {
    ExecuteBlinkLedSystem(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserRange
  // ----------------------------------
  if (!strcmp(SHORT_GPX, GetPRxdCommand()))
  {
    ExecuteGetPositionX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPX, GetPRxdCommand()))
  {
    ExecuteSetPositionX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPY, GetPRxdCommand()))
  {
    ExecuteGetPositionY(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPY, GetPRxdCommand()))
  {
    ExecuteSetPositionY(serial);
    return true;
  } else    
  if (!strcmp(SHORT_GRX, GetPRxdCommand()))
  {
    ExecuteGetRangeX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SRX, GetPRxdCommand()))
  {
    ExecuteSetRangeX(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GRY, GetPRxdCommand()))
  {
    ExecuteGetRangeY(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SRY, GetPRxdCommand()))
  {
    ExecuteSetRangeY(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GDM, GetPRxdCommand()))
  {
    ExecuteGetDelayMotion(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SDM, GetPRxdCommand()))
  {
    ExecuteSetDelayMotion(serial);
    return true;
  } else   
  if (!strcmp(SHORT_GPW, GetPRxdCommand()))
  {
    ExecuteGetPulseWidthus(serial);
    return true;
  } else   
  if (!strcmp(SHORT_SPW, GetPRxdCommand()))
  {
    ExecuteSetPulseWidthus(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserPosition
  // ----------------------------------
  if (!strcmp(SHORT_PLP, GetPRxdCommand()))
  {
    ExecutePulseLaserPosition(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALP, GetPRxdCommand()))
  {
    ExecuteAbortLaserPosition(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserLine
  // ----------------------------------
  if (!strcmp(SHORT_PLR, GetPRxdCommand()))
  {
    ExecutePulseLaserRow(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALR, GetPRxdCommand()))
  {
    ExecuteAbortLaserRow(serial);
    return true;
  } else   
  if (!strcmp(SHORT_PLC, GetPRxdCommand()))
  {
    ExecutePulseLaserCol(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALC, GetPRxdCommand()))
  {
    ExecuteAbortLaserCol(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserMatrix
  // ----------------------------------
  if (!strcmp(SHORT_PLM, GetPRxdCommand()))
  {
    ExecutePulseLaserMatrix(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALM, GetPRxdCommand()))
  {
    ExecuteAbortLaserMatrix(serial);
    return true;
  } else   
  // ----------------------------------
  // Measurement - LaserImage
  // ----------------------------------
  if (!strcmp(SHORT_PLI, GetPRxdCommand()))
  {
    ExecutePulseLaserImage(serial);
    return true;
  } else   
  if (!strcmp(SHORT_ALI, GetPRxdCommand()))
  {
    ExecuteAbortLaserImage(serial);
    return true;
  } else   
  // ----------------------------------
  // Error-Handler
  // ----------------------------------
  {
    LASError.SetCode(ecInvalidCommand);
  }
  return false;  
}

Boolean CCommand::Handle(CSerial &serial)
{
  if (Analyse(serial))
  {
    return Execute(serial);
  }  
  return false;
}



