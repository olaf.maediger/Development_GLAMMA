#include "Defines.h"
#include "IrqTimer.h"
//
CIrqTimer::CIrqTimer(int channel)
{
  FChannel = channel;
#if defined(PROCESSOR_STM32F103C8)
  FPHardwareTimer = new HardwareTimer(channel);
  FPHardwareTimer->setChannelMode(channel, TIMER_OUTPUTCOMPARE);
#elif defined(PROCESSOR_ARDUINODUE)
  FPHardwareTimer = new CHardwareTimerDue(channel);
#endif
}

void CIrqTimer::SetInterruptHandler(void (*pirqhandler)(void))
{
  FPIrqHandler = pirqhandler;
#if defined(PROCESSOR_STM32F103C8)
  FPHardwareTimer->attachInterrupt(FChannel, FPIrqHandler);
#elif defined(PROCESSOR_ARDUINODUE)
  FPHardwareTimer->SetInterruptHandler(FPIrqHandler);
#endif
}

void CIrqTimer::SetPeriodus(long unsigned periodus)
{
#if defined(PROCESSOR_STM32F103C8)
  FPHardwareTimer->setPeriod(periodus);
  //???FPHardwareTimer->setPrescaleFactor(0xFFFF);
  FPHardwareTimer->setCompare1(0x0000);
  FPHardwareTimer->setCompare2(0x0000);
  FPHardwareTimer->setCompare3(0x0000);
  FPHardwareTimer->setCompare4(0x0000);
#elif defined(PROCESSOR_ARDUINODUE)
  FPHardwareTimer->SetPeriodus(periodus);
#endif
}

void CIrqTimer::Start()
{
#if defined(PROCESSOR_STM32F103C8)
  FPHardwareTimer->resume();
#elif defined(PROCESSOR_ARDUINODUE)
  FPHardwareTimer->Start();
#endif
}

void CIrqTimer::Stop()
{
#if defined(PROCESSOR_STM32F103C8)
  FPHardwareTimer->pause(); 
#elif defined(PROCESSOR_ARDUINODUE)
  FPHardwareTimer->Stop();
#endif
}

void CIrqTimer::SetPulseCount(long unsigned pulsecount)
{ 
  FPulseCount = pulsecount;
}
void CIrqTimer::DecrementStopPulseCount()
{
  if (0 < FPulseCount)
  {
    FPulseCount--;
  }
  if (0 == FPulseCount)
  {
    Stop();
  }
}
//


