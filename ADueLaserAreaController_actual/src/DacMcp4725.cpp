//
//--------------------------------
//  Library Dac
//--------------------------------
//
#include "DacMcp4725.h"
//
CDacMcp4725::CDacMcp4725(Byte i2caddress)
{
  FI2CAddress = i2caddress;
  FValue = 0x0000;
}

Boolean CDacMcp4725::Open()
{
  Wire.begin();
  SetValue(0x0000);
  return true;
}

Boolean CDacMcp4725::Close()
{
  // Wire.end();
  return true;
}

UInt32 CDacMcp4725::GetValue()
{  
  return FValue;
}

void CDacMcp4725::SetValue(UInt32 value)
{
  FValue = value;
//#ifdef TWBR
//  uint8_t twbrback = TWBR;
//  TWBR = ((F_CPU / 400000L) - 16) / 2; // Set I2C frequency to 400kHz
//#endif
  Wire.beginTransmission(FI2CAddress);
  Wire.write(MCP4725_WRITEDAC);
  Wire.write((Byte)(value >> 4));
  Wire.write((Byte)((0x000F & value) << 4));
  Wire.endTransmission();
//#ifdef TWBR
//  TWBR = twbrback;
//#endif
}


