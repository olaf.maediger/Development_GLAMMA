//
//--------------------------------
//  Library Led
//--------------------------------
//
#include "Led.h"
//
CLed::CLed(int pin)
{
  FPin = pin;
  FInverted = false;
  FState = slUndefined;
  FPulsePeriod = 1000;
  FPulseCountPreset = 0;
  FPulseCountActual = 0;
}

CLed::CLed(int pin, bool inverted)
{
  FPin = pin;
  FInverted = inverted;
  FState = slUndefined;
  FPulsePeriod = 1000;
  FPulseCountPreset = 0;
  FPulseCountActual = 0;
}

EStateLed CLed::GetState()
{
  return FState;
}

UInt32 CLed::GetPulseCountPreset()
{
  return FPulseCountPreset;  
}

UInt32 CLed::GetPulseCountActual()
{
  return FPulseCountActual;  
}

UInt32 CLed::GetPulsePeriod()
{
  return FPulsePeriod;
}

Boolean CLed::Open()
{
  pinMode(FPin, OUTPUT);
  Off();
  FState = slOff;
  return true;
}

Boolean CLed::Close()
{
  Off();
  pinMode(FPin, INPUT);
  FState = slUndefined;
  return true;
}

void CLed::On()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FState = slOn;
  FPulseCountActual++;
}

void CLed::Off()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FState = slOff;
}

void CLed::SetPulsePeriodCount(UInt32 pulseperiod, UInt32 pulsecountpreset)
{
  FPulsePeriod = pulseperiod;
  FPulseCountPreset = pulsecountpreset;
  FPulseCountActual = 0;
}


