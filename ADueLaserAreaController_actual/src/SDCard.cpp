//
//--------------------------------
//  Library SDCard
//--------------------------------
//
#include "Defines.h"
#include "SDCard.h"
//
CSDCard::CSDCard(int pincs)
{
  FPinCS = pincs;
  FPSDClass = NULL;
  FIsOpenForWriting = false;
  FIsOpenForReading = false;
}

//--------------------------------------------

Boolean CSDCard::IsMounted()
{
  return (NULL != FPSDClass);
}

Boolean CSDCard::Mount()
{
  Boolean Result = false;
  pinMode(FPinCS, OUTPUT);
  if (!IsMounted())
  {
    FPSDClass = new SDClass();
    if (FPSDClass->begin(FPinCS))
    { 
      FIsOpenForWriting = false;
      FIsOpenForReading = false;
      Result = true;
    }
  }  
  return Result;
}

Boolean CSDCard::Unmount()
{
  Boolean Result = false;
  pinMode(FPinCS, INPUT);
  if (IsMounted())
  {
    // NC !!! if (FPSDClass->end())
    {
      delete FPSDClass;
      Result = true;
    }
  }
  FPSDClass = NULL;
  FIsOpenForWriting = false;
  FIsOpenForReading = false;
  return Result;
}

//--------------------------------------------

Boolean CSDCard::CreateDirectory(String directoryname)
{
  Boolean Result = false;
  if (IsMounted())
  {
    Result = FPSDClass->mkdir(directoryname);
  }
  return Result;
}

Boolean CSDCard::RemoveDirectory(String directoryname)
{
  Boolean Result = false;
  if (IsMounted())
  {
    Result = FPSDClass->rmdir(directoryname);
  }    
  return Result;
}

Boolean CSDCard::GetFileExists(String filename)
{
  Boolean Result = false;
  if (IsMounted())
  {
    Result = FPSDClass->exists(filename);
  }
  return Result;
}

unsigned long CSDCard::GetFileSize(String filename)
{
  long unsigned Result = 0;
  if (IsMounted())
  {
    if (FPSDClass->exists(filename))
    {
      File F = FPSDClass->open(filename, FILE_READ);
      Result = F.size();
      F.close();
    }
  }
  return Result;
}

Boolean CSDCard::RemoveFile(String filename)
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (FPSDClass->exists(filename))
    {
      Result = FPSDClass->remove(filename);
    }
  }
  return Result;
}

//--------------------------------------------
Boolean CSDCard::IsOpenForWriting()
{
  return FIsOpenForWriting;
}

Boolean CSDCard::OpenWriteFile(String filename)
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (!IsOpenForWriting())
    {
      FFileWrite = FPSDClass->open(filename, FILE_WRITE);
      FIsOpenForWriting = true;
      Result = true; // not ok, but if (NULL != FFileWrite) gives Error!
    }
  }
  return Result;
}

Boolean CSDCard::WriteSingleCharacter(char character)
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (IsOpenForWriting())
    {
      FFileWrite.print(character);
      Result = true;
    }
  }
  return Result;
}

Boolean CSDCard::WriteSingleByte(unsigned char value)
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (IsOpenForWriting())
    {
      FFileWrite.write(value);
      Result = true;
    }
  }
  return Result;
}

Boolean CSDCard::WriteTextBlock(UInt32 size, String text)
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (IsOpenForWriting())
    {
      if (0 < size)
      {
        text.substring(0, size - 1);
        FFileWrite.print(text);
        Result = true;
      }
    }
  }
  return Result;
}

Boolean CSDCard::WriteTextBlock(String text)
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (IsOpenForWriting())
    {
      if (0 < text.length())
      {
        FFileWrite.print(text);
        Result = true;
      }
    }
  }
  return Result;
}

Boolean CSDCard::WriteTextLine(String text)
{
  Boolean Result = false;
  if (IsMounted())
  {
    FFileWrite.println(text);
    Result = true;
  }
  return Result;  
}

Boolean CSDCard::WriteNumberText(long unsigned number)
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (IsOpenForWriting())
    {
      char Buffer[16] = {0};
      sprintf(Buffer, "%lu", number);
      FFileWrite.print(Buffer);
      Result = true;
    }
  }
  return Result;    
}

Boolean CSDCard::CloseWriteFile()
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (IsOpenForWriting())
    {
      FFileWrite.flush();
      FFileWrite.close();
      FIsOpenForWriting = false;
      Result = true;
    }
  }
  return Result;
}

//--------------------------------------------
Boolean CSDCard::IsOpenForReading()
{
  return FIsOpenForReading;
}

Boolean CSDCard::OpenReadFile(String filename)
{
  Boolean Result = false;
  if (IsMounted())
  {
    FFileRead = FPSDClass->open(filename, FILE_READ);
    FIsOpenForReading = true;
    Result = true;
  }
  return Result;
}

Boolean CSDCard::ReadAvailable()
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (IsOpenForReading())
    {   
      Result = (0 < FFileRead.available());
    }
  }
  return Result;  
}

char CSDCard::ReadSingleCharacter()
{
  Character Result = (char)0xFF;  
  if (IsMounted())
  {
    if (IsOpenForReading())
    {  
      Result = FFileRead.read();
    }
  }
  return Result;
}

String CSDCard::ReadTextBlock(unsigned charactercount)
{
  String Result = "";
  if (IsMounted())
  {
    if (IsOpenForReading())
    {
      unsigned CC = charactercount;
      while ((0 < CC) && (FFileRead.available()))
      {
        CC--;      
        Result += (char)(FFileRead.read());
      }
    }
  }
  return Result;  
}

String CSDCard::ReadTextLine()
{
  String Result = "";
  if (IsMounted())
  {
    if (IsOpenForReading())
    {
      bool ReadMore = false;
      do
      {
        ReadMore = false;
        if (FFileRead.available())
        {
          char C = (char)(FFileRead.read());
          switch (C)
          {
            case 0x0D: // CR (LF)
              if (FFileRead.available())
              {
                if (0x0A == (char)(FFileRead.peek()))
                {
                  FFileRead.read();
                }
              }
              ReadMore = false;
              break;
            case 0x0A: // LF (CR)
              if (FFileRead.available())
              {
                if (0x0D == (char)(FFileRead.peek()))
                {
                  FFileRead.read();
                }
              }
              ReadMore = false;
              break;
            default:
              Result += C;
              ReadMore = true;
              break;
          }
        }
      } 
      while (ReadMore); 
    }
  }
  return Result;  
}

Boolean CSDCard::CloseReadFile()
{
  Boolean Result = false;
  if (IsMounted())
  {
    if (IsOpenForReading())
    {
      FFileRead.close();
      Result = true; 
    }
  }
  return Result;  
}
//