//
//--------------------------------
//  Library Trigger
//--------------------------------
//
#include "Trigger.h"
//
CTrigger::CTrigger(int pin, ETriggerDirection triggerdirection)
{
  FPin = pin;
  FDirection = triggerdirection;
  FInverted = false;
  FLevel = tlUndefined;
}

CTrigger::CTrigger(int pin, ETriggerDirection triggerdirection, bool inverted)
{
  FPin = pin;
  FDirection = triggerdirection;
  FInverted = inverted;
  FLevel = tlUndefined;
}

ETriggerLevel CTrigger::GetLevel()
{
  if (tdInput == FDirection)
  {
    if (FInverted)
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlPassive;
      }
      else
      {
        FLevel = tlActive;
      }
    }
    else
    {
      if (0 < digitalRead(FPin))
      {
        FLevel = tlActive;
      }
      else
      {
        FLevel = tlPassive;
      }      
    }
  }
  return FLevel;
}

Boolean CTrigger::Open()
{
  switch (FDirection)
  {
    case tdOutput:
      pinMode(FPin, OUTPUT);
      break;
    default: // tdInput
      pinMode(FPin, INPUT);
      break;
  }
  SetPassive();
  FLevel = tlPassive;
  return true;
}

Boolean CTrigger::Close()
{
  SetPassive();
  pinMode(FPin, INPUT);
  FLevel = tlUndefined;
  return true;
}

void CTrigger::SetActive()
{
  if (FInverted)
  {
    digitalWrite(FPin, LOW);
  }
  else
  {
    digitalWrite(FPin, HIGH);
  }
  FLevel = tlActive;
}

void CTrigger::SetPassive()
{
  if (FInverted)
  {
    digitalWrite(FPin, HIGH);
  }
  else
  {
    digitalWrite(FPin, LOW);
  }
  FLevel = tlPassive;
}


