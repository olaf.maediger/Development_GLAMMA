#include "Defines.h"
//
#include "Error.h"
#include "Process.h"
#include "Command.h"
#include "Serial.h"
#include "Led.h"
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  #include "DacInternal.h"
#endif
#if (defined(DAC_MCP4725_X) || defined(DAC_MCP4725_Y)) 
  #include "DacMcp4725.h"
#endif
#include "SDCard.h"
#include "Trigger.h"
//
//###################################################
// Segment - Global Variables - Assignment
//###################################################
//
//---------------------------------------------------
// Segment - Global Variables 
//---------------------------------------------------
//
// PLI 2000 2000 1000000 3
//
CError LASError;
CProcess LASProcess;
CCommand LASCommand;
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Led
//---------------------------------------------------
//
CLed LedSystem(PIN_LEDSYSTEM, LEDSYSTEM_INVERTED);
CLed LedLaser(PIN_LEDLASER, LEDLASER_INVERTED);                 
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Dac
//---------------------------------------------------
//
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  CDacInternal DacInternal_XY(true, true);
#endif
#ifdef DAC_MCP4725_X
//CDacMcp4725 DacMcp4725_X(CDacMcp4725::MCP4725_I2CADDRESS_DEFAULT);
CDacMcp4725 DacMcp4725_X(CDacMcp4725::MCP4725A0_I2CADDRESS_DEFAULT);
#endif
#ifdef DAC_MCP4725_Y
//CDacMcp4725 DacMcp4725_Y(CDacMcp4725::MCP4725_I2CADDRESS_DEFAULT);
CDacMcp4725 DacMcp4725_Y(CDacMcp4725::MCP4725A0_I2CADDRESS_DEFAULT);
#endif
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - SDCard
//---------------------------------------------------
//
CSDCard SDCard(PIN_SPI_CS_SDCARD);
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Serial
//---------------------------------------------------
//
// Serial - ProgrammingPort und CommandPort!
// NC - CSerial SerialDebug(Serial);
// normal CSerial SerialCommand(Serial1);
CSerial SerialCommand(Serial); // ArduinoDue ProgrammingPort
// CSerial SerialCommand(SerialUSB); // ArduinoDue NativePort
// NC - CSerial SerialPrinter(Serial2);
// NC - Serial3
//
//---------------------------------------------------
// Segment - Global Variables - Assignment - Trigger
//---------------------------------------------------
//
CTrigger TriggerIn(PIN_TRIGGER_IN, tdInput, NOT_INVERTED);
CTrigger TriggerOut(PIN_TRIGGER_OUT, tdOutput, NOT_INVERTED);
//
//
//---------------------------------------------------
// Segment - Forward-Declaration
//---------------------------------------------------
//
//---------------------------------------------------
// Segment - Setup
//---------------------------------------------------
//
UInt16 DACValue = 0x0000;

void setup() 
{ //-------------------------------------------
  // Device - Led - LedLaser
  //-------------------------------------------
  LedLaser.Open();
  LedLaser.Off();
  //-------------------------------------------
  // Device - Led - LedSystem
  //-------------------------------------------
  LedSystem.Open();
  // LedLine.Open();
  for (int BI = 0; BI < 3; BI++)
  {
    LedSystem.Off();
    // LedLine.On();
    delay(40);
    LedSystem.On();
    // LedLine.Off();
    delay(40);
  }
  LedSystem.Off();
  //-------------------------------------------
  // Device - Dac
  //-------------------------------------------
#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y)) 
  DacInternal_XY.Open();
#endif  
#ifdef DAC_MCP4725_X
  DacMcp4725_X.Open();
#endif  
#ifdef DAC_MCP4725_Y
  DacMcp4725_Y.Open();
#endif  
  //-------------------------------------------
  // Device - Serial
  //-------------------------------------------
  SerialCommand.Open(115200);
  SerialCommand.SetRxdEcho(RXDECHO_OFF);
  //-------------------------------------------
  // Device - Trigger
  //-------------------------------------------
  TriggerIn.Open();
  TriggerOut.Open();
  //-------------------------------------------
  // Device - Command
  //-------------------------------------------
  LASCommand.WriteProgramHeader(SerialCommand);
  LASCommand.WriteHelp(SerialCommand);
  SerialCommand.WritePrompt(); 
  //-------------------------------------------
  // Device - Process 
  //-------------------------------------------
  LASProcess.Open();
  LASProcess.SetState(spWelcome);
}
//
//---------------------------------------------------
// Segment - Loop
//---------------------------------------------------
//
void loop() 
{   
  LASError.Handle(SerialCommand);    
  LASCommand.Handle(SerialCommand, SDCard);
  LASProcess.Handle(SerialCommand);
}
//
// debug:  
//  //delay(10);
//#if (defined(DAC_INTERNAL_X) || defined(DAC_INTERNAL_Y))
//  DacInternal_XY.SetValue(0, DACValue);
//  DacInternal_XY.SetValue(1, DACValue);
//#endif
//#ifdef DAC_MCP4725_X
//  DacMcp4725_X.SetValue(DACValue);
//#endif
//#ifdef DAC_MCP4725_Y
//  DacMcp4725_Y.SetValue(DACValue);
//#endif
//  DACValue += 10;
//  if (4095 < DACValue) 
//  {
//    DACValue = 0;
//  }




