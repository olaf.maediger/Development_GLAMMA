//
#ifndef Command_h
#define Command_h
//
#include "Defines.h"
#include "Utilities.h"
#include "Process.h"
#include "Serial.h"
#include "SDCard.h"
//
#define ARGUMENT_PROJECT    "LAS - LaserAreaScanner - Controller"
#define ARGUMENT_SOFTWARE   "09V04JR"
#define ARGUMENT_DATE       "190319"
#define ARGUMENT_TIME       "1047"
#define ARGUMENT_AUTHOR     "OM"
#define ARGUMENT_PORT       "SerialCommand (Serial-USB)"
#define ARGUMENT_PARAMETER  "115200, N, 8, 1"
//
#ifdef PROCESSOR_ARDUINOUNO
#define ARGUMENT_HARDWARE "ArduinoUnoR3"
#endif
#ifdef PROCESSOR_ARDUINOMEGA 
#define ARGUMENT_HARDWARE "ArduinoMega328"
#endif
#ifdef PROCESSOR_ARDUINODUE
#define ARGUMENT_HARDWARE "ArduinoDue3"
#endif
#ifdef PROCESSOR_STM32F103C8 
#define ARGUMENT_HARDWARE "Stm32F103C8"
#endif
#ifdef PROCESSOR_TEENSY32 
#define ARGUMENT_HARDWARE "Teensy32"
#endif
#ifdef PROCESSOR_TEENSY36 
#define ARGUMENT_HARDWARE "Teensy36"
#endif
// 
// HELP_COMMON
#define SHORT_H     "H"
#define SHORT_GPH   "GPH"
#define SHORT_GSV   "GSV"
#define SHORT_GHV   "GHV"
#define SHORT_GPC   "GPC"
#define SHORT_SPC   "SPC"
#define SHORT_GPP   "GPP"
#define SHORT_SPP   "SPP"
#define SHORT_SPE   "SPE"
#define SHORT_A     "A"
//
// HELP_LEDSYSTEM
#define SHORT_GLS   "GLS"
#define SHORT_LSH   "LSH"
#define SHORT_LSL   "LSL"
#define SHORT_BLS   "BLS"
//
// HELP_LASERRANGE
#define SHORT_GPX   "GPX"
#define SHORT_SPX   "SPX"
#define SHORT_GRX   "GRX"
#define SHORT_SRX   "SRX"
#define SHORT_GPY   "GPY"
#define SHORT_SPY   "SPY"
#define SHORT_GRY   "GRY"
#define SHORT_SRY   "SRY"
#define SHORT_GDM   "GDM"
#define SHORT_SDM   "SDM"
#define SHORT_GPW   "GPW"
#define SHORT_SPW   "SPW"
//
// HELP_LASERPOSITION
#define SHORT_PLP   "PLP"
#define SHORT_ALP   "ALP"
//
// HELP_LASERLINE
#define SHORT_PLR   "PLR"
#define SHORT_ALR   "ALR"
#define SHORT_PLC   "PLC"
#define SHORT_ALC   "ALC"
//
// HELP_LASERMATRIX
#define SHORT_PLM   "PLM"
#define SHORT_ALM   "ALM"
//
// HELP_LASERIMAGE
#define SHORT_PLI   "PLI"
#define SHORT_ALI   "ALI"
//
// HELP_TRIGGER
#define SHORT_STA   "STA"
#define SHORT_STP   "STP"
#define SHORT_GTL   "GTL"
#define SHORT_WTA   "WTA"
#define SHORT_WTP   "WTP"
#define SHORT_BFT   "BFT"
//
// HELP_SDCOMMAND
#define SHORT_OCF   "OCF"
#define SHORT_WCF   "WCF"
#define SHORT_CCF   "CCF"
#define SHORT_ECF   "ECF"
#define SHORT_ACF   "ACF"
//
//--------------------------------
//  Section - Constant - HELP
//--------------------------------
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HEADER = 12;
//
#define TITLE_LINE            "--------------------------------------------------"
#define MASK_PROJECT          "- Project:   %-35s -"
#define MASK_SOFTWARE         "- Version:   %-35s -"
#define MASK_HARDWARE         "- Hardware:  %-35s -"
#define MASK_DATE             "- Date:      %-35s -"
#define MASK_TIME             "- Time:      %-35s -"
#define MASK_AUTHOR           "- Author:    %-35s -"
#define MASK_PORT             "- Port:      %-35s -"
#define MASK_PARAMETER        "- Parameter: %-35s -"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HELP = 56;
// 0
#define HELP_COMMON               " Help (Common):"
#define MASK_H                    " %-3s : This Help"
#define MASK_GPH                  " %-3s : Get Program Header"
#define MASK_GSV                  " %-3s : Get Software-Version"
#define MASK_GHV                  " %-3s : Get Hardware-Version"
#define MASK_GPC                  " %-3s : Get Process Count"
#define MASK_SPC                  " %-3s <pc> : Set Process Count"
#define MASK_GPP                  " %-3s : Get Process Period{ms}"
#define MASK_SPP                  " %-3s <pp> : Set Process Period{ms}"
#define MASK_A                    " %-1s : Abort Process Execution"
// 11
#define HELP_LEDSYSTEM            " Help (LedSystem):"
#define MASK_GLS                  " %-3s : Get State LedSystem"
#define MASK_LSH                  " %-3s : Switch LedSystem On"
#define MASK_LSL                  " %-3s : Switch LedSystem Off"
#define MASK_BLS                  " %-3s <p> <n> : Blink LedSystem with <p>eriod{ms} <n>times"
// 16
#define HELP_LASERRANGE           " Help (LaserRange):"
#define MASK_GPX                  " %-3s : Get Position X"
#define MASK_SPX                  " %-3s <x> : Set Position <x>"
#define MASK_GPY                  " %-3s : Get Position Y"
#define MASK_SPY                  " %-3s <y> : Set Position <y>"
#define MASK_GRX                  " %-3s : Get Range X"
#define MASK_SRX                  " %-3s <xmin> <xmax> <dx> : Set Range X <xmin>..<xmax> <dx>{0..4095}"
#define MASK_GRY                  " %-3s : Get Range Y"
#define MASK_SRY                  " %-3s <ymin> <ymax> <dy> : Set Range Y <ymin>..<ymax> <dy>{0..4095}"
#define MASK_GDM                  " %-3s : Get Delay Motion{us}"
#define MASK_SDM                  " %-3s <delay> : Set Delay Motion{us}"
#define MASK_GPW                  " %-3s : Get PulseWidth {us}"
#define MASK_SPW                  " %-3s <width> : Set PulseWidth {us}"
// 29
#define HELP_LASERPOSITION        " Help (LaserPosition):"
#define MASK_PLP                  " %-3s <x> <y> <p> <c> : Pulse Laser Position<x><y> <p>eriod{ms} <c>ount"
#define MASK_ALP                  " %-3s : Abort Laser Position"
// 32
#define HELP_LASERLINE            " Help (LaserLine):"
#define MASK_PLR                  " %-3s <p> <n> : Pulse Laser Row <p>eriod{ms} <n>Pulses"
#define MASK_ALR                  " %-3s : Abort Laser Row"
#define MASK_PLC                  " %-3s <p> <n> : Pulse Laser Column <p>eriod{ms} <n>Pulses"
#define MASK_ALC                  " %-3s : Abort Laser Column"
// 37
#define HELP_LASERMATRIX          " Help (LaserMatrix):"
#define MASK_PLM                  " %-3s <p> <n> : Pulse Laser Matrix <p>eriod{ms} <n>Pulses"
#define MASK_ALM                  " %-3s : Abort Laser Matrix"
// 40
#define HELP_LASERIMAGE           " Help (LaserImage):"
#define MASK_PLI                  " %-3s <x> <y> <p> <c> <d> : Laser <x><y> <p>eriod{ms} <c>ount <d>elay{ms}"
#define MASK_ALI                  " %-3s : Abort Laser Image"
// 43
#define HELP_TRIGGER              " Help (Trigger):"
#define MASK_STA                  " %-3s : Set Trigger Output active"
#define MASK_STP                  " %-3s : Set Trigger Output passive"
#define MASK_GTL                  " %-3s : Get Trigger Level of Trigger Input"
#define MASK_WTA                  " %-3s : Wait Trigger Input active"
#define MASK_WTP                  " %-3s : Wait Trigger Input passive"
#define MASK_BFT                  " %-3s <t> : Break for <t>ime{ms}"
// 50
#define HELP_SDCOMMAND            " Help (SDCommand):"
#define MASK_OCF                  " %-3s <f> : Open Command<f>ile for writing"
#define MASK_WCF                  " %-3s <c> <p> .. : Write <c>ommand with <p>arameter(s) to File"
#define MASK_CCF                  " %-3s : Close Command<f>ile for writing"
#define MASK_ECF                  " %-3s <f> : Execute Command<f>ile"
#define MASK_ACF                  " %-3s : Abort Execution Commandfile"
// -> 56
//-----------------------------------------------------------------------------------------
//
const int COUNT_SOFTWAREVERSION = 1;
//
#define MASK_SOFTWAREVERSION      " Software-Version LAS%s"
//
//-----------------------------------------------------------------------------------------
//
const int COUNT_HARDWAREVERSION = 1;
//
#define MASK_HARDWAREVERSION      " Hardware-Version %s"
//
//-----------------------------------------------------------------------------------------
//
// Command-Parameter
#define COUNT_TEXTPARAMETERS 5
#define SIZE_TEXTPARAMETER   8
//
//-----------------------------------------------------------------------------------------
//
class CCommand
{
  private:
  Character FTxdBuffer[SIZE_TXDBUFFER];
  Character FRxdBuffer[SIZE_RXDBUFFER];
  Int16 FRxdBufferIndex = 0;
  Character FCommandText[SIZE_RXDBUFFER];
  PCharacter FPCommand;
  PCharacter FPParameters[COUNT_TEXTPARAMETERS];
  Int16 FParameterCount = 0;

  public:
  CCommand();
  ~CCommand();
  //
  inline PCharacter GetTxdBuffer()
  {
    return FTxdBuffer;
  }
  //
  inline PCharacter GetPCommand()
  {
    return FPCommand;
  }
  inline Byte GetParameterCount()
  {
    return FParameterCount;
  }
  inline PCharacter GetPParameters(UInt8 index)
  {
    return FPParameters[index];
  }
  //
  void ZeroRxdBuffer();
  void ZeroTxdBuffer();
  void ZeroCommandText();
  //
  //  Segment - Execution - Basic Output
  void WriteProgramHeader(CSerial &serial);
  void WriteSoftwareVersion(CSerial &serial);
  void WriteHardwareVersion(CSerial &serial);
  void WriteHelp(CSerial &serial);
  //
  //  Segment - Execution - Common
  void ExecuteGetHelp(CSerial &serial);
  void ExecuteGetProgramHeader(CSerial &serial);
  void ExecuteGetSoftwareVersion(CSerial &serial);
  void ExecuteGetHardwareVersion(CSerial &serial);
  void ExecuteGetProcessCount(CSerial &serial);
  void ExecuteSetProcessCount(CSerial &serial);
  void ExecuteGetProcessPeriod(CSerial &serial);
  void ExecuteSetProcessPeriod(CSerial &serial);
  void ExecuteAbortAll(CSerial &serial);
  //
  //  Segment - Execution - LedSystem
  void ExecuteGetLedSystem(CSerial &serial);
  void ExecuteLedSystemOn(CSerial &serial);
  void ExecuteLedSystemOff(CSerial &serial);
  void ExecuteBlinkLedSystem(CSerial &serial);
  //
  //  Segment - Execution - LaserRange
  void ExecuteGetPositionX(CSerial &serial);    // GPX
  void ExecuteSetPositionX(CSerial &serial);    // SPX
  void ExecuteGetPositionY(CSerial &serial);    // GPY
  void ExecuteSetPositionY(CSerial &serial);    // SPY
  void ExecuteGetRangeX(CSerial &serial);       // GRX
  void ExecuteSetRangeX(CSerial &serial);       // SRX
  void ExecuteGetRangeY(CSerial &serial);       // GRY
  void ExecuteSetRangeY(CSerial &serial);       // SRY
  void ExecuteGetDelayMotionus(CSerial &serial);  // GDM
  void ExecuteSetDelayMotionus(CSerial &serial);  // SDM
  void ExecuteGetPulseWidthus(CSerial &serial); // GPW
  void ExecuteSetPulseWidthus(CSerial &serial); // SPW
  //
  //  Segment - Execution - LaserPosition
  void ExecutePulseLaserPosition(CSerial &serial);
  void ExecuteAbortLaserPosition(CSerial &serial);
  //
  //  Segment - Execution - LaserLine
  void ExecutePulseLaserRow(CSerial &serial);
  void ExecuteAbortLaserRow(CSerial &serial);
  void ExecutePulseLaserCol(CSerial &serial);
  void ExecuteAbortLaserCol(CSerial &serial);
  //
  //  Segment - Execution - LaserMatrix
  void ExecutePulseLaserMatrix(CSerial &serial);
  void ExecuteAbortLaserMatrix(CSerial &serial);
  //
  //  Segment - Execution - LaserImage
  void ExecutePulseLaserImage(CSerial &serial);
  void ExecuteAbortLaserImage(CSerial &serial);
  //
  //  Segment - Execution - Trigger
  void ExecuteSetTriggerActive(CSerial &serial);
  void ExecuteSetTriggerPassive(CSerial &serial);
  void ExecuteGetTriggerLevel(CSerial &serial);
  void ExecuteWaitTriggerActive(CSerial &serial);
  void ExecuteWaitTriggerPassive(CSerial &serial);
  void ExecuteBreakForTimems(CSerial &serial);
  //
  //  Segment - Execution - SDCommand
  void ExecuteOpenCommandFile(CSerial &serial);
  void ExecuteWriteCommandFile(CSerial &serial);
  void ExecuteCloseCommandFile(CSerial &serial);
  void ExecuteExecuteCommandFile(CSerial &serial);
  void ExecuteAbortCommandFile(CSerial &serial);
  //  
  // Main
  void Init();
  Boolean DetectRxdLine(CSerial &serial);
  Boolean DetectSDCardLine(CSDCard &sdcard);
  Boolean AnalyseCommandText(CSerial &serial);
  void CallbackStateProcess(CSerial &serial, EStateProcess stateprocess, UInt8 substate);
  Boolean Handle(CSerial &serial, CSDCard &sdcard);
  Boolean Execute(CSerial &serial);
};
//
#endif // Command_h
