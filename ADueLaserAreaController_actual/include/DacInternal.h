//
//--------------------------------
//  Library DacInternal
//--------------------------------
//
#ifndef DacInternal_h
#define DacInternal_h
//
#include "Defines.h"
//
//--------------------------------
//  Section - Dac
//--------------------------------
//
const Byte DAC_CHANNEL0 = 0x00;
const Byte DAC_CHANNEL1 = 0x01;
//
class CDacInternal
{
  private:
  bool FChannel[2];
  UInt32 FValue[2];
  
  public:
  CDacInternal(bool channel0, bool channel1);
  Boolean Open();
  Boolean Close();
  UInt32 GetValue(UInt8 channel);
  void SetValue(UInt8 channel, UInt32 value);
  void SetValues(UInt32 value0, UInt32 value1);
};
//
#endif // DacInternal_h
