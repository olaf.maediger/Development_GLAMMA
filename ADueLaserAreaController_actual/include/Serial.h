//
#ifndef Serial_h
#define Serial_h
//
#include "Defines.h"
#include <stdio.h>
#include <stdarg.h>
#include "HardwareSerial.h"
//
//----------------------------------------------------
//  Segment - Constant
//----------------------------------------------------
//
#define RXDECHO_ON  true
#define RXDECHO_OFF false
//
#define CR   0x0D
#define LF   0x0A
#define ZERO 0x00
//
#define PROMPT_NEWLINE  "\r\n"
#define PROMPT_INPUT    ">"
#define PROMPT_ANSWER   "#"
#define PROMPT_RESPONSE ":"
#define PROMPT_EVENT    "!"
//
#define SIZE_TXDBUFFER      64
#define SIZE_RXDBUFFER      64
//
#define SIZE_FORMATBUFFER   96
//
//----------------------------------------------------
//  Segment - CSerialBase
//----------------------------------------------------
//
class CSerialBase
{
  protected:
  bool FIsOpen = false;
  bool FRxdEcho = false;
  
  public:
  virtual bool Open(UInt32 baudrate) = 0;
  virtual bool Close() = 0;
  void SetRxdEcho(bool rxdecho);
  //
  String BuildTime(long unsigned milliseconds);
  //
  void WriteNewLine();
  void WritePrompt();
  void WriteAnswer();
  //
  virtual int GetRxdByteCount() = 0;
  virtual char ReadCharacter() = 0;
  virtual void Write(const char* text) = 0;
  virtual void Write(String text) = 0;
};
//
//----------------------------------------------------
//  Segment - CSerialH (^=HardwareSerial)
//----------------------------------------------------
//
#if ((defined PROCESSOR_ARDUINOUNO)||(defined PROCESSOR_ARDUINOMEGA)||(defined PROCESSOR_ARDUINODUE)||(defined PROCESSOR_STM32F103C8)||(defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36))
class CSerialH : public CSerialBase
{
  protected:
  HardwareSerial* FPSerial;
  
  public:
  CSerialH(HardwareSerial *serial)
  {
    FPSerial = serial;
  } 

  bool Open(UInt32 baudrate);
  bool Close();
  //
  int GetRxdByteCount();
  char ReadCharacter();
  void Write(const char* text);
  void Write(String text);
};
#endif

#if ((defined PROCESSOR_TEENSY32)||(defined PROCESSOR_TEENSY36))
class CSerialU : public CSerialBase
{
  protected:
  usb_serial_class* FPSerial;
  
  public:
  CSerialU(usb_serial_class *serial)
  {
    FPSerial = serial;
  } 

  bool Open(UInt32 baudrate);
  bool Close();
  //
  int GetRxdByteCount();
  char ReadCharacter();
  void Write(const char* text);
  void Write(String text);
};
#endif
//
//----------------------------------------------------
//  Segment - CSerial
//----------------------------------------------------
//
class CSerial
{
  protected:
  CSerialBase* FPSerial;
  
  public:
#if (defined PROCESSOR_ARDUINOUNO)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_ARDUINOMEGA)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_ARDUINODUE)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_STM32F103C8)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
#elif (defined PROCESSOR_TEENSY32)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
  CSerial(usb_serial_class *serial);
  CSerial(usb_serial_class &serial);
#elif (defined PROCESSOR_TEENSY36)
  CSerial(HardwareSerial *serial);
  CSerial(HardwareSerial &serial);
  CSerial(usb_serial_class *serial);
  CSerial(usb_serial_class &serial);
#else
#error Undefined Processor-Type!
#endif
  bool Open(UInt32 baudrate);
  bool Close();
  void SetRxdEcho(bool rxdecho);
  //  
  int GetRxdByteCount();
  char ReadCharacter();
  //
  void WriteNewLine();
  void WriteAnswer();
  void WritePrompt();
  //
  void WriteTime();
  //
  void Write(char character);
  void Write(const char* text);
  void Write(String text);
  void Write(UInt8 number);
  void Write(UInt16 number);
  void Write(UInt32 number);
  void Write(Int8 number);
  void Write(Int16 number);
  void Write(Int32 number);
  void Write(Float number);
  void Write(Double number);
  //
  void Write(const char* mask, char character);
  void Write(const char* mask, const char* text);
  void Write(const char* mask, String text);
  void Write(const char* mask, UInt8 number);
  void Write(const char* mask, UInt16 number);
  void Write(const char* mask, UInt32 number);
  void Write(const char* mask, Int8 number);
  void Write(const char* mask, Int16 number);
  void Write(const char* mask, Int32 number);
  void Write(const char* mask, Float number);
  void Write(const char* mask, Double number);
  //
  void WriteLine(const char *text);
  void WriteLine(String text);
  void WriteLine(UInt8 number);
  void WriteLine(UInt16 number);
  void WriteLine(UInt32 number);
  void WriteLine(Int8 number);
  void WriteLine(Int16 number);
  void WriteLine(Int32 number);
  void WriteLine(Float number);
  void WriteLine(Double number);
  //
  void WriteLine(const char* mask, const char *text);
  void WriteLine(const char* mask, String text);
  void WriteLine(const char* mask, UInt8 number);
  void WriteLine(const char* mask, UInt16 number);
  void WriteLine(const char* mask, UInt32 number);
  void WriteLine(const char* mask, Int8 number);
  void WriteLine(const char* mask, Int16 number);
  void WriteLine(const char* mask, Int32 number);
  void WriteLine(const char* mask, Float number);
  void WriteLine(const char* mask, Double number);
};
//
#endif
