# Development_GLAMMA

### 2109021137 : Integration LaserAreaScanner
  * **O** Erweiterung von TabSheet **"ImageProcessing"** durch TabSheet **"LaserAreaScanner"**, \
    Verbleibende Tabsheets **"LowLevelGlamma"** und **"HighLevelGlamma"**, \
    Weiterhin bleibt die Software des MicroControllers vollständig erhalten.
  * **X** Basisversion MC-Software: **2108310940_ADueLaserAreaController_LASControllerVSCodeOk** \
    C++-Code für ArduinoDue, angepasst in VSCode
  * **X** Basisversion PC-Software: **1808301610_LASManager_06V25_PS** \
    Auslieferung letzte Version für PSimon
  * **X** Basisversion PC-Software: **2106161513_Glamma_00V17_DeliveryPSFK** \
    Auslieferung erste Version GLAMMA-PC-Software für PSimon
  
### 2109021104 : Zusammenlegung Quellen in git-Archiven
  * **X** Spiegelung des git-Archivs unter **v:/git/Development_GLAMMA**
  * **X** vollständige Einrichtung des git-Archives **"Development_GLAMMA"**

### ToDo
  * **O** 
  * **O** Einrichtung und Aktualisierung der WebSite / Infos aus DokuWiki extrahieren

